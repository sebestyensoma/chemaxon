@isTest
public class MNBWebServiceTest  {
    private static List<String> requiredCurrencies = new List<String> { 'EUR', 'GBP', 'CHF', 'JPY', 'SGD', 'USD' };

    @isTest
    static void testMNBWebService(){
        Test.setMock(HttpCalloutMock.class, new MNBWebServiceMock());
        Map<String, Decimal> currencyMap = new Map<String, Decimal>();
        currencyMap = MNBWebService.getCurrencies(Date.newInstance(2018,08,17), String.join(requiredCurrencies, ','));
        System.assertEquals(284.98, currencyMap.get('CHF'));
        System.assertEquals(323.53, currencyMap.get('EUR'));
        System.assertEquals(361.0, currencyMap.get('GBP'));
        System.assertEquals(2.56, currencyMap.get('JPY'));
        System.assertEquals(206.63, currencyMap.get('SGD'));
        System.assertEquals(283.87, currencyMap.get('USD'));
    }

	@isTest
	public static void testScheduledCreateDailyMNBExchangeRates(){
        Test.setMock(HttpCalloutMock.class, new MNBWebServiceMock());
        Test.startTest();
        ScheduledCreateDailyMNBExchangeRates sc = new ScheduledCreateDailyMNBExchangeRates('2018-08-17');
        sc.execute(null);
        Test.stopTest();
        Currency__c curr = [SELECT Name, EUR__c, GBP__c, CHF__c, JPY__c, SGD__c, USD__c FROM Currency__c LIMIT 1];
        System.assertEquals(284.98, curr.CHF__c);
        System.assertEquals(323.53, curr.EUR__c);
        System.assertEquals(361.0, curr.GBP__c);
        System.assertEquals(2.56, curr.JPY__c);
        System.assertEquals(206.63, curr.SGD__c);
        System.assertEquals(283.87, curr.USD__c);
    }
    
    @isTest
    public static void testScheduleClassPrivateMethods(){
        ScheduledCreateDailyMNBExchangeRates.scheduleNextRun('2020-10-22');
        ScheduledCreateDailyMNBExchangeRates.sendErrorEmail();

        ScheduledCreateDailyMNBExchangeRates sc1 = new ScheduledCreateDailyMNBExchangeRates();
        ScheduledCreateDailyMNBExchangeRates sc2 = new ScheduledCreateDailyMNBExchangeRates(Date.newInstance(2018, 08, 17));
        Test.startTest();
        ScheduledCreateDailyMNBExchangeRates.SCHEDULE_CRON_EXPRESSION = '0 33 11 * * ?'; //to make a unique name with cron expression
        ScheduledCreateDailyMNBExchangeRates.scheduleForEverydayRun();
        Test.stopTest();
    }
}