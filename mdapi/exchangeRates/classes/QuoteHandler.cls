public with sharing class QuoteHandler 
{   
    private Map<string, Currency__c> currencyNameMap = new Map<string, Currency__c>();
    private Map<id, Quote> quoteIdMap = new Map<id, Quote>();
    
    public QuoteHandler()
    {
        
    }
	

	// Trigger overhul in progress. 
	// Author: László Földi
	// When: 2020-05-22
	// BASE 0
	// Every Quote functionality is going through this 4 method
	// This is the LAW from now on
	public void beforeInsert(List<Quote> newList) {
		setTemplateFields(null, newList);
	}
    
	public void beforeUpdate(Map<Id, Quote> oldMap, Map<Id, Quote> newMap) {
		setTemplateFields(oldMap, newMap.values());
	}

	//public void afterInsert(Map<Id, Quote> oldMap, Map<Id, Quote> newMap) {
	
	//}
	
	//public void afterUpdate(Map<Id, Quote> oldMap, Map<Id, Quote> newMap) {
		
	//}

	public void setTemplateFields(Map<Id, Quote> oldItems, List<Quote> changedItems) {
		for(Quote current: changedItems) {
			// Ha kézzel átírják a mezőket, engedjük a usernek.
			if(
				oldItems != null && 
				(
					(
						oldItems.get(current.Id).QuoteTemplateTerms__c != null &&
						current.QuoteTemplateTerms__c != null &&
						oldItems.get(current.Id).QuoteTemplateTerms__c.hashCode() != current.QuoteTemplateTerms__c.hashCode()
					) || 
					(
						oldItems.get(current.Id).Quote_template_Terms_prices__c != null &&
						current.Quote_template_Terms_prices__c != null &&
						oldItems.get(current.Id).Quote_template_Terms_prices__c.hashCode() != current.Quote_template_Terms_prices__c.hashCode()
					)
				)
			) {
				continue;
			} 

			Boolean isConsulting = current.QLI_Consulting__c > 0;
			Boolean isLicense = current.QLI_License__c > 0;
			Boolean isPerpetualLicense = current.QLI_Perpetual_License__c > 0;
			Boolean isSubscription = current.QLI_Subscription__c > 0;
			
			current.QuoteTemplateTerms__c = '';
			current.Quote_template_Terms_prices__c = '';

			if(isConsulting && !(isLicense || isSubscription)) {
				current.QuoteTemplateTerms__c += 'Inflation: Fees may be increased by relevant inflation data.\n\n' + 
					'SEND PURCHASE ORDER TO:\n\n' + 
					current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
					'PAYMENT TERMS: Payment due 30 days from invoice date.';

				current.Quote_template_Terms_prices__c += 'Inflation: Fees may be increased by relevant inflation data.\n\n' + 
					'SEND PURCHASE ORDER TO:\n\n' + 
					current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
					'PAYMENT TERMS: Payment due 30 days from invoice date.';
			
			}

			if(isLicense) {
				current.QuoteTemplateTerms__c += 'EULA\n' +
					'ChemAxon\'s products listed on this quote are provided under terms and conditions of ChemAxon\'s End User Licence Agreement (www.chemaxon.com/eula). ' + 
					'In case you have signed a license agreement with ChemAxon, the terms and conditions of such agreement are applicable.\n\n' + 
					'Mode of Delivery: License file sent via email, software downloaded from ChemAxon\'s website (http://www.chemaxon.com).\n\n' + 
					(
						!isPerpetualLicense ?
							'Annual licenses: Upgrades, maintenance and support are included in the license price and are available for the validity of the license.\n\n'
							:''
					);

				current.Quote_template_Terms_prices__c += 'EULA\n' +
					'ChemAxon\'s products listed on this quote are provided under terms and conditions of ChemAxon\'s End User Licence Agreement (www.chemaxon.com/eula). ' + 
					'In case you have signed a license agreement with ChemAxon, the terms and conditions of such agreement are applicable.\n\n' + 
					'Mode of Delivery: License file sent via email, software downloaded from ChemAxon\'s website (http://www.chemaxon.com).\n\n' + 
					(
						!isPerpetualLicense ?
							'Annual licenses: Upgrades, maintenance and support are included in the license price and are available for the validity of the license.\n\n'
							:''
					);

				if(isPerpetualLicense) {
					current.QuoteTemplateTerms__c += 'Perpetual licenses: Free upgrades, maintenance and support for one year after purchase. ' + 
						'Thereafter Support Services are charged at ' + current.Perpetual_maintenance_charge__c + '% of the initial purchase price annually, ' + 
						'60% of the price of the upgraded version after discontinuation.';

					current.Quote_template_Terms_prices__c += 'Perpetual licenses: Free upgrades, maintenance and support for one year after purchase. ' + 
						'Thereafter Support Services are charged at ' + current.Perpetual_maintenance_charge__c + '% of the initial purchase price annually, ' + 
						'60% of the price of the upgraded version after discontinuation.';
				}

				if(!(isLicense && isSubscription)) {
					current.QuoteTemplateTerms__c += 'Inflation: Fees may be increased by relevant inflation data.\n\n' + 
						'SEND PURCHASE ORDER TO:\n' +
						current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
						'PAYMENT TERMS: Payment due 30 days from invoice date.\n\n';
				}
				
				if(!(isLicense && isSubscription)) {
					current.Quote_template_Terms_prices__c += 'Inflation: Fees may be increased by relevant inflation data.\n\n' + 
						'SEND PURCHASE ORDER TO:\n' +
						current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
						'PAYMENT TERMS: Payment due 30 days from invoice date.\n\n';
				}

			}

			if(isLicense && isSubscription) {
				current.QuoteTemplateTerms__c += '\n\n';
				current.Quote_template_Terms_prices__c += '\n\n';
			}

			if(isSubscription) {
				current.QuoteTemplateTerms__c += 'EUSA\n' +
					'ChemAxon Services listed in this document shall be provided under terms and conditions of ChemAxon\'s standard End User Subscription Agreement (EUSA; available at www.chemaxon.com/eusa).\n\n' +
					'Mode of delivery: Login information of the Hosted or Software as a Service sent via email.\n\n' + 
					'Inflation: Fees may be increased by relevant inflation data.\n\n' +
					'SEND PURCHASE ORDER TO:\n' +
					current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
					'PAYMENT TERMS: Payment due 30 days from invoice date.';

				current.Quote_template_Terms_prices__c += 'EUSA\n' +
					'ChemAxon Services listed in this document shall be provided under terms and conditions of ChemAxon\'s standard End User Subscription Agreement (EUSA; available at www.chemaxon.com/eusa).\n\n' +
					'Mode of delivery: Login information of the Hosted or Software as a Service sent via email.\n\n' + 
					'Inflation: Fees may be increased by relevant inflation data.\n\n' +
					'SEND PURCHASE ORDER TO:\n' +
					current.OpportunityOwnerName__c + ' (see address above) \n\n' + 
					'PAYMENT TERMS: Payment due 30 days from invoice date.';
			}
			
			current.QuoteTemplateTerms__c += (isLicense || isSubscription ? '\n\n' : '') + 
				'Prices shown above do not include any taxes that may apply. ' + 
				'Any such taxes are the responsibility of Customer. This is not an invoice. ' + 
				'For customers based in the United States, any applicable taxes will be determined ' + 
				'based on the laws and regulations of the taxing authority(ies) ' + 
				'governing the "Ship To" location provided by the Customer.';

			//current.Quote_template_Terms_prices__c += (isLicense || isSubscription ? '\n\n' : '') + 
				//'Total (NET): ' + (current.TotalPrice != null ? current.TotalPrice.format() : '0.0') + + '\n' +
				//'VAT: ' + (current.Tax != null ? current.Tax.format() : '0.0') + '\n' +
				//'Total (Gross) price:' + (current.GrandTotal != null ? current.GrandTotal.format() : '0.0');

		}
	}

    /**
     * sets QLI Currency objects
     */
	public void customCurrenciesSetter(List<Quote> invoicedQuoteList)
    {
        List<Messaging.SingleEmailMessage> emailMessageList = new List<Messaging.SingleEmailMessage>();
        List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
        List<QuoteLineItem> quoteLineItemToUpdateList = new List<QuoteLineItem>();
        
        Map<id, Messaging.SingleEmailMessage> emailMessageQuoteIdMap = new Map<id, Messaging.SingleEmailMessage>();
        
        Set<string> yearStringSet = new Set<string>();
        Set<id> quoteIdSet = new Set<id>(); 
        
        for(Quote quote : invoicedQuoteList)
        {
            yearStringSet.add(string.valueof(quote.Invoice_date__c.year()));
            
            quoteIdMap.put(quote.Id, quote);
            
            quoteIdSet.add(quote.Id);
        }
        
        if(yearStringSet.size() != 0)
        {
            populateCurrencyMap(yearStringSet);
        }
        
        quoteLineItemList =
            [
                SELECT
                    Id,
                    QuoteId,
                    Currency__c
                FROM
                    QuoteLineItem
                WHERE
                    QuoteId IN :quoteIdSet
            ];
            
        for(QuoteLineItem currentQLI : quoteLineItemList)
        {
            TriggerStopper.addIdToMap('quoteLineItemIdMap', currentQli.Id); // counts references for record ID
            
            id tempCurrencyId;
            
            tempCurrencyId = findCurrency(currentQLI.QuoteId);

            if(tempCurrencyId != NULL)
            {
                if(currentQLI.Currency__c != tempCurrencyId)
                {
                    currentQLI.Currency__c = tempCurrencyId;
                    
                    quoteLineItemToUpdateList.add(currentQLI);
                }
            }
            else
            {
                //emailMessageList.add(composeMissingCurrencyEmail(currentQLI));
                emailMessageQuoteIdMap.put(currentQLI.QuoteId, composeMissingCurrencyEmail(currentQLI));
            }
        }
        
        if(quoteLineItemToUpdateList.size() != 0)
        {
            update quoteLineItemToUpdateList;
        }
        
        if(emailMessageQuoteIdMap.size() != 0 && !Test.isRunningTest())
        {
            Messaging.sendEmail(emailMessageQuoteIdMap.values());
        }
    }
    
    /*
    public void customCurrenciesSetterQli(List<Quote> invoicedQuoteList, List<Quote> settledQuoteList, List<QuoteLineItem> qliList)
    {
        List<Messaging.SingleEmailMessage> emailMessageList = new List<Messaging.SingleEmailMessage>();
        List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
        
        Map<id, Messaging.SingleEmailMessage> emailMessageQuoteIdMap = new Map<id, Messaging.SingleEmailMessage>();
        
        system.debug(invoicedQuoteList);
        system.debug(settledQuoteList);
        system.debug(qliList);
        
        Set<string> yearStringSet = new Set<string>();
        Set<id> quoteIdSet = new Set<id>(); 
        
        for(Quote quote : invoicedQuoteList)
        {
            yearStringSet.add(string.valueof(quote.Invoice_date__c.year()));
            
            quoteIdMap.put(quote.Id, quote);
            
            quoteIdSet.add(quote.Id);
        }
        
        for(Quote quote : settledQuoteList)
        {
            yearStringSet.add(string.valueof(quote.Actual_payment_date__c.year()));
            
            quoteIdMap.put(quote.Id, quote);
            
            quoteIdSet.add(quote.Id);
        }
        
        if(yearStringSet.size() != 0)
        {
            populateCurrencyMap(yearStringSet);
        }
            
        for(QuoteLineItem currentQLI : qliList)
        {
            system.debug(currentQLI);
            
            id tempCurrencyId;
            
            tempCurrencyId = findCurrency(currentQLI.QuoteId);
            
            system.debug(tempCurrencyId);
            
            if(tempCurrencyId != NULL)
            {
                if(currentQLI.Currency__c != tempCurrencyId)
                {
                    currentQLI.Currency__c = tempCurrencyId;
                }
            }
            else
            {
                emailMessageQuoteIdMap.put(currentQLI.QuoteId, composeMissingCurrencyEmail(currentQLI));
            }
        }
        
        if(emailMessageQuoteIdMap.size() != 0)
        {
            system.debug(emailMessageQuoteIdMap);
            Messaging.sendEmail(emailMessageQuoteIdMap.values());
        }
    }
    */
    
    public void populateCurrencyMap(Set<string> yearStringSet)
    {
        integer currencyListSize;
        
        List<Currency__c> currencyList = new List<Currency__c>();
        
        currencyList = 
            [
                SELECT
                    Id,
                    Name
                FROM
                    Currency__c
                WHERE   
                    Currency_year__c IN :yearStringSet
                ORDER BY Name
                ASC
            ];
            
        currencyListSize = currencyList.size();
            
        for(integer i=0; i<currencyListSize; i++)
        {
            if
            (
                i+1 != currencyListSize &&
                currencyList[i].Name != currencyList[i+1].Name
            )
            {
                currencyList.addAll(createFakeCurrencyList(currencyList[i], currencyList[i+1]));
            }
        }
        
        system.debug(currencyList);
        
        for(Currency__c currentCurrency : currencyList)
        {
            currencyNameMap.put(currentCurrency.Name, currentCurrency);
        }
    }
    
    private List<Currency__c> createFakeCurrencyList(Currency__c actualCurrency, Currency__c nextCurrency)
    {
        date actualCurrencyDate;
        date nextCurrencyDate;
        
        List<Currency__c> tempCurrencyList = new List<Currency__c>();
        
        List<string> actualCurrencyNameStringList = new List<string>();
        List<string> nextCurrencyNameStringList = new List<string>();
        
        actualCurrencyNameStringList = actualCurrency.Name.split('-');
        nextCurrencyNameStringList = nextCurrency.Name.split('-');
        
        actualCurrencyDate = date.newInstance(integer.valueof(actualCurrencyNameStringList[0]), integer.valueof(actualCurrencyNameStringList[1]), integer.valueof(actualCurrencyNameStringList[2]));
        nextCurrencyDate = date.newInstance(integer.valueof(nextCurrencyNameStringList[0]), integer.valueof(nextCurrencyNameStringList[1]), integer.valueof(nextCurrencyNameStringList[2]));
        
        while(actualCurrencyDate.addDays(1) < nextCurrencyDate)
        {
            string tempDateString;
            
            Currency__c tempCurrency = new Currency__c();
            
            actualCurrencyDate = actualCurrencyDate.addDays(1);
            
            tempCurrency = actualCurrency.clone(true, false, false, false);
            
            tempDateString = createTempDateString(actualCurrencyDate);
            
            tempCurrency.Name = tempDateString;
            
            tempCurrencyList.add(tempCurrency);
        }
    
        return tempCurrencyList;
    }
    
    private string createTempDateString(date actualCurrencyDate)
    {
        string tempDateString;
        
        tempDateString = string.valueof(actualCurrencyDate.year()) + '-';
        
        if(actualCurrencyDate.month() < 10)
        {
            tempDateString += '0' + string.valueof(actualCurrencyDate.month()) + '-';
        }
        else
        {
            tempDateString += string.valueof(actualCurrencyDate.month()) + '-';
        }
        
        if(actualCurrencyDate.day() < 10)
        {
            tempDateString += '0' + string.valueof(actualCurrencyDate.day());
        }
        else
        {
            tempDateString += string.valueof(actualCurrencyDate.day());
        }
        
        return tempDateString;
    }
    
    public id findCurrency(id quoteId)
    {
        id currencyId;
        
        string tempDateString;  
        
        Quote tempQuote = new Quote();
        
        if(quoteIdMap.containsKey(quoteId))
        {
            tempQuote = quoteIdMap.get(quoteId);
        }
        
        if(tempQuote.Status == 'Invoiced' && tempQuote.Invoice_Date__c != NULL)
        {       
            tempDateString = createTempDateString(tempQuote.Invoice_Date__c);
        }
        
        if(tempDateString != NULL)
        {
            if(currencyNameMap.containsKey(tempDateString))
            {
                currencyId = currencyNameMap.get(tempDateString).Id;
            }
        }
        
        return currencyId;
    }
    
    public date createTempDateDate(string tempDateString)
    {
        date tempDate;
        
        List<string> tempDateStringList = new List<string>();
        
        tempDateStringList = tempDateString.split('-');
        
        tempDate = date.newInstance(integer.valueof(tempDateStringList[0]), integer.valueof(tempDateStringList[1]), integer.valueof(tempDateStringList[2]));
        
        return tempDate;
    }
    
    private Messaging.SingleEmailMessage composeMissingCurrencyEmail(QuoteLineItem qli)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        string missingExchangeRateString;
        
        Quote tempQuote = new Quote();
        
        List<string> toEmailStringList = new List<string>();
        List<string> ccEmailStringList = new List<string>();
        
        if(quoteIdMap.containsKey(qli.QuoteId))
        {
            tempQuote = quoteIdMap.get(qli.QuoteId);
        }
        else
        {
            return null;
        }
        
        if(tempQuote.Status == 'Invoiced')
        {
            missingExchangeRateString = createTempDateString(tempQuote.Invoice_Date__c);
        }
        
        if(tempQuote.Status == 'Settled')
        {
            missingExchangeRateString = createTempDateString(tempQuote.Actual_Payment_Date__c);
        }
        
        if(! Util.runningInASandbox ){
            toEmailStringList.add(Util.FINANCEEMAILADDRESS);
        }
        ccEmailStringList.add(Util.UCEMAILADDRESS);         
        
        mail.setToAddresses(toEmailStringList);
        mail.setCCAddresses(ccEmailStringList);
        
        mail.setSubject('Missing exchange rate');
        mail.setUseSignature(false);
        mail.setCharset('UTF-8');
        
        mail.setHtmlBody
            (                       
                'Dear Anett,'
                +
                '<br/><br/>'
                +
                'Missing exchange rate on ' + missingExchangeRateString
                +
                '<br/><br/>'
                +
                'Regards,<br/> CRM'
            );
        
        return mail;
    }
    
    public string getQuoteTerms(string invoiceType, User user, string currencyIsoCode)
    {
        string quoteTerms;
        
        system.debug('invoicetype: ' + invoiceType);
        
        if(invoiceType != NULL)
        {
            if(invoiceType == 'LLC')
            {
                quoteTerms = getQuoteTermsBeginning(user);
                quoteTerms += getQuoteTermsEnding('LLC', currencyIsoCode);
            }
            
            if(invoiceType == 'Kft')
            {
                quoteTerms = getQuoteTermsBeginning(user);
                quoteTerms += getQuoteTermsEnding('Kft', currencyIsoCode);
            }
        }
        else
        {
            if(user.CompanyName != NULL)
            {
                if(user.CompanyName.toUpperCase().startsWith('CHEMAXON LLC'))
                {
                    quoteTerms = getQuoteTermsBeginning(user);
                    quoteTerms += getQuoteTermsEnding('LLC', currencyIsoCode);
                }
                
                if(user.CompanyName.toUpperCase().startsWith('CHEMAXON LTD'))
                {
                    quoteTerms = getQuoteTermsBeginning(user);
                    quoteTerms += getQuoteTermsEnding('Kft', currencyIsoCode);
                }
            }
            else
            {
                quoteTerms = getQuoteTermsBeginning(user);
                quoteTerms += getQuoteTermsEnding('LLC', currencyIsoCode);
            }
        }
        
        return quoteTerms;
    }
    
    private string getQuoteTermsBeginning(User user)
    {
        string quoteTermsBeginning;
        
        quoteTermsBeginning =
            'Mode of Delivery: License file sent via email, software downloaded from ChemAxon\'s website (http://www.chemaxon.com)' + '\n\n' +
            'Please read our EULA at www.chemaxon.com/eula.' + '\n\n' +
            'Inflation: Fees may be increased by relevant inflation data.' + '\n\n' +
            'SEND PURCHASE ORDER TO:' + '\n' +
             user.FirstName + ' ' + user.LastName + ' (see address above)' + '\n\n';
            
        
        return quoteTermsBeginning;
    }
    
    private string getquoteTermsEnding(string invoiceType, string currencyIsoCode)
    {
        string quoteTermsEnding;
        
        if(invoiceType == 'LLC')
        {
            quoteTermsEnding = 
                'Payments should be made by wire transfer to:' + '\n' +
                'Bank of America' + '\n' +
				'193 Washington Street' + '\n' +
				'Salem, MA 01970' + '\n' +
                'Acct Name: ChemAxon LLC' + '\n' +
                'Acct# 004661202735' + '\n' +
                'Routing #: 026009593' + '\n' +
                'SWIFT CODE: BOFAUS3N' + '\n' +
                'ABA # 011000138' + '\n\n';
        }

        if(invoiceType == 'Kft')
        {
            string ibanAccountNo;
            
            ibanAccountNo = 'IBAN Account No:   ';
            
            /*
            USD --> HU31101021033141050001003994
            EUR --> HU64101021033141050001003303
            HUF és minden fennmaradó (GBP, CHF stb) --> HU26101021033141050400000004
            */
            
            if(currencyIsoCode == 'USD')
            {
                ibanAccountNo += 'HU31101021033141050001003994';
            }
            else if(currencyIsoCode == 'EUR')
            {
                ibanAccountNo += 'HU64101021033141050001003303';
            }
            else
            {
                ibanAccountNo += 'HU26101021033141050400000004';
            }
            
            quoteTermsEnding = 
                'SEND WIRE TRANSFER TO:' + '\n' +
                'Swift code:    BUDAHUHB' + '\n' +
                'Bank address:  Budapest Bank Zrt., 1023 Budapest, Lajos u. 30.' + '\n' +
                'Account name:  ChemAxon Kft., 1031 Budapest, Záhony utca 7.' + '\n' +
                ibanAccountNo;
        }
        
        return quoteTermsEnding;
    }
    
    /*
    public string getUserMonogram(string firstName, string lastName)
    {
        string monogram;
        
        monogram = '';
        
        if(firstName !=  NULL)
        {
            monogram += firstName.substring(0, 1);
        }
        
        if(lastName != NULL)
        {
            monogram += lastName.substring(0, 1);
        }
        
        return monogram;
    }
    */

	/**
	 * @description This function creates quotelineitems for the input quotes based on the opportunitylineitems of the parent opportunity for said quotes.
	 * @param Map<Id, Quote> input Opportunity Id to Quote
	 * @return void 
	 **/ 
	public static void createQlisFromOpportunity(Map<Id, Quote> input){
		List<QuoteLineItem> qlisToInsert = new List<QuoteLineItem>();
		Map<Id, List<OpportunityLineItem>> oppLineItems = new Map<Id, List<OpportunityLineItem>>();
		for(OpportunityLineItem oli : [SELECT Id, Quantity, PricebookEntryId, OpportunityId, UnitPrice FROM OpportunityLineItem WHERE OpportunityId IN :input.keySet()]){
			qlisToInsert.add(
				new QuoteLineItem(
					QuoteId = input.get(oli.OpportunityId).Id,
					PricebookEntryId = oli.PricebookEntryId,
					Quantity = oli.Quantity,
					UnitPrice = oli.UnitPrice
				)
			);
		}

		if(!qlisToInsert.isEmpty()){
			insert qlisToInsert;
		}
	}
}