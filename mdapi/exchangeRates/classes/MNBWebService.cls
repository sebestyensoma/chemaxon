/**
  @author	Gerse Mihály
  @created	2018-08-17 
 
*/
public class MNBWebService {
	/**
	 * @param exchangeRateDate String format: YYYY-MM-dd
	 * @param exchangeRateCurrencies String comma separated currencies
	 **/
	public static Map<String, Decimal> getCurrencies(String exchangeRateDate, String exchangeRateCurrencies) {

		if (String.isBlank(exchangeRateDate) || !Pattern.matches('\\d{4}-\\d{2}-\\d{2}', exchangeRateDate)) {
			return null;
		}

		HttpRequest request = new HttpRequest();
		HttpResponse response = new HttpResponse();
		Http http = new Http();

		String envelope = '';
		envelope += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://www.mnb.hu/webservices/">';
		envelope += '<soapenv:Header/>';
		envelope += '<soapenv:Body>';
		envelope += '<web:GetExchangeRates>';
		envelope += '<web:startDate>' + exchangeRateDate + '</web:startDate>';
		envelope += '<web:endDate>' + exchangeRateDate + '</web:endDate>';
		envelope += '<web:currencyNames>' + exchangeRateCurrencies + '</web:currencyNames>';
		envelope += '</web:GetExchangeRates>';
		envelope += '</soapenv:Body>';
		envelope += '</soapenv:Envelope>';

		request.setEndpoint('http://www.mnb.hu/arfolyamok.asmx');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'text/xml');
		request.setHeader('SOAPAction', 'http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetExchangeRates');
		request.setBody(envelope);

		response = http.send(request);
		
		while (response.getStatusCode() != 200 && Limits.getCallouts() < Limits.getLimitCallouts()){
			response = http.send(request);	 
		}

		String rawXML = response.getBody().replace('&lt;', '<').replace('&gt;', '>');
		Dom.Document result = new Dom.Document(); 
		try {
			result.load(rawXML);
		}
		catch(Exception e) {
			if(String.isNotBlank(rawXML)){
				Util.sendTextBasedEmailToAddress(Util.UCEMAILADDRESS , 'Problem with the MNB xml: ',rawXML);
			}
			throw e;
		}
		Dom.XMLNode root = result.getRootElement();
		String exchangeRate;
		Map<String, Decimal> currencies = new Map<String, Decimal> ();

		// Envelope -> Body -> GetExchangeRatesResponse -> GetExchangeRatesResult -> MNBExchangeRates -> Day -> Rate
		// Ha még nem elérhető a napi árfolyam List index out of bounds exceptiont fogunk kapni
		for (dom.XmlNode node : root.getChildElements() [0].getChildElements() [0].getChildElements() [0].getChildElements() [0].getChildElements() [0].getChildElements()) {
			Double unit = Double.valueOf(node.getAttribute('unit', null));
			String curr = node.getAttribute('curr', null);
			Decimal currencyValue = Decimal.valueOf(node.getText().replace(',', '.'));
			System.debug(node.getName() + '  ' + node.getAttribute('unit', null) + '  ' + node.getAttribute('curr', null) + '  ' + node.getText());
			if (unit != 1) {
				currencyValue = currencyValue / unit;
			}
			currencies.put(curr, currencyValue.setScale(2));
		}
		return currencies;
	}

	public static Map<String, Decimal> getCurrencies(Date exchangeRateDate, String exchangeRateCurrencies) {
		return getCurrencies(((Datetime) exchangeRateDate).format('YYYY-MM-dd'), exchangeRateCurrencies);
	}
}