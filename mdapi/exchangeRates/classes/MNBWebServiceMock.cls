@isTest
public class MNBWebServiceMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request){
        System.assertEquals('POST', request.getMethod());
        System.assertEquals('http://www.mnb.hu/arfolyamok.asmx', request.getEndpoint());

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'text/xml');
        response.setStatusCode(200);
        response.setBody(
            '	<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
		'		<s:Body>' +
		'			<GetExchangeRatesResponse xmlns="http://www.mnb.hu/webservices/" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
		'				<GetExchangeRatesResult>' +
		'					<MNBExchangeRates>' +
		'						<Day date="2018-08-17">' +
		'							<Rate unit="1" curr="CHF">284,98</Rate>' +
		'							<Rate unit="1" curr="EUR">323,53</Rate>' +
		'							<Rate unit="1" curr="GBP">361</Rate>' +
		'							<Rate unit="100" curr="JPY">256,36</Rate>' +
		'							<Rate unit="1" curr="SGD">206,63</Rate>' +
		'							<Rate unit="1" curr="USD">283,87</Rate>' +
		'						</Day>' +
		'					</MNBExchangeRates>' +
		'				</GetExchangeRatesResult>' +
		'			</GetExchangeRatesResponse>' +
		'		</s:Body>' +
		'	</s:Envelope>'
        );

        return response;
    }
}
