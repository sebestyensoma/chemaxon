trigger CurrencyTrigger on Currency__c (after insert, after update) 
{	
	Date lastCurrencyDate;
	
	List<Quote> quoteList = new List<Quote>();
	List<Quote> invoicedQuoteList = new List<Quote>();
	List<Quote> settledQuoteList = new List<Quote>();
	List<Currency__c> currencyList = new List<Currency__c>();
	
	Set<Date> dateSet = new Set<Date>();
	
	QuoteHandler qh = new QuoteHandler();
	
	if(trigger.isInsert)
	{
		currencyList = [ SELECT Name FROM Currency__c ORDER BY Name DESC LIMIT 2 ];
		
		if(currencyList.size() > 1)
		{
			lastCurrencyDate = qh.createTempDateDate(currencyList[1].Name);
		}
		else
		{
			lastCurrencyDate = date.newInstance(date.today().year(), date.today().month(), date.today().day());
		}
			
		for(Currency__c currentCurrency : trigger.new)
		{
			date tempDate;
			
			tempDate = qh.createTempDateDate(currentCurrency.Name);
			
			dateSet.add(tempDate);
			
			if(lastCurrencyDate < tempDate)
			{
				while(lastCurrencyDate < tempDate)
				{
					dateSet.add(lastCurrencyDate);
					lastCurrencyDate = lastCurrencyDate.addDays(1); 
				}
			}
		}
	}
	
	if(dateSet.size() != 0)
	{
		quoteList =
			[
				SELECT
					Id,
					Status,
					Invoice_Date__c,
					Actual_Payment_Date__c
				FROM
					Quote
				WHERE
					Invoice_Date__c IN :dateSet
			];

		qh.customCurrenciesSetter(quoteList);
	}
}