global class CalculateRenewalExecutor implements Database.Batchable<sObject>, Database.Stateful {

	private Integer targetYear;
	public Integer quoteCount;
	private Id KJ;
	private String opptyId;

	public CalculateRenewalExecutor(Integer targetYear) {
		this.targetYear = targetYear;
		KJ = '005D00000055VJe'; // [SELECT Id FROM User WHERE Name = :'Judit Kerékgyártó' LIMIT 1].Id;
		opptyId = null;
	}

	public CalculateRenewalExecutor(String opptyId){
		this.opptyId = opptyId;
		KJ = '005D00000055VJe';
		targetYear = Date.today().year();
	}

	public static void runBatchableClassWithTargetYear( Integer targetYear ){
		CalculateRenewalExecutor cre = new CalculateRenewalExecutor(targetYear);
		Database.executeBatch(cre, 50);
	}

	global Database.QueryLocator start(Database.BatchableContext context) {
		if(opptyId != null && opptyId != ''){
			return Database.getQueryLocator('SELECT Id FROM Quote WHERE OpportunityId = \'' + opptyId + '\'' );
		}else{
			return Database.getQueryLocator('SELECT Id FROM Quote WHERE CALENDAR_YEAR(Invoice_Date__c) = ' + targetYear + ' AND Renewed__c = false');
		}
		// TODO: a köv. sor csak a 2019.12.04. utáni opp-ok renewal-ja miatt kell. Renewal után az előző sor kell.
		//return Database.getQueryLocator('SELECT Id FROM Quote WHERE CALENDAR_YEAR(Invoice_Date__c) = ' + targetYear + ' and calendar_year(CreatedDate) = 2019 and calendar_month(CreatedDate) = 12 and DAY_IN_MONTH(createddate) > 4');
	}
	
	global void execute(Database.BatchableContext context, Quote[] quotes) {
		quoteCount = null;
		Set<Id> renewedQuotes = new Set<Id>();
        for (Quote q : [ SELECT Renewed_From__c FROM Quote WHERE Renewed_From__c != null ]) {
            renewedQuotes.add(q.Renewed_From__c);
		}
		
		Set<Id> quoteIds = new Set<Id>();
		for (Quote q : quotes) {
			if(! renewedQuotes.contains(q.Id)){
				quoteIds.add(q.Id);
			}
		}

		
		
		Map</* Quote */ Id, /* Opportunity */ Id> oldOppIds = new Map<Id, Id>();
		for (QuoteLineItem item : [ SELECT QuoteId, Quote.OpportunityId FROM QuoteLineItem WHERE QuoteId IN :quoteIds AND License_Type__c != 'Consulting' AND PricebookEntry.isActive = true ]) {
			oldOppIds.put(item.quoteId, item.Quote.OpportunityId);
		}
		if (oldOppIds.isEmpty()) {
			quoteCount = 0;
			return;
		}
		
		// Get old oppties
		Map<Id, Opportunity> oldOpps = new Map<Id, Opportunity>([
			SELECT of_Opps__c, Winning_Competitor__c, Win_Loss_Reason__c, Win_Loss_Contact__c, Win_Loss_Comment__c, Type_of_Users__c, Type_of_Company__c, Type_Description__c,
				Type, T_of_Opp__c, T_Total_Price_Rollup__c, T_Today_30_Days__c, T_Product_Area_Interest_text__c, T_End_User_Date__c, T_Default_Invoice_Type__c, T_Closed_Won__c,
				SystemModstamp, SyncedQuoteId, StageName, Short_Description_of_Problem__c, Sales_Price_Rollup__c, RequirementsDescription__c, Renewed_From__c, Renewal_Percentage__c,
				Renewal_Base__c, Quote_Total_Price__c, Purchase_Amount__c, Products_Involved__c, Product__c, Product_Area_Interest__c, Product_Area_Interest_G__c, Pricebook2Id,
				Plugins_Contained__c, Partner__c, PERCENT_del__c, OwnerId, Other_Description__c, NumberofLicenses__c, Number_of_Users__c, NextStep, Name_requirement__c, Name,
				My_Amount_del__c, MainCompetitor__c, List_Price_Rollup__c, License_type_del__c, License_Key_Generation_VAR_Opp__c, LeadSource, Last_License_NO__c, LastViewedDate,
				LastReferencedDate, LastModifiedDate, LastModifiedById, LastActivityDate, IsWon, IsDeleted, IsClosed, IntegrationNeeds__c, InitialBPRDate__c, Id,
				HasOpportunityLineItem, ForecastCategoryName, ForecastCategory, FiscalYear, FiscalQuarter, Fiscal, Feature_request__c, External_id__c, Expected_Next_Visit__c,
				End_User__c, Description, DealLostReason__c, Database__c, Currency__c, CurrencyIsoCode, CreatedDate, CreatedById, ContractEndDate__c, Comment__c, CloseDate,
				CampaignId, Calculated_Support__c, Billing_Company__c, Amount_EUR__c, Amount, AccountId, Owner.IsActive
			FROM Opportunity
			WHERE Id IN :oldOppIds.values()
		]);
		
		//get old quotes
		Map<Id, Quote> oldQuotes = new Map<Id, Quote>([
			SELECT technical_Targetprice_Other_Triggered__c, technical_Quote_Item_Perpetual_Sum__c, technical_Quote_Item_License_Type_Sum__c, technical_Quote_Item_Annual_Sum__c,
				technical_Discount_Other_Triggered__c, of_Quotes__c, Total_Original_Price__c, TotalPrice, Tax_number__c, Tax, Target_Price__c, Target_Discount__c, T_Invoice_Date__c,
				T_Actual_Payment_Date__c, SystemModstamp, Subtotal, Status, Show_discount_on_quote__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName,
				ShippingLongitude, ShippingLatitude, ShippingHandling, ShippingCountry, ShippingCity, Renewed_From__c, Quote_display_user_address__c, Quote_display_terms__c,
				Quote_display_support_expiry__c, Quote_display_subtotal__c, Quote_display_subtotalB__c, Quote_display_number__c, Quote_display_license_exp__c,
				Quote_display_expiration_date__c, Quote_display_discount__c, Quote_display_additional_details__c, QuoteNumber, Pricebook2Id, Prepared_for_Contact_Name__c,
				Plugins_Contained__c, Phone, Payment_Deadline__c, PO_No__c, OpportunityId, Name, Multiyear__c, LineItemCount, License_Type__c, License_Support_Expiration_Date__c,
				License_Key_Generation__c, License_Expiration_Date__c, LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, IsSyncing, IsDeleted,
				Invoice_Type__c, Invoice_No__c, Invoice_Display_Visible_Productslist__c, Invoice_Display_Visible_MoD__c, Invoice_Display_Visible_CoU__c,
				Invoice_Display_Visible_CoP__c, Invoice_Display_Reference_to__c, Invoice_Display_Products__c, Invoice_Display_Payment_Due_Days__c,
				Invoice_Display_Blank_Lines_Before_PL__c, Invoice_Display_Blank_Lines_Before_PI__c, Invoice_Display_Additional_LLC_Info__c, Invoice_Date__c,
				Instructions_for_Finance__c, Id, Grand_total_without_round__c, Grand_Total_wTax__c, Grand_Total_rounded__c, GrandTotal, Fax, External_id__c, ExpirationDate,
				Email, Discount, Description, Custom_Expiry__c, CurrencyIsoCode, CreatedDate, CreatedById, Copy_Shipping__c, Copy_Billing__c, Contact_Quote__c, Contact_License__c,
				Contact_Invoice__c, ContactId, Company_Name__c, BillingStreet, BillingState, BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry,
				BillingCity, Additional_LLC_Information__c, Actual_Payment_Date__c, Account_Name__c
			FROM Quote
			WHERE Id IN :oldOppIds.keySet()
		]); 
		
		List<Id> oppIdList = oldOppIds.values();
		String oliQueryString = 
			' SELECT ' +
			String.join(new List<String>(Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().keySet()), ',') +
			' FROM OpportunityLineItem ' +
			' WHERE OpportunityId IN :oppIdList';

		List<OpportunityLineItem> oliList = Database.query(oliQueryString);

		Map<Id/*opp id*/, List<OpportunityLineItem>> oldOLIsByOppId = new Map<Id, List<OpportunityLineItem>>();
		//for(OpportunityLineItem oli :[SELECT Id, OpportunityId, TotalPrice, License_Type__c, Target_Price__c, Discount, Created_By_Form__c FROM OpportunityLineItem WHERE OpportunityId IN :oldOppIds.values()]){
		for(OpportunityLineItem oli : oliList){
			List<OpportunityLineItem> tmp = oldOLIsByOppId.get(oli.OpportunityId);
			if(tmp == null){
				tmp = new List<OpportunityLineItem>();
				oldOLIsByOppId.put(oli.OpportunityId, tmp);
			}
			tmp.add(oli);
		}

		// Create new Opportunities
		Map</* old Quote */ Id, /* new */ Opportunity> newOpps = new Map<Id, Opportunity>();
		Map<Id/*new opp id*/, Id/*old opp id*/> oldOppIdByNewOppId = new Map<Id, Id>();
		for (Quote q : oldQuotes.values()) {
			Opportunity oldOpp = oldOpps.get(q.OpportunityId);
			if (oldOpp.Short_Description_of_Problem__c == null) oldOpp.Short_Description_of_Problem__c = 'DESCRIPTION HERE';
			
			Date renewCloseDate = q.Invoice_Date__c.addYears(1).toStartOfMonth().addDays(-1);
			if (renewCloseDate.year() == q.Invoice_Date__c.year()) {
				renewCloseDate = Date.newInstance(q.Invoice_Date__c.year() + 1, 1, 1);
			}
			
			Opportunity newOpp = oldOpp.clone();
			newOpp.Name = newOpp.Name + String.valueOf(targetYear + 1);
			newOpp.StageName = 'Proposal/Quote';
			newOpp.ForecastCategoryName = 'Commit';
			newOpp.LeadSource = 'Sales';
			//newOpp.CloseDate = renewCloseDate;
			newOpp.Renewed_From__c = oldOpp.Id;
			newOpp.SyncedQuoteId = NULL;
			if(!oldOpp.Owner.IsActive) newOpp.OwnerId = KJ;

			newOpps.put(q.Id, newOpp);
			newOpp.T_Opportunity_renewal_test__c = true;
		}
		insert newOpps.values();
		for(Opportunity opp : newOpps.values()){
			oldOppIdByNewOppId.put(opp.Id, opp.Renewed_From__c);
		}

		// Create new Quotes
		Map</* old Quote*/ Id, /* new */ Quote> newQuotes = new Map<Id, Quote>();
		for (Quote q : oldQuotes.values()) {
			
			
			Quote newQuote = q.clone(false, true, false, false);
			newQuote.OpportunityId = newOpps.get(q.Id).Id;
			newQuote.Status = 'Draft';
			newQuote.ExpirationDate = (q.ExpirationDate != null && q.ExpirationDate.year() != 3000) ? q.ExpirationDate.addYears(1) : q.ExpirationDate;
			newQuote.Renewed_From__c = q.Id;
			
			newQuote.PO_No__c = null;
			newQuote.Payment_Deadline__c = null;
			newQuote.Actual_Payment_Date__c = null;
			newQuote.Invoice_No__c = null;
			newQuote.Invoice_Date__c = null;
			newQuote.License_Expiration_Date__c = null; // (q.License_Expiration_Date__c != null && q.License_Expiration_Date__c.year() != 3000) ? q.License_Expiration_Date__c.addYears(1) : q.License_Expiration_Date__c;
			newQuote.License_Support_Expiration_Date__c = null; // (q.License_Support_Expiration_Date__c != null && q.License_Support_Expiration_Date__c.year() != 3000) ? q.License_Support_Expiration_Date__c.addYears(1) : q.License_Support_Expiration_Date__c;
			newQuote.Custom_Expiry__c = null;
			newQuote.Target_Price__c = null;
			newQuote.Target_Discount__c = null;
			newQuote.Multiyear__c = false;

			newQuotes.put(q.Id, newQuote);
			q.Renewed__c = true; //mark old quote as already handled
		}
		Integer counter = 0;
		for (Quote q : newQuotes.values()) {
			System.debug(counter++ + ' - old quote id: ' + q.Renewed_From__c + ' Billing state: ' + q.BillingState);
		}
		insert newQuotes.values();
		
		List<OpportunityLineItem> newOLIs = new List<OpportunityLineItem>();
		//Map<Id/*opp id*/, Set<String/*qli license type*/>> qliLicenseType = new Map<Id, Set<String>>();
		Map<Id/*opp id*/, Map<String/*qli license type*/, Date/*quote License Expiration Date vagy License Support Expiration Date*/>> oppCloseDate = new Map<Id, Map<String, Date>>();
		// Create new QLI's
		List<QuoteLineItem> newItems = new List<QuoteLineItem>();		
		for (QuoteLineItem item : [
			SELECT technical_Quote_Price_last_updated__c, technical_Quote_Line_Item_Perpetual_ID__c, technical_Quote_Line_Item_Annual_ID__c, Valid__c, User_Limit__c, User_LOG__c,
				Unit_Price__c, UnitPrice, Total_Price_without_round__c, Total_Price_rounded__c, TotalPrice, Target_Pricing_In_Action__c, Target_Price__c, T_Product_Code_Type__c,
				T_Calculate_Prices__c, TEST_Quote_Price__c, System_Message__c, SystemModstamp, Subtotal, SortOrder, Quote_display_quantity__c, Quote_display_line_discount__c,
				Quote_Target_Price__c, Quote_Price__c, QuoteId, Quantity, Product_Code__c, PricebookEntryId, Price_in_USD__c, Plugin__c, Package__c, Owner_in_Sales_Report__c,
				Original_Price__c, Opportunity_Renewal_Percentage__c, ListPrice, LineNumber, License_type__c, LastModifiedDate, LastModifiedById, Isplugin__c, IsPluginNF__c,
				IsDeleted, Id, External_id__c, End_User__c, Discount_target_price__c, Discount, Description, Custom_Quantity__c, Currency__c, CurrencyIsoCode, Created_By_Form__c,
				CreatedDate, CreatedById, Condition__c, Comment__c, Bundle_LOG__c, Quote.OpportunityId, Quote.Opportunity.Renewal_Percentage__c, 
				PricebookEntry.Product2Id, PricebookEntry.Product2.Name, PricebookEntry.IsActive//, //Product2Id, //Product2.Name,
				//AVA_SFCLOUD__SalesTax_Line__c, AVA_SFCLOUD__Sales_Tax_Details__c, AVA_SFCLOUD__Sales_Tax_Rate__c, AVA_SFCLOUD__Tax_Override__c
			FROM QuoteLineItem
			WHERE QuoteId IN :oldOppIds.keySet()
				AND License_Type__c != 'Consulting'
				AND PricebookEntry.Product2.isActive = true
				AND PricebookEntry.isActive = true
		]) {

			QuoteLineItem newItem = item.clone();
			newItem.QuoteId = newQuotes.get(item.QuoteId).Id;
			if (item.License_Type__c == 'Perpetual') {
				//newItem.Target_Price__c = item.TotalPrice * newOpps.get(item.QuoteId).Renewal_Percentage__c / 100; // 20150212 BÁ: item.UnitPrice -> item.TotalPrice
				newItem.Target_Price__c = item.TotalPrice * 0.25; // 2019.11.15. Perpetual --> mindig 25% (gp)
				//newItem.Target_Price__c = newItem.Target_Price__c.round(System.RoundingMode.HALF_EVEN); // 20150213 BÁ: kerekítés
				newItem.Target_Price__c = newItem.Target_Price__c.round(System.RoundingMode.HALF_UP); // 2019.12.02.
				newItem.License_Type__c = 'Perpetual Renewal';
			}
			else if (item.License_Type__c == 'Perpetual Renewal' || item.License_Type__c == 'Annual Renewal' || item.License_Type__c == 'Annual' || item.License_Type__c == 'Subscription' || item.License_Type__c == 'Subscription Renewal') {
				newItem.Target_Price__c = item.TotalPrice * 1.01; // 20150212 BÁ: item.UnitPrice -> item.TotalPrice
				//newItem.Target_Price__c = newItem.Target_Price__c.round(System.RoundingMode.HALF_EVEN); // 20150213 BÁ: kerekítés
				newItem.Target_Price__c = newItem.Target_Price__c.round(System.RoundingMode.HALF_UP); // 2019.12.02.
				if (item.License_Type__c == 'Annual') newItem.License_Type__c = 'Annual Renewal';
				else if (item.License_Type__c == 'Subscription') newItem.License_Type__c = 'Subscription Renewal';
				else newItem.License_Type__c = item.License_Type__c;
			}
			//else if(item.License_Type__c == 'Consulting'){
				//newItem.License_Type__c = 'Consulting';
			//}			
			newItem.Created_By_Form__c = true; // 20150213 BÁ: azért kell, hogy a pricing megtörténjen insertkor
			newItem.Discount = null; // 20150213 BÁ
			newItem.External_ID__c = null;
			//newItem.End_User__c = null; // Nora Lapusnyik kérése 2018. 08. 23.
			newItem.Currency__c = null;
			newItem.Created_By_Form__c = false; // Azért, hogy ne fusson a QuotLineItem trigger újraárazása. 2019.02.11. gp 

			newItems.add(newItem);

			//populateLicenseMap(qliLicenseType, item);
			populateOppCloseDateMap(oppCloseDate, item, oldQuotes);


			// create new OLIs
			OpportunityLineItem newOLI = createNewOLI(item, newOpps);
			newOLIs.add(newOLI);	
		}
		for (QuoteLineItem qli : newItems) {
			if (!qli.PricebookEntry.IsActive) System.debug('pricebook entry inaktív: ' + qli.PricebookEntryId);
		}
		insert newItems;
				
		insert newOLIs;

		quoteCount = newQuotes.size();

		// update opportunities close date
		for(Opportunity opp : newOpps.values()){
			if(!oppCloseDate.containsKey(oldOppIdByNewOppId.get(opp.Id))) continue;
			
			Date closeDate;
			//if(oppCloseDate.containsKey(opp.Id)){
				if(oppCloseDate.get(oldOppIdByNewOppId.get(opp.Id)).containsKey('Perpetual')) closeDate = oppCloseDate.get(oldOppIdByNewOppId.get(opp.Id)).get('Perpetual');
				if(oppCloseDate.get(oldOppIdByNewOppId.get(opp.Id)).containsKey('Annual')) closeDate = oppCloseDate.get(oldOppIdByNewOppId.get(opp.Id)).get('Annual');
			//}
			if(closeDate != null) opp.CloseDate = closeDate.addYears(1);
		}

		update newOpps.values();

		update oldQuotes.values(); //mark Quote.Renewed__c = true
		System.debug('old quotes: ' + oldQuotes.values().size());
		System.debug('new quotes: ' + newQuotes.values().size());
	}
	
	global void finish(Database.BatchableContext context) {}



	//private void populateLicenseMap(Map<Id, Set<String>> qliLicenseType, QuoteLineItem item){
		//Set<String> tmp = qliLicenseType.get(item.Quote.OpportunityId);
		//if(tmp == null){
			//tmp = new Set<String>();
			//qliLicenseType.put(item.Quote.OpportunityId, tmp);
		//}
		//tmp.add(item.License_type__c);
	//}

	private void populateOppCloseDateMap(Map<Id, Map<String, Date>> oppCloseDate, QuoteLineItem item, Map<Id, Quote> oldQuotes){
		Map<String, Date> tmp = oppCloseDate.get(item.Quote.OpportunityId);
		if(tmp == null){
			tmp = new Map<String, Date>();
			oppCloseDate.put(item.Quote.OpportunityId, tmp);
		}

		if(String.isBlank(item.License_type__c) || !oldQuotes.containsKey(item.QuoteId)) return;

		if(item.License_type__c.startsWith('Annual') && (!tmp.containsKey('Annual') || tmp.get('Annual') < oldQuotes.get(item.QuoteId).License_Expiration_Date__c)) tmp.put('Annual', oldQuotes.get(item.QuoteId).License_Expiration_Date__c);
		if(item.License_type__c.startsWith('Perpetual') && (!tmp.containsKey('Perpetual') || tmp.get('Perpetual') < oldQuotes.get(item.QuoteId).License_Support_Expiration_Date__c)) tmp.put('Perpetual', oldQuotes.get(item.QuoteId).License_Support_Expiration_Date__c);
	}

	private OpportunityLineItem createNewOLI(QuoteLineItem item, Map<Id, Opportunity> newOpps){
		OpportunityLineItem newOLI = new OpportunityLineItem(
			//AVA_SFCLOUD__SalesTax_Line__c = item.AVA_SFCLOUD__SalesTax_Line__c,
			//AVA_SFCLOUD__Sales_Tax_Details__c = item.AVA_SFCLOUD__Sales_Tax_Details__c,
			//AVA_SFCLOUD__Sales_Tax_Rate__c = item.AVA_SFCLOUD__Sales_Tax_Rate__c,
			//AVA_SFCLOUD__Tax_Override__c = item.AVA_SFCLOUD__Tax_Override__c,
			//CXN_Discount__c						nincs qli-n
			Description = item.Description,
			IsPluginNF__c = item.IsPluginNF__c,
			//Name = item.PricebookEntry.Product2.Name,
			//OpportunityId,						qli.quote.opp
			//Opportunity_Currency_ISO_Code__c,		formula
			//Opportunity_Price__c,					formula
			Original_Price__c = item.Original_Price__c,
			Package__c = item.Package__c,
			PricebookEntryId = item.PricebookEntryId,
			//Product2Id = item.Product2Id,
			//ProductCode,							formula
			Quantity = item.Quantity,
			//ServiceDate,							nincs qli-n			datetime
			//technical_Oppty_Price_last_updated__c,	nincs qli-n		 date
			TotalPrice = item.TotalPrice,
			//Total_List_Price__c,						formula
			T_Calculate_Prices__c = item.T_Calculate_Prices__c,
			User_Limit__c = item.User_Limit__c
		);
		newOLI.OpportunityId = newOpps.get(item.QuoteId).Id; //opp.Id;
		//if (newOLI.License_Type__c == 'Perpetual') {
		if (item.License_Type__c == 'Perpetual') {
			//newOLI.Target_Price__c = oli.TotalPrice * opp.Renewal_Percentage__c / 100;
			newOLI.Target_Price__c = item.TotalPrice * item.Quote.Opportunity.Renewal_Percentage__c / 100;
			newOLI.Target_Price__c = newOLI.Target_Price__c.round(System.RoundingMode.HALF_UP);
			newOLI.License_Type__c = 'Perpetual Renewal';
		}
		//else if (newOLI.License_Type__c == 'Perpetual Renewal' || newOLI.License_Type__c == 'Annual Renewal' || newOLI.License_Type__c == 'Annual') {
		else if (item.License_Type__c == 'Perpetual Renewal' || item.License_Type__c == 'Annual Renewal' || item.License_Type__c == 'Annual' || item.License_Type__c == 'Subscription' || item.License_Type__c == 'Subscription Renewal') {
			//newOLI.Target_Price__c = oli.TotalPrice * 1.01;
			newOLI.Target_Price__c = item.TotalPrice * 1.01;
			newOLI.Target_Price__c = newOLI.Target_Price__c.round(System.RoundingMode.HALF_UP);
			if (item.License_Type__c == 'Annual') newOLI.License_Type__c = 'Annual Renewal';
			else if (item.License_Type__c == 'Subscription') newOLI.License_Type__c = 'Subscription Renewal';
			else newOLI.License_Type__c = item.License_Type__c;
		}
		//else if(item.License_Type__c == 'Consulting'){
			//newOLI.License_Type__c = 'Consulting';
		//}
		newOLI.Discount = null;
		//newOLI.External_ID__c = null;
		//newOLI.Currency__c = null;
		newOLI.Created_By_Form__c = false;
		newOLI.UnitPrice = null;

		System.debug(JSON.serialize(newOLI));
		return newOLI;
	}

	@AuraEnabled
	public static String runOpportunityRenewalWithOpptyId(String opptyId){
		if(opptyId != null && opptyId != ''){
			CalculateRenewalExecutor CRE = new CalculateRenewalExecutor(opptyId);
			List<Quote> quoteList = [SELECT Id FROM Quote WHERE OpportunityId = :opptyId];
			CRE.execute(null, quoteList);
			return 'OK';
		}else{
			return 'Hibas opportunity Id';
		}
	}
}