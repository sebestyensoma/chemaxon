@isTest
private class testQuoteLineItemEdit 
{
    @isTest(seeAllData=true)
    static void myUnitTest() 
    {
        Account account01 = new Account();
        account01.Name = 'name';
        account01.CurrencyIsoCode = 'EUR';
        insert account01;
        
        Opportunity opportunity01 = new Opportunity();
        opportunity01.Name = 'name';
        opportunity01.AccountId = account01.Id;
        opportunity01.StageName = 'stagename';
        opportunity01.CloseDate = system.today();
        opportunity01.CurrencyIsoCode = 'EUR';
        insert opportunity01;
        
        Quote quote01 = new Quote();
        quote01.Name = 'name';
        quote01.OpportunityId = opportunity01.Id;
        insert quote01;
        
        PricebookEntry CPentry = 
        	[
        		SELECT
        			Id
        		FROM
        			PricebookEntry
        		WHERE
        			Product2Id IN (SELECT Id FROM Product2 WHERE ProductCode = 'D2S') 
        			AND 
        			CurrencyIsoCode='EUR' 
        			AND 
        			IsActive = TRUE 
        			AND 
        			Pricebook2.IsStandard = TRUE 
        		LIMIT 1
        	];
        	
        QuoteLineItem qli01 = new QuoteLineItem();
        qli01.PricebookEntryId = cpentry.Id;
        qli01.QuoteId = quote01.Id;
        qli01.Quantity = 1;
        qli01.UnitPrice = 1;
        insert qli01;
        
        ApexPages.StandardController sc = new ApexPages.standardController(qli01);
	    QuoteLineItemEdit qlie = new QuoteLineItemEdit(sc);
	    
	    qlie.getQuoteLineItem();
	    qlie.save();
    }
}