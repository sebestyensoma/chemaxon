public without sharing class OpportunityRenewalCorrector implements Database.Batchable<sObject> {
    public class OpportunityRenewalException extends Exception{ }
    public OpportunityRenewalCorrector() {

    }

    public Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, OpportunityId, Renewed_From__c FROM Quote WHERE Renewed__c = false AND Renewed_From__c != null' );
    }

    public void execute(Database.BatchableContext context, Quote[] newQuoteList) {
        List<Opportunity> newOpptyList = getNewOpptiesWithQuotes( newQuoteList );
        Set<Id> oldQuoteIdSet = new Set<Id>();
        for(Quote quoteObj : newQuoteList){
            oldQuoteIdSet.add(quoteObj.Renewed_From__c);
        }
        List<Quote> oldQuoteList = [SELECT OpportunityId, License_Expiration_Date__c, License_Support_Expiration_Date__c, QLI_Annual_License__c, QLI_Subscription__c, QLI_Perpetual_License__c FROM Quote WHERE Id IN : oldQuoteIdSet];
        List<QuoteLineItem> quoteLineItemList = [SELECT License_Type__c, QuoteId FROM QuoteLineItem WHERE QuoteId IN : oldQuoteIdSet];
        List<Opportunity> opptiesToUpdate = new List<Opportunity>();

        for(Opportunity newOppty : newOpptyList){
            List<Date> possibleCloseDateList = new List<Date>();

            for(Quote oldQuote : oldQuoteList){
                if(oldQuote.OpportunityId == newOppty.Renewed_From__c){
                    Boolean annualLicenseType = oldQuote.QLI_Annual_License__c + oldQuote.QLI_Subscription__c > 0 ? true : false;
                    Boolean perpetualLicenseType = oldQuote.QLI_Perpetual_License__c > 0 ? true : false;
                    
                    if( ! annualLicenseType && perpetualLicenseType ){
                        if( oldQuote.License_Support_Expiration_Date__c != null ){
                            possibleCloseDateList.add( oldQuote.License_Support_Expiration_Date__c );
                        }else if( oldQuote.License_Expiration_Date__c != null ) {
                            possibleCloseDateList.add( oldQuote.License_Expiration_Date__c );
                        }
                    }else{
                        if( oldQuote.License_Expiration_Date__c != null ){
                            possibleCloseDateList.add( oldQuote.License_Expiration_Date__c );
                        }else if( oldQuote.License_Support_Expiration_Date__c != null ) {
                            possibleCloseDateList.add( oldQuote.License_Support_Expiration_Date__c );
                        }
                    }
                }
            }
            
            Date closeDate = getMaxOpptyCloseDate(possibleCloseDateList);
            if( closeDate == null ){
                if(newOppty.Renewed_From__r.CloseDate != null){
                    closeDate = newOppty.Renewed_From__r.CloseDate.addYears(1);
                }
            }
            if(closeDate != null){
                if(newOppty.CloseDate != closeDate){
                    newOppty.CloseDate = closeDate;
                    opptiesToUpdate.add(newOppty);
                }
            }
            
            
        }

        if( ! opptiesToUpdate.isEmpty() ){
            update opptiesToUpdate;
        }


    }
    public void finish(Database.BatchableContext context) {}

    private static List<Opportunity> getNewOpptiesWithQuotes(List<Quote> newQuotes){
        Set<Id> opptyIdSet = new Set<Id>();
        for(Quote quoteObject : newQuotes){
            opptyIdSet.add(quoteObject.OpportunityId);
        }
        return [SELECT Id, CloseDate, Renewed_From__c, Renewed_From__r.CloseDate FROM Opportunity WHERE Id IN : opptyIdSet];
    }

    private static Date getMaxOpptyCloseDate(List<Date> possibleDates){
        if(possibleDates == null || possibleDates.isEmpty()) return null;
        Date maxDate = possibleDates.get(0);
        if(possibleDates.size() > 1){
            for(Integer i = 1; i < possibleDates.size(); i++){
                if(possibleDates.get(i) > maxDate){
                    maxDate = possibleDates.get(i);
                }
            }
        }
        return maxDate;
    }
}
