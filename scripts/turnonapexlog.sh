echo ----- Getting apex log -----
ANSWER=`sfdx force:data:soql:query --query "SELECT id FROM DebugLevel WHERE DeveloperName='APEXLOG'" --usetoolingapi --json --loglevel fatal`
#ANSWER='{ "status": 0, "result": { "size": 0, "totalSize": 0, "done": true, "queryLocator": null, "entityTypeName": null, "records": [] } }'
#ANSWER='{ "status": 0, "result": { "size": 1, "totalSize": 1, "done": true, "queryLocator": null, "entityTypeName": "DebugLevel", "records": [ { "attributes": { "type": "DebugLevel", "url": "/services/data/v48.0/tooling/sobjects/DebugLevel/7dl1X0000000QQLQA2" }, "Id": "7dl1X0000000QQLQA2" } ] } }'
echo $ANSWER
SIZE=`jq -n "$ANSWER" | jq -r '.result.size'`
echo $SIZE
 
if [ "$SIZE" -eq "0" ]; then
   echo ----- Creating apex log -----
   sfdx force:data:record:create --usetoolingapi -s DebugLevel -v "DEVELOPERNAME=APEXLOG MASTERLABEL=APEXLOG APEXCODE=FINEST APEXPROFILING=NONE CALLOUT=NONE DATABASE=NONE NBA=NONE SYSTEM=NONE VALIDATION=NONE VISUALFORCE=FINER WAVE=NONE WORKFLOW=NONE"
   ANSWER=`sfdx force:data:soql:query --query "SELECT id FROM DebugLevel WHERE DeveloperName='APEXLOG'" --usetoolingapi --json --loglevel fatal`
fi

DLID=`jq -n "$ANSWER" | jq -r '.result.records[0].Id'`
echo $DLID

echo ----- Getting user id -----
USER=`sfdx force:user:display --json`
echo $USER
USERID=`jq -n "$USER" | jq -r '.result.id'`
echo $USERID

echo ----- Getting TraceFlag -----
TRACEFLAG=`sfdx force:data:soql:query --query "SELECT Id FROM TraceFlag WHERE TracedEntityId='$USERID'" --usetoolingapi --json --loglevel fatal`
TRACEFLAGSIZE=`jq -n "$TRACEFLAG" | jq -r '.result.size'`
echo $TRACEFLAG
echo $TRACEFLAGSIZE

DATE=`date --date="1 day" +"%Y-%m-%dT%H:%M:00%z"`
echo $DATE

if [ "$TRACEFLAGSIZE" -eq "1" ]; then
    TRACEFLAGID=`jq -n "$TRACEFLAG" | jq -r '.result.records[0].Id'`
    sfdx force:data:record:delete -i $TRACEFLAGID --usetoolingapi -s TraceFlag
fi

sfdx force:data:record:create --usetoolingapi -s TraceFlag -v "StartDate=null ExpirationDate='$DATE' LOGTYPE=DEVELOPER_LOG DebugLevelId='$DLID' TracedEntityId='$USERID'" --json

