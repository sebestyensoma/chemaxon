public with sharing class QuoteLineItemEdit 
{
	private QuoteLineItem quoteLineItem;
    private id quoteLineItemId;
    private id quoteId;
    
    private Map<id, QuoteLineItem> quoteLineItemIdMap = new Map<id, QuoteLineItem>();
    
    public QuoteLineItemEdit(ApexPages.standardcontroller stdController)
    {
        quoteLineItem = (QuoteLineItem)stdController.getRecord();
        
        quoteLineItemId = quoteLineItem.Id;
        
        quoteId = [ SELECT QuoteId FROM QuoteLineItem WHERE Id = :quoteLineItemId ].QuoteId;
        
        populateQuoteLineItemIdMap(quoteId);
    }
    
    private void populateQuoteLineItemIdMap(id quoteId)
    {
    	List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
    	
    	quoteLineItemList =
    		[
    			SELECT
    				Id,
    				QuoteId,
    				Discount,
    				UnitPrice,
    				Quote.Name,
    				TotalPrice,
    				Description,
    				PricebookEntry.Product2.Name,
    				PricebookEntry.Product2.ProductCode,
    				Currency__c,
    				Target_Price__c,
    				Custom_Quantity__c,
					Most_probable__c,
					Product_family__c,
					Consulting_team__c
    			FROM
    				QuoteLineItem
    			WHERE
    				QuoteId = :quoteId
    		];
    		
    	for(QuoteLineItem currentQli : quoteLineItemList)
    	{
    		quoteLineItemIdMap.put(currentQli.Id, currentQli);
    	}
    }
    
    public pagereference save()
    {
    	quoteLineItemIdMap.put(quoteLineItem.Id, quoteLineItem);
    	
    	update quoteLineItemIdMap.values();
    	
    	pagereference pageref = new pagereference('/' + quoteId);
    	pageref.setredirect(true);
    	
    	return pageref;
    }
    
    public QuoteLineItem getQuoteLineItem()
    {
    	return quoteLineItem;
    }
}