public with sharing class DealTypeReportController extends AbstractReportController {

    // PROPERTIES
    
    private static final String[] dealTypeLabels = new String[] {
        'Annual', 'Annual Renewal', 'Perpetual', 'Perpetual Renewal', 'Consulting', 'Total'
    };
    
    // REPORTING
    
    protected override List<String> createColumnLabels() {
        return dealTypeLabels;
    }
    
    protected override void doReport() {
        // Fetch raw data
        List<QuoteLineItem> rawList = [
            SELECT License_Type__c, Quote.Invoice_Date__c,
                Total_in_EUR__c, Total_in_USD__c, Total_in_GBP__c, Total_in_HUF__c, Total_in_JPY__c, Total_in_CHF__c, Total_in_SGD__c
            FROM QuoteLineItem
            WHERE Quote.Invoice_Date__c != null
            ORDER BY Quote.Invoice_Date__c ASC
        ];
        
        // Process rows
        String fieldName = 'Total_in_' + currencyName + '__c';
        for (QuoteLineItem item : rawList) {
            Decimal d = (Decimal)item.get(fieldName);
            if (d == null) continue;
            
            String key = extractKey(item);
            Map<String, Decimal> tableRow = getOrCreateRow(key);
            
            // Always add to grand total
            Decimal rowTotal = tableRow.get('Total');
            rowTotal += d;
            tableRow.put('Total', rowTotal);
            
            // Null/Other licenses do not get their own column
            String licenseType = item.License_Type__c;
            if (licenseType == null || licenseType == 'Other' || !tableRow.containsKey(licenseType)) continue;
            
            rowTotal = tableRow.get(licenseType);
            rowTotal += d;
            tableRow.put(licenseType, rowTotal);
        }
    }
    
    /** Render the HTML code for the table body */
    public override String getTableHtml() {
        XmlStreamWriter tableHtml = new XmlStreamWriter();
        tableHtml.writeStartElement(null, 'tbody', null);
        
        for (String key : tableKeys) {
            Map<String, Decimal> tableRow = tableData.get(key);
            
            tableHtml.writeStartElement(null, 'tr', null);
            tableHtml.writeStartElement(null, 'td', null);
            tableHtml.writeAttribute(null, null, 'class', 'key');
            tableHtml.writeCharacters(key);         
            tableHtml.writeEndElement();
                
            for (String label : columnLabels) {
                tableHtml.writeStartElement(null, 'td', null);
            tableHtml.writeAttribute(null, null, 'class', 'num');
                tableHtml.writeCharacters(formatInteger(tableRow.get(label)));          
                tableHtml.writeEndElement();
            }   
            tableHtml.writeEndElement();
        }
        
        tableHtml.writeEndElement();
        return tableHtml.getXmlString();
    }
}