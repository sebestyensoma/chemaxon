/*
 * @author      Attention CRM Consulting <chemaxon@attentioncrm.hu>
 * @modified    2016-06-14 (ŐK) futureLeadReassign: update contact and lead owner
 */
global class LeadHandlerFuture
{
	@future
	public static void createWebsiteDemoRequestRecords(Set<id> leadIdSet)
	{
		/*
			- ha van forum id, akkor az alapján keresünk meglévő nyitott leadet. ha nem találunk, akkor
			- ha van forum id, akkor az alapján keresünk meglévő contactot. ha nem találunk, akkor
			- email alapján keresünk meglévő nyitott leadet. ha nem találunk, akkor
			- email alapján keresünk meglévő contactot

			ha bármelyiket megtaláljuk, akkor ahhoz hozzuk létre a demo request rekordot. 
			ha egyiket sem találjuk, akkor új leadet kreálunk és ahhoz kapcsoljuk a demo request rekordot
			
			konvertáláskor át kell vinni a contactra
		*/
		
		List<Contact> existingOpenContactList;
		List<Lead> newLeadList;
		List<Lead> existingOpenLeadList;
		List<Lead> leadIdToDeleteList = new List<Lead>();
		List<Demo_Request__c> demoRequestList = new List<Demo_Request__c>();
		List<string> leadStatusList = new List<string>();
		Set<string> forumIdSet = new Set<string>();
		Set<string> emailSet = new Set<string>();
		Map<string, Lead> forumIdLeadMap = new Map<string, Lead>();
		Map<string, Lead> emailLeadMap = new Map<string, Lead>();
		Map<string, Contact> forumIdContactMap = new Map<string, Contact>();
		Map<string, Contact> emailContactMap = new Map<string, Contact>();
		
		leadStatusList.add('New');
		leadStatusList.add('Open');
		leadStatusList.add('Emailed');
		leadStatusList.add('Called');
		
		newLeadList =
			[
				SELECT Id, Email, Forum_Id__c, Demo_Request_Product__c, Demo_Request_Data__c
				FROM Lead
				WHERE
					Id IN :leadIdSet
			];
			
		for(Lead currentL : newLeadList)
		{
			forumIdSet.add(currentL.Forum_Id__c);
			emailSet.add(currentL.Email);
		}
		
		existingOpenLeadList = 
			[
				SELECT Id, Email, Forum_Id__c
				FROM Lead
				WHERE
					(
						(Forum_Id__c != NULL AND Forum_Id__c IN :forumIdSet)
						OR
						(Email != NULL AND Email IN :emailSet)
					)
					AND
					Status IN :leadStatusList
					AND
					Id NOT IN :leadIdSet
			];	
			
		for(Lead currentL : existingOpenLeadList)
		{
			forumIdLeadMap.put(currentL.Forum_Id__c, currentL);
			emailLeadMap.put(currentL.Email, currentL);
		}
		
		existingOpenContactList = 
			[
				SELECT Id, Email, Forum_Id__c
				FROM Contact
				WHERE
					(Email != NULL AND Email IN :emailSet)
					OR
					(Forum_Id__c != NULL AND Forum_Id__c IN :forumIdSet)
			];
		
		for(Contact currentC : existingOpenContactList)
		{
			forumIdContactMap.put(currentC.Forum_Id__c, currentC);
			emailContactMap.put(currentC.Email, currentC);
		}
		
		
		for(Lead currentL : newLeadList)
		{
			system.debug('##### newLeadList: ' + newLeadList);
			
			if(forumIdLeadMap.containsKey(currentL.Forum_Id__c))
			{
				system.debug('##### ');
				system.debug('##### ' + forumIdLeadMap.get(currentL.Forum_Id__c).Id);
				
				Demo_Request__c dr = new Demo_Request__c();
				dr.Demo_Request_Product__c = currentL.Demo_Request_Product__c;
				dr.Demo_Request_Data__c = currentL.Demo_Request_Data__c;
				dr.Lead__c = forumIdLeadMap.get(currentL.Forum_Id__c).Id;
				demoRequestList.add(dr);
				
				leadIdToDeleteList.add(currentL);
				
				continue;
			}
			else if(forumIdContactMap.containsKey(currentL.Forum_Id__c))
			{
				system.debug('##### ');
				
				Demo_Request__c dr = new Demo_Request__c();
				dr.Demo_Request_Product__c = currentL.Demo_Request_Product__c;
				dr.Demo_Request_Data__c = currentL.Demo_Request_Data__c;
				dr.Contact__c = forumIdContactMap.get(currentL.Forum_Id__c).Id;
				demoRequestList.add(dr);
				
				leadIdToDeleteList.add(currentL);
				
				continue;
			}
			else if(emailLeadMap.containsKey(currentL.Email))
			{
				system.debug('##### ');
				
				Demo_Request__c dr = new Demo_Request__c();
				dr.Demo_Request_Product__c = currentL.Demo_Request_Product__c;
				dr.Demo_Request_Data__c = currentL.Demo_Request_Data__c;
				dr.Lead__c = emailLeadMap.get(currentL.Email).Id;
				demoRequestList.add(dr);
				
				leadIdToDeleteList.add(currentL);
				
				continue;
			}
			else if(emailContactMap.containsKey(currentL.Email))
			{
				system.debug('##### ');
				
				Demo_Request__c dr = new Demo_Request__c();
				dr.Demo_Request_Product__c = currentL.Demo_Request_Product__c;
				dr.Demo_Request_Data__c = currentL.Demo_Request_Data__c;
				dr.Contact__c = emailContactMap.get(currentL.Email).Id;
				demoRequestList.add(dr);
				
				leadIdToDeleteList.add(currentL);
				
				continue;
			}
			else
			{
				system.debug('##### ');
				
				Demo_Request__c dr = new Demo_Request__c();
				dr.Demo_Request_Product__c = currentL.Demo_Request_Product__c;
				dr.Demo_Request_Data__c = currentL.Demo_Request_Data__c;
				dr.Lead__c = currentL.Id;
				demoRequestList.add(dr);
			}
		}
		
		if(demoRequestList.size() != 0)
		{
			insert demoRequestList;
			
			List<id> demoRequestIdList = new List<id>();
			
			for(Demo_Request__c currentDR : demoRequestList)
			{
				demoRequestIdList.add(currentDR.Id);
			}
			
			/*
			if(demoRequestIdList.size() != 0)
			{
				LeadHandlerFuture lhf = new LeadHandlerFuture();
				
				lhf.sendEmail(demoRequestIdList);
			}
			*/
			
			string sfdcBaseUrl = url.getSalesforceBaseUrl().toExternalForm();
			List<Demo_Request__c> demoRequestList2;
			List<Messaging.SingleEmailMessage> emailMessageList = new list<Messaging.SingleEmailMessage>();
			
			demoRequestList2 =
				[
					SELECT	Id, Contact__c, Contact__r.OwnerId, Contact__r.Owner.Email, Contact__r.Owner.FirstName,
							Lead__c, Lead__r.LastName, Lead__r.FirstName, Lead__r.Company, Lead__r.OwnerId,
							Lead__r.Owner.Email, Lead__r.Owner.FirstName, Demo_Request_Product__c, Demo_Request_Data__c
					FROM	Demo_Request__c
					WHERE	Id IN :demoRequestList
				];
				
			for(Demo_Request__c currentDR : demoRequestList2)
			{
				if(currentDR.Lead__c != NULL)
				{
					String [] addr = new String[] {currentDR.Lead__r.Owner.Email};
					//String [] addr = new String[] {'adam.bobor@attentioncrm.hu'};
					string emailBody;
					
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					
					emailBody = 'Dear ' + currentDR.Lead__r.Owner.FirstName + ', ' + '<br/><br/>';
					emailBody += currentDR.Lead__r.FirstName + ' ' + currentDR.Lead__r.LastName + ' (' + currentDR.Lead__r.Company + ')';
					emailBody += ' has requested a demo for ' + currentDR.Demo_Request_Product__c + '. Please contact the user for further information' + '<br/><br/>';
					emailBody += 'Lead link: ' + sfdcBaseUrl + '/' + currentDR.Lead__c + '<br/>';
					emailBody += 'Demo Request link: ' + sfdcBaseUrl + '/' + currentDR.Id; 
									
					mail.setToAddresses(addr);
			        mail.setSubject('New Demo Request');
			        mail.setPlainTextBody(emailBody);
			        mail.setHtmlBody(emailBody);
			
			        emailMessageList.add(mail);
				}
			}
			
			if(emailMessageList.size() != 0 && !Test.isRunningTest())
			{
				Messaging.sendEmail(emailMessageList);
			}
		}
		
		if(leadIdToDeleteList.size() != 0)
		{
			system.debug(leadIdToDeleteList);
			
			delete leadIdToDeleteList;
		}
	}
	
	private void sendEmail(List<id> demoRequestIdList)
	{
		string sfdcBaseUrl = url.getSalesforceBaseUrl().toExternalForm();
		List<Demo_Request__c> demoRequestList;
		List<Messaging.SingleEmailMessage> emailMessageList = new list<Messaging.SingleEmailMessage>();
		
		demoRequestList =
			[
				SELECT	Id, Contact__c, Contact__r.OwnerId, Contact__r.Owner.Email, Contact__r.Owner.FirstName, Contact__r.Account.Name,
						Lead__c, Lead__r.LastName, Lead__r.FirstName, Lead__r.Company, Lead__r.OwnerId,
						Lead__r.Owner.Email, Lead__r.Owner.FirstName, Demo_Request_Product__c, Demo_Request_Data__c
				FROM	Demo_Request__c
				WHERE	Id IN :demoRequestList
			];
			
		for(Demo_Request__c currentDR : demoRequestList)
		{
			if(currentDR.Lead__c != NULL)
			{
				String [] addr = new String[] {currentDR.Lead__r.Owner.Email};
				string emailBody;
				
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				
				emailBody = 'Dear ' + currentDR.Lead__r.Owner.FirstName + ', ' + '<br/><br/>';
				emailBody += currentDR.Lead__r.FirstName + ' ' + currentDR.Lead__r.LastName + '(' + currentDR.Lead__r.Company + ')';
				emailBody += ' has requested a demo for ' + currentDR.Demo_Request_Product__c + '. Please contact the user for further information' + '<br/><br/>';
				emailBody += 'Lead link: ' + sfdcBaseUrl + '/' + currentDR.Lead__c + '<br/>';
				emailBody += 'Demo Request link: ' + sfdcBaseUrl + '/' + currentDR.Id; 
								
				mail.setToAddresses(addr);
		        mail.setSubject('New Demo Request');
		        mail.setPlainTextBody(emailBody);
		        mail.setHtmlBody(emailBody);
		
		        emailMessageList.add(mail);
			}
			else if(currentDR.Contact__c != NULL)
			{
				String [] addr = new String[] {currentDR.Contact__r.Owner.Email};
				string emailBody;
				
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				
				emailBody = 'Dear ' + currentDR.Contact__r.Owner.FirstName + ', ' + '<br/><br/>';
				emailBody += currentDR.Contact__r.FirstName + ' ' + currentDR.Contact__r.LastName + '(' + currentDR.Contact__r.Account.Name + ')';
				emailBody += ' has requested a demo for ' + currentDR.Demo_Request_Product__c + '. Please contact the user for further information' + '<br/><br/>';
				emailBody += 'Lead link: ' + sfdcBaseUrl + '/' + currentDR.Contact__c + '<br/>';
				emailBody += 'Demo Request link: ' + sfdcBaseUrl + '/' + currentDR.Id; 
								
				mail.setToAddresses(addr);
		        mail.setSubject('New Demo Request');
		        mail.setPlainTextBody(emailBody);
		        mail.setHtmlBody(emailBody);
		
		        emailMessageList.add(mail);
			}
		}
		
		if(emailMessageList.size() != 0 && !Test.isRunningTest())
		{
			Messaging.sendEmail(emailMessageList);
		}
	}
	@future
	public static void futureLeadReassign(Id lIdReassign) {
		
		Database.DMLOptions dmo = new Database.DMLOptions();
		dmo.assignmentRuleHeader.useDefaultRule  = true;
		
		Lead lead = [select id,ConvertedOpportunityId,ownerid,Country,email,lastName,State,company from Lead where id=:lIdReassign]; 
		Lead leadClone  = new Lead( Country=lead.Country,State=lead.State,Email=lead.Email,lastName=lead.LastName,company=lead.Company, T_Dummy__c=true);
		
		system.debug('before'+lead.ownerId);
		leadClone.setOptions(dmo);
		
		insert leadClone;
		
		leadClone=[select id,ownerID from Lead where id=:leadClone.Id];
		
		Opportunity convertedOpp = [select id,Name,ownerid,accountid,Win_Loss_Contact__c from Opportunity where id=:lead.ConvertedOpportunityId];
		convertedOpp.ownerid=leadClone.ownerId;
		
		update convertedOpp;
		
		if(convertedOpp.Win_Loss_Contact__c != null){
			Contact relatedcontact = [SELECT Id FROM Contact WHERE Id = :convertedOpp.Win_Loss_Contact__c];
		 	relatedcontact.ownerId = leadClone.ownerId;
		 	update relatedcontact;
		}
		
	 	if(convertedOpp.AccountID != null){
		 	Account relatedaccount = [SELECT Id FROM Account WHERE Id = :convertedOpp.AccountID];
		 	update relatedaccount;
		 	relatedaccount.ownerId = leadClone.ownerId;
	 	}
	 	
		delete leadClone;
		
		system.debug('after'+leadClone.ownerID);
		
		List<Lead> toErase = [Select ID From Lead Where IsDeleted = true and id = :leadClone.id limit 1 ALL ROWS];
        Database.emptyRecycleBin(toErase);
	}
}