public with sharing class extensionOLIMultiLayout {
	public integer lineItemCount{get; set;}
	public Map<Id,PricebookEntry> allPBEs{get;set;}
	public List<PricebookEntry> listPBEs{get;set;}
	public Map<Id,Boolean> isPBEChecked{get;set;}
	public Opportunity standardOpportunity{get;set;}
	public Map<Id,Integer> mapPBEId2UserLimits{get;set;}
	
	public Map<Id,OpportunityLineItem> mapPBEId2LicenseTypes{get;set;}
	
	public Integer userLimit{get;set;}
	
	public OpportunityLineItem proxyOLI = new OpportunityLineItem();
	
	public string selectedPackage{get; set;}
	
	public OpportunityLineItem getProxyOLI()
	{
		return proxyOLI;
	}
	
	public String outJSONtfListString{
										get{
											Set<Id> setOLI = new Set<Id>();
											for(Id loopId:isPBEChecked.keySet()){
												if(isPBEChecked.get(loopId)==true){
													setOLI.add(loopId);
												}
											}
											return JSON.serialize(setOLI);
										}
									}
	public String inJSONtfListString{get;set;}
	
	public extensionOLIMultiLayout(ApexPages.StandardController stdController){
		standardOpportunity = (Opportunity)stdController.getRecord();
		
		lineItemCount = [ SELECT COUNT() FROM OpportunityLineItem WHERE OpportunityID = :standardOpportunity.Id ];
		
		isPBEChecked = new Map<Id,Boolean>();
		inJSONtfListString = ApexPages.currentPage().getParameters().get('tf');
		Set<Id> checkedIds = new Set<Id>();
		if(inJSONtfListString!=null){
			checkedIds = (Set<id>)JSON.deserialize(inJSONtfListString, Set<Id>.class);
			mapPBEId2UserLimits = new Map<Id,Integer>();
			mapPBEId2LicenseTypes = new Map<Id,OpportunityLineItem>();
		}
		
		listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode, Product2.Package_Type__c, UnitPrice FROM PricebookEntry WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Opportunity WHERE Id = :standardOpportunity.Id ) AND IsActive=true AND CurrencyIsoCode = :standardOpportunity.CurrencyIsoCode ORDER BY Product2.Name];
		allPBEs = new Map<Id,PricebookEntry>(listPBEs);
		for(Id loopId : allPBEs.keyset()){
			if(inJSONtfListString!=null){
				mapPBEId2UserLimits.put(loopId,1);
				mapPBEId2LicenseTypes.put(loopId,new OpportunityLineItem(License_type__c='Annual'));
			}
			if(checkedIds!=null && checkedIds.contains(loopId)){
				isPBEChecked.put(loopId,true);
			}
			else{
				isPBEChecked.put(loopId,false);
			}
		}
	}
	
	public List<selectOption> getPackageName() 
    {
        List<selectOption> options = new List<selectOption>(); // lista, ami a picklist értékeit tárolja
       
        options.add(new selectOption('--None--','--None--'));
        options.add(new selectOption('All plugins','All plugins'));
        options.add(new selectOption('All products','All products'));
        options.add(new selectOption('Startup','Startup'));
        
        return options; // visszatérünk a picklistértékekkel
    }
    
    public void packageSelection()
    {
    	system.debug(selectedPackage);
    	
    	for(id id : isPBEChecked.keyset())
    	{
    		boolean tempBoolean;
    		string tempPackageType;
    		
    		tempPackageType = allPBEs.get(id).Product2.Package_type__c;
    		
    		if(tempPackageType != NULL && tempPackageType.contains(selectedPackage))
    		{
    			tempBoolean = true;
    			isPBEChecked.put(id, tempBoolean);
    		}
    		else
    		{
    			tempBoolean = false;
    			isPBEChecked.put(id, tempBoolean);
    		}
    	}
    	
    	system.debug(isPBEChecked);
    }
	
	public Pagereference saveForAll(){
		List<OpportunityLineItem> listOLI = new List<OpportunityLineItem>();
		
		if(inJSONtfListString == NULL)
		{
			// && (userLimit == NULL || userLimit == 0 || )
			if((userLimit == NULL || userLimit == 0) && proxyOLI.License_type__c == NULL)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number and the license type!'));
			
				return null;
			}
			else if(userLimit == NULL || userLimit == 0)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
			
				return null;
			}
			else if(proxyOLI.License_type__c == NULL)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the license type!'));
			
				return null;
			}
		}
		
		for(Id loopId:isPBEChecked.keySet()){
			if(isPBEChecked.get(loopId)==true){
				if(mapPBEId2UserLimits==null){
					
					system.debug(allPBEs);
					system.debug(loopId);
					
					listOLI.add(new OpportunityLineItem(OpportunityId=standardOpportunity.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,Created_By_Form__c=true,User_Limit__c=userLimit,License_type__c=proxyOLI.License_type__c));
				}
				else
				{
					if(mapPBEId2UserLimits.get(loopId) == NULL || mapPBEId2LicenseTypes.get(loopId).License_type__c == NULL)
					{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number and/or the license type!'));
						return null;
					}
					else
					{
						listOLI.add(new OpportunityLineItem(OpportunityId=standardOpportunity.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,Created_By_Form__c=true,User_Limit__c=mapPBEId2UserLimits.get(loopId),License_Type__c=mapPBEId2LicenseTypes.get(loopId).License_type__c));
					}
				}
			}
		}
		if(listOLI==null || listOLI.size()==0){
			return null;
		}
		else{
			insert listOLI;
			return new Pagereference('/'+standardOpportunity.Id);
		}
	}
}

/*
public with sharing class extensionOLIMultiLayout {
	public Map<Id,PricebookEntry> allPBEs{get;set;}
	public List<PricebookEntry> listPBEs{get;set;}
	public Map<Id,Boolean> isPBEChecked{get;set;}
	public Opportunity standardOpportunity{get;set;}
	public Map<Id,Integer> mapPBEId2UserLimits{get;set;}
	public Integer userLimit{get;set;}
	public String outJSONtfListString{
										get{
											Set<Id> setOLI = new Set<Id>();
											for(Id loopId:isPBEChecked.keySet()){
												if(isPBEChecked.get(loopId)==true){
													setOLI.add(loopId);
												}
											}
											return JSON.serialize(setOLI);
										}
									}
	public String inJSONtfListString{get;set;}
	
	public extensionOLIMultiLayout(ApexPages.StandardController stdController){
		standardOpportunity = (Opportunity)stdController.getRecord();
		isPBEChecked = new Map<Id,Boolean>();
		inJSONtfListString = ApexPages.currentPage().getParameters().get('tf');
		Set<Id> checkedIds = new Set<Id>();
		if(inJSONtfListString!=null){
			checkedIds = (Set<id>)JSON.deserialize(inJSONtfListString, Set<Id>.class);
			mapPBEId2UserLimits = new Map<Id,Integer>();
		}
		
		listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode,UnitPrice FROM PricebookEntry WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Opportunity WHERE Id = :standardOpportunity.Id ) AND IsActive=true ORDER BY Product2.Name];
		allPBEs = new Map<Id,PricebookEntry>(listPBEs);
		for(Id loopId : allPBEs.keyset()){
			if(inJSONtfListString!=null){
				mapPBEId2UserLimits.put(loopId,1);
			}
			if(checkedIds!=null && checkedIds.contains(loopId)){
				isPBEChecked.put(loopId,true);
			}
			else{
				isPBEChecked.put(loopId,false);
			}
		}
		
	}
	
	public Pagereference saveForAll(){
		List<OpportunityLineItem> listOLI = new List<OpportunityLineItem>();
		
		if(inJSONtfListString == NULL && userLimit == NULL)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
			
			return null;
		}
		
		for(Id loopId:isPBEChecked.keySet()){
			if(isPBEChecked.get(loopId)==true){
				if(mapPBEId2UserLimits==null){
					
					system.debug(allPBEs);
					system.debug(loopId);
					
					listOLI.add(new OpportunityLineItem(OpportunityId=standardOpportunity.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,User_Limit__c=userLimit));
				}
				else
				{
					if(mapPBEId2UserLimits.get(loopId) == NULL)
					{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
						return null;
					}
					else
					{
						listOLI.add(new OpportunityLineItem(OpportunityId=standardOpportunity.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,User_Limit__c=mapPBEId2UserLimits.get(loopId)));
					}
				}
			}
		}
		if(listOLI==null || listOLI.size()==0){
			return null;
		}
		else{
			insert listOLI;
			return new Pagereference('/'+standardOpportunity.Id);
		}
		
	}
	
}
*/