/** Naponta beállítja az accounton a LastVisitDate-et
 * @author      Attention CRM <attention@attentioncrm.hu>
 * @version     1.0
 * @created     2016-03-03
 */

global class DailyLastVisitDate implements Schedulable, Database.Batchable<sObject>{
    
    public void execute(SchedulableContext SC){
        Database.executeBatch(new DailyLastVisitDate(),200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(
            [select id,Last_Visit_Date__c from Account  
            ]
        );
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        Map<id,Event> eventMap;
        String errorString='';
        Map<id,Account> accMap = new Map<id,Account>();
        List<Account> accsToUpdate = new List<Account>();
        
        List<Account> accList = (List<Account>)scope;
        String account_prefix = Schema.SObjectType.Account.getKeyPrefix();
        
        for(Account acc : accList){
            accMap.put(acc.id,acc); 
        }
        
        
        
        eventMap = new Map<Id, Event>([
                select id,ActivityDate,whatId,WhoId from Event where ActivityDate<=TODAY and subject='Meeting' and whatId!=null
            ]);
        
        if(eventMap!=null && !eventMap.isEmpty()){
            system.debug('tdbg'+eventMap);    
            for(Event event : eventMap.values()){
                //system.debug('tdbg'+String.valueof(event.whatId).startsWith(account_prefix)+' '+account_prefix+' '+'accMap.get(event.whatId).Last_Visit_Date__c'+accMap.get(event.whatId).Last_Visit_Date__c+'<='+event.ActivityDate); 
                if((String.valueof(event.whatId)).startsWith(account_prefix) && accMap.containsKey(event.whatId) && (accMap.get(event.whatId).Last_Visit_Date__c==null || accMap.get(event.whatId).Last_Visit_Date__c<=event.ActivityDate)){
                    accMap.get(event.whatId).Last_Visit_Date__c=date.Newinstance(event.ActivityDate.year(),event.ActivityDate.month(),event.ActivityDate.day());
                    accsToUpdate.add(accMap.get(event.whatId));              
                }           
            }
            
            if(!accsToUpdate.isEmpty()){
                update accsToUpdate;
            }
        }
        
    }
    global void finish(Database.BatchableContext ctx){
        AsyncApexJob oAsyncApexJob = [
        Select a.TotalJobItems, a.Status, a.ParentJobId, a.NumberOfErrors,
             a.MethodName, a.LastProcessedOffset, a.LastProcessed, a.JobType,
             a.JobItemsProcessed, a.Id, a.ExtendedStatus, a.CreatedDate,
             a.CreatedById, a.CompletedDate, a.ApexClassId
        From AsyncApexJob a
        WHERE Id =: ctx.getJobId()
        ];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] sToAddress = new String[]{'david.turi@attentioncrm.hu'};
        mail.setToAddresses(sToAddress);
        mail.setSenderDisplayName('CHEMAXON');
        mail.setSubject('Batch Job Summary: DailyLastVisitDate');
        String body=
        '<span style="color:' + ((oAsyncApexJob.NumberOfErrors == 0) ? ('green') : ('red')) + '">' +
            '<br/>Status: ' + oAsyncApexJob.Status +
            '<br/>NumberOfErrors: ' + oAsyncApexJob.NumberOfErrors +
            '<br/>JobItemsProcessed: ' + oAsyncApexJob.JobItemsProcessed +
            '<br/>TotalJobItems: ' + oAsyncApexJob.TotalJobItems +
            '</span>';
        
        
        mail.setPlainTextBody(body);
        
        
        
        if(oAsyncApexJob.NumberOfErrors > 0){
            mail.setPlainTextBody(
            'Have ' + oAsyncApexJob.NumberOfErrors + 
            ' error(s). \n\n AsyncApexJob: \n' + 
            JSON.serializePretty(oAsyncApexJob));
        }
        //Massmail permission needed!
        Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail});
    }

}