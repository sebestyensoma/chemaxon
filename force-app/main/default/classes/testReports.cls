@isTest(seeAllData=true)
private class testReports {

    static testMethod void newMonthlyReports() {
        PageReference page = new PageReference('/apex/monthlyreport');
        Test.setCurrentPage(page);
        
        MonthlyReport ctrl = new MonthlyReport();
        ctrl.init();
        
        ctrl.mode = 'all';
        ctrl.attemptReport();
        
        ctrl.mode = 'new';
        ctrl.attemptReport();
        
        ctrl.mode = 'renewal';
        ctrl.attemptReport();
    }

    static testMethod void monthlyReportUSA() {
        PageReference page = new PageReference('/apex/monthlyreportUSA');
        Test.setCurrentPage(page);
        
        MonthlyReportUSA ctrl = new MonthlyReportUSA();
        ctrl.init();
    }
    
    static testMethod void monthlyReports() {
        PageReference page = new PageReference('/apex/monthly_report_all');
        page.getParameters().put('currency', 'USD');
        Test.setCurrentPage(page);
        
        ReportController ctrl = new ReportController();
        ctrl.reportAll();
        
        ctrl = new ReportController();
        ctrl.reportAP();
//Test.startTest();        
        //ctrl = new ReportController();
        //ctrl.reportAPRenewal();
    }
    
	static testMethod void monthlyReports2() {
        PageReference page = new PageReference('/apex/monthly_report_all');
        page.getParameters().put('currency', 'USD');
        Test.setCurrentPage(page);
        
        ReportController ctrl = new ReportController();
        ctrl.reportAPRenewal();
    }

    static testMethod void dealTypeReport() {
        AbstractReportController ctrl;
        
        ctrl = new DealTypeReportController();
        ctrl.getCurrencyNames();
        ctrl.getPeriods();
        ctrl.currencyName = 'USD';
        ctrl.period = 'y';
        ctrl.runReport();
        ctrl.getTableHtml();
    }
    
    static testMethod void settledReport() {
        AbstractReportController ctrl;
        
        ctrl = new SettledReportController();
        ctrl.currencyName = 'USD';
        ctrl.period = 'm';
        ctrl.runReport();
        ctrl.getTableHtml();
    }
    
    static testMethod void productReport() {
        AbstractReportController ctrl;
        
        ctrl = new ProductReportController();
        ctrl.currencyName = 'USD';
        ctrl.period = 'q';
        ctrl.runReport();
        ctrl.getTableHtml();
    }
        
    static testMethod void countryReport() {
        AbstractReportController ctrl;
        
        ctrl = new CountryReportController();
        ctrl.currencyName = 'USD';
        ctrl.period = 'q';
        ctrl.runReport();
        ctrl.getTableHtml();
    }
    
    static testMethod void purchaseReport() {
        Map<Id, String> acctmap = PurchaseReportController.lookupAccountName('active');
        List<Id> accid = new List<Id>(acctmap.keySet());
        
        PurchaseReportController ctrl = new PurchaseReportController();
        ctrl.accounts = String.join(accid, ',');
        ctrl.currencyName = 'USD';
        ctrl.runReport();
        ctrl.getTableHtml();
    }
    
}