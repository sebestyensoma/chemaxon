public with sharing class ContractDateExpireAlert {


    /*
    *   This code has to run exactly once per day at a specified time
    */
    
    public void sendEmails() {
        
        Date dateFrom = Date.today();
        Date dateTo = Date.today().addMonths(1);
        
        List<Agreement__c> contracts = new List<Agreement__c>();
        contracts = [
            SELECT name, id, contract_with__c, contract_term_validity_date__c
            FROM Agreement__c
            WHERE contract_term_validity_date__c >= :dateFrom AND contract_term_validity_date__c <= :dateTo
        ];
        
        List<Id> ids = new List<Id>();
        for(Agreement__c contract: contracts) {
            ids.add(contract.id);
        }
        
        List<Contract_Contact__c> ccs = new List<Contract_Contact__c>();
        ccs = [
            SELECT id, Contract__r.name, Contact__r.name, Contact__r.email, Contract__r.Contract_term_validity_date__c
            FROM Contract_Contact__c
            WHERE Contract__c IN :ids
        ];
        
        
        /*
        *   TODO: Perhaps a more professional looking solution??
        */
        
        for(Contract_Contact__c cc: ccs) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List<String> {cc.Contact__r.email});
            mail.setSubject('Contract expire');
            
            String body = getEmailBody(cc.Contact__r.name, cc.Contract__r.name, String.valueOf(cc.Contract__r.Contract_term_validity_date__c));
            
            mail.setHtmlBody(body);
            
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> {mail};
            
            Messaging.sendEmail(mails);
        }
        
        
    }
    
    private String getEmailBody(String name, String contract_name, String contract_date) {
        String body = 
            '<h1>Dear ' + name + '!</h1>' +
            '<p>' +
                'The following contract [' + contract_name + '] is due to expire on ' +
                contract_date + '.' +
            '</p>';
            
            return body;
    }
    
}