public with sharing class LeadController
{
	private List<Schema.Lead> leadList()
	{
		id currentUserId;
		List<Schema.Lead> leadList = new List<Schema.Lead>();
		datetime tempCreatedDate;
		
		tempCreatedDate = datetime.newInstance(2014,03,28);
		
		currentUserId = userinfo.getUserId();
		
		leadList =
            [
                SELECT
                    Id,
                    Name,
                    Company,
                    CreatedDate,
                    IsUnreadByOwner
                FROM
                    Lead
                WHERE
                	OwnerId = :currentUserId
                	AND
                	IsUnreadByOwner = true
                	AND
                	CreatedDate >= :tempCreatedDate
                ORDER BY
                	CreatedDate ASC
                LIMIT 1000
            ];
			
		return leadList;		
	}
	
	public List<Schema.Lead> getLeadList()
	{
		List<Schema.Lead> leadList = leadList();
		
		return leadList;
	}
}