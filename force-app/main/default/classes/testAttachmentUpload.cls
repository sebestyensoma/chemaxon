@isTest
private class testAttachmentUpload {

    @isTest
    static void testNormUpload() {
        Account acc = new Account(
            name = 'Kandisz Nóra'
        );
        
        insert acc;
        
        Agreement__c c = new Agreement__c(
            contract_with__c = acc.id
        );
        
        insert c;
        
        PageReference page = new PageReference('/apex/AttachmentUpload');
        page.getParameters().put('cid', c.id);
        Test.setCurrentPage(page);
            
        AttachmentUpload ctrl = new AttachmentUpload();
        ctrl.initPage();
        
        ctrl.att.name = 'Such_Pdf.pdf,';
        ctrl.att.body = blob.toPDF('Much body. WOW');
        ctrl.selectedItem = 'Draft';
        
        ctrl.upload();
        
        System.assertEquals([SELECT id FROM Attachment WHERE parentId = :c.id].size(), 1);
        
        System.assertNotEquals([SELECT id FROM Attachment WHERE parentId = :acc.id].size(), 1);
        
    }
    
    @isTest
    static void testAdvUpload() {
        Account acc = new Account(
            name = 'Har Mónika'
        );
        
        insert acc;
        
        Agreement__c c = new Agreement__c(
            contract_with__c = acc.id
        );
        
        insert c;
        
        PageReference page = new PageReference('/apex/AttachmentUpload');
        page.getParameters().put('cid', c.id);
        Test.setCurrentPage(page);
            
        AttachmentUpload ctrl = new AttachmentUpload();
        ctrl.initPage();

        ctrl.att.name = 'Advancált.pdf,';
        ctrl.att.body = blob.toPDF('Há\' nem gondoltad vóna he?');
        ctrl.selectedItem = 'Countersigned';
        
        ctrl.upload();
        
        System.assertEquals([SELECT id FROM Attachment WHERE parentId = :c.id].size(), 1);
        
        System.assertEquals([SELECT id FROM Attachment WHERE parentId = :acc.id].size(), 1);
    }
    
}