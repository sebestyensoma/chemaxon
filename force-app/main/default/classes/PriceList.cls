public with sharing class PriceList {
	public string style {get; set;}
	public string result {get; set;}
	public string selectedProductId {get; set;}
	public string selectedCurrencyIsoCode {get; set;}
	public List<SelectOption> productCodeList {get; set;}
	public List<SelectOption> currencyIsoCodeList {get; set;}
	
	public Boolean displayURL {get; set;}
	public string url {get;set;}

	private decimal PERPETUALDIVISOR;
	private Map<string, Product2> product2IdMap = new Map<string, Product2>();
	

	public PriceList() {
		getPriceListExcelParams();
		PERPETUALDIVISOR = 0.4375;
		
		selectedCurrencyIsoCode = userinfo.getDefaultCurrency();
		
		makeStyle();
		fillProductCodeList();
		fillCurrencyIsoCodeList();
	}
	
	public PageReference Excel() {
		return new PageReference('/apex/pricelistexcel?' + 
			'selectedProductId=' + selectedProductId +
			'&selectedCurrencyIsoCode=' + selectedCurrencyIsoCode
		);
	}

	public void getPriceListExcelParams() {
		selectedProductId = ApexPages.currentPage().getParameters().get('selectedProductId');
		selectedCurrencyIsoCode = ApexPages.currentPage().getParameters().get('selectedCurrencyIsoCode');
	}
	
	public void makeStyle() {
		style = 
			'.pricetable, .pricetable td { border: 2px solid #A85722; border-collapse: collapse; } ' + 
			'.sor { text-align: right; } ' + 
			'.sor:hover { background-color: #ffff99; font-weight: bold; } ' + 
			'.fejlec { font-weight: bold; text-align: center; } ' +
			'.oldallec { width: 75px; font-weight: bold; } ';
	}
	
	private void fillProductCodeList() {
		productCodeList = new List<SelectOption>();
			
		for(Product2 currentP : [
			SELECT Id, Name, ProductCode, User_Log__c, Bundle_log__c, Plugin__c, Tool__c, Product_Link__c, Only_annual__c, Product_Description__c
			FROM Product2
			WHERE IsActive = true
			ORDER BY ProductCode ASC, Name ASC
		]) {
			product2IdMap.put(currentP.id, currentP);
			productCodeList.add(new SelectOption(currentP.Id, currentP.ProductCode + ' (' +currentP.Name + ')'));
		}
	}
	
	private void fillCurrencyIsoCodeList() {
		currencyIsoCodeList = new List<SelectOption> {
			new SelectOption('CHF', 'CHF'),
			new SelectOption('EUR', 'EUR'),
			new SelectOption('GBP', 'GBP'),
			new SelectOption('HUF', 'HUF'),
			new SelectOption('JPY', 'JPY'),
			new SelectOption('SGD', 'SGD'),
			new SelectOption('USD', 'USD')
		};
	}
	
	public pagereference getPriceList() {
		if (product2IdMap.keySet().contains(selectedProductId) && product2IdMap.get(selectedProductId).Product_Link__c != null) {
			displayURL = true;
			url = product2IdMap.get(selectedProductId).Product_Link__c;
		} 
		else {
			displayURL = false;
			Product2 product;
			List<PricebookEntry> pricebookEntryList;
		
			XMLStreamWriter xml = new XMLStreamWriter();
		
			result = '';
			
			pricebookEntryList = [
				SELECT UnitPrice, Product2Id, CurrencyIsoCode
				FROM PricebookEntry
				WHERE Product2Id = :selectedProductId
					AND CurrencyIsoCode = :selectedCurrencyIsoCode
			];
			
			// Validate
			
			if (pricebookEntryList.size() == 0) {
				addMessage('warning',
					'There is no pricebookEntry related to this product. <br/>'
					+ 'Please create a screenshot about the errormessage and send it to abarany@chemaxon.com for further investigation.'
				);
				return null;
			}
			if (product2IdMap.containsKey(selectedProductId)) {
				product = product2IdMap.get(selectedProductId);
			
				if (product.User_Log__c == 0) {
					addMessage('warning',
						'Problem with the selected product: user log (0) is invalid. <br/>'
						+'Please create a screenshot and send it to abarany@chemaxon.com for further investigation.'
					);
					return null;
				}
			
				if ((product.Plugin__c || product.Tool__c) && product.Bundle_Log__c == 0) {
					addMessage('warning', 'Problem with the selected product: bundle log (0) is invalid. <br/>'
						+'Please create a screenshot and send it to abarany@chemaxon.com for further investigation.'
					);
					return null;
				}
			}
			else {
				addMessage('warning', 'There is no product with the selected product code. <br/>'
					+'Please create a screenshot and send it to abarany@chemaxon.com for further investigation.'
				);
				return null;
			}


			// Generate
			
			if (product.Product_Description__c == null) {
				product.Product_Description__c = '';
			}
			
			if(tablazatNemKell(product)){
				writeDescription(xml, product);
				result = xml.getXmlString().replace('###DESCRIPTION###', product.Product_Description__c);
			} 
			else if (product.PlugIn__c || product.Tool__c) {
				xml = getPluginPrice(product, pricebookEntryList[0], 'Annual');
				result = xml.getXmlString().replace('###DESCRIPTION###', product.Product_Description__c);
				result += '<br/><br/>';
			
				xml = getPluginPrice(product, pricebookEntryList[0], 'Perpetual');
				result += xml.getXmlString().replace('###DESCRIPTION###', '');
				
			}
			else if (product.productCode.equalsIgnoreCase('CON')) {
				xml = getConsultancyPrice(product, pricebookEntryList[0]);
				result = xml.getXmlString().replace('###DESCRIPTION###', product.Product_Description__c);
			}
			else if(product.productCode.equalsIgnoreCase('SCS')){
				// Synergy cloud solution terméknek csak a product description mezőben levő táblázat kell
				writeDescription(xml, product);
				result = xml.getXmlString().replace('###DESCRIPTION###', product.Product_Description__c);
			}
			else
			{
				xml = getNotPluginPrice(product, pricebookEntryList[0]);
			
				result = xml.getXmlString().replace('###DESCRIPTION###', product.Product_Description__c);
			}
		}
		return null;
	}
	
	private XMLStreamWriter getConsultancyPrice(Product2 prod, PricebookEntry pbe) {
		XMLStreamWriter xml = new XMLStreamWriter();
		writeDescription(xml, prod);
		
		xml.WriteStartElement(null, 'table', null);
			xml.writeAttribute(null, null, 'class', 'pricetable');
			for (Integer i = 1; i <= 50; i++) {
				xml.WriteStartElement(null, 'tr', null);
					xml.writeAttribute(null, null, 'class', 'fejlec');

					xml.writeStartElement(null, 'td', null);
						xml.writeCharacters(String.valueOf(i) + (i > 1 ? ' days' : ' day'));
					xml.writeEndElement();

//					xml.WriteStartElement(null, 'td', null);
//						xml.WriteCharacters('Daily Rate');
//					xml.writeEndElement();

					xml.WriteStartElement(null, 'td', null);
						xml.WriteCharacters(formatNumber(pbe.UnitPrice * i, 0, ''));
					xml.writeEndElement();
				xml.writeEndElement();
			}
		xml.writeEndElement();
		
		return xml;
	}
	
	private XMLStreamWriter getPluginPrice(Product2 product, PricebookEntry pricebookEntry, string licenseType) {
		XMLStreamWriter xml = new XMLStreamWriter();
		writeDescription(xml, product);
		
		xml.WriteStartElement(null, 'table', null);
			xml.writeAttribute(null, null, 'class', 'pricetable');
			
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'fejlec');
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'rowspan', '2');
					xml.WriteCharacters('Max. nodes or users');
				xml.writeEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'colspan', '6');
					xml.WriteCharacters(product.ProductCode + ' (' + product.Name + ') - ' + selectedCurrencyIsoCode + ' - ' + licenseType + ' price');
				xml.writeEndElement();
			xml.writeEndElement();
			
			String term = 'plugin bundle';
			if (product.Tool__c) term = 'tool';
			
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'fejlec');
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('1 ' + term);
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('2 ' + term + 's');
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('3 ' + term + 's');
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('4 ' + term + 's');
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('5 ' + term + 's');
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					String text = (product.Plugin__c) ? 'All plugin bundles' : 'All tools (all 7)';
					xml.writeCharacters(text);
				xml.WriteEndElement();
			xml.writeEndElement();
		
		for(integer i = 1; i <= 500;) {
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'sor');
			
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'class', 'oldallec');
					xml.WriteCharacters(string.valueof(i));
				xml.WriteEndElement();
				
				for (integer j = 1; j <= 6; j++) {
					xml.WriteStartElement(null, 'td', null);
						xml.writeCharacters(formatNumber(calculateSalesPrice(product, pricebookEntry, i, j, licenseType), 0, ''));
					xml.WriteEndElement();
				}
				
			xml.WriteEndElement();
			
			if (i < 50) {
				i++;
			}
			else if (i < 100) {
				i += 10;
			}
			else {
				i += 100;
			}
		}
		
		xml.WriteEndElement();
		return xml;
	}
	
	private XMLStreamWriter getNotPluginPrice(Product2 product, PricebookEntry pricebookEntry) {
		XMLStreamWriter xml = new XMLStreamWriter();
			
		writeDescription(xml, product);
		
		xml.WriteStartElement(null, 'table', null);
			xml.writeAttribute(null, null, 'class', 'pricetable');
			
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'fejlec');
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'rowspan', '2');
					xml.writeCharacters('Max. nodes or users');
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'colspan', '2');
					xml.WriteCharacters(product.ProductCode + ' (' + product.Name + ') - ' + selectedCurrencyIsoCode);
				xml.writeEndElement();
			xml.writeEndElement();
			
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'fejlec');
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeCharacters('Annual Price');
				xml.WriteEndElement();
				
				if (!product.Only_annual__c)
				{
					xml.WriteStartElement(null, 'td', null);
						xml.writeCharacters('Perpetual Price');
					xml.WriteEndElement();
				}
			xml.writeEndElement();
		
		for(integer i = 1; i <= 500;) {
			boolean isFixedPrice = (product.ProductCode.equalsIgnoreCase('JEK') || product.ProductCode.equalsIgnoreCase('PLXA'));
			
			xml.WriteStartElement(null, 'tr', null);
				xml.writeAttribute(null, null, 'class', 'sor');
				
				xml.WriteStartElement(null, 'td', null);
					xml.writeAttribute(null, null, 'class', 'oldallec');
					xml.WriteCharacters(string.valueof(i));
				xml.WriteEndElement();
				
				xml.WriteStartElement(null, 'td', null);
					if (isFixedPrice) {
						xml.writeCharacters(formatNumber(calculateFixPrice(product, pricebookEntry, i, 'annual'), 0, ''));
					}
					else {
						xml.writeCharacters(formatNumber(calculateSalesPrice(product, pricebookEntry, i, null, 'annual'), 0, ''));
					}
				xml.WriteEndElement();
				
				if (!product.Only_annual__c) {
					xml.WriteStartElement(null, 'td', null);
					
						if (isFixedPrice) {
							xml.writeCharacters(formatNumber(calculateFixPrice(product, pricebookEntry, i, 'perpetual'), 0, ''));
						}
						else {
							xml.writeCharacters(formatNumber(calculateSalesPrice(product, pricebookEntry, i, null, 'perpetual'), 0, ''));
						}
						
					xml.WriteEndElement();
				}
			xml.WriteEndElement();
			
			if (i < 50) {
				i++;
			}
			else if (i < 100) {
				i += 10;
			}
			else {
				i += 100;
			}
		}
		
		xml.WriteEndElement();
		return xml;
	}
	
	private void writeDescription(XMLStreamWriter xml, Product2 product) {
		xml.writeStartElement(null, 'div', null);
			xml.writeCharacters('###DESCRIPTION###');
		xml.writeEndElement();
	}
	
	/*
		productcode
		plugin
		userlog
		unitprice
		userlimit
	*/
	private decimal calculateSalesPrice(Product2 product, PricebookEntry pricebookEntry, integer userLimit, integer numberOfPlugins, string licenseType) {
		decimal price;
		
		// van értelmes user log (nem null és nem 1)
		if (product.User_Log__c != NULL && product.User_Log__c != 1 && product.User_Log__c != 0) {
			if (pricebookEntry.UnitPrice != NULL && pricebookEntry.UnitPrice > 0) {
				if (product.ProductCode.equalsIgnoreCase('IJCV')) {
					price = getPriceForIJCV(product, pricebookEntry, userLimit);
				}
				else if (product.Tool__c) {
					price = pricebookEntry.UnitPrice * Math.pow(product.User_Log__c.doubleValue(), Math.log(userLimit) / Math.log(2));
					
					if (numberOfPlugins > 1) {
						price *= Math.pow(product.Bundle_Log__c.doubleValue(), Math.log(numberOfPlugins) / Math.log(2));
					}
				}
				else if (product.Plugin__c && numberOfPlugins > 1) {
					if (numberOfPlugins > 6) numberOfPlugins = 6;
					
					decimal exp1;
					decimal exp2;
					decimal productUserLog;
					decimal productBundleLog;
					
					productUserLog = product.User_LOG__c;
					productBundleLog = product.Bundle_LOG__c;
					
					if (userLimit != NULL && userLimit > 1) {
						exp1 = 
							math.exp
							(
								math.log(productUserLog) * 
								math.log(userLimit) / 
								math.log(2.0)
							);
						
						exp2 = 
							math.exp
							(
								math.log(productBundleLog) * 
								math.log(numberOfPlugins) / 
								math.log(2.0)
							);
						
						price = pricebookEntry.UnitPrice * exp1 * exp2 /*/ numberOfPlugins*/; // nem kell osztani, mert itt nem a darabonkénti ár a lényeg, hanem az összesen
					}
					else {
						exp2 = 
							math.exp
							(
								math.log(productBundleLog) * 
								math.log(numberOfPlugins) / 
								math.log(2.0)
							);
						
						price = pricebookEntry.UnitPrice * exp2 /*/ numberOfPlugins*/; // nem kell osztani, mert itt nem a darabonkénti ár a lényeg, hanem az összesen
					}
				}
				else {
					decimal exp1;
					decimal productUserLog;
					
					productUserLog = product.User_Log__c;
					
					exp1 = 
						math.exp
						(
							math.log(productUserLog) * 
							math.log(userLimit) / 
							math.log(2.0)
						);
					
					price =	pricebookEntry.UnitPrice * exp1;
				}
			}
			else {
				price = 0;
			}
		}
		else {
			if (pricebookEntry.UnitPrice > 0) {
				if (product.User_Log__c == 1) {
					price = userLimit * pricebookEntry.UnitPrice;
				}
				else {
					price = 0;
				}
			}
			else {
				price = 0;
			}
		}
		
		if (price > 0) {
			if (licenseType.toLowerCase().contains('annual')) {
				price = customRounder(price, null);
			}
			else if (licenseType.toLowerCase().contains('perpetual')) {
				price = customRounder(price, PERPETUALDIVISOR);
			}
			else {
				price = 0;
			}
			
			// constulting esetében nincs teendő: price = price
		}
		
		return price;
	}
	
	private decimal calculateFixPrice(Product2 product, PricebookEntry pricebookEntry, integer userLimit, string licenseType) {
		decimal price;
		
		if (product.ProductCode.equalsIgnoreCase('JEK')) {
			price = getPriceForJEK(userLimit);
		}
		if (product.ProductCode.equalsIgnoreCase('PLXA')) {
			price = getPriceForPLXA(userLimit);
		}
		
		if (price > 0) {
			if (licenseType.toLowerCase().contains('annual')) {
				price = customRounder(price, null);
			}
			else if (licenseType.toLowerCase().contains('perpetual')) {
				price = customRounder(price, PERPETUALDIVISOR);
			}
			else {
				price = 0;
			}
			// constulting esetében nincs teendő: price = price
		}
		
		return price;
	}
	
	private decimal customRounder(decimal price, decimal perpetualDivisor) {
		decimal roundedPrice;
		decimal tempPar;
		
		if (perpetualDivisor == NULL) {
			tempPar = math.pow(double.valueof(10), double.valueof(math.floor(math.log10(double.valueof(price)) - double.valueof(2)))); // 6-tal jó volt
			
			if (tempPar != NULL && tempPar != 0) {
				roundedPrice = math.floor(price / double.valueof(tempPar)) * double.valueof(tempPar);
			}
			else {
				roundedPrice = 0;
			}
		}
		else {
			tempPar = math.pow(double.valueof(10.0), double.valueof(math.floor(math.log10(price / PERPETUALDIVISOR) - 2)));
			
			if (tempPar != NULL && tempPar != 0) {	
				roundedPrice = math.floor(price / PERPETUALDIVISOR / tempPar) * tempPar;
			}
			else {
				roundedPrice = 0;
			}
		}
		
		return roundedPrice;
	}
	
	/** Szám átalakítása stringgé "szépen", ezres tagolással, a megadott számú tizedesjegyre kerekítve. Null értékre nullát ad vissza.
	 * @param n Az átalakítandó szám
	 * @param accuracy Tizedesjegyek száma az outputban
	 * @param separator A tizedesjegyeket leválasztó karakter (pl. pont vagy vessző)
	 */
	private String formatNumber(Decimal n, Integer accuracy, String separator) {
        if (n == null) {
        	String prettyText = '0' + separator;
        	return prettyText.rightPad(accuracy + 2).replace(' ', '0');
        }
        
        n = n.setScale(accuracy, RoundingMode.HALF_UP);
        String base = String.valueOf(Math.abs(n));
        String prettyText = '';
        
        if (accuracy > 0) {
            prettyText = separator + base.substringAfterLast('.');
        }
        base = base.substringBeforeLast('.');
        
        Integer copyTo = base.length();
        Integer copyFrom = copyTo - 3;
        do {
            if (copyFrom < 0) copyFrom = 0;
            if (copyTo != base.length()) prettyText = ' ' + prettyText;
            prettyText = base.substring(copyFrom, copyTo) + prettyText;
            copyFrom -= 3;
            copyTo -= 3;
        } while(copyTo > 0);
        
        if (n < 0) prettyText = '-' + prettyText;
        return prettyText;
    }
	
	private void addMessage(string severity, string message) {
		Apexpages.Message apexMessage;
		
		if (severity.equalsIgnoreCase('confirm')) {
			apexMessage = new Apexpages.Message(ApexPages.Severity.CONFIRM, message);
		}
		else if (severity.equalsIgnoreCase('error')) {
			apexMessage = new Apexpages.Message(ApexPages.Severity.ERROR, message);
		}
		else if (severity.equalsIgnoreCase('fatal')) {
			apexMessage = new Apexpages.Message(ApexPages.Severity.FATAL, message);
		}
		else if (severity.equalsIgnoreCase('info')) {
			apexMessage = new Apexpages.Message(ApexPages.Severity.INFO, message);
		}
		else if (severity.equalsIgnoreCase('warning')) {
			apexMessage = new Apexpages.Message(ApexPages.Severity.WARNING, message);
		}
		else {
			apexMessage = new Apexpages.Message(ApexPages.Severity.ERROR, 'problem with pagemessages');
		}       
	
		Apexpages.addmessage(apexMessage);
	}
	
	// PRODUCT-SPECIFIC PRICING
	
	private Decimal getPriceForJEK(Integer userLimit) {
		if (selectedCurrencyIsoCode.equalsIgnoreCase('USD')) {
			if (userLimit >= 1000) return 13000;
			else if (userLimit >= 500) return 9800;
			else if (userLimit > 40) return 6500;
			else if (userLimit > 30) return 6200;
			else if (userLimit > 20) return 5900;
			else if (userLimit > 10) return 5200;
			else if (userLimit > 5) return 3900;
			else if (userLimit > 1) return 2600;
			else return 1300;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('CHF')) {
			if (userLimit >= 1000) return 11400;
			else if (userLimit >= 500) return 8500;
			else if (userLimit > 40) return 5700;
			else if (userLimit > 30) return 5400;
			else if (userLimit > 20) return 5100;
			else if (userLimit > 10) return 4500;
			else if (userLimit > 5) return 3400;
			else if (userLimit > 1) return 2300;
			else return 1100;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('GBP')) {
			if (userLimit >= 1000) return 7500;
			else if (userLimit >= 500) return 5600;
			else if (userLimit > 40) return 3800;
			else if (userLimit > 30) return 3500;
			else if (userLimit > 20) return 3400;
			else if (userLimit > 10) return 3000;
			else if (userLimit > 5) return 2200;
			else if (userLimit > 1) return 1500;
			else return 700;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('HUF')) {
			if (userLimit >= 1000) return 2867000;
			else if (userLimit >= 500) return 2135000;
			else if (userLimit > 40) return 1433500;
			else if (userLimit > 30) return 1342000;
			else if (userLimit > 20) return 1281000;
			else if (userLimit > 10) return 1128500;
			else if (userLimit > 5) return 854000;
			else if (userLimit > 1) return 579500;
			else return 274500;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('JPY')) {
			if (userLimit >= 1000) return 1305100;
			else if (userLimit >= 500) return 971900;
			else if (userLimit > 40) return 652500;
			else if (userLimit > 30) return 610900;
			else if (userLimit > 20) return 583100;
			else if (userLimit > 10) return 513700;
			else if (userLimit > 5) return 388800;
			else if (userLimit > 1) return 263800;
			else return 125000;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('SGD')) {
			if (userLimit >= 1000) return 16000;
			else if (userLimit >= 500) return 11900;
			else if (userLimit > 40) return 8000;
			else if (userLimit > 30) return 7500;
			else if (userLimit > 20) return 7100;
			else if (userLimit > 10) return 6300;
			else if (userLimit > 5) return 4800;
			else if (userLimit > 1) return 3200;
			else return 1500;
		}
		else {
			if (userLimit >= 1000) return 9400;
			else if (userLimit >= 500) return 7000;
			else if (userLimit > 40) return 4700;
			else if (userLimit > 30) return 4400;
			else if (userLimit > 20) return 4200;
			else if (userLimit > 10) return 3700;
			else if (userLimit > 5) return 2800;
			else if (userLimit > 1) return 1900;
			else return 900;
		}
		return 0;
	}
	
	private Decimal getPriceForPLXA(Integer userLimit) {
		if (selectedCurrencyIsoCode.equalsIgnoreCase('USD')) {
			if (userLimit <= 10) return userLimit * 1000;
			else return 10000 + (userLimit - 10) * 900;
		}
		else if (selectedCurrencyIsoCode.equalsIgnoreCase('EUR')) {
			if (userLimit <= 10) return userLimit * 720;
			else return 7200 + (userLimit - 10) * 648;
		}
		return 0;
	}
	
	private Decimal getPriceForIJCV(Product2 prod, PricebookEntry pbe, Decimal userLimit) {
		if (userLimit <= 10) {
			return pbe.UnitPrice * userLimit;
		}
		else if (userLimit <= 19) {
			return pbe.UnitPrice * 10 + (userLimit - 10) * 62.2; 
		}
		else {
			return pbe.UnitPrice * math.exp(
				math.log(prod.User_Log__c) * 
				math.log(userLimit) / 
				math.log(2.0)
			);
		}
	}
	

	private Boolean tablazatNemKell(Product2 product){
		// CXN-2019-04-05 / 14455
		// CXN-2019-04-05 / 14458
		// CXN-2019-04-05 / 14460
		// ezekhez a termékekhez nem kell táblázat
		if(product.Name == 'JChem Neo4j' || product.Name == 'JChem Microservices Markush Enumeration' || product.Name == 'JChem Microservices Calculations') return true;

		return false;
	}

}