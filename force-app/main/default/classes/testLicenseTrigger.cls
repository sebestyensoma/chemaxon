@isTest(seealldata=true)
private class testLicenseTrigger
{
    /*
    static testMethod void myUnitTest01()
    {
        License__c license01 =
          new License__c
            (
              Name = 'license 12345',
              Account_Name__c = 'accountName',
              License_ID__c = '0123',
              License_Type__c = 'Evaluation',
              Contact_Last_Name_Eval__c = 'last name'
            );
        insert license01;
        
        License__c license02 = [SELECT Lead__c, Name FROM License__c WHERE Id = :license01.ID];
        Lead lead01 = [SELECT Id, Email FROM Lead WHERE Licence_No__c = :license02.Name];
        
        System.assertEquals(license02.Lead__c, lead01.Id);
    }
    
    static testMethod void myUnitTest02()
    {
        Lead lead01 =
          new Lead
            (
          LastName = 'lastName 99999999 - ' + system.now(),
          Company = 'company',
          Email = 'myunittest02@email.hu',
          Status = 'New'
            );
        insert lead01;
        
        License__c license01 =
          new License__c
            (
              Name = 'Name - ' + system.now(),
              License_ID__c = '0123',
              Account_Email__c = 'myunittest02@email.hu',
              License_Type__c = 'Evaluation'
            );
        insert license01;
        
        License__c license02 = [SELECT Lead__c FROM License__c WHERE Id = :license01.ID];
        
        System.assertEquals(license02.Lead__c, lead01.Id);
    }
    
    static testMethod void myUnitTest03()
    {
      License__c license01 =
          new License__c
            (
              Name = 'Name - ' + system.now(),
              License_ID__c = '0123',
              Account_ID_text__c = 'asdfasfsafsd'
            );
        insert license01;
        
        License__c license02 = [SELECT Account__c FROM License__c WHERE Id = :license01.ID];
        
        System.assertEquals(NULL, license02.Account__c);
    }
    
    static testMethod void myUnitTest04()
    {
      Account account01 =
        new Account
          (
            Name = '998 Account - ' + system.now()
          );
      insert account01;
      
      License__c license01 =
          new License__c
            (
              Name = 'Name - ' + system.now(),
              License_ID__c = '0123',
              Account_ID_text__c = account01.Id
            );
        insert license01;
        
        License__c license02 = [SELECT Account__c FROM License__c WHERE Id = :license01.Id];
        
        System.assertEquals(account01.Id, license02.Account__c);
    }
    
    static testMethod void myUnitTest05()
    {
      Account account01 =
        new Account
          (
            Name = '997 Account - ' + system.now()
          );
      insert account01;
      
      Opportunity opportunity01 =
        new Opportunity
          (
            Name = 'opportunity',
            AccountId = account01.Id,
            CloseDate = date.today(),
            StageName = 'New'
          );
      insert opportunity01;
      
      License__c license01 =
          new License__c
            (
              Name = 'Name - ' + system.now(),
              License_ID__c = '0123',
              Account_ID_text__c = account01.Id,
              Opportunity_ID_text__c = opportunity01.Id
            );
        insert license01;
        
        License__c license02 = 
          [
            SELECT
              Account__c,
              Opportunity_Account__c
            FROM
              License__c
            WHERE
            Id = :license01.Id
          ];
        
        System.assertEquals(account01.Id, license02.Opportunity_Account__c);
    }
    */
}