public class extensionQLIMultiLayout {
	public integer lineItemCount{get; set;}
    public Map<Id,PricebookEntry> allPBEs{get;set;}
    public List<PricebookEntry> listPBEs{get;set;}
    public Map<Id,Boolean> isPBEChecked{get;set;}
    //public Map<Id,QuoteLineItem> consultingTeam{get;set;}
    public Quote standardQuote{get;set;}
    private string CurrencyIsoCode;
    public Map<Id,Integer> mapPBEId2UserLimits{get;set;}
    
    public Map<Id,QuoteLineItem> mapPBEId2LicenseTypes{get;set;}
    
    public Integer userLimit{get;set;}
    
    public QuoteLineItem proxyQLI = new QuoteLineItem();
    
    public string selectedPackage{get; set;}
	
	public QuoteLineItem getProxyQLI()
	{
		return proxyQLI;
	}
    
    public String outJSONtfListString{
                                        get{
                                            Set<Id> setQLI = new Set<Id>();
                                            for(Id loopId:isPBEChecked.keySet()){
                                                if(isPBEChecked.get(loopId)==true){
                                                    setQLI.add(loopId);
                                                }
                                            }
                                            return JSON.serialize(setQLI);
                                        }
                                    }
    public String inJSONtfListString{get;set;}
    
	public String urlStr{get;set;}
	public String vanHiba{get;set;}
    
    public extensionQLIMultiLayout(ApexPages.StandardController stdController){
        //standardQuote = (Quote)stdController.getRecord();
        standardQuote = [SELECT Id, CurrencyIsoCode FROM Quote WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
		
		urlStr = '/lightning/r/Quote/'+String.valueOf(standardQuote.Id)+'/view?0.source=alohaHeader';
		vanHiba = 'true';

        lineItemCount = [ SELECT COUNT() FROM QuoteLineItem WHERE QuoteID = :standardQuote.Id ];
        
        CurrencyIsoCode = [SELECT CurrencyIsoCode FROM Quote WHERE Id = :standardQuote.Id].CurrencyIsoCode;
        isPBEChecked = new Map<Id,Boolean>();
        //consultingTeam = new Map<Id,QuoteLineItem>();
        inJSONtfListString = ApexPages.currentPage().getParameters().get('tf');
        Set<Id> checkedIds = new Set<Id>();
        if(inJSONtfListString!=null){
            checkedIds = (Set<id>)JSON.deserialize(inJSONtfListString, Set<Id>.class);
            mapPBEId2UserLimits = new Map<Id,Integer>();
            mapPBEId2LicenseTypes = new Map<Id,QuoteLineItem>();
        }
		
		system.debug(standardQuote.CurrencyIsoCode);

        listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode, Product2.Package_Type__c, UnitPrice FROM PricebookEntry WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Quote WHERE Id = :standardQuote.Id ) AND IsActive=true AND CurrencyIsoCode=:CurrencyIsoCode ORDER BY Product2.Name];
        allPBEs = new Map<Id,PricebookEntry>(listPBEs);
        for(Id loopId : allPBEs.keyset()){
            if(inJSONtfListString!=null){
                mapPBEId2UserLimits.put(loopId,1);
                mapPBEId2LicenseTypes.put(loopId,new QuoteLineItem(License_type__c='Annual'));
            }
            if(checkedIds!=null && checkedIds.contains(loopId)){
                isPBEChecked.put(loopId,true);
            }
            else{
                isPBEChecked.put(loopId,false);
            }

			//consultingTeam.put(loopId, new QuoteLineItem());
        }
    }
    
    public List<selectOption> getPackageName() 
    {
        List<selectOption> options = new List<selectOption>(); // lista, ami a picklist értékeit tárolja
       
        options.add(new selectOption('--None--','--None--'));
        options.add(new selectOption('All plugins','All plugins'));
        options.add(new selectOption('All products','All products'));
        options.add(new selectOption('Startup','Startup'));
        
        return options; // visszatérünk a picklistértékekkel
    }
    
    public void packageSelection()
    {
    	system.debug(selectedPackage);
    	
    	for(id id : isPBEChecked.keyset())
    	{
    		boolean tempBoolean;
    		string tempPackageType;
    		
    		tempPackageType = allPBEs.get(id).Product2.Package_type__c;
    		
    		if(tempPackageType != NULL && tempPackageType.contains(selectedPackage))
    		{
    			tempBoolean = true;
    			isPBEChecked.put(id, tempBoolean);
    		}
    		else
    		{
    			tempBoolean = false;
    			isPBEChecked.put(id, tempBoolean);
    		}
    	}
    	
    	system.debug(isPBEChecked);
    }

    public Pagereference saveForAll(){
        List<QuoteLineItem> listQLI = new List<QuoteLineItem>();
        if(inJSONtfListString == NULL)
        {
            if((userLimit == NULL || userLimit == 0) && proxyQLI.License_type__c == NULL)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number and the license type!'));
			
				return null;
			}
			else if(userLimit == NULL || userLimit == 0)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
			
				return null;
			}
			else if(proxyQLI.License_type__c == NULL)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the license type!'));
			
				return null;
			}
			/*
			else if(proxyQLI.License_type__c == 'Subscription' && (proxyQLI.Subscription_from__c == null || proxyQLI.Subscription_to__c == null))
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please enter the subscription period!'));
			
				return null;
			}
			else if(proxyQLI.License_type__c == 'Subscription' && (proxyQLI.Subscription_from__c > proxyQLI.Subscription_to__c))
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'The start of the subscription cannot be a later date than the end!'));
			
				return null;
			}
			*/
        }

		//if(proxyQLI.License_type__c != 'Subscription'){
			//proxyQLI.Subscription_from__c = null;
			//proxyQLI.Subscription_to__c = null;
		//}
        
        for(Id loopId:isPBEChecked.keySet()){
            if(isPBEChecked.get(loopId)==true){
                if(mapPBEId2UserLimits==null){
                	
                	system.debug(allPBEs);
					system.debug(loopId);
					//if (proxyQLI.License_type__c == 'Consulting' && consultingTeam.get(loopId).Consulting_team__c == null)
					if (proxyQLI.License_type__c == 'Consulting' && proxyQLI.Consulting_team__c == null)
					{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the Consulting team!'));
						return null;
					}
					else
					{
						listQLI.add(new QuoteLineItem(
							QuoteId=standardQuote.Id,
							PricebookEntryId=loopId,
							Quantity=1,
							UnitPrice=allPBEs.get(loopId).UnitPrice,
							Created_By_Form__c=true,
							User_Limit__c=userLimit,
							License_type__c=proxyQLI.License_type__c, 
							//Subscription_from__c = proxyQLI.Subscription_from__c, 
							//Subscription_to__c = proxyQLI.Subscription_to__c, 
							//Consulting_team__c = consultingTeam.get(loopId).Consulting_team__c
							Consulting_team__c = proxyQLI.Consulting_team__c
						));
					}
                }
                else
                {
                    if(mapPBEId2UserLimits.get(loopId) == NULL || mapPBEId2LicenseTypes.get(loopId).License_type__c == NULL)
					{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number and/or the license type!'));
						return null;
					}
					else
					{
						listQLI.add(new QuoteLineItem(QuoteId=standardQuote.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,Created_By_Form__c=true,User_Limit__c=mapPBEId2UserLimits.get(loopId),License_type__c=mapPBEId2LicenseTypes.get(loopId).License_type__c));
					}
                }
            }
        }
        if(listQLI==null || listQLI.size()==0){
            return null;
        }
        else{
            insert listQLI;
            //return new Pagereference('/'+standardQuote.Id);
			vanHiba = 'false';			
            return null;
        }        
    }



	public pagereference cancel()
    {
        pagereference pageref;
            
        pageref = new Pagereference('/' + standardQuote.Id);
            
        return pageref;
    }

	public pagereference setValues()
    {
        System.debug('SETvALUES()-ben');
		pagereference pageref;
            
        pageref = new Pagereference('/apex/QuoteLineMultiLayout?id=' + standardQuote.Id + '&sfdc.override=1&tf=' + outJSONtfListString);  
		pageref.setredirect(true);
        return pageref;
    }

	//public void changeLicenseType(){
		
	//}

	public void boom(){
		Integer i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
	}

}

/*
public class extensionQLIMultiLayout {
    public Map<Id,PricebookEntry> allPBEs{get;set;}
    public List<PricebookEntry> listPBEs{get;set;}
    public Map<Id,Boolean> isPBEChecked{get;set;}
    public Quote standardQuote{get;set;}
    private string CurrencyIsoCode;
    public Map<Id,Integer> mapPBEId2UserLimits{get;set;}
    public Integer userLimit{get;set;}
    public String outJSONtfListString{
                                        get{
                                            Set<Id> setQLI = new Set<Id>();
                                            for(Id loopId:isPBEChecked.keySet()){
                                                if(isPBEChecked.get(loopId)==true){
                                                    setQLI.add(loopId);
                                                }
                                            }
                                            return JSON.serialize(setQLI);
                                        }
                                    }
    public String inJSONtfListString{get;set;}
    
    public extensionQLIMultiLayout(ApexPages.StandardController stdController){
        standardQuote = (Quote)stdController.getRecord();
        //CurrencyIsoCode = [SELECT CurrencyIsoCode FROM Quote WHERE Id = :standardQuote.Id].CurrencyIsoCode;
        isPBEChecked = new Map<Id,Boolean>();
        inJSONtfListString = ApexPages.currentPage().getParameters().get('tf');
        Set<Id> checkedIds = new Set<Id>();
        if(inJSONtfListString!=null){
            checkedIds = (Set<id>)JSON.deserialize(inJSONtfListString, Set<Id>.class);
            mapPBEId2UserLimits = new Map<Id,Integer>();
        }
		
		//system.debug(standardQuote.CurrencyIsoCode);

        listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode,UnitPrice FROM PricebookEntry WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Quote WHERE Id = :standardQuote.Id ) AND IsActive=true ORDER BY Product2.Name];
        allPBEs = new Map<Id,PricebookEntry>(listPBEs);
        for(Id loopId : allPBEs.keyset()){
            if(inJSONtfListString!=null){
                mapPBEId2UserLimits.put(loopId,1);
            }
            if(checkedIds!=null && checkedIds.contains(loopId)){
                isPBEChecked.put(loopId,true);
            }
            else{
                isPBEChecked.put(loopId,false);
            }
        }
        
    }
    
    public Pagereference saveForAll(){
        List<QuoteLineItem> listQLI = new List<QuoteLineItem>();
        
        if(inJSONtfListString == NULL && userLimit == NULL)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
            
            return null;
        }
        
        for(Id loopId:isPBEChecked.keySet()){
            if(isPBEChecked.get(loopId)==true){
                if(mapPBEId2UserLimits==null){
                	
                	system.debug(allPBEs);
					system.debug(loopId);
					
                    listQLI.add(new QuoteLineItem(QuoteId=standardQuote.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,User_Limit__c=userLimit));
                }
                else
                {
                    if(mapPBEId2UserLimits.get(loopId) == NULL)
					{
						ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the user number!'));
						return null;
					}
					else
					{
						listQLI.add(new QuoteLineItem(QuoteId=standardQuote.Id,PricebookEntryId=loopId,Quantity=1,UnitPrice=allPBEs.get(loopId).UnitPrice,User_Limit__c=mapPBEId2UserLimits.get(loopId)));
					}
                }
            }
        }
        if(listQLI==null || listQLI.size()==0){
            return null;
        }
        else{
            insert listQLI;
            return new Pagereference('/'+standardQuote.Id);
        }
        
    }
    
}
*/