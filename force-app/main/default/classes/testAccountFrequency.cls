/**
 * @author      Attention CRM Consulting (koszi) <krisztian.oszi@attentioncrm.hu>
 * @version     1.0
 * @since       2016-08-17 (?K)
 */
@isTest(seeAllData=true)
private class testAccountFrequency {

	@isTest(seeAllData=true)
	private static void test(){
		Date today = system.today();

		Account acc = new Account();
		acc.Name = 'Attention Test';
		acc.Invoice_frequency__c = 'Monthly';
		acc.First_Invoice_date__c = today.addMonths(-1);
		acc.reporting_frequency__c = 'Semesterly';
		acc.First_Reporting_Date__c = today.addMonths(-1);
		insert acc;

		acc.Invoice_frequency__c = 'Yearly';
		acc.First_Invoice_date__c = today.addMonths(-1);
		acc.reporting_frequency__c = 'Tri-annually';
		acc.First_Reporting_Date__c = today.addMonths(-1);
		update acc;

		acc.Invoice_frequency__c = 'Quarterly';
		acc.First_Invoice_date__c = today.addMonths(-1);
		acc.reporting_frequency__c = 'Biannually';
		acc.First_Reporting_Date__c = today.addMonths(-1);
		update acc;

	}

}