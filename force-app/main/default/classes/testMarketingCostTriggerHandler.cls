@isTest 
private class testMarketingCostTriggerHandler {

	@isTest
	private static void testOsszegezMarketingCostInsert() {
		
		Campaign c = new Campaign(Name = 'teszt', Type = 'Conference', Internal_External__c = 'External', CurrencyIsoCode = 'EUR');
		insert c;

		Marketing_cost__c mc = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 100);
		insert mc;

		Marketing_cost__c mc2 = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 200);
		insert mc2;

		c = [SELECT BudgetedCost FROM Campaign WHERE Id = :c.Id];

		System.assertEquals(300, c.BudgetedCost);
	}

	@isTest
	private static void testOsszegezMarketingCostUpdate() {
		
		Campaign c = new Campaign(Name = 'teszt', Type = 'Conference', Internal_External__c = 'External', CurrencyIsoCode = 'EUR');
		insert c;

		Marketing_cost__c mc = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 100);
		insert mc;

		Marketing_cost__c mc2 = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 200);
		insert mc2;

		mc.Amount__c = 1000;
		update mc;

		mc2.Amount__c = 2000;
		update mc2;

		c = [SELECT BudgetedCost FROM Campaign WHERE Id = :c.Id];

		System.assertEquals(3000, c.BudgetedCost);
	}

	@isTest
	private static void testOsszegezMarketingCostDelete() {
		
		Campaign c = new Campaign(Name = 'teszt', Type = 'Conference', Internal_External__c = 'External', CurrencyIsoCode = 'EUR');
		insert c;

		Marketing_cost__c mc = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 100);
		insert mc;

		Marketing_cost__c mc2 = new Marketing_cost__c(Name = 'tesztname', Campaign__c = c.Id, Type__c = 'Accomodation', CurrencyIsoCode = 'EUR', Amount__c = 200);
		insert mc2;

		delete mc;

		c = [SELECT BudgetedCost FROM Campaign WHERE Id = :c.Id];

		System.assertEquals(200, c.BudgetedCost);
	}
}