@isTest
private class testCampaignMember 
{
    static testMethod void myUnitTest01() 
    {
        List<Lead> leadList = new List<Lead>();
        List<Lead> leadNewList = new List<Lead>();
        List<Campaign> campaignList = new List<Campaign>();
        List<CampaignMember> campaignMemberList = new List<CampaignMember>();
        
        Set<id> leadIdSet = new Set<id>();
        
        Lead lead01 = new Lead(LastName = 'name01', Company = 'company', LeadSource = 's01');
        leadList.add(lead01);
        
        Lead lead02 = new Lead(LastName = 'name02', Company = 'company', LeadSource = 's02');
        leadList.add(lead02);
        
        Lead lead03 = new Lead(LastName = 'name03', Company = 'company', LeadSource = 's03');
        leadList.add(lead03);
        
        if(leadList.size() != 0)
        {
        	insert leadList;
        	
        	for(Lead lead : leadList)
        	{
        		leadidSet.add(lead.Id);
        	}
        }
        
        Campaign campaign01 = new Campaign(Name = 'campaign01', Internal_External__c = 'Internal');
        campaignList.add(campaign01);
        
        Campaign campaign02 = new Campaign(Name = 'campaign02', Internal_External__c = 'External');
        campaignList.add(campaign02);
        
        Campaign campaign03 = new Campaign(Name = 'campaign03', Internal_External__c = 'blabla');
        campaignList.add(campaign03);
        
        if(campaignList.size() != 0)
        {
        	insert campaignList;
        }
        
        CampaignMember campaignMember01 = new CampaignMember(LeadId = leadList[0].Id, CampaignId = campaignList[0].Id);
        campaignMemberList.add(campaignMember01);
        
        CampaignMember campaignMember02 = new CampaignMember(LeadId = leadList[1].Id, CampaignId = campaignList[1].Id);
        campaignMemberList.add(campaignMember02);
        
        CampaignMember campaignMember03 = new CampaignMember(LeadId = leadList[2].Id, CampaignId = campaignList[2].Id);
        campaignMemberList.add(campaignMember03);
        
        if(campaignMemberList.size() != 0)
        {
        	insert campaignMemberList;
        }
        
     	leadNewList = [SELECT LeadSource FROM Lead WHERE Id IN :leadIdSet ORDER BY Name];
     	
     	system.assertEquals('Event Internal', leadNewList[0].LeadSource);
     	system.assertEquals('Event External', leadNewList[1].LeadSource);
     	system.assertEquals('s03', leadNewList[2].LeadSource);   
    }
}