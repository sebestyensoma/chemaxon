public with sharing class CountryReportController extends AbstractReportController {
	
	public Map<String, Boolean> showColumns { get; set; }
	
	protected override List<String> createColumnLabels() {
		List<String> countryLabels = new List<String>();
		
		for (AggregateResult countryRow : [
			SELECT BillingCountry
			FROM Account
			GROUP BY BillingCountry
			ORDER BY Billingcountry ASC
		]) {
			countryLabels.add((String)countryRow.get('BillingCountry'));
		}
		countryLabels.add('N/A');
		countryLabels.add('Total');
		
		return countryLabels;
	}
	
	protected override void doReport() {
		// Initialize visibility flags to false
		showColumns = new Map<String, Boolean>();
		for (String columnLabel : columnLabels) {
			showColumns.put(columnLabel, false);
		}
		
		// Fetch and process rows
        String fieldName = 'Total_in_' + currencyName + '__c';
        
        for (QuoteLineItem item : [
            SELECT License_Type__c, Quote.Invoice_Date__c, Quote.Opportunity.Account.BillingCountry,
                Total_in_EUR__c, Total_in_USD__c, Total_in_GBP__c, Total_in_HUF__c, Total_in_JPY__c, Total_in_CHF__c, Total_in_SGD__c
            FROM QuoteLineItem
            WHERE Quote.Invoice_Date__c != null
            ORDER BY Quote.Invoice_Date__c ASC
        ]) {
            Decimal d = (Decimal)item.get(fieldName);
            if (d == null) continue;
            
            String key = extractKey(item);
			String countryCode = item.Quote.Opportunity.Account.BillingCountry;
			if (countryCode == null) countryCode = 'N/A';
            
            Map<String, Decimal> tableRow = getOrCreateRow(key);
            Decimal rowTotal = tableRow.get(countryCode);
            if (rowTotal == null) continue;
            
            rowTotal += d;
            tableRow.put(countryCode, rowTotal);
            
            if (rowTotal > 0) showColumns.put(countryCode, true);
            
            rowTotal = tableRow.get('Total');
            rowTotal += d;
            tableRow.put('Total', rowTotal);
        }
	}
	
    /** Render the HTML code for the table body */
    public override String getTableHtml() {
        XmlStreamWriter tableHtml = new XmlStreamWriter();
        tableHtml.writeStartElement(null, 'tbody', null);
        
        for (String key : tableKeys) {
            Map<String, Decimal> tableRow = tableData.get(key);
            
            tableHtml.writeStartElement(null, 'tr', null);
            tableHtml.writeStartElement(null, 'td', null);
            tableHtml.writeAttribute(null, null, 'class', 'key');
            tableHtml.writeCharacters(key);         
            tableHtml.writeEndElement();
                
            for (String label : columnLabels) {
            	if (showColumns.get(label) == false) continue;
            	
                tableHtml.writeStartElement(null, 'td', null);
           		tableHtml.writeAttribute(null, null, 'class', 'num');
                tableHtml.writeCharacters(formatInteger(tableRow.get(label)));          
                tableHtml.writeEndElement();
            }   
            tableHtml.writeEndElement();
        }
        
        tableHtml.writeEndElement();
        return tableHtml.getXmlString();
    }
}