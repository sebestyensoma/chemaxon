@isTest
private class testAccount 
{
    static testMethod void myUnitTest01()
    {
        List<Account> newAccountList = new List<Account>();
        List<User> userList = new List<User>();
        Map<string, User> userIdMap = new Map<string, User>();
        
        userList =
        	[
        		SELECT
        			Id,
        			UserName
        		FROM
        			User
        		WHERE
        			IsActive = true
        	];
        	
        for(User user : userList)
        {
        	userIdMap.put(user.UserName, User);	
        }
        
        Account account01 = new Account(Name = 'name01');
        newAccountList.add(account01);
        
        Account account02 = new Account(Name = 'name02');
        newAccountList.add(account02);
        
        Account account03 = new Account(Name = 'name03');
        newAccountList.add(account03);
        
        Account account04 = new Account(Name = 'name04');
        newAccountList.add(account04);
        
        if(newAccountList.size() != 0)
        {
        	insert newAccountList;
        }
        
        if(userIdMap.containsKey('attentioncrm@chemaxon.com.test'))
        {
	        System.runAs(userIdMap.get('attentioncrm@chemaxon.com.test'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[0];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('mmedzihradszky@chemaxon.com.test'))
        {
	        System.runAs(userIdMap.get('mmedzihradszky@chemaxon.com.test'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[1];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('adrijver@chemaxon.com.test'))
        {
	        System.runAs(userIdMap.get('adrijver@chemaxon.com.test'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[2];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(true, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('alukacs@chemaxon.com.test'))
        {
	        System.runAs(userIdMap.get('alukacs@chemaxon.com.test'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[3];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
        
        
        if(userIdMap.containsKey('attentioncrm@chemaxon.com'))
        {
	        System.runAs(userIdMap.get('attentioncrm@chemaxon.com'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[0];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('mmedzihradszky@chemaxon.com'))
        {
	        System.runAs(userIdMap.get('mmedzihradszky@chemaxon.com'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[1];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('adrijver@chemaxon.com'))
        {
	        System.runAs(userIdMap.get('adrijver@chemaxon.com'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[2];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(true, vanhiba);
	        }
        }
        
        if(userIdMap.containsKey('alukacs@chemaxon.com'))
        {
	        System.runAs(userIdMap.get('alukacs@chemaxon.com'))
	        {
	        	boolean vanhiba;
	        	
	        	vanhiba = false;
	        	
	        	try
	        	{
	        		delete newAccountList[3];
	        	}
	        	catch(exception e)
	        	{
	        		vanhiba = true;
	        	}
	        	
	        	system.assertEquals(false, vanhiba);
	        }
        }
    }
}