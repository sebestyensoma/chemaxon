@isTest 
private class testBatchableSetQuoteDisplayFields {

	@isTest
	private static void testName() {
		Pricebook2 pb = new Pricebook2(
			Name = 'US Sales Book',
			isActive = true
		);

		insert pb;

		List<Product2> products = new List<Product2>();
		Product2 prod1 = new Product2(
			Name = 'Prod1',
			isActive = true,
			ProductCode = 'JEK'
		);
		products.add(prod1);
		Product2 prod2 = new Product2(
			Name = 'Prod2',
			isActive = true
		);
		products.add(prod2);
		insert products;

		List<PricebookEntry> stdpbes = new List<Pricebookentry>();
		PricebookEntry stdpbe1 = new PricebookEntry(
			Pricebook2Id = Test.getStandardPricebookId(),
			Product2Id = prod1.Id,
			UnitPrice = 100,
			isActive = true
		);
		stdpbes.add(stdpbe1);
		PricebookEntry stdpbe2 = new PricebookEntry(
			Pricebook2Id = Test.getStandardPricebookId(),
			Product2Id = prod2.Id,
			UnitPrice = 200,
			isActive = true
		);
		stdpbes.add(stdpbe2);
		insert stdpbes;
		
		List<PricebookEntry> pbes = new List<Pricebookentry>();
		PricebookEntry pbe1 = new PricebookEntry(
			Pricebook2Id = pb.Id,
			Product2Id = prod1.Id,
			UnitPrice = 100,
			isActive = true
		);
		pbes.add(pbe1);
		PricebookEntry pbe2 = new PricebookEntry(
			Pricebook2Id = pb.Id,
			Product2Id = prod2.Id,
			UnitPrice = 200,
			isActive = true
		);
		pbes.add(pbe2);
		insert pbes;

		Opportunity oppMaster = new Opportunity();
		oppMaster.Name = 'Tesztopp';
		oppMaster.StageName = 'Proposal/Quote';
		oppMaster.CloseDate = system.today().addMonths(1);
		insert oppMaster;

		Opportunity newOpp = new Opportunity();
		newOpp.Name = 'Tesztopp';
		newOpp.StageName = 'Proposal/Quote';
		newOpp.CloseDate = system.today().addMonths(1);
		newOpp.T_Opportunity_renewal_test__c = true;
		newOpp.Renewed_From__c = oppMaster.Id;
		newOpp.OwnerId = [SELECT Id FROM User WHERE Name = 'John Yucel' LIMIT 1].Id;
		insert newOpp;

		List<OpportunityLineItem> oppProd = new List<OpportunityLineItem>();
		OpportunityLineItem oppProd1 = new OpportunityLineItem();
		oppProd1.OpportunityId = oppMaster.Id;
		oppProd1.PricebookEntryId = pbe1.Id;
		oppProd1.Quantity = 1;
		oppProd1.TotalPrice = pbe1.UnitPrice * oppProd1.Quantity;
		oppProd1.License_type__c = 'PERPETUAL RENEWAL';
		oppProd.add(oppProd1);
		OpportunityLineItem oppProd2 = new OpportunityLineItem();
		oppProd2.OpportunityId = oppMaster.Id;
		oppProd2.PricebookEntryId = pbe2.Id;
		oppProd2.Quantity = 1;
		oppProd2.TotalPrice = pbe2.UnitPrice * oppProd2.Quantity;
		oppProd2.License_type__c = 'PERPETUAL';
		oppProd.add(oppProd2);
		insert oppProd;
		
		//Test.startTest();
			Quote q1 = new Quote();
			q1.Name = 'TestFirstQuote';
			q1.Pricebook2Id = pb.Id;
			q1.OpportunityId = oppMaster.Id;
			q1.Invoice_Type__c = 'Kft';
			q1.CurrencyIsoCode = 'USD';
			insert q1;

			Quote q2 = new Quote();
			q2.Name = 'TestSecondQuote';
			q2.Pricebook2Id = pb.Id;
			q2.OpportunityId = newOpp.Id;
			q2.Invoice_Type__c = 'Kft';
			q2.Renewed_From__c = q1.Id;
			insert q2;
		//Test.stopTest();


		QuoteLineItem qli01 = new QuoteLineItem();
        qli01.PricebookEntryId = pbe1.Id;
        qli01.QuoteId = q1.Id;
        qli01.Quantity = 1;
        qli01.UnitPrice = 1;
        qli01.License_Type__c = 'Subscription';
		qli01.Created_By_Form__c = false;
		qli01.User_Limit__c = 2000;
        insert qli01;


		Test.startTest();
		DataBase.executeBatch(new BatchableSetQuoteDisplayFields());
		Test.stopTest();

	}
}