public with sharing class AttachmentList {
    
    //To be able to use apex:inputField with Date sObject on the VisualForce page
    public Account dummy { get; set; }
    
    public Account[] accountsWithAttachment { get; set; }
    public Account[] accountsToTable { get; set; }
    
    public List<Attachment> listOfAttachments { get; set; }
    
    public Boolean renderAccountTable { get; set; }
    public Boolean renderText { get; set; }
    
    private final Integer numPerPage = 100;
    
    public Integer numOfPages { get; set; }
    public Integer currentPageNumber { get; set; }
    
    public String actionParam { get; set; }
    
    public List<SelectOption> extensions { get; set; }
    public String selectedExtension { get; set; }
    
    /**
    * Initialises every necessary property for the page
    **/
    public void initPage() {
        //Setting up the default dates
        dummy = new Account(
            Agreement_expires_on__c = Date.newInstance(2007, 11, 12),
            Agreement_signed_on__c = Date.today()
        );
        
        extensions = new List<SelectOption>();
        extensions.add(new SelectOption('%', 'Any type'));
        extensions.add(new SelectOption('image%', 'Images'));
        extensions.add(new SelectOption('%pdf', 'PDF'));
        extensions.add(new SelectOption('%doc%', 'DOC'));
        extensions.add(new SelectOption('%xls%', 'XLS'));
        
        selectedExtension = extensions[0].getValue();
        
        currentPageNumber = 1;
        numOfPages = 1;
        renderAccountTable = false;
        renderText = false;
    }
    
    /**
    * Gets the users with at least one attachment from the specified time intervall
    **/
    public void getAccounts() {
        // List out every single attachment within the specified interval
         listOfAttachments = (selectedExtension != 'image%') ?
            [
            	SELECT id, ParentId, CreatedDate, name, ContentType
            	FROM Attachment
            	WHERE CreatedDate >= :dummy.Agreement_expires_on__c AND CreatedDate < :dummy.Agreement_signed_on__c.addDays(1) AND name LIKE :selectedExtension
            	order by ParentId desc
        	] :	
			[
            	SELECT id, ParentId, CreatedDate, name, ContentType
            	FROM Attachment
            	WHERE CreatedDate >= :dummy.Agreement_expires_on__c AND CreatedDate < :dummy.Agreement_signed_on__c.addDays(1) AND ContentType LIKE :selectedExtension
            	order by ParentId desc
        	];
        
        List<String> dummyIdList = new List<String>();
        for(Attachment att: listOfAttachments) {
            if(att.ParentId != null && String.valueOf(att.ParentId).startsWith('001')) {
                dummyIdList.add(att.ParentId);
            }
            
        }
                
        accountsWithAttachment = [
            SELECT id, name
            FROM Account
            WHERE id IN :dummyIdList
            order by name
        ];
        
        renderText = accountsWithAttachment.isEmpty();
        renderAccountTable = !renderText;
        
		//TODO: Csak egy egyszerű felső egészrész kellene
        numOfPages = (Math.mod(accountsWithAttachment.size(), numPerPage) == 0) ? 
                    accountsWithAttachment.size() / numPerPage : 1 + (accountsWithAttachment.size() / numPerPage); 
        
        accountsToTable = new Account[] {};
        Integer n = accountsWithAttachment.size() > numPerPage ? numPerPage : accountsWithAttachment.size();   
        
        for(Integer i = 0; i < n; i++) {
            accountsToTable.add(accountsWithAttachment[i]);
        }
        
    }
    
    /**
    * Sets the output list for the VisualForce table;
    **/
    public void showCorrectTable() {
    	if(accountsToTable != null) {
	    	
	    	accountsToTable.clear();
	        Integer targetPage = Integer.valueOf(actionParam);
	        currentPageNumber = (targetPage < 1 || targetPage > numOfPages) ? currentPageNumber : targetPage;
	        
	        //Az oldalon megjelenítendő utolsó elem indexének megállípítása (100 elem / oldal)
	        Integer n = ((numPerPage - 1) + ((currentPageNumber - 1) * numPerPage)) <= accountsWithAttachment.size() - 1 ? 
	                    (numPerPage - 1) + ((currentPageNumber - 1) * numPerPage) : accountsWithAttachment.size() - 1;
	        
	        for(Integer i = ((currentPageNumber - 1) * numPerPage); i <= n; i++) {
	            accountsToTable.add(accountsWithAttachment[i]);
	        }
	        
    	}
        
    }
    
}