@isTest(seeAllData=true)
private class testClone {

    static testMethod void cloneOpportunity() {
		Product2 prod = new Product2(
			Name = 'Test Product',
			ProductCode = 'ABC123'
		);
		insert prod;
		
		Pricebook2 pb = new Pricebook2(
			Name = 'Test Pricebook',
			CurrencyIsoCode = 'EUR'
		);
		insert pb;
		
		PricebookEntry spbe = new PricebookEntry(
			Product2Id = prod.Id,
			Pricebook2Id = Test.getStandardPricebookId(),
			UnitPrice = 100,
			isActive = true
		);
		insert spbe;
		
		PricebookEntry pbe = new PricebookEntry(
			Product2Id = prod.Id,
			Pricebook2Id = pb.Id,
			UnitPrice = 100,
			isActive = true
		);
		insert pbe;
    	
		Account acc = new Account(
			Name = 'Test Account',
			Phone = '123456789'
		);
		insert acc;
		
		Opportunity opp = new Opportunity(
			Name = 'Test Opp',
			AccountId = acc.Id,
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			CurrencyIsoCode = pb.CurrencyIsoCode,
			Pricebook2Id = pb.Id
		);
		insert opp;
		
		Quote q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.Id,
			Pricebook2Id = pb.Id
		);
		insert q;
		
		QuoteLineItem qli = new QuoteLineItem(
			QuoteId = q.Id,
			PricebookEntryId = pbe.Id,
			UnitPrice = 100,
			Quantity = 1
		);
		insert qli;
		
		PageReference page = new PageReference('/apex/cloneopportunity');
		page.getParameters().put('id', opp.Id);
		Test.setCurrentPage(page);
		Test.startTest();
		
		CloneOpportunity ctrl = new CloneOpportunity(null);
		ctrl.start();
    }

    static testMethod void cloneQuote() {
		Product2 prod = new Product2(
			Name = 'Test Product',
			ProductCode = 'ABC123'
		);
		insert prod;
		
		Pricebook2 pb = new Pricebook2(
			Name = 'Test Pricebook',
			CurrencyIsoCode = 'EUR'
		);
		insert pb;
		
		PricebookEntry spbe = new PricebookEntry(
			Product2Id = prod.Id,
			Pricebook2Id = Test.getStandardPricebookId(),
			UnitPrice = 100,
			isActive = true
		);
		insert spbe;
		
		PricebookEntry pbe = new PricebookEntry(
			Product2Id = prod.Id,
			Pricebook2Id = pb.Id,
			UnitPrice = 100,
			isActive = true
		);
		insert pbe;
    	
		Account acc = new Account(
			Name = 'Test Account',
			Phone = '123456789'
		);
		insert acc;
		
		Opportunity opp = new Opportunity(
			Name = 'Test Opp',
			AccountId = acc.Id,
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			CurrencyIsoCode = pb.CurrencyIsoCode,
			Pricebook2Id = pb.Id
		);
		insert opp;
		
		Quote q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.Id,
			Pricebook2Id = pb.Id
		);
		insert q;
		
		QuoteLineItem qli = new QuoteLineItem(
			QuoteId = q.Id,
			PricebookEntryId = pbe.Id,
			UnitPrice = 100,
			Quantity = 1
		);
		insert qli;
		
		PageReference page = new PageReference('/apex/clonequote');
		page.getParameters().put('quoteid', q.Id);
		Test.setCurrentPage(page);
		Test.startTest();
		
		CloneQuote ctrl = new CloneQuote();
		ctrl.onStart();
    }
    
}