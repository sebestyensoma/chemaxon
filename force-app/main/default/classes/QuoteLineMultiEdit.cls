public with sharing class QuoteLineMultiEdit {
    
	public String quoteId { get; set; }
	public List<QuoteLineItem> quoteLineItemList { get; set; }
    
    public QuoteLineMultiEdit(ApexPages.standardcontroller stdController) {
		quoteId = stdController.getId();
		loadQuoteLineItems();
    }
    
    public PageReference save() {
		PageReference returnPage = null;
        try {
            update quoteLineItemList;
            returnPage = cancel();
        } catch(exception e) {
			System.debug(e.getMessage() + ' @' + e.getLineNumber());
			System.debug(e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getMessage()));
        }
            
		return returnPage;
    }
    
    public PageReference quickSave() {
		PageReference returnPage = null;
		try {
			system.debug(quoteLineItemList);
			for(QuoteLineItem currentQli: quoteLineItemList) {
                currentQli.T_Calculate_Prices__c = !currentQli.T_Calculate_Prices__c;
            }
            update quoteLineItemList;
            
            returnPage = new Pagereference('/' + QuoteId);
            returnPage.setredirect(true);
            
        } catch(exception e) {
			System.debug(e.getMessage() + ' @' + e.getLineNumber());
			System.debug(e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getMessage()));
        }

		return returnPage;
    }
    
	public pagereference cancel() {
		return new Pagereference('/' + quoteId);
	}
    
    private void loadQuoteLineItems() {
		quoteLineItemList = [
			SELECT
				Id, Discount, UnitPrice, TotalPrice, PricebookEntry.ProductCode, End_User__c, 
				User_Limit__c, License_Type__c, Target_Price__c, System_Message__c, 
				Original_Price__c, T_Calculate_Prices__c, Most_probable__c, Consulting_team__c
            FROM QuoteLineItem
            WHERE
				QuoteId = :quoteId
			ORDER BY SortOrder, PricebookEntry.ProductCode
		];
	}

}