@isTest 
private class testSchedulableQuoteHasPDF {

	@isTest(seealldata=true)
	private static void testName() {
		
		Product2 prod = new Product2(Name = 'teszt');
		insert prod;
		
		Pricebook2 pb = new Pricebook2(Name = 'qweq', IsActive = true);
		insert pb;
		
		Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

		PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;

		PricebookEntry pbe = new PricebookEntry(IsActive = true, Pricebook2Id = pb.Id, UnitPrice = 1111, Product2Id = prod.Id, UseStandardPrice = false);
		insert pbe;

		Account account = new Account(Name = 'name', Tax_Number__c = 'asdfasdf');
    	insert account;

		Opportunity opp = new Opportunity(Name = 'oppname01', AccountId = account.Id, StageName = 'stagename', CloseDate = system.today());
		insert opp;

		Contact contact = new Contact(LastName = 'lastName');
    	insert contact;

		Quote q = new Quote(Name = 'teszt', OpportunityId = opp.Id, ContactId = contact.Id, Quote_has_PDF__c = true);
		insert q;

		Quote q2 = new Quote(Name = 'teszt2', OpportunityId = opp.Id, ContactId = contact.Id, Quote_has_PDF__c = false);
		insert q2;

		//QuoteDocument qd = new QuoteDocument(QuoteId = q2.Id);
		//insert qd;

		test.starttest();
		//String jobID = system.schedule(
		                               //'testSchedulableInvoice',
		                               //'0 0 23 * * ?',
		                               //new SchedulableInvoice()
		//);
		SchedulableQuoteHasPDF obj = new SchedulableQuoteHasPDF();
		obj.execute(null);


		test.stoptest();
	}
}