public class MarketingCostTriggerHandler  {

	public static void AI(Map<Id, Marketing_cost__c> triggerNew){
		osszegez(null, triggerNew);
	}

	public static void AU(Map<Id, Marketing_cost__c> triggerOld, Map<Id, Marketing_cost__c> triggerNew){
		osszegez(triggerOld, triggerNew);		
	}

	public static void AD(Map<Id, Marketing_cost__c> triggerOld){
		osszegez(triggerOld, null);		
	}


	private static void osszegez(Map<Id, Marketing_cost__c> marketingCostOld, Map<Id, Marketing_cost__c> marketingCostNew){
		// a Campaign-hoz kapcsolódó Marketing Cost-ok Amount-jával összegezzük a Campaign BudgetedCost mezőjét
		Set<Id> cId = new Set<Id>();
		if(marketingCostOld != null){
			for(Marketing_cost__c mc : marketingCostOld.values()){
				if(mc.Campaign__c != null) cId.add(mc.Campaign__c);
			}
		}
		if(marketingCostNew != null){
			for(Marketing_cost__c mc : marketingCostNew.values()){
				if(mc.Campaign__c != null) cId.add(mc.Campaign__c);
			}
		}

		System.debug(JSON.serializePretty(marketingCostOld));
		System.debug(JSON.serializePretty(marketingCostNew));
		Map<Id, Campaign> campaignMap = new Map<Id, Campaign>([SELECT BudgetedCost FROM Campaign WHERE Id IN :cId]);
		for(Campaign c : campaignMap.values()){
			c.BudgetedCost = 0;
		}

		AggregateResult[] groupedResults = [SELECT SUM(Amount__c) sum, Campaign__c FROM Marketing_cost__c WHERE Campaign__c IN :cId GROUP BY Campaign__c];
		for (AggregateResult ar : groupedResults)  {
			Campaign tmp = campaignMap.get(Id.valueOf(String.valueOf(ar.get('Campaign__c'))));
			tmp.BudgetedCost = Integer.valueOf(ar.get('sum'));
			//campaignMap.put(Id.valueOf(String.valueOf(ar.get('Campaign__c'))), tmp);
		}

		System.debug(JSON.serializePretty(campaignMap));

		update campaignMap.values();

	}

}