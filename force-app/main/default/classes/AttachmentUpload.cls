public with sharing class AttachmentUpload {
    
    public Id cId { get; set; }
    
    public Attachment att { get; set; }
    
    public List<SelectOption> docTypes { get; set; }
    public String selectedItem { get; set; }
    
    public void initPage() {
        cId = ApexPages.currentPage().getParameters().get('cid');
        
        docTypes = new List<SelectOption>();
        docTypes.add(new SelectOption('Draft', 'Draft'));
        docTypes.add(new SelectOption('Final', 'Final'));
        docTypes.add(new SelectOption('Countersigned', 'Countersigned'));
        
        selectedItem = docTypes[0].getValue();
        
        att = new Attachment(
            parentId = cId
        );
    }
    
    public void upload() {
        if(att.body == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a file to be uploaded!'));
            return;
        }
        
        if(isImage(att.name)) {
            att.ContentType = 'image/' + att.name.split('.', 2)[1].toLowerCase();
        }
        
        try {
            INSERT att;
            
            if(selectedItem == 'Countersigned') {
                Attachment att_t = att.clone(false, true, false, false);
                att_t.parentId = [SELECT Id, Contract_with__c From Agreement__c WHERE Id = :att.ParentId][0].Contract_with__c;
                INSERT att_t;
            }
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Attachment was uploaded successfully!'));
        }catch(Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'An error has occurred during upload! ' + e.getMessage()));
        }
        
    }
    
    private  Boolean isImage(String name) {
        String temp = name.split('.', 2)[1].toLowerCase();
        return temp == 'png' || temp == 'bmp' || temp == 'jpg' || temp == 'gif' || temp == 'jpeg';
    }
}