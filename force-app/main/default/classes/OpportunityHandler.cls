public with sharing class OpportunityHandler
{
	/*
	public void setRenewalUnitPrice(Map<id, Opportunity> opportunityIdMap)
	{
		List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
		
		oliList =
			[
				SELECT
					ID,
					Discount,
					UnitPrice,
					OpportunityId,
					License_type__c,
					Original_Price__c
				FROM
					OpportunityLineItem
				WHERE
					OpportunityId IN :opportunityIdMap.keyset()
			];
		
		for(OpportunityLineItem currentOli : oliList)
		{
			if(currentOli.License_Type__c.toUpperCase().contains('RENEWAL'))
			{
				Opportunity tempOpp = new Opportunity();
			
				tempOpp = opportunityIdMap.get(currentOli.OpportunityId);
			
				currentOli.Discount = null; // kinullázzuk a kedvezményt, hogy ne zavarjon induljon az új ár
				currentOli.UnitPrice = currentOli.Quote_Price__c * (1 - (tempOpp.Renewal_Percentage__c / 100));
			}
		}
		
		//update oliList;
	}
	*/
	
	/*
	public decimal calculateSalesPrice(OpportunityLineItem oli, integer numberOfExistingPlugins, integer numberOfNewPlugins) // UnitPrice fieldbe
	{
		integer numberOfPlugins;
		decimal opportunityPrice;
		decimal salesPrice;
		decimal PERPETUALDIVISOR;
		
		PERPETUALDIVISOR = 0.4375;
		numberOfPlugins = numberOfExistingPlugins + numberOfNewPlugins;
		
		if(oli.PricebookEntry.Product2.ProductCode == 'JEK')
		{
			if(oli.User_Limit__c >= 1000)
			{
				opportunityPrice = 9400;
			}
			else if(oli.User_Limit__c >= 500)
			{
				opportunityPrice = 7000;
			}
			else if(oli.User_Limit__c > 40)
			{
				opportunityPrice = 4700;
			}
			else if(oli.User_Limit__c > 30)
			{
				opportunityPrice = 4400;
			}
			else if(oli.User_Limit__c > 20)
			{
				opportunityPrice = 4200;
			}
			else if(oli.User_Limit__c > 10)
			{
				opportunityPrice = 3700;
			}
			else if(oli.User_Limit__c > 5)
			{
				opportunityPrice = 2800;
			}
			else if(oli.User_Limit__c > 1)
			{
				opportunityPrice = 1900;
			}
			else
			{
				opportunityPrice = 940;
			}
		}
		else
		{
			if // 1: ki van töltve a producton az user log
			(
				oli.PricebookEntry.Product2.User_LOG__c != 1 &&
				oli.PricebookEntry.Product2.User_LOG__c != NULL
			)
			{
				// 1T
				system.debug('1T');
				
				if(oli.PricebookEntry.Product2.Plugin__c && numberOfPlugins > 1) // 2: plugin-e az aktuális termék és több plugin van, mint 1
				{
					// 2T
					system.debug('2T');
					
					/*
					if(numberOfPlugins > 6) // ha több, mint 6 plugin van, akkor is 6-os árral számolunk 
					{
						numberOfPlugins = 6;
					}
					*
					
					if // 3: a termék unitprice-a nagyobb, mint 0
					(
						oli.PricebookEntry.UnitPrice != NULL &&
						oli.PricebookEntry.UnitPrice > 0
					)
					{
						// 3T
						system.debug('3T');
						
						decimal exp1;
						decimal exp2;
						decimal productUserLog;
						decimal productBundleLog;
						decimal userLimit;
						
						productUserLog = oli.PricebookEntry.Product2.User_LOG__c;
						productBundleLog = oli.PricebookEntry.Product2.Bundle_LOG__c;
						userLimit = oli.User_Limit__c;
						
						if // 4: userlimit nagyobb, mint 1
						(
							oli.User_Limit__c != NULL &&
							oli.User_Limit__c > 1
						)
						{
							// 4T
							system.debug('4T');
							
							exp1 = 
								math.exp
								(
									math.log(productUserLog) * 
									math.log(userLimit) / 
									math.log(2.0)
								);
							
							exp2 = 
								math.exp
								(
									math.log(productBundleLog) * 
									math.log(numberOfPlugins) / 
									math.log(2.0)
								);
							
							opportunityPrice = 
								oli.PricebookEntry.UnitPrice *
								exp1 *
								exp2 /
								numberOfPlugins;
						}
						else
						{
							// 4F
							system.debug('4F');	
							
							exp2 = 
								math.exp
								(
									math.log(productBundleLog) * 
									math.log(numberOfPlugins) / 
									math.log(2.0)
								);
							
							opportunityPrice = 
								oli.PricebookEntry.UnitPrice *
								exp2 /
								numberOfPlugins;
						}
					}
					else
					{
						// 3F
						system.debug('3F');
						
						opportunityPrice = 0;
					}
				}
				else
				{
					// 2F
					system.debug('2F');
					
					if // 5: a termék unitprice-a nagyobb, mint 0
					(
						oli.PricebookEntry.UnitPrice != NULL &&
						oli.PricebookEntry.UnitPrice > 0
					)
					{
						// 5T
						system.debug('5T');
						
						if // 6: 1 plugin van és ez plugin; vagy nem plugin
						(
							(numberOfPlugins == 1 && oli.PricebookEntry.Product2.Plugin__c) ||
							!oli.PricebookEntry.Product2.Plugin__c
						)
						{
							// 6T
							system.debug('6T');
							
							decimal exp1;
							decimal productUserLog;
							decimal userLimit;
							
							productUserLog = oli.PricebookEntry.Product2.User_LOG__c;
							userLimit = oli.User_Limit__c;
							
							exp1 = 
								math.exp
								(
									math.log(productUserLog) * 
									math.log(userLimit) / 
									math.log(2.0)
								);
							
							opportunityPrice = 
								oli.PricebookEntry.UnitPrice *
								exp1;
						}
						else
						{
							// 6F
							system.debug('6F');
							
							opportunityPrice = oli.PricebookEntry.UnitPrice;
						}
					}
					else
					{
						// 5F
						system.debug('5F');
						
						opportunityPrice = 0;
					}
				}
			}
			else // nincs kitöltve a producton az user log
			{
				// 1F
				system.debug('1F');
				
				if(oli.PricebookEntry.UnitPrice > 0) // 7: termék unitprice-a nagyobb mint 0
				{
					// 7T
					system.debug('7T');
					
					if(oli.pricebookEntry.Product2.User_LOG__c == 1) // 8: user log = 1
					{
						// 8T
						system.debug('8T');
						
						opportunityPrice = oli.PricebookEntry.UnitPrice * oli.User_Limit__c;
					}
					else
					{
						// 8F
						system.debug('8F');
						
						opportunityPrice =  0;
					}
				}
				else
				{
					// 7F
					system.debug('7F');
					
					opportunityPrice = 0;
				}
			}
		}
		
		if(opportunityPrice > 0) // 101: opportunityprice nagyobb, mint 0
		{
			// 101T
			system.debug('101T');
			
			if(oli.License_Type__c.toUpperCase().contains('ANNUAL')) // 102: annual
			{
				// 102T
				system.debug('102T');
				
				decimal tempPar;
				
				system.debug(opportunityPrice);
				
				system.debug('double(10): ' + double.valueof(10.0));
				system.debug('opportunityPrice: ' + opportunityPrice);
				system.debug('ln(opportunityPrice): ' + math.log(opportunityPrice));
				//system.debug('ln(opportunityPrice)-2: ' + math.log(opportunityPrice) - 2);
				system.debug('floor(ln(opportunityPrice)-2): ' + math.floor(math.log(opportunityPrice) - 2));
				system.debug('double(floor(ln(opportunityPrice)-2)): ' + double.valueof(math.floor(math.log(opportunityPrice) - 2)));
				
				tempPar = math.pow(double.valueof(10), double.valueof(math.floor(math.log10(opportunityPrice) - 2))); // 6-tal jó volt
				
				system.debug('opportunityPrice: ' + opportunityPrice);
				
				salesPrice = math.floor(opportunityPrice / double.valueof(tempPar)) * double.valueof(tempPar);
				
				/*
				salesPrice = 
					oli.PricebookEntry.UnitPrice * 
					math.log
						(
							math.log(oli.User_Limit__c) / 
							math.log(2) * 
							math.log(oli.PricebookEntry.Product2.User_Log__c)
						) *
						10;
				*
				
				system.debug('tempPar: ' + tempPar);
				system.debug('opportunityPrice / tempPar: ' + opportunityPrice / tempPar);
				system.debug('floor(opportunityPrice / tempPar): ' + math.floor(opportunityPrice / tempPar));
				system.debug('salesPrice: ' + salesPrice);
				system.debug('opportunityPrice: ' + opportunityPrice);
			}
			else if(oli.License_Type__c.toUpperCase().contains('PERPETUAL')) // 103: perpetual
			{
				// 103T
				system.debug('103T');
				
				decimal tempPar;
				
				tempPar = math.pow(double.valueof(10.0), double.valueof(math.floor(math.log10(opportunityPrice / PERPETUALDIVISOR) - 2)));
				
				salesPrice = math.floor(opportunityPrice / PERPETUALDIVISOR / tempPar) * tempPar;
			}
			else if(oli.License_Type__c.toUpperCase().contains('CONSULTING')) // 104: consulting
			{
				// 104T
				system.debug('104T');
				
				salesPrice = opportunityPrice;
			}
			else
			{
				// 104F
				system.debug('104F');
				
				salesPrice = 0;
			}
		}
		else
		{
			// 101F
			system.debug('101F');
			
			salesPrice = 0;
		}
		
		system.debug(opportunityPrice);
		system.debug(salesPrice);
		
		return salesPrice;
	}
	*/
	
	public date setLastDayOfMonth(date paramDate)
	{
		date returnDate;
		
		returnDate = date.newInstance(paramDate.year(), paramDate.month(), date.daysInMonth(paramDate.year(), paramDate.month()));
		
		return returnDate;
	}
}