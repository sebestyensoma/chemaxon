@isTest
public class CalculateRenewalExecutorTest {

    @TestSetup
    static void makeData(){

        id pricebookID = Test.getStandardPricebookId();
        Product2 product = new Product2(Name='test Prod', Family='Hardware', IsActive = true);
        Product2 product2 = new Product2(Name='test Prod2', Family='Hardware', IsActive = false);
        insert product2;
        insert product;
        PricebookEntry priceEntry = new PricebookEntry(Pricebook2Id = pricebookID, UnitPrice = 10000, IsActive=true, Product2Id = product.Id);
        PricebookEntry priceEntry2 = new PricebookEntry(Pricebook2Id = pricebookID, UnitPrice = 20000, IsActive = true, Product2Id = product2.Id);
        insert priceEntry;
        insert priceEntry2;
    }

    @isTest
    public static void testBatchableClassWithBulkData(){

        makeBulkData(40);

        List<QuoteLineItem> oldQliList = [select quoteId, license_type__c from QuoteLineItem];
        List<Quote> quoteList = [select id from quote where renewed__c = true];
        System.assertEquals(0, quoteList.size());
        Test.startTest();
        CalculateRenewalExecutor.runBatchableClassWithTargetYear(Date.today().year());
        Test.stopTest();
        /*
        List<Quote> oldQuotes = [select id, invoice_date__c from quote where renewed__c = true];
        List<Quote> newQuotes = [select id from quote where renewed__c = false and renewed_from__c != null];
        System.assertEquals(31, oldQuotes.size());
        System.assertEquals(31, newQuotes.size());

        Quote quote1 = [select id, License_Expiration_Date__c, name from quote where account.name = 'soma 0' limit 1];
        Quote renewedQUote1 = [select id, name, opportunity.CloseDate from quote where renewed_from__c = :quote1.Id ];
        System.assertEquals(quote1.License_Expiration_Date__c.addYears(1), renewedQUote1.Opportunity.CloseDate);

        Quote quote2 = [select id, License_Support_Expiration_Date__c, name from quote where account.name = 'Perpetual Test' limit 1];
        Quote renewedQUote2 = [select id, name, opportunity.CloseDate from quote where renewed_from__c = :quote2.Id ];
        System.assertEquals(quote2.License_Support_Expiration_Date__c.addYears(1), renewedQUote2.Opportunity.CloseDate);

        Quote quote3 = [select id, Renewed__c, License_Expiration_Date__c, License_Support_Expiration_Date__c, name, Opportunity.CloseDate from quote where account.name = 'Subscription Test' limit 1];
        QuoteLineItem qli1 = [select id from QuoteLineItem where license_type__c = 'Subscription' limit 1];
        System.assertEquals(true, quote3.Renewed__c);
        Quote renewedQUote3 = [select id, name, opportunity.CloseDate from quote where renewed_from__c = :quote3.Id ];
        System.assertEquals(quote3.Opportunity.CloseDate, renewedQUote3.Opportunity.CloseDate);
        */

    }

    @isTest
    static void testQLITrigger(){
        
        makeBulkData(10);

        QuoteLineItem qli = [select End_User__c, quote.opportunityId from QuoteLineItem LIMIT 1];
        Account acc = [select name from account limit 1];
        Opportunity oppty = [select id, stagename, closedate from opportunity where id = : qli.quote.OpportunityId limit 1];
        qli.End_User__c = acc.Id;
        oppty.StageName = 'Closed Won';
        update oppty;
        oppty = [select id, stagename, closedate from opportunity where id = : qli.quote.OpportunityId limit 1];
        update qli;
        Account updatedAcc = [select first_purchase_date__c from account where id = : acc.Id];
        System.assertEquals(oppty.CloseDate, updatedAcc.First_Purchase_Date__c);
    }

    @isTest
    static void testAnnualLicense(){
        Quote quoteObject = insertQuoteAndQLI('soma', 'Annual', 1);
        Test.startTest();
        CalculateRenewalExecutor.runOpportunityRenewalWithOpptyId(quoteObject.OpportunityId);
        Test.stopTest();
        Opportunity newOppty = [SELECT CloseDate, Name FROM Opportunity WHERE Renewed_From__c = :quoteObject.OpportunityId LIMIT 1];
        System.assertEquals('soma1'+Date.today().addYears(1).year() , newOppty.Name);
        System.assertEquals(quoteObject.License_Expiration_Date__c.addYears(1), newOppty.CloseDate);
    }

    @isTest
    static void testPerpetualLicense(){
        Quote quoteObject = insertQuoteAndQLI('soma', 'Perpetual', 1);
        Test.startTest();
        CalculateRenewalExecutor.runOpportunityRenewalWithOpptyId(quoteObject.OpportunityId);
        Test.stopTest();
        Opportunity newOppty = [SELECT CloseDate, Name FROM Opportunity WHERE Renewed_From__c = :quoteObject.OpportunityId LIMIT 1];
        System.assertEquals('soma1'+Date.today().addYears(1).year() , newOppty.Name);
        System.assertEquals(quoteObject.License_Support_Expiration_Date__c.addYears(1), newOppty.CloseDate);
    }

    @isTest
    static void testlWithMoreQuoteLineItems(){
        Quote quoteObject1 = insertQuoteAndQLI('soma', 'Annual', 1);
        QuoteLineItem qli2 = [SELECT QuoteId, PricebookEntryId, Product2Id, Quantity, UnitPrice FROM QuoteLineItem WHERE QuoteId = :quoteObject1.Id LIMIT 1];
        qli2.Id = null;
        qli2.License_type__c = 'Perpetual';
        insert qli2;
        Test.startTest();
        CalculateRenewalExecutor.runOpportunityRenewalWithOpptyId(quoteObject1.OpportunityId);
        Test.stopTest();
        
        Opportunity newOppty = [SELECT Name, CloseDate FROM Opportunity WHERE Renewed_From__c = :quoteObject1.OpportunityId LIMIT 1];
        System.assertEquals(quoteObject1.License_Expiration_Date__c.addYears(1) , newOppty.CloseDate); //az annual felulirja
    }

    @isTest
    static void correctorClassAnnualTest(){
        TriggerStopper.stopOpp = true;
        TriggerStopper.stopQuote = true;
        TriggerStopper.stopQuoteLine = true;
        TriggerStopper.stopOppLine = true;
        Quote quoteObject1 = insertQuoteAndQLI('soma', 'Annual', 1);
        Opportunity oppty1 = [select id, accountid from opportunity where id = :quoteObject1.OpportunityId LIMIT 1];
        Opportunity oppty2 = new Opportunity(Name = 'soma2');
        oppty2.CloseDate = date.today().addDays(30);
        oppty2.AccountId = oppty1.AccountId;
        oppty2.StageName = 'Proposal/Quote';
        oppty2.Renewed_From__c = oppty1.Id;
        quoteObject1.Renewed__c = true;
        insert oppty2;
        update quoteObject1;
        Quote quoteObject2 = new Quote();
        quoteObject2.Name = oppty2.Name;
        quoteObject2.OpportunityId = oppty2.Id;
        quoteObject2.Pricebook2Id = Test.getStandardPricebookId();
        quoteObject2.Renewed_From__c = quoteObject1.Id;
        insert quoteObject2;
        Test.startTest();
        Database.executeBatch(new OpportunityRenewalCorrector());
        Test.stopTest();
        oppty2 = [select closedate from opportunity where id = : oppty2.Id];
        System.assertEquals(quoteObject1.License_Expiration_Date__c, oppty2.closedate);
    }

    @isTest
    static void correctorClassPerpetualTest(){
        TriggerStopper.stopOpp = true;
        TriggerStopper.stopQuote = true;
        TriggerStopper.stopQuoteLine = true;
        TriggerStopper.stopOppLine = true;
        Quote quoteObject1 = insertQuoteAndQLI('soma', 'Perpetual Renewal', 1);
        Opportunity oppty1 = [select id, accountid from opportunity where id = :quoteObject1.OpportunityId LIMIT 1];
        Opportunity oppty2 = new Opportunity(Name = 'soma2');
        oppty2.CloseDate = date.today().addDays(30);
        oppty2.AccountId = oppty1.AccountId;
        oppty2.StageName = 'Proposal/Quote';
        oppty2.Renewed_From__c = oppty1.Id;
        quoteObject1.Renewed__c = true;
        insert oppty2;
        update quoteObject1;
        Quote quoteObject2 = new Quote();
        quoteObject2.Name = oppty2.Name;
        quoteObject2.OpportunityId = oppty2.Id;
        quoteObject2.Pricebook2Id = Test.getStandardPricebookId();
        quoteObject2.Renewed_From__c = quoteObject1.Id;
        insert quoteObject2;
        Test.startTest();
        Database.executeBatch(new OpportunityRenewalCorrector());
        Test.stopTest();
        oppty2 = [select closedate from opportunity where id = : oppty2.Id];
        System.assertEquals(quoteObject1.License_Support_Expiration_Date__c, oppty2.closedate);
    }

    @isTest
    static void correctorClassAnnualANDPerpetualTest(){
        Id productId = [SELECT Id FROM Product2 WHERE IsActive = true LIMIT 1].Id;
        Id priceEntryId = [SELECT Id FROM PricebookEntry WHERE Name = 'test Prod' LIMIT 1].Id;
        TriggerStopper.stopOpp = true;
        TriggerStopper.stopQuote = true;
        TriggerStopper.stopQuoteLine = true;
        TriggerStopper.stopOppLine = true;
        Quote quoteObject1 = insertQuoteAndQLI('soma', 'Annual', 1);
        Opportunity oppty1 = [select id, accountid from opportunity where id = :quoteObject1.OpportunityId LIMIT 1];
        Opportunity oppty2 = new Opportunity(Name = 'soma2');
        oppty2.CloseDate = date.today().addDays(70);
        oppty2.AccountId = oppty1.AccountId;
        oppty2.StageName = 'Proposal/Quote';
        oppty2.Renewed_From__c = oppty1.Id;
        quoteObject1.Renewed__c = true;
        insert oppty2;
        update quoteObject1;
        Quote quoteObject2 = new Quote();
        quoteObject2.Name = oppty2.Name;
        quoteObject2.OpportunityId = oppty2.Id;
        quoteObject2.Pricebook2Id = Test.getStandardPricebookId();
        quoteObject2.Renewed_From__c = quoteObject1.Id;
        insert quoteObject2;
        QuoteLineItem qli2 = new QuoteLineItem();
        qli2.QuoteId = quoteObject1.Id;
        qli2.PricebookEntryId = priceEntryId;
        qli2.Product2Id = productId;
        qli2.Quantity = 1;
        qli2.UnitPrice = 1000.0;
        qli2.License_type__c = 'Perpetual';
        insert qli2;

        Test.startTest();
        Database.executeBatch(new OpportunityRenewalCorrector());
        Test.stopTest();
        oppty2 = [select closedate from opportunity where id = : oppty2.Id];
        System.assertEquals(quoteObject1.License_Expiration_Date__c, oppty2.closedate);
    }

    @isTest
    static void correctorClassWithoutLicenseExprationDate(){
        TriggerStopper.stopOpp = true;
        TriggerStopper.stopQuote = true;
        TriggerStopper.stopQuoteLine = true;
        TriggerStopper.stopOppLine = true;
        Quote quoteObject1 = insertQuoteAndQLI('soma', 'Subscription', 1);
        Opportunity oppty1 = [select id, accountid, closedate from opportunity where id = :quoteObject1.OpportunityId LIMIT 1];
        Opportunity oppty2 = new Opportunity(Name = 'soma2');
        oppty2.CloseDate = date.today().addDays(90);
        oppty2.AccountId = oppty1.AccountId;
        oppty2.StageName = 'Proposal/Quote';
        oppty2.Renewed_From__c = oppty1.Id;
        quoteObject1.Renewed__c = true;
        quoteObject1.License_Expiration_Date__c = null;
        quoteObject1.License_Support_Expiration_Date__c = null;
        insert oppty2;
        update quoteObject1;
        Quote quoteObject2 = new Quote();
        quoteObject2.Name = oppty2.Name;
        quoteObject2.OpportunityId = oppty2.Id;
        quoteObject2.Pricebook2Id = Test.getStandardPricebookId();
        quoteObject2.Renewed_From__c = quoteObject1.Id;
        insert quoteObject2;
        Test.startTest();
        Database.executeBatch(new OpportunityRenewalCorrector());
        Test.stopTest();
        oppty2 = [select closedate from opportunity where id = : oppty2.Id];
        System.assertEquals(Date.today().addDays(30).addYears(1), oppty2.closedate);
    }


    private static Quote insertQuoteAndQLI(String name, String licenseType, Integer i){
        Id pricebookID = Test.getStandardPricebookId();
        Id productId = [SELECT Id FROM Product2 WHERE IsActive = true LIMIT 1].Id;
        Id priceEntryId = [SELECT Id FROM PricebookEntry WHERE Name = 'test Prod' LIMIT 1].Id;

        Account acc = new Account();
        acc.Name = name + String.valueOf(i);
        acc.BillingPostalCode = '1234';
        acc.BillingCity = 'Budapest';
        acc.BillingStreet = 'Kossut Lajos utca ' + i;
        acc.BillingCountry = 'Hungary';
        acc.Copy_Billing_Address_to_Shipping_Address__c = true;
        insert acc;

        Opportunity oppty = new Opportunity(Name = acc.Name);
        oppty.CloseDate = date.today().addDays(30);
        oppty.AccountId = acc.Id;
        oppty.StageName = 'Proposal/Quote';
        insert oppty;

        Quote quoteObject = new Quote();
        quoteObject.Name = oppty.Name;
        quoteObject.OpportunityId = oppty.Id;
        quoteObject.Invoice_Date__c = date.today();
        quoteObject.Pricebook2Id = pricebookID;
        quoteObject.License_Expiration_Date__c = Date.today().addDays(35);
        quoteObject.License_Support_Expiration_Date__c = Date.today().addDays(45);
        insert quoteObject;

        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = quoteObject.Id;
        qli.PricebookEntryId = priceEntryId;
        qli.Product2Id = productId;
        qli.Quantity = 1;
        qli.UnitPrice = 1000.0;
        qli.License_type__c = licenseType;
        insert qli;

        return quoteObject;
    }


    private static void makeBulkData(Integer count){

        id pricebookID = Test.getStandardPricebookId();
        Id productId = [SELECT Id FROM Product2 WHERE IsActive = true LIMIT 1].Id;
        Id product2Id = [SELECT Id FROM Product2 WHERE IsActive = false LIMIT 1].Id;
        Id priceEntryId = [SELECT Id FROM PricebookEntry WHERE Name = 'test Prod' LIMIT 1].Id;
        Id priceEntry2Id = [SELECT Id FROM PricebookEntry WHERE Name = 'test Prod2' LIMIT 1].Id;

        List<Account> accountList = new List<Account>();
        List<Opportunity> opptyList = new List<Opportunity>();
        List<Quote> quoteList = new List<Quote>();
        List<QuoteLineItem> qliList = new List<QuoteLineItem>();
        for(Integer i = 0; i < count; i++){
            Account acc = new Account(Name='soma '+ String.valueOf(i));
            acc.BillingPostalCode = '1234';
            acc.BillingCity = 'Budapest';
            acc.BillingStreet = 'Kossut Lajos utca ' + i;
            acc.BillingCountry = 'Hungary';
            acc.Copy_Billing_Address_to_Shipping_Address__c = true;
            if(i == 10){
                acc.Name = 'Perpetual Test';
            }else if(i == 11){
                acc.Name = 'Subscription Test';
            }else if(i == 32){
                acc.Name = 'Inactive Product Test';
            }
            accountList.add(acc);
        }
        insert accountList;
        for(Account acc : accountList){
            Opportunity oppty = new Opportunity(Name = acc.Name);
            oppty.CloseDate = date.today().addDays(30);
            oppty.AccountId = acc.Id;
            oppty.StageName = 'Proposal/Quote';
            opptyList.add(oppty);
        }
        insert opptyList;
        Integer counter = 0;
        for(Opportunity oppty : opptyList){
            Quote quoteObject = new Quote();
            quoteObject.Name = oppty.Name;
            quoteObject.OpportunityId = oppty.Id;
            quoteObject.Invoice_Date__c = date.today();
            quoteObject.Pricebook2Id = pricebookID;
            if(Math.mod(counter, 2) == 0){
                quoteObject.License_Expiration_Date__c = Date.today().addDays(35);
                quoteObject.License_Support_Expiration_Date__c = Date.today().addDays(12);
            }else{
                quoteObject.License_Expiration_Date__c = Date.today().addDays(111);
                quoteObject.License_Support_Expiration_Date__c = Date.today().addDays(47);
            }
            quoteList.add(quoteObject);
        }
        insert quoteList;
        counter = 0;
        for(Quote quoteObject : quoteList){
            QuoteLineItem qli = new QuoteLineItem();
            qli.QuoteId = quoteObject.Id;
            qli.PricebookEntryId = priceEntryId;
            qli.Product2Id = productId;
            qli.Quantity = 1;
            qli.UnitPrice = 1000.0;
            qli.License_type__c = 'Annual';
            if(quoteObject.Name == 'Perpetual Test') qli.License_type__c = 'Perpetual';
            else if(quoteObject.Name == 'Subscription Test') qli.License_type__c = 'Subscription';
            if(counter++ >= 30){ 
                qli.License_type__c = 'Consulting'; //make some negative test cases
                if(quoteObject.Name == 'Inactive Product Test'){
                    qli.PricebookEntryId = priceEntry2Id; //oppty and quote will be created, but qli and oli not
                    qli.Product2Id = product2Id;
                    qli.License_type__c = 'Perpetual';
                }
            }
            qliList.add(qli);
        }
        insert qliList;
    }


}
