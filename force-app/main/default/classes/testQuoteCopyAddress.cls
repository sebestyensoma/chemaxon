@isTest
private class testQuoteCopyAddress {

	private static Account acc;
	private static Opportunity opp;
	private static Quote q;

	private static void createObjects() {
		acc = new Account(
			Name = 'Test Account',
			Phone = '1234567890',
			Tax_Number__c = '123456789',
			ShippingCountry = 'Norway',
			ShippingCity = 'Oslo',
			ShippingStreet = 'St. Olavs Gate 2',
			ShippingPostalCode = '0165',
			BillingCountry = 'Norway',
			BillingCity = 'Oslo',
			BillingStreet = 'St. Olavs Gate 2',
			BillingPostalCode = '0165'
		);
		insert acc;
		
		opp = new Opportunity(
			Name = 'Test Opp',
			AccountId = acc.id,
			CloseDate = Date.today(),
			StageName = 'Prospecting'
		);
		insert opp;
		
		q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.id,
			ShippingCountry = 'Hungary',
			ShippingCity = 'Budapest',
			ShippingStreet = 'Madách Imre út 13/14',
			ShippingPostalCode = '1075',
			BillingCountry = 'Hungary',
			BillingCity = 'Budapest',
			BillingStreet = 'Madách Imre út 13/14',
			BillingPostalCode = '1075'
		);
	}

    static testMethod void insertAndCopy() {
    	createObjects();
    	
    	q.Copy_Billing__c = true;
    	q.Copy_Shipping__c = true;
		q.Invoice_Type__c = 'Kft';
    	insert q;
    	
    	acc = [ SELECT ShippingCountry, BillingCountry FROM Account WHERE Id = :acc.Id ];
    	System.assertEquals(acc.ShippingCountry, 'Norway');
    	System.assertEquals(acc.BillingCountry, 'Norway');
    }

    static testMethod void insertAndCopyOne() {
    	createObjects();
    	
    	q.Copy_Billing__c = true;
    	q.Copy_Shipping__c = false;
		q.Invoice_Type__c = 'Kft';
    	insert q;
    	
    	acc = [ SELECT ShippingCountry, BillingCountry FROM Account WHERE Id = :acc.Id ];
    	System.assertEquals(acc.BillingCountry, 'Norway');
    	System.assertEquals(acc.ShippingCountry, 'Norway');
    }

    static testMethod void insertAndDontCopy() {
    	createObjects();
    	
    	q.Copy_Billing__c = false;
    	q.Copy_Shipping__c = false;
		q.Invoice_Type__c = 'Kft';
    	insert q;
    	
    	acc = [ SELECT ShippingCountry, BillingCountry FROM Account WHERE Id = :acc.Id ];
    	System.assertEquals(acc.ShippingCountry, 'Norway');
    	System.assertEquals(acc.BillingCountry, 'Norway');
    }

    static testMethod void updateAndCopy() {
    	createObjects();
    	
    	q.Copy_Billing__c = false;
    	q.Copy_Shipping__c = false;
		q.Invoice_Type__c = 'Kft';
    	insert q;
    	
    	q.Copy_Billing__c = true;
    	q.Copy_Shipping__c = true;
    	update q;
    	
    	acc = [ SELECT ShippingCountry, BillingCountry FROM Account WHERE Id = :acc.Id ];
    	System.assertEquals(acc.ShippingCountry, 'Norway');
    	System.assertEquals(acc.BillingCountry, 'Norway');
    }

    static testMethod void updateAndDontCopy() {
    	createObjects();
    	
    	q.Copy_Billing__c = true;
    	q.Copy_Shipping__c = true;
		q.Invoice_Type__c = 'Kft';
    	insert q;
    	
    	q.BillingCountry = 'Hungary';
    	q.ShippingCountry = 'Hungary';
    	q.Copy_Billing__c = false;
    	q.Copy_Shipping__c = false;
    	update q;
    	
    	acc = [ SELECT ShippingCountry, BillingCountry FROM Account WHERE Id = :acc.Id ];
    	System.assertEquals(acc.ShippingCountry, 'Norway');
    	System.assertEquals(acc.BillingCountry, 'Norway');
    }
}