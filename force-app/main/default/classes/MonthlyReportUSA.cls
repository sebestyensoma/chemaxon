public with sharing class MonthlyReportUSA {

	private static final String DEFAULT_CURRENCY = 'USD';
	private static final String[] currencies { get; set; }
	public static final String[] monthNames { get; set; }
	static {
		monthNames = new String[] { 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' };
		currencies = new String[] { 'USD', 'HUF', 'EUR', 'GBP', 'CHF', 'JPY', 'SGD' };
	}
	
	public String mode { get; set; }
	public String currencyCode { get; set; }
	public Boolean reportReady { get; set; }
	public List<SelectOption> currencyOptions { get; set; }
	
	public Integer years { get; set; }
	public List<Integer> yearNums { get; set; }
	public List<Integer> yearLabels { get; set; }
	public List<Boolean> yearBanding { get; set; }
	
	// Raw data objects
	// They map MONTHS to lists of figures by YEAR
	public Map<String, String[]> annuals { get; set; }
	public Map<String, String[]> perpetuals { get; set; }
	public Map<String, String[]> totals { get; set; }
	public Map<String, String[]> midYearSums { get; set; }
	public Map<String, Decimal[]> differences { get; set; }
	public Map<String, Integer[]> newClients { get; set; }
	public Integer[] newClientsTotal { get; set; }

	public void init() {
		currencyOptions = new List<SelectOption>();
		for (String curr : currencies) {
			currencyOptions.add(new SelectOption(curr, curr));
		}
		
		currencyCode = ApexPages.currentPage().getParameters().get('currency');
		if (currencyCode == null) currencyCode = DEFAULT_CURRENCY;
		
		// Initialize containers
		Integer maxYear = Date.today().year();
		years = maxYear - 2000 + 1;
		yearNums = new Integer[years];
		yearLabels = new Integer[years];
		yearBanding = new Boolean[years];
		for (Integer i = 0; i < years; i++) {
			yearNums[i] = i;
			yearLabels[years - i - 1] = 2000 + i;
			yearBanding[i] = (Math.mod(i, 2) == 0);
		}
		
		reportReady = false;
		attemptReport();
	}
	
	public void attemptReport() {
		if (mode == null || currencyCode == null) return;
		
		if (mode == 'all' || mode == 'consulting') doReportAll();
		if (mode == 'new') doReportAP();
		if (mode == 'renewal') doReportAP();
		
		reportReady = true;
	}
	
	// REPORTS
	
	private void doReportAll() {
		String sql = 
			'SELECT calendar_year(quote.invoice_date__c) year, calendar_month(quote.invoice_date__c) month, sum(total_in_' + currencyCode + '__c) total'
			+ ' FROM QuoteLineItem'
			+ ' WHERE Quote.Opportunity.Account.BillingCountry = \'United States of America\' AND Quote.Invoice_Date__c != null AND Total_in_' + currencyCode + '__c != null #LICENSE_FILTER'
			+ ' GROUP BY calendar_year(quote.invoice_date__c), calendar_month(quote.invoice_date__c)'
			+ ' ORDER BY calendar_year(quote.invoice_date__c) DESC, calendar_month(quote.invoice_date__c) ASC';
		
		if (mode == 'all') {
			sql = sql.replace('#LICENSE_FILTER', '');
		}
		else {
			sql = sql.replace('#LICENSE_FILTER', ' AND License_type__c = \'Consulting\'');
		}
		
		AggregateResult[] allRows = Database.query(sql);
		
		// Populate decimal amounts
		List<List<Decimal>> dTotals = new List<List<Decimal>>();
		List<List<Decimal>> dMidYears = new List<List<Decimal>>();
		List<List<Decimal>> dDiffs = new List<List<Decimal>>();
		for (Integer y = 0; y < years; y++) {
			dTotals.add(new Decimal[12]);
		}
		
		for (AggregateResult row : allRows) {
			Integer yearNum = (Integer)row.get('year') - 2000;
			Integer monthNum = (Integer)row.get('month') - 1;
			Decimal total = (Decimal)row.get('total');
			
			dTotals[yearNum][monthNum] = total;
		}
		allRows.clear();
		allRows = null;
		
		// Calculate mid-year sums and differences
		for (Integer y = 0; y < years; y++) {
			dMidYears.add(createMidYearSums(dTotals[y]));
			if (y > 0) {
				dDiffs.add(createDifferences(dMidYears[y], dMidYears[y-1]));
			}
			else {
				dDiffs.add(new Decimal[12]);
			}
		}
		
		// Reverse, transpose and format
		totals = new Map<String, String[]>();
		midYearSums = new Map<String, String[]>();
		differences = new Map<String, Decimal[]>();
		for (Integer m = 0; m < 12; m++) {
			String monthName = monthNames[m];
			String[] totalRow = new String[years];
			String[] midYearRow = new String[years];
			Decimal[] diffRow = new Decimal[years];
			
			for (Integer y = 0; y < years; y++) {
				Integer tpYear = years - y - 1;
				
				totalRow[y] = formatDecimal(dTotals[tpYear][m]);
				midYearRow[y] = formatDecimal(dMidYears[tpYear][m]);
				diffRow[y] = (dDiffs[tpYear][m] != null) ? dDiffs[tpYear][m] : 0;
			}
			
			totals.put(monthName, totalRow);
			midYearSums.put(monthName, midYearRow);
			differences.put(monthName, diffRow);
		}
		
		dTotals.clear();
		dMidYears.clear();
		dDiffs.clear();
		
		getNewClients();
	}
	
	private void doReportAP() {
		String licenseTypes;
		if (mode == 'new') licenseTypes = '\'Annual\', \'Perpetual\'';
		if (mode == 'renewal') licenseTypes = '\'Annual Renewal\', \'Perpetual Renewal\'';
		
		AggregateResult[] allRows = Database.query(
			'SELECT calendar_year(quote.invoice_date__c) year, calendar_month(quote.invoice_date__c) month, sum(total_in_' + currencyCode + '__c) total, License_Type__c'
			+ ' FROM QuoteLineItem'
			+ ' WHERE Quote.Opportunity.Account.BillingCountry = \'United States of America\' AND Quote.Invoice_Date__c != null'
				+ ' AND Total_in_' + currencyCode + '__c != null'
				+ ' AND License_Type__c IN (' + licenseTypes + ')'
			+ ' GROUP BY calendar_year(quote.invoice_date__c), calendar_month(quote.invoice_date__c), License_Type__c'
			+ ' ORDER BY calendar_year(quote.invoice_date__c) DESC, calendar_month(quote.invoice_date__c) ASC, License_Type__c ASC'
		);
	
		// Populate decimal amounts
		List<List<Decimal>> dAnnuals = new List<List<Decimal>>();
		List<List<Decimal>> dPerpetuals = new List<List<Decimal>>();
		List<List<Decimal>> dTotals = new List<List<Decimal>>();
		List<List<Decimal>> dMidYears = new List<List<Decimal>>();
		List<List<Decimal>> dDiffs = new List<List<Decimal>>();
		for (Integer y = 0; y < years; y++) {
			dAnnuals.add(new Decimal[12]);
			dPerpetuals.add(new Decimal[12]);
			dTotals.add(new Decimal[12]);
		}
		
		for (AggregateResult row : allRows) {
			Integer yearNum = (Integer)row.get('year') - 2000;
			Integer monthNum = (Integer)row.get('month') - 1;
			Decimal total = (Decimal)row.get('total');
			
			String lType = (String)row.get('License_Type__c');
			if (lType == 'Annual' || lType == 'Annual Renewal') dAnnuals[yearNum][monthNum] = total;
			if (lType == 'Perpetual' || lType == 'Perpetual Renewal') dPerpetuals[yearNum][monthNum] = total;
			
			if (dTotals[yearNum][monthNum] == null) dTotals[yearNum][monthNum] = 0;
			dTotals[yearNum][monthNum] += total;
		}
		allRows.clear();
		allRows = null;
		
		// Calculate mid-year sums and differences
		for (Integer y = 0; y < years; y++) {
			dMidYears.add(createMidYearSums(dTotals[y]));
			
			if (y > 0) {
				dDiffs.add(createDifferences(dMidYears[y], dMidYears[y-1]));
			}
			else {
				dDiffs.add(new Decimal[12]);
			}
		}
		
		// Reverse, transpose and format
		annuals = new Map<String, String[]>();
		perpetuals = new Map<String, String[]>();
		totals = new Map<String, String[]>();
		midYearSums = new Map<String, String[]>();
		differences = new Map<String, Decimal[]>();
		for (Integer m = 0; m < 12; m++) {
			String monthName = monthNames[m];
			String[] annualRow = new String[years];
			String[] perpetualRow = new String[years];
			String[] totalRow = new String[years];
			String[] midYearRow = new String[years];
			Decimal[] diffRow = new Decimal[years];
			
			for (Integer y = 0; y < years; y++) {
				Integer tpYear = years - y - 1;
				
				annualRow[y] = formatDecimal(dAnnuals[tpYear][m]);
				perpetualRow[y] = formatDecimal(dPerpetuals[tpYear][m]);
				totalRow[y] = formatDecimal(dTotals[tpYear][m]);
				midYearRow[y] = formatDecimal(dMidYears[tpYear][m]);
				diffRow[y] = (dDiffs[tpYear][m] != null) ? dDiffs[tpYear][m] : 0;
			}
			
			annuals.put(monthName, annualRow);
			perpetuals.put(monthName, perpetualRow);
			totals.put(monthName, totalRow);
			midYearSums.put(monthName, midYearRow);
			differences.put(monthName, diffRow);
		}
		
		dAnnuals.clear();
		dPerpetuals.clear();
		dTotals.clear();
		dMidYears.clear();
		dDiffs.clear();
		
		getNewClients();
	}

	// HELPER METHODS
	
	private Decimal[] createMidYearSums(Decimal[] year) {
		Decimal[] midYearSums = new Decimal[12];
		Decimal sum = 0;
		for (Integer m = 0; m < 12; m++) {
			if (year[m] == null) year[m] = 0;
			sum += year[m];
			midYearSums[m] = sum;
		}
		
		return midYearSums;
	}
	
	private Decimal[] createDifferences(Decimal[] thisYear, Decimal[] lastYear) {
		Decimal[] diffs = new Decimal[12];
		for (Integer m = 0; m < 12; m++) {
			Decimal diff = (lastYear[m] != 0) ? (100.0 * (thisYear[m] - lastYear[m]) / lastYear[m]) : 0;
			diffs[m] = diff.round(RoundingMode.HALF_UP);
		}
		return diffs;
	}
	
	private void getNewClients() {
		List<List<Integer>> iNewClients = new List<List<Integer>>();
		newClientsTotal = new List<Integer>();
		for (integer i = 0; i < years; i++) {
			Integer[] clientsYear = new Integer[12];
			for (integer m = 0; m < 12; m++) clientsYear[m] = 0;
			iNewClients.add(clientsYear);
			newClientsTotal.add(0);
		}
		
		for (Account acc : [ SELECT CreatedDate FROM Account WHERE BillingCountry = 'United States of America' ]) {
			Integer year = acc.CreatedDate.year();
			Integer month = acc.CreatedDate.month() - 1;
			iNewClients[year - 2000][month] += 1;
			newClientsTotal[years - (year - 2000) - 1] += 1;
		}
		
		newClients = new Map<String, Integer[]>();
		for (Integer m = 0; m < 12; m++) {
			String monthName = monthNames[m];
			Integer[] clientsRow = new Integer[years];
			
			for (Integer y = 0; y < years; y++) {
				Integer tpYear = years - y - 1;
				clientsRow[y] = iNewClients[tpYear][m];
			}
			
			newClients.put(monthName, clientsRow);
		}
		iNewClients.clear();
	}
	
	/** Format a Decimal in "100 000 000" format */
	private static String formatDecimal(Decimal n) {
		if (n == null) return '0';
		
		String base = String.valueOf(Math.abs(n.round(RoundingMode.HALF_UP)));
		String result = '';
		Integer copyTo = base.length();
		Integer copyFrom = copyTo - 3;
		if (copyFrom < 0) copyFrom = 0;
		
		while (copyTo > 0) {
			result = base.substring(copyFrom, copyTo) + ' ' + result;
			copyFrom -= 3;
			copyTo -=3;
			if (copyFrom < 0) copyFrom = 0;
		}
		
		if (n < 0) result = '-' + result;
		return result;
	}
}