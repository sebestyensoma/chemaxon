public with sharing class LLCContractController {
	public Account myAccount{get;set;}
	public List<Package__c> myPackages{get;set;}
	public List<Product2> myProducts{get;set;}
	public List<QuoteLineItem> myProducts2{get;set;}
	public List<QuoteLineItem> myQLIs{get;set;}
	//public List<Quote> myQuotes{get;set;}
	public List<Quote> myQuotes = new List<Quote>();
	public List<Product2> productCategories{get;set;}
	
	public boolean hasAnnualLicense{get;set;}
	public boolean hasPerpetualLicense{get;set;}
	public boolean subscr{get;set;}
	
	public decimal numberOfAnnualLicenses{get;set;}
	public decimal numberOfPerpetualLicenses{get;set;}
	public decimal taxRate{get;set;}
	
	public string productsList{get;set;}
	public string conditionsOfUsageList{get;set;}
	
	public string blankLinesBeforePaymentInstructions{get;set;}
	public string blankLinesBeforeProductsList{get;set;}
	
	//public Map<string, List<QLIandProduct>> licenseTypeQLIandProductMap = new Map<string, List<QLIandProduct>>();
	public Map<string, List<QLIandProduct>> licenseTypeQLIandProductMap{get;set;}
	
	public Map<string, List<QLIandProduct>> userNumberQLIandProductMap{get;set;} 
	
	public Map<String/*adó*/, Decimal/*összeg*/> taxMap{get;set;} 
	public Decimal grandTotal{get;set;}

	public Class QLIandProduct{
		public QuoteLineItem myQLI{get;set;}
		public Product2 myProduct{get;set;}
		public QLIandProduct(QuoteLineItem inQLI,Product2 inProduct){
			myQLI=inQLI;
			myProduct=inProduct;
		}
	} 	
	
	public List<QLIandProduct> myQLIandProducts {get;set;}
	
	public void onStart(){
		numberOfAnnualLicenses = 0;
		numberOfPerpetualLicenses = 0;
		subscr = false;

		Id quoteId = Apexpages.currentPage().getParameters().get('qid');
		
		myQuotes = [
			SELECT 
				Tax, BillingName,ShippingName,CurrencyIsoCode,Id,OpportunityId,TotalPrice,Description,Invoice_Display_Reference_to__c,Invoice_display_payment_due_days__c,
				Invoice_display_products__c,Invoice_display_Visible_CoU__c,Invoice_display_Visible_CoP__c,Invoice_display_Visible_MoD__c,Invoice_display_Visible_Productslist__c,
				Invoice_Display_Blank_Lines_Before_PI__c,Invoice_Display_Blank_Lines_Before_PL__c,Grand_Total_rounded__c,License_Expiration_Date__c,License_Support_Expiration_Date__c,
				Invoice_Display_Additional_LLC_Info__c,Invoice_Date__c,Invoice_No__c, PO_No__c,Invoice_Type__c,Grand_Total_wTax__c,Subtotal,
				BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,
				ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet
			FROM Quote 
			WHERE Id = :quoteId
		];
		
		Map<Id,Package__c> mapIdToPackage = new Map<Id,Package__c>([SELECT Package_Type__c,User_Limit__c,Target_Price__c FROM Package__c WHERE Quote__c = :quoteid]);
		
		Map<Id,QuoteLineItem> mapIdToQLIs = 
			new Map<Id,QuoteLineItem>([SELECT																				
				User_Limit__c,
				Quote_display_quantity__c,
				License_type__c,
				ListPrice,
				Package__c,
				Quantity,
				Subtotal,
				TotalPrice,
				UnitPrice,
				QuoteId,
				PricebookEntryId
			FROM 
				QuoteLineItem 
			WHERE 
				QuoteId= :quoteId
			AND
				Package__c=null			
			ORDER BY Id
			]);
		
		if(mapIdToQLIs!=null && mapIdToQLIs.size()>0){
			myQLIs=mapIdToQLIs.values();
		}
		
		Map<Id,PricebookEntry> mapIdToPBEntrys = new Map<Id,PricebookEntry>([SELECT Id,Product2Id FROM PricebookEntry WHERE ID IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE ID IN :mapIdToQLIs.keySet())]);															
		productCategories = [SELECT Name,Family  FROM Product2 WHERE ID IN (SELECT Product2Id FROM PricebookEntry WHERE ID IN :mapIdToPBEntrys.keySet()) AND Family !=null ORDER BY Family ];
			
		for(Integer i=1;i<productCategories.size();i++){
			if(productCategories[i].Family  == productCategories[i-1].Family){
				productCategories.remove(i);
				i=i-1;
			}
		}
		
		MAP<Id,Product2> mapIdToProduct = new MAP<Id,Product2>([SELECT Name FROM Product2 WHERE ID IN (SELECT Product2Id FROM PricebookEntry WHERE ID IN :mapIdToPBEntrys.keySet())]);
		
		MAP<Id,QuoteLineItem> mapIdToProduct2 = new MAP<Id,QuoteLineItem>([SELECT Id, PricebookEntry.Product2.Name, License_type__c, User_limit__c, Quote_display_quantity__c FROM QuoteLineItem WHERE QuoteId = :quoteId]);
		
		if(mapIdToProduct2!=null && mapIdToProduct2.size()>0){
			myProducts2=mapIdToProduct2.values();
		}
		
		myQLIandProducts = new List<QLIandProduct>();
		
		licenseTypeQLIandProductMap = new Map<string, List<QLIandProduct>>();
		userNumberQLIandProductMap = new Map<string, List<QLIandProduct>>();
		
		for(QuoteLineItem loopQLI:mapIdToQLIs.values()){
			if(mapIdToProduct.containsKey(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)){
				myQLIandProducts.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
				
				// licenseTypeQLIandProductMap
				
				if(!licenseTypeQLIandProductMap.containsKey(loopQLI.License_type__c))
				{
					Set<string> tempLicenseTypeSet = new Set<string>();
					
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
					
					tempLicenseTypeSet.add('Annual');
					tempLicenseTypeSet.add('Annual Renewal');
					tempLicenseTypeSet.add('Perpetual');
					tempLicenseTypeSet.add('Perpetual Renewal');
					
					system.debug(tempList);
					
					if(tempLicenseTypeSet.contains(loopQLI.License_type__c))
					{
						licenseTypeQLIandProductMap.put(loopQLI.License_type__c, tempList);
					}
					else
					{
						licenseTypeQLIandProductMap.put(loopQLI.License_type__c + loopQLI.Quote_display_quantity__c, tempList);
					}
				}
				else
				{
					Set<string> tempLicenseTypeSet = new Set<string>();
					
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					// tempList = licenseTypeQLIandProductMap.get(loopQLI.License_type__c);
					
					tempLicenseTypeSet.add('Annual');
					tempLicenseTypeSet.add('Annual Renewal');
					tempLicenseTypeSet.add('Perpetual');
					tempLicenseTypeSet.add('Perpetual Renewal');
					
					system.debug(tempList);
					
					// tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
					
					system.debug(tempList);
					
					//licenseTypeQLIandProductMap.put(loopQLI.License_type__c, tempList);
					
					if(tempLicenseTypeSet.contains(loopQLI.License_type__c))
					{
						tempList = licenseTypeQLIandProductMap.get(loopQLI.License_type__c);
						tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
						
						licenseTypeQLIandProductMap.put(loopQLI.License_type__c, tempList);
					}
					else
					{
						tempList = licenseTypeQLIandProductMap.get(loopQLI.License_type__c + loopQLI.Quote_display_quantity__c);
						tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
						
						licenseTypeQLIandProductMap.put(loopQLI.License_type__c + loopQLI.Quote_display_quantity__c, tempList);
					}
				}
				
				productsList = '';
				Set<String> productsListItems = new Set<String>();
				
				for(String tempKey : licenseTypeQLIandProductMap.keySet())
				{
					Set<string> tempKeySet = new Set<string>();
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					tempList = licenseTypeQLIandProductMap.get(tempKey);
					
					tempKeySet.add('Annual');
					tempKeySet.add('Annual Renewal');
					tempKeySet.add('Perpetual');
					tempKeySet.add('Perpetual Renewal');
					
					if(tempKeySet.contains(tempKey))
					{
						if(tempKey == 'Perpetual Renewal')
						{
							tempKey = 'Annual Maintenance Renewal';
						}
						
						productsList += tempKey + ' license for ';
							
						for(QLIandProduct tempQLIandProduct : tempList)
						{
							productsListItems.add(tempQLIandProduct.myProduct.Name);
						}
					}
					else
					{
						for(QLIandProduct tempQLIandProduct : tempList)
						{
							productsListItems.add(tempQLIandProduct.myProduct.Name + ' ' + tempQLIandProduct.myQLI.Quote_display_quantity__c);
						}
					}
					
					List<String> sortedProducts = new List<String>(productsListItems);
					sortedProducts.sort();
					
					productsList += String.join(sortedProducts, ', ');
					productsList += '.<br/>';
					
					productsListItems.clear();
				}
				
				// userNumberQLIandProductMap
				
				if(!userNumberQLIandProductMap.containsKey(loopQLI.Quote_display_quantity__c))
				{
					Set<string> tempLicenseTypeSet = new Set<string>();
					
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
					
					tempLicenseTypeSet.add('Annual');
					tempLicenseTypeSet.add('Annual Renewal');
					tempLicenseTypeSet.add('Perpetual');
					tempLicenseTypeSet.add('Perpetual Renewal');
					
					system.debug(tempList);
					
					if(tempLicenseTypeSet.contains(loopQLI.License_type__c))
					{
						userNumberQLIandProductMap.put(loopQLI.Quote_display_quantity__c, tempList);
					}
				}
				else
				{
					Set<string> tempLicenseTypeSet = new Set<string>();
					
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					tempList = userNumberQLIandProductMap.get(loopQLI.Quote_display_quantity__c);
					
					tempLicenseTypeSet.add('Annual');
					tempLicenseTypeSet.add('Annual Renewal');
					tempLicenseTypeSet.add('Perpetual');
					tempLicenseTypeSet.add('Perpetual Renewal');
					
					system.debug(tempList);
					
					tempList.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
					
					system.debug(tempList);
					
					//licenseTypeQLIandProductMap.put(loopQLI.License_type__c, tempList);
					
					if(tempLicenseTypeSet.contains(loopQLI.License_type__c))
					{
						userNumberQLIandProductMap.put(loopQLI.Quote_display_quantity__c, tempList);
					}
				}
				
				conditionsOfUsageList = '';
				Set<String> conditionsItems = new Set<String>();
				
				for(String tempKey : userNumberQLIandProductMap.keySet())
				{
					List<QLIandProduct> tempList = new List<QLIandProduct>();
					tempList = userNumberQLIandProductMap.get(tempKey);
						
					for(QLIandProduct tempQLIandProduct : tempList)
					{
						conditionsItems.add(tempQLIandProduct.myProduct.Name);
					}
					
					List<String> sortedProducts = new List<String>(conditionsItems);
					sortedProducts.sort();
					
					conditionsOfUsageList += String.join(sortedProducts, ', ');
					conditionsOfUsageList += 
						' ' + 
						' may be accessed and used by up to ' +
						tempKey;
					conditionsOfUsageList += '.<br/>';
					
					conditionsItems.clear();
				}
				
				if
				(
					loopQLI.License_type__c != NULL &&
					loopQLI.License_type__c != ''
				)
				{
					if(loopQLI.License_type__c.contains('Annual'))
					{
						hasAnnualLicense = true;
						numberOfAnnualLicenses++;
					}
					
					if(loopQLI.License_type__c.contains('Perpetual'))
					{
						hasPerpetualLicense = true;
						numberOfPerpetualLicenses++;
					}
				}
			}

			if(loopQLI.License_type__c == 'Subscription') subscr = true;
		}
		
		Map<Id,Account> mapIdToAccount = new Map<Id,Account>([
			SELECT Name, BillingCity,BillingCountry,BillingState,BillingPostalCode,BillingStreet,ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet
			FROM Account 
			WHERE ID IN (SELECT AccountId FROM Opportunity WHERE ID = :myQuotes[0].OpportunityId)
		]);
			if(mapIdToAccount!=null && mapIdToAccount.size()>0){
				myAccount= mapIdToAccount.values()[0];
		}
		myPackages = [SELECT Package_Type__c,User_Limit__c FROM Package__c WHERE Quote__c=:quoteId];


		///////////////////////////////////////////////
		//taxMap = new Map<String, Decimal>();
		//AggregateResult[] groupedResults = 
			//[
				//SELECT sum(AVA_SFCLOUD__SalesTax_Line__c) ossz, AVA_SFCLOUD__Sales_Tax_Rate__c ado 
				//FROM QuoteLineItem 
				//WHERE QuoteId = :quoteId AND AVA_SFCLOUD__Sales_Tax_Rate__c != null 
				//GROUP BY AVA_SFCLOUD__Sales_Tax_Rate__c
			//];
		
		grandTotal = 0;	
		//for (AggregateResult ar : groupedResults)  {
			//Decimal ossz = (Decimal)ar.get('ossz');
			//taxMap.put((String)ar.get('ado'),ossz );
			//grandTotal += ossz;
		//}

		//grandTotal += myQuotes[0].Grand_Total_wTax__c;

		if(myQuotes[0].Tax == null) myQuotes[0].Tax = 0;
		taxRate = myQuotes[0].Tax / myQuotes[0].Subtotal * 100;

		grandTotal = myQuotes[0].Subtotal + myQuotes[0].Tax;
	}
	
	public List<Quote> getMyQuotes()
	{
		for(Quote quote : myQuotes)
		{
			if(quote.Invoice_Display_Additional_LLC_Info__c != NULL && quote.Invoice_Display_Additional_LLC_Info__c != '')
			{
				quote.Invoice_Display_Additional_LLC_Info__c = quote.Invoice_Display_Additional_LLC_Info__c.replaceAll('\n', '<br/>');
			}

			if(quote.Invoice_display_products__c != NULL && quote.Invoice_display_products__c != '')
			{
				quote.Invoice_display_products__c = quote.Invoice_display_products__c.replaceAll('\n', '<br/>');
			}
			
			if(quote.Invoice_Display_Blank_Lines_Before_PI__c == NULL || quote.Invoice_Display_Blank_Lines_Before_PI__c == 0)	
			{
				blankLinesBeforePaymentInstructions = '';
			}
			else
			{
				blankLinesBeforePaymentInstructions = '';
				
				for(integer i=0; i<quote.Invoice_Display_Blank_Lines_Before_PI__c; i++)
				{
					blankLinesBeforePaymentInstructions += '<br/>';
				}
			}
			
			if(quote.Invoice_Display_Blank_Lines_Before_PL__c == NULL || quote.Invoice_Display_Blank_Lines_Before_PL__c == 0)	
			{
				blankLinesBeforeProductsList = '';
			}
			else
			{
				blankLinesBeforeProductsList = '';
				
				for(integer i=0; i<quote.Invoice_Display_Blank_Lines_Before_PL__c; i++)
				{
					blankLinesBeforeProductsList += '<br/>';
				}
			}
		}
		
		return myQuotes;
	}
}

/*
public with sharing class LLCContractController {
	public Account myAccount{get;set;}
	public List<Package__c> myPackages{get;set;}
	public List<Product2> myProducts{get;set;}
	public List<QuoteLineItem> myQLIs{get;set;}
	//public List<Quote> myQuotes{get;set;}
	public List<Quote> myQuotes = new List<Quote>();
	public List<Product2> productCategories{get;set;}
	
	public Class QLIandProduct{
		public QuoteLineItem myQLI{get;set;}
		public Product2 myProduct{get;set;}
		public QLIandProduct(QuoteLineItem inQLI,Product2 inProduct){
			myQLI=inQLI;
			myProduct=inProduct;
		}
	} 	
	
	public List<QLIandProduct> myQLIandProducts {get;set;}
	
	public void onStart(){
		Id quoteId = Apexpages.currentPage().getParameters().get('qid');
		
		myQuotes = [SELECT Id,OpportunityId,TotalPrice,Description,Additional_LLC_Information__c,Invoice_Date__c,Invoice_No__c, PO_No__c,Invoice_Type__c FROM Quote WHERE Id = :quoteId];
		
		Map<Id,Package__c> mapIdToPackage = new Map<Id,Package__c>([SELECT Package_Type__c,User_Limit__c,Target_Price__c FROM Package__c WHERE Quote__c = :quoteid]);
		
		Map<Id,QuoteLineItem> mapIdToQLIs = 
			new Map<Id,QuoteLineItem>([SELECT																				
				User_Limit__c, 
				ListPrice,
				Package__c,
				Quantity,
				Subtotal,
				TotalPrice,
				UnitPrice,
				QuoteId,
				PricebookEntryId
			FROM 
				QuoteLineItem 
			WHERE 
				QuoteId= :quoteId
			AND
				Package__c=null			
			ORDER BY Id
			]);
		
		if(mapIdToQLIs!=null && mapIdToQLIs.size()>0){
			myQLIs=mapIdToQLIs.values();
		}
		
		Map<Id,PricebookEntry> mapIdToPBEntrys = new Map<Id,PricebookEntry>([SELECT Id,Product2Id FROM PricebookEntry WHERE ID IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE ID IN :mapIdToQLIs.keySet())]);															
		productCategories = [SELECT Name,Family  FROM Product2 WHERE ID IN (SELECT Product2Id FROM PricebookEntry WHERE ID IN :mapIdToPBEntrys.keySet()) AND Family !=null ORDER BY Family ];
			
		for(Integer i=1;i<productCategories.size();i++){
			if(productCategories[i].Family  == productCategories[i-1].Family){
				productCategories.remove(i);
				i=i-1;
			}
		}
		
		MAP<Id,Product2> mapIdToProduct = new MAP<Id,Product2>([SELECT Name FROM Product2 WHERE ID IN (SELECT Product2Id FROM PricebookEntry WHERE ID IN :mapIdToPBEntrys.keySet())]);
		
		if(mapIdToProduct!=null && mapIdToProduct.size()>0){
			myProducts=mapIdToProduct.values();
		}
		
		myQLIandProducts = new List<QLIandProduct>();
		
		for(QuoteLineItem loopQLI:mapIdToQLIs.values()){
			if(mapIdToProduct.containsKey(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)){
				myQLIandProducts.add(new QLIandProduct(loopQLI,mapIdToProduct.get(mapIdToPBEntrys.get(loopQLI.PricebookEntryId).Product2Id)));
			}
		}
		
		Map<Id,Account> mapIdToAccount = new Map<Id,Account>([SELECT Name, BillingCity,BillingCountry,BillingPostalCode,BillingStreet FROM Account WHERE ID IN (SELECT AccountId FROM Opportunity WHERE ID = :myQuotes[0].OpportunityId)]);
			if(mapIdToAccount!=null && mapIdToAccount.size()>0){
				myAccount= mapIdToAccount.values()[0];
		}
		myPackages = [SELECT Package_Type__c,User_Limit__c FROM Package__c WHERE Quote__c=:quoteId];
	}
	
	public List<Quote> getMyQuotes()
	{
		for(Quote quote : myQuotes)
		{
			if(quote.Additional_LLC_Information__c != NULL && quote.Additional_LLC_Information__c != '')
			{
				quote.Additional_LLC_Information__c = quote.Additional_LLC_Information__c.replaceAll('\n', '<br/>');
			}
		}
		
		return myQuotes;
	}
}
*/