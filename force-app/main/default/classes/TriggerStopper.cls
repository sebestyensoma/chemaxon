public class TriggerStopper 
{
    public static boolean stopOpp = false;
    public static boolean stopQuote = false;
    public static boolean stopOppLine = false;
    public static boolean stopQuoteLine = false;
    
    public static Set<id> quoteLineItemIdSet = new Set<id>();
    public static Map<id, integer> quoteLineItemIdMap = new Map<id, integer>();
    public static Map<id, integer> opportunityLineItemIdMap = new Map<id, integer>();
    
    public static void addIdToMap(string mapName, id recordId)
    {
    	system.debug('DEBUG: addIdToMapInvoked - ' + recordId);
    	
    	if(mapName == 'quoteLineItemIdMap')
    	{
    		if(quoteLineItemIdMap.containsKey(recordId))
    		{
    			integer tempNumber;
    			tempNumber = quoteLineItemIdMap.get(recordId);
    			tempNumber++;
    			
    			quoteLineItemIdMap.put(recordId, tempNumber);
    		}
    		else
    		{
    			quoteLineItemIdMap.put(recordId, 1);
    		}
    	}
    	
    	
    	if(mapName == 'opportunityLineItemIdMap')
    	{
    		if(opportunityLineItemIdMap.containsKey(recordId))
    		{
    			integer tempNumber;
    			tempNumber = opportunityLineItemIdMap.get(recordId);
    			tempNumber++;
    			
    			opportunityLineItemIdMap.put(recordId, tempNumber);
    		}
    		else
    		{
    			opportunityLineItemIdMap.put(recordId, 1);
    		}
    	}
    }
}