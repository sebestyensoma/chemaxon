global class QuoteHandlerFuture 
{
	@future
	public static void setOpportunityStage(List<id> opportunityIdList, string newStageName)
	{
		List<Opportunity> opportunityList = new List<Opportunity>();
		
		opportunityList = [ SELECT Id, StageName, ForecastCategory FROM Opportunity WHERE Id IN :opportunityIdList AND StageName != :newStageName];
		
		for(Opportunity currentO : opportunityList)
		{
			//if(newStageName == 'Closed Won')
			//{
				currentO.StageName = newStageName;
			//}
		}
		
		update opportunityList;
	}
}