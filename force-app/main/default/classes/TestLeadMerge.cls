/**
 * @author      Attention CRM Consulting <neckermann@attentioncrm.hu>
 * @version     1
 * @update		2016-09-28 (?K)
 */
 

@isTest(SeeAllData=false)
private class TestLeadMerge {

    @isTest(SeeAllData=false)
	static void myUnitTest() {
        
        Lead masterLead = new Lead(LastName='Test',company='Test',LeadSource = 'Website Registration', Forum_Id__c='xxxxxxx',Download_packs__c='Marvin',email='test@test.com');
        insert masterLead;
        
        //Test.startTest();
        Lead leadToMerge = new Lead(LastName='Test',company='Test',LeadSource = 'Website Product Download', Forum_Id__c='xxxxxxx',Download_packs__c='JChem',email='test@test.com');
    	insert leadToMerge;
    	
    	Lead leadToMerge2 = new Lead(LastName='Test',company='Test',LeadSource = 'Website Product Download', Forum_Id__c='xxxxxxx',Download_packs__c='Marvin JS',email='test@test.com');
    	insert leadToMerge2;
    	
    	Lead leadToMerge3 = new Lead(LastName='Test',company='Test',LeadSource = 'Website Product Download', Forum_Id__c='yyy',Download_packs__c='Marvin JS',email='test@test.com');
    	insert leadToMerge3;
    	
    	masterLead=[ select id,createddate,Download_packs__c,T_Master__c,T_MasterId__c from Lead where id=:masterLead.id ];
    	
    	try{
    		leadToMerge =[ select id,createddate,Download_packs__c,T_Master__c,T_MasterId__c from Lead where id=:leadToMerge.id ];
    	}catch(Exception ex){
    		system.debug(ex.getMessage());
    	}
    	
    	try{
    		leadToMerge2 =[ select id,createddate,Download_packs__c,T_Master__c,T_MasterId__c from Lead where id=:leadToMerge2.id ];
    	}catch(Exception ex){
    		system.debug(ex.getMessage());
    	}
    	
    	try{
    		leadToMerge3 =[ select id,createddate,Download_packs__c,T_Master__c,T_MasterId__c from Lead where id=:leadToMerge3.id ];
    	}catch(Exception ex){
    		system.debug(ex.getMessage());
    	}
    	
    	system.debug('DBG'+masterLead);
    	system.debug('DBG'+leadToMerge);
    	system.debug('DBG'+leadToMerge2);
    	system.debug('DBG'+leadToMerge3);
    	
    	system.assertequals(
    		new Set<String>(masterLead.Download_packs__c.split(';')),
    		new Set<String> { 'Marvin', 'JChem', 'Marvin JS' }
    	);
    }


	//*
	@isTest(SeeAllData=true)
	static void testLeadMergeBatch() {
		Test.startTest();
		String batchInstanceId = database.executeBatch(
			new LeadMergeBatch(),
			50
		);

		Test.StopTest();
	}
	//*/

	@isTest(SeeAllData=true)
	static void testLeadMerge2() {
		new LeadMergeBatch2().action('Email');
		Test.startTest();
		new LeadMergeBatch2().action('Forum_id__c');
		Test.StopTest();
	}

}