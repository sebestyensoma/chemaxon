public with sharing class ProductReportController extends AbstractReportController {
	
	private static final String[] productLabels = new String[] {
		/* Naming */
		'S2N', 'N2S', 'D2S', 'Subtotal',
		
		/* MA/MB/MJ */
		'MA', 'MB', 'MJ', 'Subtotal',
		
		/* JC SUM */
		'JWS', 'JCSP', 'JCXL', 'JCO', 'IJCV', 'IJCE', 'IJCS', 'Subtotal',
		
		/* JCB/JOC/MS/ME */
		'JCB', 'JOC', 'MS', 'ME', 'Subtotal',
		
		/* Disc. Tools */
		'SFW', 'SCR3', 'SCR2', 'MD', 'JK', 'ECFP', 'CNF', 'Subtotal',
		
		/* Calc Team Plg */
		'PROT', 'PRE', 'PART', 'NMR', 'ISM', 'HCKL', 'GEO', 'CP', 'CHG', 'Subtotal',
		
		/* All TRF */
		'STD', 'SCHK', 'REAC', 'MET', 'FRG', 'Subtotal',
		
		/* Misc. */
		'PLX', 'JEK', 'CREG', 'CON', 'AS', 'Subtotal',
		
		'Total'
	};
	
	public Map<String, Integer> spansByCategory { get; set; }
	public List<String> productCategories { get; set; }
	public List<String> productCodes { get; set; }
	public Map<String, Boolean> showColumns { get; set; }
	public Map<String, List<String>> codesByCategory { get; set; }
	public Map<String, String> columnToCategory { get; set; }
	
	// REPORTING
	
	protected override List<String> createColumnLabels() {
		codesByCategory = new Map<String, List<String>>();
		
		AggregateResult[] products = [
			SELECT ProductCode, Product2.Product_Code_Type__c
			FROM PriceBookEntry
			WHERE isActive = true
			GROUP BY ProductCode, Product2.Product_Code_Type__c
			ORDER BY Product2.Product_Code_Type__c ASC, ProductCode ASC
		];
		for (AggregateResult pbe : products) {
			String catName = (String)pbe.get('Product_Code_Type__c');
			String code = (String)pbe.get('ProductCode');
			if (code == null) continue;
			if (catName == null) catName = 'Miscellaneous';
			
			String[] codes = codesByCategory.get(catName);
			if (codes == null) {
				codes = new List<String>();
				codesByCategory.put(catName, codes);
			}
			
			codes.add(code);
		}
		
		productCategories = new List<String>();
		productCodes = new List<String>();
		spansByCategory = new Map<String, Integer>();
		columnToCategory = new Map<String, String>();
		for (String catName : codesByCategory.keySet()) {
			productCategories.add(catName);
			for (String code : codesByCategory.get(catName)) {
				productCodes.add(code);
				columnToCategory.put(code, catName);
			}
			productCodes.add('Subtotal');
			spansByCategory.put(catName, codesByCategory.get(catName).size() + 1);
		}
		productCodes.add('Total');
		
		return productCodes;
	}
	
	protected override void doReport() {
		// Fetch raw data
		List<QuoteLineItem> rawList = [
			SELECT Quote.Invoice_Date__c, Quote.Actual_Payment_Date__c,
				PriceBookEntry.ProductCode, PriceBookEntry.Product2.Product_Code_Type__c,
				Total_in_EUR__c, Total_in_USD__c, Total_in_GBP__c, Total_in_HUF__c, Total_in_JPY__c, Total_in_CHF__c, Total_in_SGD__c
			FROM QuoteLineItem
            WHERE Quote.Invoice_Date__c != null
			ORDER BY Quote.Invoice_Date__c ASC
		];
		
		// Process rows
		String fieldName = 'Total_in_' + currencyName + '__c';
		for (QuoteLineItem item : rawList) {
			Decimal d = (Decimal)item.get(fieldName);
			if (d == null) continue;
			
			String key = extractKey(item);
			Map<String, Decimal> tableRow = getOrCreateRow(key);
			Decimal rowTotal = tableRow.get(item.pricebookentry.productcode);
			if (rowTotal == null) continue;
			
			rowTotal += d;
			tableRow.put(item.pricebookentry.productcode, rowTotal);
			
			rowTotal = tableRow.get('Total');
			rowTotal += d;
			tableRow.put('Total', rowTotal);
		}
	}
	
	public override String getTableHtml() {
		XmlStreamWriter tableHtml = new XmlStreamWriter();
		tableHtml.writeStartElement(null, 'tbody', null);
		
		for (String key : tableKeys) {
			Decimal subTotal = 0;
			Map<String, Decimal> tableRow = tableData.get(key);
			
			tableHtml.writeStartElement(null, 'tr', null);
			tableHtml.writeStartElement(null, 'td', null);
			tableHtml.writeAttribute(null, null, 'class', 'key');
			tableHtml.writeCharacters(key);			
			tableHtml.writeEndElement();
				
			for (String label : columnLabels) {
				if (label != 'Subtotal') {
					subTotal += tableRow.get(label);
					tableHtml.writeStartElement(null, 'td', null);
					tableHtml.writeAttribute(null, null, 'class', 'num');
					tableHtml.writeCharacters(formatInteger(tableRow.get(label)));			
					tableHtml.writeEndElement();
				}
				else {
					tableHtml.writeStartElement(null, 'td', null);
					tableHtml.writeAttribute(null, null, 'class', 'total');
					tableHtml.writeCharacters(formatInteger(subTotal));			
					tableHtml.writeEndElement();
					subTotal = 0;
				}
			}	
			tableHtml.writeEndElement();
		}
		
		tableHtml.writeEndElement();
		return tableHtml.getXmlString();
	}
}