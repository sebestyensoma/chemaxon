public class ContentDocumentLinkTriggerHandler  {

	public static void AI(Map<Id, ContentDocumentLink> triggerNew){
		fillingDocName(triggerNew);
	}


	private static void fillingDocName(Map<Id, ContentDocumentLink> triggerNew){
		//filling the related Agreement document's name with the attachment's name
		Set<Id> cdId = new Set<Id>();
		Set<Id> cdlId = new Set<Id>();
		for(ContentDocumentLink cdl : triggerNew.values()){
			if(cdl.LinkedEntityId!=null && cdl.LinkedEntityId.getSObjectType().getDescribe().getName()=='Agreement_document__c'){
				cdId.add(cdl.ContentDocumentId);
				cdlId.add(cdl.LinkedEntityId);
			}
		}
		
		Map<Id, ContentDocument> cdMap = new Map<Id, ContentDocument>([select id,Title, FileExtension FROM ContentDocument where id IN :cdId]);
		Map<Id, Agreement_document__c> adMap = new Map<Id, Agreement_document__c>([SELECT Name FROM Agreement_document__c WHERE Id IN :cdlId]);
		List<Agreement_document__c> adToUpdate = new List<Agreement_document__c>();

		for(ContentDocumentLink cdl : triggerNew.values()){
			if(cdl.LinkedEntityId!=null && cdl.LinkedEntityId.getSObjectType().getDescribe().getName()=='Agreement_document__c'){
				ContentDocument cd = cdMap.get(cdl.ContentDocumentId);
				
				Agreement_document__c ad = adMap.get(cdl.LinkedEntityId);
				ad.Name=cd.Title + '.' + cd.FileExtension;
				adToUpdate.add(ad);		
			}
		}


		update adToUpdate;
	}

}