@isTest
private class testAgreementName {

    static testMethod void insertTest() {
        
        Account acc = new Account (Name='test Account');
        insert acc;
        
        Agreement__c agr = new Agreement__c (Contract_with__c=acc.id,Contract_category__c='CDA' /*,X1_year_auto_renewal__c='1'*/);
        insert agr;
        
        agr.Effective_date_to_date__c=Date.newinstance(2015,5,6);
        update agr;
    }
    
    static testMethod void relatedTest() {
        
        Account acc = new Account (Name='test Account');
        insert acc;
        
        Agreement__c agrRel = new Agreement__c (Contract_with__c=acc.id,Contract_category__c='CDA' /*,X1_year_auto_renewal__c='1'*/,Effective_date_to_date__c=Date.newinstance(2015,5,6));
        insert agrRel;
        
        Agreement__c agr = new Agreement__c (Contract_with__c=acc.id,Contract_category__c='CDA' ,Related_contract__c=agrRel.id/*,X1_year_auto_renewal__c='1'*/,Effective_date_to_date__c=Date.newinstance(2015,5,6));
        insert agr;
        
        
        agr.Effective_date_to_date__c=Date.newinstance(2015,6,6);
        update agr;
        
        
    }
    
    
    
   
    
}