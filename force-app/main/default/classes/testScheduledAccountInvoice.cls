/*
 * @author      Attention CRM Consulting (koszi) <krisztian.oszi@attentioncrm.hu>
 * @version     1.0
 * @since       2016-06-14 (ŐK)
 */
@isTest 
private class testScheduledAccountInvoice {
	@isTest(seeAllData=true) 
	static void test(){
		system.Test.startTest();
        String sch = '0 0 23 * * ?';
        String jobId = System.schedule('testScheduledAccountInvoice', sch, new ScheduledAccountInvoice());
        system.Test.stopTest();
	}
}