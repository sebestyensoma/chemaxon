public with sharing class AppsciEmail 
{
	String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
	
	public void AppsciEmailSending(List<Task> taskList)
	{
		List<Messaging.SingleEmailMessage> sendEmails = new List<Messaging.SingleEmailMessage>();
		
		List<Appsci_Emails__c> appsciEmailsList = Appsci_Emails__c.getAll().values();
		Map<string, Appsci_Emails__c> appsciemailsStringMap = new Map<string, Appsci_Emails__c>(); 
	
		system.debug(appsciEmailsList);
		
		for(Appsci_Emails__c apem : appsciEmailsList)
		{
			appsciemailsStringMap.put(apem.Name, apem);
		}
		
		for(Task task : taskList)
		{
			if(appsciEmailsStringMap.containsKey(task.Application_Scientist__c))
			{
				string tempActivityDate;
				
				if(task.ActivityDate != NULL)
				{
					tempActivityDate = string.valueof(task.ActivityDate).substring(0, 10);
				}
				else
				{
					tempActivityDate = '-';
				}
				
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				List<string> toEmailStringList = new List<string>();
				List<string> ccEmailStringList = new List<string>();
				
				toEmailStringList.add(appsciEmailsStringMap.get(task.Application_Scientist__c).Email__c);
				ccEmailStringList.add('chemaxon@attentioncrm.hu');
				
				mail.setToAddresses(toEmailStringList);
				mail.setCCAddresses(ccEmailStringList);
				
				mail.setSubject('New task has been assigned to you (' + task.Application_Scientist__c + ')');
				mail.setUseSignature(false);
				mail.setCharset('UTF-8');			
				
				mail.setHtmlBody
					(						
						'Dear Application Scientist,'
						+
						'<br/><br/>'
						+
						'A new task has been assigned to you.'
						+
						'<br/><br/>'
						+
						'Subject: ' + task.Subject
						+
						'<br/>'
						+
						'Due Date: '+ tempActivityDate
						+
						'<br/>'
						+
						'You can access the task here: ' + sfdcBaseUrl + '/' + task.id
						+
						'<br/>'
						+
						'<br/><br/>'
						+
						'Best regards,<br/> Attention Chemaxon Team'
					);
				sendEmails.add(mail);
				system.debug(mail);
			}
		}
		
		if(sendEmails.size() != 0)
		{
			Messaging.sendEmail(sendEmails);
		}
	}
}