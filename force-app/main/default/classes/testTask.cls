@isTest
private class testTask
{
    static testMethod void myUnitTest01()
    {
        List<Task> taskList = new List<Task>();
        List<Appsci_Emails__c> customSettingsList = new List<Appsci_Emails__c>();
        
        Appsci_Emails__c cs01 = new Appsci_Emails__c(Name = 'name01', Email__c = 'email@email.hu');
        customSettingsList.add(cs01);
        
        if(customSettingsList.size() != 0)
        {
        	insert customSettingsList;
        }
        
        Task task01 = new Task(Subject = 'subject', Application_Scientist__c = 'name01');
        taskList.add(task01);
        
        Task task02 = new Task(Subject = 'subject', ActivityDate = system.today(), Application_Scientist__c = 'name02');
        taskList.add(task02);
        
        Task task03 = new Task(Subject = 'subject', ActivityDate = system.today(), Application_Scientist__c = 'name01');
        taskList.add(task03);
        
        if(taskList.size() != 0)
        {
        	insert taskList;
        	
        	taskList[1].Application_Scientist__c = 'name01';
        
        	update taskList[0];
        }
    }
}