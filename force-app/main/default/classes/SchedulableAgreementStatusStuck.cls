global class SchedulableAgreementStatusStuck implements Schedulable {
	// CXN-2019-08-14 / 14662
	// Amikor az Agreement StatusCheck "Stuck"-ra van állítva valamelyik szerződés esetében a SF hetente küldjön 1-1 reminder emailt az Account Ownernek és a complaince@chemaxon.com-ra
	// azzal a szöveggel, hogy "The Agreement Status Check for agreement <Agreement Number> <Agreement Name> is in "Stuck". Please contact Compliance ASAP.
	// Compliance Note to Account owner: ........" A pontok helyén pedig legyen a <Compliance Note to Account owner> filed aktuális tartalma.

	private Map<Id/*owner id*/, Contact> contactByOwnerId = new Map<Id, Contact>();


	global void execute(SchedulableContext sc) {
		
		List<Messaging.SingleEmailMessage> emailMsg = new List<Messaging.SingleEmailMessage> ();

		OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'Compliance' LIMIT 1];
		EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Agreement_stuck' LIMIT 1];
		
		List<Agreement__c> agrList = [SELECT Id, Link__c, Compliance_Note_to_Account_owner__c, Contract_with__r.OwnerId, Contract_with__r.Owner.Email, Contract_number__c FROM Agreement__c WHERE Stuck_Contact_Compliance_checkbox__c = true];

		
		createDummyContact(agrList);

		for(Agreement__c agr : agrList){
			Messaging.SingleEmailMessage outEmail = new Messaging.SingleEmailMessage();
			outEmail.setTemplateId(et.Id);
			outEmail.setTargetObjectId((contactByOwnerId.get(agr.Contract_with__r.OwnerId)).Id);
			outEmail.setWhatId(agr.Id);
			outEmail.setSaveAsActivity(false);
			outEmail.setOrgWideEmailAddressId(owa.Id);
			outEmail.setCcAddresses(new List<String>{'compliance@chemaxon.com'}); // 'chemaxon@attentioncrm.hu', 
			emailMsg.add(outEmail);
		}

		Messaging.sendEmail(emailMsg);

		deleteDummyContact();
	}


	private void createDummyContact(List<Agreement__c> agrList){
		for(Agreement__c agr : agrList){
			Contact tmpContact = new Contact();
			tmpContact.Email = agr.Contract_with__r.Owner.Email;
            tmpContact.LastName = 'tempCloseCaseRemindercontactUniqueForDummy';
            tmpContact.Title = 'tempCloseCaseRemindercontactUniqueForDummy'; 
            tmpContact.Department = 'tempCloseCaseRemindercontactUniqueForDummy';
			contactByOwnerId.put(agr.Contract_with__r.OwnerId, tmpContact);
		}
		insert contactByOwnerId.values();
	}

	@Future
    private static void deleteDummyContact(){
        // törli a dummy contact-okat
        List<Contact> tempcons = new List<Contact>([
            Select id 
            From Contact 
            Where Lastname ='tempCloseCaseRemindercontactUniqueForDummy' 
                and title = 'tempCloseCaseRemindercontactUniqueForDummy' 
                and Department = 'tempCloseCaseRemindercontactUniqueForDummy'
                and createdDate = TODAY
        ]);
        if(!tempcons.isEmpty()){
            delete tempcons;
        }
    }
}