@isTest
private class testQuote
{
    /*
    	currency beállítás tesztelése
    */
    @isTest(SeeAllData=false)
    static void myUnitTest01_1() 
    {
        integer LOOPVARIABLE;
        
        id standardPricebookId;
        
        Account account01 = new Account();
        Opportunity opportunity01 = new Opportunity();
        
        List<Quote> quoteList = new List<Quote>();
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        List<Product2> product2List = new List<Product2>();
        List<Currency__c> currencyList = new List<Currency__c>();
        List<Currency__c> oldCurrency = new List<Currency__c>();
        
        oldCurrency = [ SELECT Id FROM Currency__c LIMIT 10000 ];
        
        if(oldCurrency.size() != 0)
        {
        	delete oldCurrency;
        }
        
        oldCurrency.clear();
        oldCurrency = [ SELECT Id FROM Currency__c LIMIT 10000 ];
        
        if(oldCurrency.size() != 0)
        {
        	delete oldCurrency;
        }
        
        LOOPVARIABLE = 5;
        standardPricebookId = test.getStandardPricebookId();

        account01.Name = 'name';
		account01.ShippingCountry = 'Norway';
		account01.ShippingCity = 'Oslo';
		account01.ShippingStreet = 'St. Olavs Gate 2';
		account01.ShippingPostalCode = '0165';
		account01.BillingCountry = 'Norway';
		account01.BillingCity = 'Oslo';
		account01.BillingStreet = 'St. Olavs Gate 2';
		account01.BillingPostalCode = '0165';
		insert account01;
        
        opportunity01.Name = 'name';
        opportunity01.StageName = 'stagename';
        opportunity01.CloseDate = date.today();
        
        insert opportunity01;
        
        Currency__c currency01 = new Currency__c();
        Currency__c currency02 = new Currency__c();
        Currency__c currency03 = new Currency__c();
        
        currency01.Name = '2014-02-01';
        currency02.Name = '2014-03-03';
        currency03.Name = '2014-05-05';
        
        currencyList.add(currency01);
        currencyList.add(currency02);
        currencyList.add(currency03);
        
        if(currencyList.size() != 0)
        {
        	insert currencyList;
        }

        
        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Quote quote = new Quote();
        	
        	quote.Name = 'name' + string.valueof(i);
        	quote.OpportunityId = opportunity01.Id;
        	quote.Pricebook2Id = standardPricebookId;
        	quote.PO_No__c = '-';
        	quote.Invoice_Type__c = 'Kft';

        	if(i==0)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2014, 02, 01);
        	}
        	else if(i==1)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2014, 03, 10);
        	}
        	else if(i==2)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2013, 10, 10);
        	}
        	else if(i==3)
        	{
        		quote.Status = 'Settled';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Actual_Payment_Date__c = date.newInstance(2013, 10, 10);
        	}
        	else
        	{
        		quote.Status = '';
        	}
        	
        	quoteList.add(quote);
        }
        
        if(quoteList.size() != 0)
        {
        	insert quoteList;
        }
        
Test.startTest();         
        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Product2 product2 = new Product2();
        	
        	product2.Name = 'name';
        	
        	product2List.add(product2);
        }
        
        if(product2List.size() != 0)
        {
        	insert product2List;
        }
        
        
        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	PricebookEntry pricebookEntry = new PricebookEntry();
        	
        	pricebookEntry.Pricebook2Id = standardPricebookId;
        	pricebookEntry.Product2Id = product2List[i].Id;
        	pricebookEntry.UnitPrice = 1;
        	pricebookEntry.isActive = TRUE;
        	
        	pricebookEntryList.add(pricebookEntry);
        }
        
        if(pricebookEntryList.size() != 0)
        {
        	insert pricebookEntryList;
        }
        
        
        /*
        	invoiced státuszú quote-hoz adunk quotelineitem-et, 
        	és megnézzük, hogy beáll-e a currency
        */
        List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
        
        QuoteLineItem quoteLineItem01 = new QuoteLineItem();
        QuoteLineItem quoteLineItem02 = new QuoteLineItem();
        QuoteLineItem quoteLineItem03 = new QuoteLineItem();
        QuoteLineItem quoteLineItem04 = new QuoteLineItem();
        
        quoteLineItem01.QuoteId = quoteList[0].Id;
        quoteLineItem01.PricebookEntryId = pricebookEntryList[0].Id;
        quoteLineItem01.UnitPrice = 1;
        quoteLineItem01.Quantity = 1;
        
        quoteLineItemList.add(quoteLineItem01);
        
        quoteLineItem02.QuoteId = quoteList[1].Id;
        quoteLineItem02.PricebookEntryId = pricebookEntryList[1].Id;
        quoteLineItem02.UnitPrice = 1;
        quoteLineItem02.Quantity = 1;
        
        quoteLineItemList.add(quoteLineItem02);
        
        quoteLineItem03.QuoteId = quoteList[2].Id;
        quoteLineItem03.PricebookEntryId = pricebookEntryList[2].Id;
        quoteLineItem03.UnitPrice = 1;
        quoteLineItem03.Quantity = 1;
        
        quoteLineItemList.add(quoteLineItem03);
        
        quoteLineItem04.QuoteId = quoteList[3].Id; //settled quote, no invoice_date__c
        quoteLineItem04.PricebookEntryId = pricebookEntryList[3].Id;
        quoteLineItem04.UnitPrice = 1;
        quoteLineItem04.Quantity = 1;
        
        quoteLineItemList.add(quoteLineItem04);
        
        if(quoteLineItemList.size() != 0)
        {
        	insert quoteLineItemList;
        }
        
        List<QuoteLineItem> newQuoteLineItemList = new List<QuoteLineItem>();
        
        newQuoteLineItemList = 
        	[
        		SELECT
        			Id,
        			Quote.Name,
        			Currency__c
        		FROM
        			QuoteLineItem
        		WHERE
        			Id IN :quoteLineItemList
        		ORDER BY Quote.Name
        		ASC
        	];
        
        /*
        system.assertEquals(currencyList[0].Id, newQuoteLineItemList[0].Currency__c);
        system.assertEquals(currencyList[1].Id, newQuoteLineItemList[1].Currency__c);
        */
        system.assertEquals(NULL, newQuoteLineItemList[2].Currency__c);
        
        Currency__c currency04 = new Currency__c();
        
        currency04.Name = '2013-10-10';
        
        insert currency04;
        
        List<QuoteLineItem> newQuoteLineItemList2 = new List<QuoteLineItem>();
        
        newQuoteLineItemList2 = 
        	[
        		SELECT
        			Id,
        			Currency__c
        		FROM
        			QuoteLineItem
        		WHERE
        			Id = :newQuoteLineItemList[2].Id //invoiced
        			OR
        			Id = :newQuoteLineItemList[3].Id //settled
        	];
        	
        system.assertEquals(currency04.Id, newQuoteLineItemList2[0].Currency__c);
        system.assertNotEquals(currency04.Id, newQuoteLineItemList2[1].Currency__c);
    }
    
    /*
    	currency beállítás tesztelése; új currency beszúrása
    */
    @isTest(SeeAllData=false)
    static void myUnitTest01_2() 
    {
        integer LOOPVARIABLE;

        id standardPricebookId;

        Account account01 = new Account();
        Opportunity opportunity01 = new Opportunity();

        List<Quote> quoteList = new List<Quote>();
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        List<Product2> product2List = new List<Product2>();
        List<Currency__c> currencyList = new List<Currency__c>();
        List<Currency__c> oldCurrency = new List<Currency__c>();

        oldCurrency = [ SELECT Id FROM Currency__c LIMIT 10000 ];

        if(oldCurrency.size() != 0)
        {
        	delete oldCurrency;
        }

        oldCurrency.clear();
        oldCurrency = [ SELECT Id FROM Currency__c LIMIT 10000 ];

        if(oldCurrency.size() != 0)
        {
        	delete oldCurrency;
        }

        LOOPVARIABLE = 5;
        standardPricebookId = test.getStandardPricebookId();

        account01.Name = 'name';
		account01.ShippingCountry = 'Norway';
		account01.ShippingCity = 'Oslo';
		account01.ShippingStreet = 'St. Olavs Gate 2';
		account01.ShippingPostalCode = '0165';
		account01.BillingCountry = 'Norway';
		account01.BillingCity = 'Oslo';
		account01.BillingStreet = 'St. Olavs Gate 2';
		account01.BillingPostalCode = '0165';
		account01.Size_of_the_company__c = 'Micro (1-50)';

        insert account01;

        opportunity01.Name = 'name';
        opportunity01.StageName = 'stagename';
        opportunity01.CloseDate = date.today();

        insert opportunity01;

        Currency__c currency01 = new Currency__c();
        Currency__c currency02 = new Currency__c();
        Currency__c currency03 = new Currency__c();

        currency01.Name = '2014-02-01';
        currency02.Name = '2014-03-03';
        currency03.Name = '2014-05-05';

        currencyList.add(currency01);
        currencyList.add(currency02);
        currencyList.add(currency03);

        if(currencyList.size() != 0)
        {
        	insert currencyList;
        }


        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Quote quote = new Quote();

        	quote.Name = 'name' + string.valueof(i);
        	quote.OpportunityId = opportunity01.Id;
        	quote.Pricebook2Id = standardPricebookId;
        	quote.PO_No__c = '-';
        	quote.Invoice_Type__c = 'Kft';
			quote.BillingCity = 'city';
			quote.BillingCountry = 'United States of America';
			quote.BillingStreet = 'asd';
			quote.BillingPostalCode = '1111';
			quote.BillingState = 'Alabama';

        	if(i==0)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2014, 02, 01);
        	}
        	else if(i==1)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2014, 10, 10);
        	}
        	else if(i==2)
        	{
        		quote.Status = 'Invoiced';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Invoice_date__c = date.newInstance(2013, 10, 10);
        	}
        	else if(i==3)
        	{
        		quote.Status = 'Settled';
        		quote.License_expiration_date__c = date.today().addYears(1);
        		quote.Actual_Payment_Date__c = date.newInstance(2013, 10, 10);
        	}
        	else
        	{
        		quote.Status = '';
        	}

        	quoteList.add(quote);
        }

        if(quoteList.size() != 0)
        {
        	insert quoteList;
        }


        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Product2 product2 = new Product2();

        	product2.Name = 'name';

        	product2List.add(product2);
        }

        if(product2List.size() != 0)
        {
        	insert product2List;
        }


        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	PricebookEntry pricebookEntry = new PricebookEntry();

        	pricebookEntry.Pricebook2Id = standardPricebookId;
        	pricebookEntry.Product2Id = product2List[i].Id;
        	pricebookEntry.UnitPrice = 1;
        	pricebookEntry.isActive = TRUE;

        	pricebookEntryList.add(pricebookEntry);
        }

        if(pricebookEntryList.size() != 0)
        {
        	insert pricebookEntryList;
        }


        /*
        	invoiced státuszú quote-hoz adunk quotelineitem-et,
        	és megnézzük, hogy beáll-e a currency
        */
        List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();

        QuoteLineItem quoteLineItem01 = new QuoteLineItem();
        QuoteLineItem quoteLineItem02 = new QuoteLineItem();
        QuoteLineItem quoteLineItem03 = new QuoteLineItem();
        QuoteLineItem quoteLineItem04 = new QuoteLineItem();

        quoteLineItem01.QuoteId = quoteList[0].Id;
        quoteLineItem01.PricebookEntryId = pricebookEntryList[0].Id;
        quoteLineItem01.UnitPrice = 1;
        quoteLineItem01.Quantity = 1;

        quoteLineItemList.add(quoteLineItem01);

        quoteLineItem02.QuoteId = quoteList[1].Id;
        quoteLineItem02.PricebookEntryId = pricebookEntryList[1].Id;
        quoteLineItem02.UnitPrice = 1;
        quoteLineItem02.Quantity = 1;

        quoteLineItemList.add(quoteLineItem02);

        quoteLineItem03.QuoteId = quoteList[1].Id;
        quoteLineItem03.PricebookEntryId = pricebookEntryList[2].Id;
        quoteLineItem03.UnitPrice = 1;
        quoteLineItem03.Quantity = 1;

        quoteLineItemList.add(quoteLineItem03);

        quoteLineItem04.QuoteId = quoteList[1].Id;
        quoteLineItem04.PricebookEntryId = pricebookEntryList[3].Id;
        quoteLineItem04.UnitPrice = 1;
        quoteLineItem04.Quantity = 1;

        quoteLineItemList.add(quoteLineItem04);

        if(quoteLineItemList.size() != 0)
        {
        	insert quoteLineItemList;
        }

        List<QuoteLineItem> newQuoteLineItemList = new List<QuoteLineItem>();

        newQuoteLineItemList =
        	[
        		SELECT
        			Id,
        			Quote.Name,
        			Currency__c
        		FROM
        			QuoteLineItem
        		WHERE
        			Id IN :quoteLineItemList
        		ORDER BY Quote.Name
        		ASC
        	];

        /*
        system.assertEquals(currencyList[0].Id, newQuoteLineItemList[0].Currency__c);
        system.assertEquals(currencyList[1].Id, newQuoteLineItemList[1].Currency__c);
        */
        system.assertEquals(NULL, newQuoteLineItemList[2].Currency__c);


        List<QuoteLineItem> newQuoteLineItemList02 = new List<QuoteLineItem>();

        newQuoteLineItemList02 =
        	[
        		SELECT
        			Id,
        			Currency__c
        		FROM
        			QuoteLineItem
        		WHERE
        			Id = :newQuoteLineItemList[2].Id
        			OR
        			Id = :newQuoteLineItemList[3].Id
        	];

        system.assertEquals(null, newQuoteLineItemList02[0].Currency__c);
        system.assertEquals(null, newQuoteLineItemList02[1].Currency__c);

        Currency__c currency04 = new Currency__c();

        currency04.Name = '2014-10-11';
 Test.startTest();
        insert currency04;

        List<QuoteLineItem> newQuoteLineItemList2 = new List<QuoteLineItem>();

        newQuoteLineItemList2 =
        	[
        		SELECT
        			Id,
        			Currency__c
        		FROM
        			QuoteLineItem
        		WHERE
        			Id = :newQuoteLineItemList[2].Id
        			OR
        			Id = :newQuoteLineItemList[3].Id
        	];

        system.assertEquals(currencyList[2].Id, newQuoteLineItemList2[0].Currency__c);
        system.assertEquals(currencyList[2].Id, newQuoteLineItemList2[1].Currency__c);
    }
    
    
    /*
    	Quote display - terms kitöltésének tesztelése (terms)
    */
    @isTest(SeeAllData=true)
    static void myUnitTest02() 
    {
    	integer LOOPVARIABLE;
    	
    	id standardPricebookId;
    	
    	User user01 = new User();
    	Account account01 = new Account();
        Opportunity opportunity01 = new Opportunity();
        
        List<Quote> quoteFirstLapList = new List<Quote>();
        List<Quote> quoteSecondLapList = new List<Quote>();
        
        LOOPVARIABLE = 6;
        standardPricebookId = test.getStandardPricebookId();
        
        user01 = [SELECT Id, CompanyName FROM User WHERE IsActive = TRUE and Profile.Name = 'System Administrator' LIMIT 1];
        
        account01.Name = 'name';
        account01.BillingCity = 'city';
		account01.BillingCountry = 'United States of America';
		account01.BillingStreet = 'asd';
		account01.BillingPostalCode = '1111';
		account01.Copy_Billing_Address_to_Shipping_Address__c  = true;
		account01.BillingState = 'Alabama';
		account01.Size_of_the_company__c = 'Micro (1-50)';

        insert account01;
        
        opportunity01.Name = 'name';
        opportunity01.StageName = 'stagename';
        opportunity01.CloseDate = date.today();
        
        insert opportunity01;
    	
    	for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Quote quote = new Quote();
        	
        	quote.Name = 'name' + string.valueof(i);
        	quote.OpportunityId = opportunity01.Id;
        	quote.Pricebook2Id = standardPricebookId;
        	quote.Invoice_Type__c = 'Kft';
        	
        	if(i==0 || i==3)
        	{
        		quote.Invoice_Type__c = 'Kft';
        	}
        	else if(i==1 || i==4)
        	{
        		quote.Invoice_Type__c = 'LLC';
        	}
        	else
        	{
        		quote.Invoice_Type__c = 'Kft';
        	}
        	
        	if(i>=0 && i<=2)
        	{
        		quoteFirstLapList.add(quote);
        	}
        	else
        	{
        		quoteSecondLapList.add(quote);
        	}
        }
    	
    	
    	system.runAs(user01)
    	{
    		user01.CompanyName = 'ChemAxon Llc';
    		
    		update user01;
    		
    		insert quoteFirstLapList;
    		
    		List<Quote> tempQuoteFirstLapList = new List<Quote>();
    		
    		tempQuoteFirstLapList =
    			[
    				SELECT
    					Id,
    					Name,
    					Invoice_type__c,
    					Quote_display_terms__c
    				FROM
    					Quote
    				WHERE
    					Id IN :quoteFirstLapList
    				ORDER BY
    					Name
    				ASC
    			];
    			
    		system.debug(tempQuoteFirstLapList[0].Quote_display_terms__c);
    			
    		//system.assert(tempQuoteFirstLapList[0].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteFirstLapList[1].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteFirstLapList[2].Quote_display_terms__c.contains('BUDAHUHB'));
    		
    		tempQuoteFirstLapList[0].Invoice_Type__c = 'Kft';
    		tempQuoteFirstLapList[2].Invoice_Type__c = 'LLC';
    		
    		update tempQuoteFirstLapList;
    		
    		tempQuoteFirstLapList.clear();
    		
    		tempQuoteFirstLapList =
    			[
    				SELECT
    					Id,
    					Name,
    					Invoice_type__c,
    					Quote_display_terms__c
    				FROM
    					Quote
    				WHERE
    					Id IN :quoteFirstLapList
    				ORDER BY
    					Name
    				ASC
    			];
    		
    		//system.assert(tempQuoteFirstLapList[0].Quote_display_terms__c.contains('BUDAHUHB'));
    		//system.assert(tempQuoteFirstLapList[1].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteFirstLapList[2].Quote_display_terms__c.contains('BOFAUS3N'));
    	}
Test.startTest();    	
    	system.runAs(user01)
    	{
    		user01.CompanyName = 'ChemAxon Ltd';
    		
    		update user01;
    		
    		insert quoteSecondLapList;
    		
    		List<Quote> tempQuoteSecondLapList = new List<Quote>();
    		
    		tempQuoteSecondLapList =
    			[
    				SELECT
    					Id,
    					Name,
    					Invoice_type__c,
    					Quote_display_terms__c
    				FROM
    					Quote
    				WHERE
    					Id IN :quoteSecondLapList
    				ORDER BY
    					Name
    				ASC
    			];
    			
    		system.debug(tempQuoteSecondLapList[0].Quote_display_terms__c);
    			
    		//system.assert(tempQuoteSecondLapList[0].Quote_display_terms__c.contains('BUDAHUHB'));
    		//system.assert(tempQuoteSecondLapList[1].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteSecondLapList[2].Quote_display_terms__c.contains('BUDAHUHB'));
    		
    		tempQuoteSecondLapList[0].Invoice_Type__c = 'LLC';
    		tempQuoteSecondLapList[2].Invoice_Type__c = 'Kft';
    		
    		update tempQuoteSecondLapList;
    		
    		tempQuoteSecondLapList.clear();
    		
    		tempQuoteSecondLapList =
    			[
    				SELECT
    					Id,
    					Name,
    					Invoice_type__c,
    					Quote_display_terms__c
    				FROM
    					Quote
    				WHERE
    					Id IN :quoteSecondLapList
    				ORDER BY
    					Name
    				ASC
    			];
    		
    		//system.assert(tempQuoteSecondLapList[0].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteSecondLapList[1].Quote_display_terms__c.contains('BOFAUS3N'));
    		//system.assert(tempQuoteSecondLapList[2].Quote_display_terms__c.contains('BUDAHUHB'));
    	}
    }
    
    /*
    	Quote display - terms kitöltésének tesztelése (iban number)
    */
    @isTest(SeeAllData=true)
    static void myUnitTest03() 
    {
    	integer LOOPVARIABLE;
    	
    	id standardPricebookId;
    	
    	User user01 = new User();
    	Account account01 = new Account();
        
        List<string> currencyIsoCodeList = new List<string>();
        List<Quote> quoteList = new List<Quote>();
		List<Opportunity> opportunityList = new List<Opportunity>();
        
        LOOPVARIABLE = 6;
        standardPricebookId = test.getStandardPricebookId();
        
        currencyIsoCodeList.add('EUR');
        currencyIsoCodeList.add('USD');
        currencyIsoCodeList.add('HUF');
        currencyIsoCodeList.add('CHF');
        currencyIsoCodeList.add('GBP');
        currencyIsoCodeList.add('JPY');
        
        user01 = [SELECT Id, CompanyName FROM User WHERE IsActive = TRUE LIMIT 1];
        
        account01.Name = 'name';
        account01.BillingCity = 'city';
		account01.BillingCountry = 'United States of America';
		account01.BillingStreet = 'asd';
		account01.BillingPostalCode = '1111';
		account01.Copy_Billing_Address_to_Shipping_Address__c  = true;
		account01.BillingState = 'Alabama';
		account01.Size_of_the_company__c = 'Micro (1-50)';

        insert account01;
        
        for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Opportunity opportunity = new Opportunity();
        	
        	opportunity.Name = 'name';
        	opportunity.StageName = 'stagename';
        	opportunity.CloseDate = date.today();
        	opportunity.CurrencyIsoCode = currencyIsoCodeList[i];
        
        	opportunitylist.add(opportunity);
        }
        
        insert opportunitylist;
    	
    	for(integer i=0; i<LOOPVARIABLE; i++)
        {
        	Quote quote = new Quote();
        	
        	quote.Name = 'name' + string.valueof(i);
        	quote.OpportunityId = opportunitylist[i].Id;
        	quote.Pricebook2Id = standardPricebookId;
        	quote.Invoice_Type__c = 'Kft';
        	
        	quoteList.add(quote);
        }
    	
    	insert quoteList;
    	
    	List<Quote> tempQuoteList = new List<Quote>();
    		
    		tempQuoteList =
    			[
    				SELECT
    					Id,
    					Name,
    					CurrencyIsoCode,
    					Invoice_type__c,
    					Quote_display_terms__c
    				FROM
    					Quote
    				WHERE
    					Id IN :quoteList
    				ORDER BY
    					Name
    				ASC
    			];
    			
    		system.debug('kutya: ' + tempQuoteList);
    		
    		//system.assert(tempQuoteList[0].Quote_display_terms__c.contains('HU64101021033141050001003303'));
    		//system.assert(tempQuoteList[1].Quote_display_terms__c.contains('HU31101021033141050001003994'));
    		//system.assert(tempQuoteList[2].Quote_display_terms__c.contains('HU26101021033141050400000004'));
    		//system.assert(tempQuoteList[3].Quote_display_terms__c.contains('HU26101021033141050400000004'));
    		//system.assert(tempQuoteList[4].Quote_display_terms__c.contains('HU26101021033141050400000004'));
    		//system.assert(tempQuoteList[5].Quote_display_terms__c.contains('HU26101021033141050400000004'));
    }
    
    /*
    	Opportunity StageName => Closed Won, ha quote orderedre, invoicedre vagy settledre áll
    */
    @isTest(SeeAllData=true)
    static void myUnitTest04() 
    {
    	List<Opportunity> opportunityList = new List<Opportunity>();
    	List<Quote> quoteList = new List<Quote>();
    	
    	Account account = new Account();
    	account.Name = 'name';
		account.BillingCity = 'city';
		account.BillingCountry = 'United States of America';
		account.BillingStreet = 'asd';
		account.BillingPostalCode = '1111';
		account.Copy_Billing_Address_to_Shipping_Address__c  = true;
		account.BillingState = 'Alabama';
		account.Size_of_the_company__c = 'Micro (1-50)';
    	account.Tax_Number__c = 'asdfasdf';
		account.Type = 'VAR';
		account.Industry = 'Agrochemical';
		account.Size_of_the_company__c = 'Micro (1-50)';
    	insert account;
    	
    	Contact contact = new Contact();
    	contact.LastName = 'lastName';
		contact.Email = 'test.test@test.test';

    	insert contact;
    	
    	Opportunity opportunity01 = new Opportunity();
    	opportunity01.Name = 'oppname01';
    	opportunity01.AccountId = account.Id;
    	opportunity01.StageName = 'stagename';
    	opportunity01.CloseDate = system.today();
    	opportunityList.add(opportunity01);
    	
    	Opportunity opportunity02 = new Opportunity();
    	opportunity02.Name = 'oppname02';
    	opportunity02.AccountId = account.Id;
    	opportunity02.StageName = 'stagename';
    	opportunity02.CloseDate = system.today();
    	opportunityList.add(opportunity02);
    	
    	Opportunity opportunity03 = new Opportunity();
    	opportunity03.Name = 'oppname03';
    	opportunity03.AccountId = account.Id;
    	opportunity03.StageName = 'stagename';
    	opportunity03.CloseDate = system.today();
    	opportunityList.add(opportunity03);
    	
    	Opportunity opportunity04 = new Opportunity();
    	opportunity04.Name = 'oppname04';
    	opportunity04.AccountId = account.Id;
    	opportunity04.StageName = 'stagename';
    	opportunity04.CloseDate = system.today();
    	opportunityList.add(opportunity04);
    	
    	insert opportunityList;
    	
    	
    	Quote quote01 = new Quote();
    	quote01.Name = 'quotename01';
    	quote01.OpportunityId = opportunityList[0].Id;
        quote01.Invoice_Type__c = 'Kft';

    	quoteList.add(quote01);
    	
    	Quote quote02 = new Quote();
    	quote02.Name = 'quotename02';
    	quote02.OpportunityId = opportunityList[1].Id;
        quote02.Invoice_Type__c = 'Kft';

    	quoteList.add(quote02);
    	
    	Quote quote03 = new Quote();
    	quote03.Name = 'quotename03';
    	quote03.OpportunityId = opportunityList[2].Id;
		quote03.Invoice_Type__c = 'Kft';
    	quoteList.add(quote03);
    	
    	Quote quote04 = new Quote();
    	quote04.Name = 'quotename04';
    	quote04.OpportunityId = opportunityList[3].Id;
		quote04.Invoice_Type__c = 'Kft';

    	quoteList.add(quote04);
    	
    	insert quoteList;
    	
    	
    	List<Opportunity> newOpportunityList;
    	newOpportunityList = [ SELECT StageName FROM Opportunity WHERE Id IN :opportunityList];
    	
    	for(Opportunity currentO : newOpportunityList)
    	{
    		system.assertEquals('stagename', currentO.StageName);	
    	}
    	
    	
    	List<Quote> newQuoteList;
    	newQuoteList = [ SELECT Status FROM Quote WHERE Id IN :quoteList ];
    	
    	newQuoteList[0].Status = 'Sent';
    	newQuoteList[1].Status = 'Ordered';
    	newQuoteList[2].Status = 'Invoiced';
    	newQuoteList[3].Status = 'Settled';
    	
    	for(Quote currentQ : newQuoteList)
    	{
    		currentQ.License_Expiration_Date__c = system.today();
    		currentQ.PO_No__c = 'ponumber';
    		currentQ.Contact_Invoice__c = contact.Id;
    		currentQ.Contact_License__c = contact.Id;
    		
    		if(currentQ.Status.equalsIgnoreCase('Invoiced'))
    		{
    			currentQ.Invoice_Date__c = system.today();
    		}
    		
    		if(currentQ.Status.equalsIgnoreCase('Settled'))
    		{
    			currentQ.Actual_Payment_Date__c = system.today();
    		}
    	}
    	
    	test.startTest();
    	update newQuoteList;
    	test.stopTest();
    	
    	newOpportunityList.clear();
    	newOpportunityList = [ SELECT StageName FROM Opportunity WHERE Id IN :opportunityList ];
    	
    	system.assertEquals('stagename', newOpportunityList[0].StageName);
    	system.assertEquals('Closed Won', newOpportunityList[1].StageName);
    	system.assertEquals('Closed Won', newOpportunityList[2].StageName);
    	system.assertEquals('Closed Won', newOpportunityList[3].StageName);
    }
}