public with sharing class OlIPriceUpdater {
	@future
    public static void updateOLIs(String oIdSetJson){
    	system.debug('oIdSetJson:'+oIdSetJson);
		List<Id> opptyIds = (List<id>)JSON.deserialize(oIdSetJson, List<Id>.class);
		system.debug('opptyIds.size():'+opptyIds.size());
		
		List<OpportunityLineItem> listUpdateOLIs = [	SELECT technical_Oppty_Price_last_updated__c
							FROM OpportunityLineItem 
							WHERE
								OpportunityId IN :opptyIds
								AND 
								Package__c=null];
							
		for(OpportunityLineItem loopOLI:listUpdateOLIs){
			loopOLI.technical_Oppty_Price_last_updated__c=Datetime.now();
		}
		if(listUpdateOLIs!=null && listUpdateOLIs.size()>0){
			update listUpdateOLIs;
			system.debug('OLIPriceUpdater: listUpdateOLIs.size():'+listUpdateOLIs.size());
		}
    }
}