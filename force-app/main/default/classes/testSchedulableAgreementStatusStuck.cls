@isTest 
private class testSchedulableAgreementStatusStuck {

	@isTest
	private static void testName() {
		Account acc = new Account(Copy_Billing_Address_to_Shipping_Address__c  = true, name = 'teszt', BillingCity = 'city', BillingCountry = 'United States of America', BillingState = 'Alabama', Size_of_the_company__c = 'Micro (1-50)');
		insert acc;

		Agreement__c agr = new Agreement__c(Contract_with__c = acc.Id, Stuck_Contact_Compliance_checkbox__c = true);
		insert agr;


		SchedulableAgreementStatusStuck sch = new SchedulableAgreementStatusStuck();
		sch.execute(null);
	}
}