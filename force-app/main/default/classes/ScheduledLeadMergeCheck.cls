global without sharing class ScheduledLeadMergeCheck implements schedulable {
	
	 global void execute(SchedulableContext SC) {
	 	
	 	database.executeBatch(new LeadMergeBatch(),100);
	 	
	 }

}