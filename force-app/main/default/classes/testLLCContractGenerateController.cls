@isTest 
private class testLLCContractGenerateController {

	@isTest
	private static void testName() {
		Account account01 = new Account(Name = 'name');
        insert account01;

		Opportunity opportunity01 = new Opportunity(Name = 'name', StageName = 'stagename', CloseDate = date.today());      
        insert opportunity01;

		Quote quote = new Quote(Invoice_Type__c = 'LLC', Name = 'name', OpportunityId = opportunity01.Id, PO_No__c = '-', Status = 'Invoiced', License_expiration_date__c = date.today().addYears(1), Invoice_date__c = date.newInstance(2014, 02, 01));
		insert quote;


		LLCContractGenerateController ctrl = new LLCContractGenerateController();
		
		PageReference pageRef = new PageReference('/apex/LLCContractGenerate');
		ApexPages.currentPage().getParameters().put('qId', quote.Id);
		ctrl.onStart();
	}

	@isTest
	private static void testName2() {
		Account account01 = new Account(Name = 'name');
        insert account01;

		Opportunity opportunity01 = new Opportunity(Name = 'name', StageName = 'stagename', CloseDate = date.today());      
        insert opportunity01;

		Quote quote = new Quote(Invoice_Type__c = 'Kft', Name = 'name', OpportunityId = opportunity01.Id, PO_No__c = '-', Status = 'Invoiced', License_expiration_date__c = date.today().addYears(1), Invoice_date__c = date.newInstance(2014, 02, 01));
		insert quote;


		LLCContractGenerateController ctrl = new LLCContractGenerateController();
		
		PageReference pageRef = new PageReference('/apex/LLCContractGenerate');
		ApexPages.currentPage().getParameters().put('qId', quote.Id);
		ctrl.onStart();
	}

}