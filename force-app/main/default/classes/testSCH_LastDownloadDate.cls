@isTest
private class testSCH_LastDownloadDate
{
    /*
    static testMethod void myUnitTest01()
    {
        Test.startTest();
		String sch = '0 5 * * * ?';
		String jobId = System.schedule('SCH_LastDownloadDate', sch, new SCH_LastDownloadDate());
		Test.stopTest();
    }
    
    static testMethod void myUnitTest02()
    {
        Last_Download_Date_Check__c lddc01 = 
        	new Last_Download_Date_Check__c
        		(
        			Name = 'Last Download Date',
        			Date__c = dateTime.newInstance(2013,02,01,12,00,00)
        		);
        insert lddc01;
        
        Account account01 =
        	new Account
        		(
        			Name = 'account01'
        		);
        insert account01;
        
        Contact contact01=
        	new Contact
        		(
        			LastName = 'lastName',
        			AccountId = account01.Id,
        			Last_Download_Date__c = date.newInstance(2013,01,01)
        		);
       	insert contact01;
       	
       	Lead lead01 = 
       		new Lead
       			(
       				LastName = 'lastName',
       				Company = 'Company',
       				Last_Download_Date__c = system.today().addDays(1)
       			);
       	insert lead01;
        
        License__c license01 =
        	new License__c
        		(
        			Name = 'license01',
        			License_ID__c = '1234',
        			Account__c = account01.Id,
        			Contact_Name__c = contact01.Id,
        			Lead__c = lead01.Id
        		);
        insert license01;
        
        Test.startTest();
		String sch = '0 5 * * * ?';
		String jobId = System.schedule('SCH_LastDownloadDate', sch, new SCH_LastDownloadDate());
		Test.stopTest();
		
		Account account02 = [SELECT Last_Download_Date__c FROM Account WHERE Id = :account01.Id];
		Contact contact02 = [SELECT Last_Download_Date__c FROM Contact WHERE Id = :contact01.Id];
		Lead lead02 = [SELECT Last_Download_Date__c FROM Lead WHERE Id = :lead01.Id];
		
		system.assertEquals(date.today(), account02.Last_Download_Date__c);
		system.assertEquals(date.today(), contact02.Last_Download_Date__c);
		system.assertEquals(date.today().addDays(1), lead02.Last_Download_Date__c);
    }
    */
}