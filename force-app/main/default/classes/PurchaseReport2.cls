public with sharing class PurchaseReport2 {

	public String accounts { get; set; }
	public String currencyName { get; set; }
	public Boolean hasReportData { get; set; }

	private Set<Id> acctIds;

	// ACTIONS

	@RemoteAction
	public static Map<Id, String> lookupAccountName(String searchKey) {
		String likeClause = searchKey + '%';
		
		List<Account> rawResults;
		try {
			rawResults = [
				SELECT id, Name
				FROM Account
				WHERE Name LIKE :likeClause
				ORDER BY Name
			];
		}
		catch (Exception e) {
			rawResults = new List<Account>();
		}
		
		Map<Id, String> results = new Map<Id, String>();
		for (Account acct : rawResults) {
			results.put(acct.id, acct.Name);
		}
		
		return results;
	}
	
	public PageReference runReport() {
		hasReportData = true;
		if (accounts.length() == 0) return null;
		
		List<String> rawIds = accounts.split(',');
		acctIds = new Set<Id>();
		for (String a : rawIds) {
			acctIds.add(Id.valueOf(a));
		}
		return null;
	}
	
	public void download() {
		currencyName = ApexPages.currentPage().getParameters().get('currency');
		List<String> rawIds = ApexPages.currentPage().getParameters().get('accounts').split(',');
		
		acctIds = new Set<Id>();
		for (String a : rawIds) {
			acctIds.add(Id.valueOf(a));
		}
	}
	
	public String getTableHtml() {
		QuoteLineItem[] rawList = [
			SELECT
				Quote.Opportunity.Account.Name, Quote.Invoice_No__c, End_User__c,
				License_Type__c, Quote.Invoice_Date__c, Quote.Payment_Deadline__c, Quote.Actual_Payment_Date__c,
				PriceBookEntry.Product2.ProductCode, User_Limit__c, TotalPrice,
				Total_in_EUR__c, Total_in_USD__c, Total_in_HUF__c, Total_in_GBP__c, Total_in_SGD__c, Total_in_JPY__c, Total_in_CHF__c
			FROM QuoteLineItem
			WHERE
				Quote.Invoice_Date__c != null AND
				(Quote.Opportunity.AccountId IN :acctIds
				OR (End_User__c != null AND End_User__c IN :acctIds))
			ORDER BY Quote.Opportunity.Account.Name ASC, Quote.Name ASC
		];
		if (rawList == null || rawList.size() == 0) return 'No results found';
		
		String currencyField = 'Total_in_' + currencyName + '__c';
		String acctName;
		String quoteNo;
		String lastAcctName = rawList[0].quote.opportunity.account.name;
		String lastQuoteNo = rawList[0].quote.invoice_no__c;
		Decimal acctTotal = 0.0;
		Decimal quoteTotal = 0.0;
		
		String tableHTML = '';
		String quoteRows =  '';
		String acctRows = '';
		
		for (QuoteLineItem item : rawList) {
			Decimal itemTotal = (Decimal)item.get(currencyField);
			if (itemTotal == null) continue;
			itemTotal.setScale(2);
			
			acctName = item.quote.opportunity.account.name;
			quoteNo = item.quote.invoice_no__c;
			
			// New account reached
			if (acctName != lastAcctName) {
				acctTotal += quoteTotal;
				acctRows += renderQuoteRow(lastQuoteNo, quoteTotal) + quoteRows;				
				tableHTML += renderAcctRow(lastAcctName, acctTotal) + acctRows;

				quoteTotal = 0.0;
				acctTotal = 0.0;
				acctRows = '';
				quoteRows = '';
				lastAcctName = acctName;
				lastQuoteNo = quoteNo;
			}
			// New quote reached
			else if (quoteNo != lastQuoteNo) {
				acctRows += renderQuoteRow(lastQuoteNo, quoteTotal) + quoteRows;
				acctTotal += quoteTotal;
				quoteTotal = 0.0;
				quoteRows = '';
				lastQuoteNo = quoteNo;
			}
			
			quoteTotal += itemTotal;
			quoteRows += renderItemRow(item, currencyField);
		}
		acctRows += (renderQuoteRow(quoteNo, quoteTotal) + quoteRows);
		tableHTML += (renderAcctRow(acctName, acctTotal) + acctRows);
		
		return tableHTML;
	}
	
	// HELPER METHODS
	
	private static String renderAcctRow(String acctName, Decimal total) {
		String result = '<tr class="account"><td colspan="7" class="expanded">' + acctName
			+ '</td><td class="num">' + total
			+ '</td></tr>';	
		return result;
	}
	
	private static String renderQuoteRow(String quoteNo, Decimal total) {
		String result = '<tr class="invoice"><td colspan="7" class="expanded">Invoice no. ' + quoteNo
			+ '</td><td class="num">' + total
			+ '</td></tr>';
		return result;
	}
	
	private static String renderItemRow(QuoteLineItem item, String currencyField) {
		String invoicedate = (item.quote.invoice_date__c != null) ? item.quote.invoice_date__c.format() : '';
		String deadline = (item.quote.payment_deadline__c != null) ? item.quote.payment_deadline__c.format() : '';
		String paymentdate = (item.quote.actual_payment_date__c != null) ? item.quote.actual_payment_date__c.format() : '';
		
		String result = '<tr><td>' + item.License_Type__c
			+ '</td><td>' + invoicedate
			+ '</td><td>' + deadline
			+ '</td><td>' + paymentdate
			+ '</td><td>' + item.PriceBookEntry.Product2.ProductCode
			+ '</td><td>' + item.User_Limit__c
			+ '</td><td>' + item.Total_Price_rounded__c.longValue()
			+ '</td><td class="num">' + ((Decimal)item.get(currencyField)).longValue()
			+ '</td></tr>';
		return result;
	}
}