public without sharing class Competitors {

	private static String NONE = '--None--';
	private ApexPages.StandardController controller {get; set;}
	private String sType;
	private Id oId;
	private Map<String/*ForWhat*/, Map<String/*Competitor*/, List<String/*Product*/>>> competitorsMap = new Map<String, Map<String, List<String>>>();
	
	public CompetitorLine newLine {get;set;}
	public List<Competitor__c> existingLines {get;set;}  
	public String deleteParam {get;set;}
	
	public boolean getExistingLinesSizeIsNotZero(){
		return existingLines.size() > 0;
	}
	
	public Competitors(ApexPages.StandardController controller) {
        this.controller = controller;
        oId = controller.getID();
        system.debug('Competitors> ' + oId);
        Schema.SObjectType objectSchema = controller.getRecord().getSObjectType();
        Schema.DescribeSObjectResult dr = objectSchema.getDescribe();
        sType = dr.getName();
        system.debug('Competitors> ' + sType);
        
		String queryString = 
			'SELECT Id, Name, Account__c, ForWhat__c, Competitor__c, Product__c ' + 
			'FROM Competitor__c ' +
			'WHERE ' + sType + '__c = :oId ' + 
			'order BY ForWhat__c, Competitor__c, Product__c';
		existingLines = (List<Competitor__c>) Database.query(queryString);
		
		for(Competitors__c co : Competitors__c.getAll().values()){
			if(competitorsMap.containsKey(co.ForWhat__c)){
				Map<String, List<String>> innerMap = competitorsMap.get(co.ForWhat__c);
				if(innerMap.containsKey(co.Competitor__c)){
					List<String> innerList = innerMap.get(co.Competitor__c);
					innerList.add(co.Product__c);
				}
				else{
					List<String> innerList = new List<String>{co.Product__c};
					innerMap.put(co.Competitor__c, innerList);
				}
			}
			else{
				List<String> innerList = new List<String>{co.Product__c};
				Map<String, List<String>> innerMap = new Map<String, List<String>>{co.Competitor__c => innerList};
				competitorsMap.put(co.ForWhat__c, innerMap);
			}
		}
		
		newLine =  new CompetitorLine(competitorsMap);
	}
	
	public class CompetitorLine{
		
		public Map<String, Map<String, List<String>>> competitorsMap;
		
		public CompetitorLine( Map<String, Map<String, List<String>>> competitorsMap){
			this.competitorsMap = competitorsMap;
			forWhat = new List<SelectOption>();
			forWhat.add(new SelectOption(NONE, NONE));
			for(String forWhatIt : competitorsMap.keySet()){
				 forWhat.add(new SelectOption(forWhatIt, forWhatIt));
			}
		}
		
		///---
		
		public List<SelectOption> forWhat {get;set;}
		
		public String selectedForWhat {
			get;
			set{
				selectedForWhat = value;
				system.debug('Selected ForWat >>>>>>>>>>>>>>>>>>>>>>>>>>>> ' + selectedForWhat);
				selectedCompetitor = null;
				Map<String, List<String>> innerMap = this.competitorsMap.get(selectedForWhat);
				competitor = new List<SelectOption>();
				competitor.add(new SelectOption(NONE, NONE));
				if(getSelectedForWhatIsNotNull()){
					List<String> innerMapKeySetList = new List<String>(innerMap.keySet());
					innerMapKeySetList.sort();
					for(String competitorIt : innerMapKeySetList){
						 competitor.add(new SelectOption(competitorIt, competitorIt));
					}
				}
			}
		}
		
		public boolean getSelectedForWhatIsNotNull(){
			return selectedForWhat != null && selectedForWhat != NONE;
		}
		
		///---
		
		public List<SelectOption> competitor {get;set;}
		
		public String selectedCompetitor {
			get;
			set{
				selectedCompetitor = value;
				system.debug('Selected Competitor >>>>>>>>>>>>>>>>>>>>>>>>>>>> ' + selectedCompetitor);
				selectedProduct = null;
				if(getSelectedCompetitorIsNotNull()){
					Map<String, List<String>> innerMap = this.competitorsMap.get(selectedForWhat);
					List<String> innerList = innerMap.get(selectedCompetitor);
					if(innerList != null){
						innerList.sort();
						product = new List<SelectOption>();
						product.add(new SelectOption(NONE, NONE));
						for(String productIt : innerList){
							 product.add(new SelectOption(productIt, productIt));
						}
					}
					else{
						selectedCompetitor = null;
						system.debug('Selected Competitor <<<<<<<<<<<<<<<<<<<<<<<<<<<< ' + selectedCompetitor);
					}
				}
			}
		}
		
		public boolean getSelectedCompetitorIsNotNull(){
			return
				selectedForWhat != null && selectedForWhat != NONE
				&& 
				selectedCompetitor != null && selectedCompetitor != NONE;
		}
		
		///---
		
		public List<SelectOption> product {get;set;}
		
		public String selectedProduct {
			get;
			set{
				if(getSelectedCompetitorIsNotNull()){
					selectedProduct = value;
					system.debug('Selected Product >>>>>>>>>>>>>>>>>>>>>>>>>>>> ' + selectedProduct);
				}
				else{
					selectedProduct = null;	
					system.debug('Selected Product <<<<<<<<<<<<<<<<<<<<<<<<<<<< ' + selectedProduct);
				}
			}
		}
		
		public boolean getSelectedProductIsNotNull(){
			return
				selectedForWhat != null && selectedForWhat != NONE
				&& 
				selectedCompetitor != null && selectedCompetitor != NONE
				&& 
				selectedProduct != null && selectedProduct != NONE;
		}
		
	}
	
	/*public class Competition{
		public Competition(CompetitorLine competitorLine){
			this.forWht  =    CompetitorLine.selectedForWhat;
			this.competitor = CompetitorLine.selectedCompetitor;
			this.product =    CompetitorLine.selectedProduct;
		}
		public string forWhat {get;set;}
		public string competitor {get;set;}
		public string product {get;set;}
	}*/
	
	public void add(){
		Competitor__c c = new Competitor__c();
			c.forWhat__c =    newLine.selectedForWhat;
			c.competitor__c = newLine.selectedCompetitor;
			c.product__c =    newLine.selectedProduct;
			((Sobject)c).put(sType + '__c', oId);
		insert c;
		existingLines.add(c);
		newLine = new CompetitorLine(competitorsMap);
	}
	
	public pageReference back(){
		return new PageReference('/' + oid);
	}
	
	public void save(){
		add();
	}
	
	public void del(){
		for(integer i = 0; i < existingLines.size(); i++){
			Competitor__c c = existingLines.get(i);
			if(c.Id == deleteParam){	
				delete c;
				existingLines.remove(i);
			}
		}
	}
}