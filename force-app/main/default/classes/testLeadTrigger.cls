@isTest
private class testLeadTrigger
{
    /* License__c objektumot már nem használják, kommentezve 2019.02.12. gp
	static testMethod void myUnitTest01()
    {
        test.startTest();

        Account account01 = new Account(Name = 'Account Name');
        insert account01;
        
        License__c license01 = 
        	new License__c
        		(
        			Name = 'license01',
        			License_ID__c = '0123',
        			Account_ID_text__c = account01.Id,
        			License_Type__c = 'Evaluation',
        			Account_Name__c = 'Account Name',
        			Contact_Last_Name_Eval__c = 'last name',
        			Products_Names__c='test'
        		);
        insert license01;
        
        License__c license02 = [SELECT Lead__c, Account__c FROM License__c WHERE Id = :license01.Id];
        System.debug(JSON.serialize([SELECT ID FROM Lead]));
        system.debug('##### ' + license02);
        Lead lead=[select id,LastName,FirstName,Status,Company,Email,Licence_No__c,Country,State,Account_Alias__c,Product_Area_Interest__c from Lead where id=:license02.Lead__c];
        system.debug('##### ' + lead);
        
        Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(license02.Lead__c);
		lc.setConvertedStatus('Qualified');

		Database.LeadConvertResult lcr = Database.convertLead(lc);
		
		Lead lead01 = [SELECT ConvertedAccountId, ConvertedContactId, LeadSource FROM Lead WHERE Id = :license02.Lead__c];
		
		system.debug('##### ' + lead01);
		
		License__c license03 = [SELECT Lead__c, Account__c, Contact_Name__c FROM License__c WHERE Id = :license01.Id];
		
		system.debug('##### ' + license03);
		
		system.assertequals(lead01.ConvertedAccountId, license03.Account__c);
		system.assertequals(lead01.ConvertedContactId, license03.Contact_Name__c);		
		
		test.stopTest();
    }
//*/    
    static testMethod void myUnitTest02()
    {
    	LeadController lc = new LeadController();
    	lc.getLeadList();
    }
    
    // nincs semmi
    @isTest(SeeAllData=false)
    static void myUnitTest03() 
    {
    	Lead newLead = new Lead();
    	newLead.LeadSource = 'Website Demo Request';
    	newLead.LastName = 'lastName';
    	newLead.Company = 'company';
    	newLead.Forum_Id__c = '1234';
    	newLead.Demo_Request_Product__c = 'product';
    	newLead.Demo_Request_Data__c = 'data';
    	newLead.Product_Area_Interest__c='test';
    	
    	test.startTest();
    	insert newLead;
    	test.stopTest();
    	
    	List<Demo_Request__c> demoRequestList;
    	List<Lead> newLeadList;
    	
    	demoRequestList = 
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c, Lead__c
    			FROM Demo_Request__c 
    		];
    		
    	newLeadList =
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c
    			FROM Lead
    		];
    	
    	system.assertEquals(newLeadList[0].Id, demoRequestList[0].Lead__c);
    	system.assertEquals(newLeadList[0].Demo_Request_Product__c, demoRequestList[0].Demo_Request_Product__c);
    	system.assertEquals(newLeadList[0].Demo_Request_Data__c, demoRequestList[0].Demo_Request_Data__c);
    }
    
    // van nyitott lead
    @isTest(SeeAllData=false)
    static void myUnitTest04() 
    {
    	Lead newLead01 = new Lead();
    	newLead01.LeadSource = 'leadsource';
    	newLead01.LastName = 'lastName01';
    	newLead01.Company = 'company';
    	newLead01.Forum_Id__c = '1234';
    	newLead01.Demo_Request_Product__c = 'product01';
    	newLead01.Demo_Request_Data__c = 'data';
    	newLead01.Product_Area_Interest__c='test';
    	
    	insert newLead01;
		
		Lead newLead02 = new Lead();
    	newLead02.LeadSource = 'Website Demo Request';
    	newLead02.LastName = 'lastName02';
    	newLead02.Company = 'company';
    	newLead02.Forum_Id__c = '1234';
    	newLead02.Demo_Request_Product__c = 'product02';
    	newLead02.Demo_Request_Data__c = 'data';
    	newLead02.Product_Area_Interest__c='test';
    	
    	test.starttest();
    	insert newLead02;
    	
    	system.debug([ SELECT Id, LastName FROM Lead ]);
    	test.stoptest();
    	
    	system.debug([ SELECT Id, LastName FROM Lead ]);
		
    	
    	List<Demo_Request__c> demoRequestList;
    	List<Lead> newLeadList;
    	
    	demoRequestList = 
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c, Lead__c
    			FROM Demo_Request__c
    			ORDER BY Demo_Request_Product__c
    			ASC
    		];
    		
    	newLeadList =
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c
    			FROM Lead
    			ORDER BY LastName
    			ASC
    		];
    		
    	system.debug(demoRequestList);
    	system.debug(newLeadList);
    	
    	system.assertEquals(1, newLeadList.size());
    	system.assertEquals(1, demoRequestList.size());
    	system.assertEquals(newLeadList[0].Id, demoRequestList[0].Lead__c);
    }
    
    // nincs lead, van contact
    @isTest(SeeAllData=false)
    static void myUnitTest05() 
    {
		Account account01 = new Account();
		account01.Name = 'name';
		insert account01;
		
		Contact contact01 = new Contact();
		contact01.AccountId = account01.Id;
		contact01.LastName = 'lastName';
		contact01.Forum_Id__c = '1234';
		insert contact01;
		
		Lead newLead02 = new Lead();
    	newLead02.LeadSource = 'Website Demo Request';
    	newLead02.LastName = 'lastName02';
    	newLead02.Company = 'company';
    	newLead02.Forum_Id__c = '1234';
    	newLead02.Demo_Request_Product__c = 'product02';
    	newLead02.Demo_Request_Data__c = 'data';	
    	newLead02.Product_Area_Interest__c='test';	
    	
    	test.starttest();
    	insert newLead02;
    	test.stoptest();
    	
    	system.debug([ SELECT Id, LastName FROM Lead ]);
		
    	
    	List<Demo_Request__c> demoRequestList;
    	List<Lead> newLeadList;
    	
    	demoRequestList = 
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c, Lead__c, Contact__c
    			FROM Demo_Request__c
    			ORDER BY Demo_Request_Product__c
    			ASC
    		];
    		
    	newLeadList =
    		[
    			SELECT Id, Demo_Request_Product__c, Demo_Request_Data__c
    			FROM Lead
    			ORDER BY LastName
    			ASC
    		];
    		
    	system.debug(demoRequestList);
    	system.debug(newLeadList);
    	
    	system.assertEquals(0, newLeadList.size());
    	system.assertEquals(1, demoRequestList.size());
    	system.assertEquals(null, demoRequestList[0].Lead__c);
    	system.assertEquals(contact01.Id, demoRequestList[0].Contact__c);
    }
    
    // konvertálás
    @isTest(SeeAllData=false)
    static void myUnitTest06() 
    {
    	Lead newLead = new Lead();
    	newLead.LeadSource = 'Website Demo Request';
    	newLead.LastName = 'lastName';
    	newLead.Company = 'company';
    	newLead.Forum_Id__c = '1234';
    	newLead.Demo_Request_Product__c = 'product';
    	newLead.Demo_Request_Data__c = 'data';
    	newLead.Product_Area_Interest__c='test';
    	insert newLead;
    	
    	Demo_Request__c dr = new Demo_Request__c();
    	dr.Lead__c = newLead.Id;
    	insert dr;
    	
    	
    	Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(newLead.Id);
		lc.setConvertedStatus('Qualified');

		Database.LeadConvertResult lcr = Database.convertLead(lc);
		
		
		List<Lead> leadList;
		
		leadList =
			[
				SELECT Id, ConvertedContactId
				FROM Lead
				WHERE Id = :newLead.Id
			];
			
		List<Demo_Request__c> drList;
		
		drList =
			[
				SELECT Id, Contact__c, Lead__c
				FROM Demo_Request__c
			];
		
		system.assertEquals(leadList[0].Id, drList[0].Lead__c);
		system.assertEquals(leadList[0].ConvertedContactId, drList[0].Contact__c);
    }
}