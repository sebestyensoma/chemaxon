public with sharing class OpportunityLineMultiEdit 
{
    string oppId;
    string sfdcBaseUrl;
    List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();
    List<Opportunity> opportunityList = new List<Opportunity>();
    Opportunity opportunity = new Opportunity();
    
    private final OpportunityLineItem opportunityLineItem;
    private id opportunityLineItemId;
    
    /*
    public opportunityLineMultiEdit(ApexPages.standardcontroller controller)
    {
        try
        {
            oppId = ApexPages.currentPage().getParameters().get('oppId');
            
            opportunityLoad();
            opportunityLineItemListLoad();
        }
        catch(exception e)
        {
            oppId = '';
        }
    }
    */
    
    public opportunityLineMultiEdit(ApexPages.standardcontroller stdController)
    {
        opportunityLineItem = (OpportunityLineItem)stdController.getRecord();
        
        opportunityLineItemId = opportunityLineItem.Id;
        
        oppId = [ SELECT OpportunityId FROM OpportunityLineItem WHERE Id = :opportunityLineItemId LIMIT 1 ].OpportunityId;
    }
    
    public opportunityLineMultiEdit()
    {
        system.debug('debu');
        
        sfdcBaseUrl = Url.getSalesforceBaseUrl().toExternalForm();
        
        try
        {
            oppId = ApexPages.currentPage().getParameters().get('oppId');
            
            opportunityLoad();
            opportunityLineItemListLoad();
        }
        catch(exception e)
        {
            oppId = '';
        }
    }
    
    void opportunityLoad()
    {
        opportunitylist =
            [
                SELECT
                    Id,
                    Name,
                    T_Total_Price_Rollup__c,
                    CurrencyISOCode
                FROM
                    Opportunity
                WHERE
                    Id = :oppId
                LIMIT 1
            ];
        
        if(opportunityList.size() != 0)
        {
            opportunity = opportunityList[0];
        }
    }
    
    void opportunityLineItemListLoad()
    {
        opportunityLineItemList =
            [
                SELECT
                    Id,
                    Discount,
                    UnitPrice,
                    TotalPrice,
                    PricebookEntry.ProductCode,
                    User_Limit__c,
                    Target_Price__c,
                    License_Type__c,
                    System_Message__c,
                    Original_Price__c,
                    T_Calculate_Prices__c
                FROM
                    OpportunityLineItem
                WHERE
                    OpportunityId = :oppId
                ORDER BY
                    SortOrder, PricebookEntry.ProductCode
            ];
    }
    
    public pagereference gotoMultiEdit()
    {
        pagereference pageref = new pagereference('/apex/OpportunityLineMultiEdit?oppId=' + oppId);
        pageref.setredirect(true);
        
        return pageref;
    }
    
    public pagereference save()
    {
        try
        {
            update opportunityLineItemList;
            
            pagereference pageref;
            
            pageref = new Pagereference('/' + oppId);
            
            return pageref;
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getMessage()));
            
            return null;
        }
    }
    
    public pagereference quickSave()
    {
        try
        {
            for(OpportunityLineItem currentOli : opportunityLineItemList)
            {
            	if(currentOli.T_Calculate_Prices__c == true)
            	{
            		currentOli.T_Calculate_Prices__c = false;
            	}
            	else
            	{
            		currentOli.T_Calculate_Prices__c = true;
            	}
            }
            
            update opportunityLineItemList;
            
            pagereference pageref;
            
            pageref = new Pagereference('/apex/OpportunityLineMultiEdit?oppId=' + oppId);
            
            pageref.setredirect(true);
            
            return pageref;
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getMessage()));
            
            return null;
        }
    }
    
    public pagereference cancel()
    {
        pagereference pageref;
            
        pageref = new Pagereference('/' + oppId);
            
        return pageref;
    }
    
    
    public Opportunity getOpportunity()
    {
        return opportunity;
    }
    
    public List<OpportunityLineItem> getOpportunityLineItemList()
    {
        return opportunityLineItemList;
    }
    
    public string getOppId()
    {
        return oppId;
    }
}