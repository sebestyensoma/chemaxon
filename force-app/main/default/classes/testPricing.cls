@isTest
private class testPricing 
{
    /*
    	testing
    	QuoteLineItem.trigger
    	OpportunityLineItem.trigger
    	PriceHandler.cls
    	PriceHandlerFuture.cls
    */
    
    @isTest(seeAllData=true)
    static void myUnitTest01() 
    {
		id standardPricebookId;
		List<Product2> product2List = new List<Product2>();
		List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
		List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
		
		standardPricebookId = test.getStandardPricebookId();
		
		Product2 product01 = new Product2();
		product01.Name = 'name01';
		product01.ProductCode = 'code01';
		product01.IsActive = true;
		product01.User_LOG__c = 1.7;
		product01.Bundle_LOG__c = 1.8;
		product2List.add(product01);
		
		insert product2List;
		
		
		PricebookEntry pricebookEntry01 = new PricebookEntry();
		pricebookEntry01.Pricebook2Id = standardPricebookId;
		pricebookEntry01.Product2Id = product2List[0].Id;
		pricebookEntry01.UnitPrice = 1410;
		pricebookEntry01.IsActive = true;
		pricebookEntryList.add(pricebookEntry01);
		
		insert pricebookEntryList;
		
		
		Account account01 = new Account();
		account01.Name = 'account name';
		insert account01;
		
		Opportunity opportunity01 = new Opportunity();
		opportunity01.Name = 'opportunity name';
		opportunity01.AccountId = account01.Id;
		opportunity01.StageName = 'New';
		opportunity01.CloseDate = date.newInstance(2014, 12, 31);
		insert opportunity01;
		
		Quote quote01 = new Quote();
		quote01.Name = 'quote name';
		quote01.OpportunityId = opportunity01.Id;
		quote01.Pricebook2Id = standardPricebookId;
		insert quote01;
		
		
		QuoteLineItem quoteLineItem01 = new QuoteLineItem();
		quoteLineItem01.PricebookEntryId = pricebookEntryList[0].Id;
		quoteLineItem01.QuoteId = quote01.Id;
		quoteLineItem01.Quantity = 1;
		quoteLineItem01.UnitPrice = 1;
		quoteLineItem01.User_Limit__c = 1;
		quoteLineItem01.License_type__c = 'Annual';
		quoteLineItemList.add(quoteLineItem01);
		
		insert quoteLineItemList;
		
		delete quoteLineItemList;
    }
}