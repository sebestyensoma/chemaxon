@isTest
private class testOpportunityQuoteLineMultiEdit
{
    @isTest(seeAllData=false)
    static void myUnitTest01()
    {
        id standardPricebookId;
        List<Product2> product2List = new List<Product2>();
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
		List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();
        
        standardPricebookId = test.getStandardPricebookId();
        
        Product2 product01 = new Product2();
		product01.Name = 'name01';
		product01.ProductCode = 'code01';
		product01.IsActive = true;
		product01.User_LOG__c = 1.7;
		product01.Bundle_LOG__c = 1.8;
		product2List.add(product01);
		
		insert product2List;
		
		
		PricebookEntry pricebookEntry01 = new PricebookEntry();
		pricebookEntry01.Pricebook2Id = standardPricebookId;
		pricebookEntry01.Product2Id = product2List[0].Id;
		pricebookEntry01.UnitPrice = 1410;
		pricebookEntry01.IsActive = true;
		pricebookEntry01.CurrencyIsoCode = 'EUR';
		pricebookEntryList.add(pricebookEntry01);
		
		insert pricebookEntryList;
		        
        
        Account account01 = new Account(CurrencyIsoCode = 'EUR', Name = 'name');
        insert account01;
        
        Opportunity opportunity01 = 
        	new Opportunity
        		(
        			Name = 'name',
        			AccountId = account01.Id,
        			StageName = 'stage',
        			CloseDate = system.today(),
        			CurrencyIsoCode = 'EUR'
        		);
		insert opportunity01;
		
		
		OpportunityLineItem oli01 = new OpportunityLineItem();
		oli01.PricebookEntryId = pricebookEntryList[0].Id;
		oli01.OpportunityId = opportunity01.Id;
		oli01.Quantity = 1;
		oli01.UnitPrice = 1;
		insert oli01;
		
		
		
		/*
		OpportunityLineItem oli01 =
			new OpportunityLineItem
				(
					OpportunityId = opportunity01.Id,
					
				);
		*/
        
        ApexPages.StandardController controller = 
        	new ApexPages.standardController(oli01);
        
        OpportunityLineMultiEdit olme = new OpportunityLineMultiEdit(controller);
        
        olme.save();
        olme.quickSave();
        olme.cancel();
        olme.getOpportunity();
        olme.getOpportunityLineItemList();
        olme.getOppId();
        
        //QuoteLineMultiEdit qlme = new QuoteLineMultiEdit();
    }
    
    @isTest(seeAllData=true)
    static void myUnitTest02()
    {
        id standardPricebookId;
        List<Product2> product2List = new List<Product2>();
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
		List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();
        
        standardPricebookId = test.getStandardPricebookId();
        
        Product2 product01 = new Product2();
		product01.Name = 'name01';
		product01.ProductCode = 'code01';
		product01.IsActive = true;
		product01.User_LOG__c = 1.7;
		product01.Bundle_LOG__c = 1.8;
		product2List.add(product01);
		
		insert product2List;
		
		
		PricebookEntry pricebookEntry01 = new PricebookEntry();
		pricebookEntry01.Pricebook2Id = standardPricebookId;
		pricebookEntry01.Product2Id = product2List[0].Id;
		pricebookEntry01.UnitPrice = 1410;
		pricebookEntry01.IsActive = true;
		pricebookEntry01.CurrencyIsoCode = 'EUR';
		pricebookEntryList.add(pricebookEntry01);
		
		insert pricebookEntryList;
		        
        
        Account account01 = new Account(CurrencyIsoCode = 'EUR', Name = 'name');
        insert account01;
        
        Opportunity opportunity01 = 
        	new Opportunity
        		(
        			Name = 'name',
        			AccountId = account01.Id,
        			StageName = 'stage',
        			CloseDate = system.today(),
        			CurrencyIsoCode = 'EUR'
        		);
		insert opportunity01;
		
		
		Quote quote01 = new Quote();
		quote01.Name = 'quote name';
		quote01.OpportunityId = opportunity01.Id;
		quote01.Pricebook2Id = standardPricebookId;
		insert quote01;
		
		
		QuoteLineItem quoteLineItem01 = new QuoteLineItem();
		quoteLineItem01.PricebookEntryId = pricebookEntryList[0].Id;
		quoteLineItem01.QuoteId = quote01.Id;
		quoteLineItem01.Quantity = 1;
		quoteLineItem01.UnitPrice = 1;
		quoteLineItem01.User_Limit__c = 1;
		quoteLineItem01.License_type__c = 'Annual';
		
		insert quoteLineItem01;
		
        
        ApexPages.StandardController controller = 
        	new ApexPages.standardController(quoteLineItem01);
        
        QuoteLineMultiEdit qlme = new QuoteLineMultiEdit(controller);
        
        qlme.save();
        qlme.quickSave();
        qlme.cancel();
        qlme.getQuote();
        qlme.getQuoteLineItemList();
        qlme.getQuoteId();
        
        //QuoteLineMultiEdit qlme = new QuoteLineMultiEdit();
    }
}