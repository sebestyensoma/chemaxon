@isTest
private class TestExtensionOLIMultiLayout {
    @istest(SeeAllData=true) static void myExtOLITest() {
    	PriceBook2 standardPB = [SELECT Id FROM PriceBook2 WHERE IsStandard = true];		
		Opportunity myOppty = new Opportunity(Name='test Oppty', Pricebook2Id=standardPB.Id,StageName='Needs Analysis', CloseDate=Date.today().addDays(10), CurrencyIsoCode='EUR');
		insert myOppty;
		PageReference testPref =  Page.OpportunityLineMultiLayout;
		testPref.getParameters().put('id',myOppty.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(myOppty);
    	extensionOLIMultiLayout ext = new extensionOLIMultiLayout(myStandardContr);
    	ext.userLimit=10;
    	List<PricebookEntry> listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode, UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :standardPB.Id AND IsActive=true AND CurrencyIsoCode = 'EUR' ORDER BY Product2.Name];
    	for(PricebookEntry loopPBE:listPBEs){
    		ext.isPBEChecked.put(loopPBE.Id,true);
    	}
    	ext.saveForAll();
    	String json = ext.outJSONtfListString;    	
    }
    
    @istest(SeeAllData=true) static void myExtQLITest() {
    	PriceBook2 standardPB = [SELECT Id FROM PriceBook2 WHERE IsStandard = true];		
		Opportunity myOppty = new Opportunity(Name='test Oppty', Pricebook2Id=standardPB.Id,StageName='Needs Analysis', CloseDate=Date.today().addDays(10), CurrencyIsoCode = 'USD');
		insert myOppty;
		Quote myQuote = new Quote(Name='test quote', OpportunityId=myOppty.Id, Pricebook2Id=standardPB.Id, Invoice_Type__c = 'LLC');
		insert myQuote;
		
		PageReference testPref =  Page.QuoteLineMultiLayout;
		testPref.getParameters().put('id',myQuote.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(myQuote);
    	extensionQLIMultiLayout ext = new extensionQLIMultiLayout(myStandardContr);
    	ext.userLimit=10;
    	List<PricebookEntry> listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode,UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :standardPB.Id AND IsActive=true AND CurrencyIsoCode = 'USD' ORDER BY Product2.Name];
    	for(PricebookEntry loopPBE:listPBEs){
    		ext.isPBEChecked.put(loopPBE.Id,true);
			//ext.consultingTeam.put(loopPBE.Id, new QuoteLineItem(Consulting_team__c = 'Biologics'));
    	}
		ext.proxyQLI.License_type__c = 'Consulting';	
		ext.proxyQLI.Consulting_team__c = 'Biologics';
    	ext.saveForAll();


		ext.proxyQLI.License_type__c = 'Consulting';
		ext.proxyQLI.Consulting_team__c = 'Biologics';
    	ext.userLimit = null;
		ext.saveForAll();

		ext.proxyQLI.License_type__c = null;
		ext.userLimit = 1;
    	ext.saveForAll();

		ext.proxyQLI.License_type__c = null;
		ext.userLimit = null;
    	ext.saveForAll();

		ext.proxyQLI.License_type__c = 'Subscription';
    	ext.proxyQLI.Subscription_from__c = null;
		ext.saveForAll();

		ext.proxyQLI.License_type__c = 'Subscription';
    	ext.proxyQLI.Subscription_from__c = System.today().addDays(1);
    	ext.proxyQLI.Subscription_to__c = System.today();

		ext.saveForAll();
		


    	String json = ext.outJSONtfListString;
		
		ext.getPackageName(); 
		ext.cancel();
		ext.setValues();   	
		ext.getProxyQLI();  
		ext.selectedPackage = ''; 	
		//ext.packageSelection();
		ext.boom();
    }
}

/*
@isTest
private class TestExtensionOLIMultiLayout {
    @istest(SeeAllData=true) static void myExtOLITest() {
    	PriceBook2 standardPB = [SELECT Id FROM PriceBook2 WHERE IsStandard = true];		
		Opportunity myOppty = new Opportunity(Name='test Oppty', Pricebook2Id=standardPB.Id,StageName='Needs Analysis', CloseDate=Date.today().addDays(10));
		insert myOppty;
		PageReference testPref =  Page.OpportunityLineMultiLayout;
		testPref.getParameters().put('id',myOppty.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(myOppty);
    	extensionOLIMultiLayout ext = new extensionOLIMultiLayout(myStandardContr);
    	ext.userLimit=10;
    	List<PricebookEntry> listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode, UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :standardPB.Id AND IsActive=true ORDER BY Product2.Name];
    	for(PricebookEntry loopPBE:listPBEs){
    		ext.isPBEChecked.put(loopPBE.Id,true);
    	}
    	ext.saveForAll();
    	String json = ext.outJSONtfListString;    	
    }
    
    @istest(SeeAllData=true) static void myExtQLITest() {
    	PriceBook2 standardPB = [SELECT Id FROM PriceBook2 WHERE IsStandard = true];		
		Opportunity myOppty = new Opportunity(Name='test Oppty', Pricebook2Id=standardPB.Id,StageName='Needs Analysis', CloseDate=Date.today().addDays(10));
		insert myOppty;
		Quote myQuote = new Quote(Name='test quote', OpportunityId=myOppty.Id, Pricebook2Id=standardPB.Id);
		insert myQuote;
		
		PageReference testPref =  Page.QuoteLineMultiLayout;
		testPref.getParameters().put('id',myQuote.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(myQuote);
    	extensionQLIMultiLayout ext = new extensionQLIMultiLayout(myStandardContr);
    	ext.userLimit=10;
    	List<PricebookEntry> listPBEs = [SELECT Id,Product2.Name, Product2.ProductCode,UnitPrice FROM PricebookEntry WHERE Pricebook2Id = :standardPB.Id AND IsActive=true ORDER BY Product2.Name];
    	for(PricebookEntry loopPBE:listPBEs){
    		ext.isPBEChecked.put(loopPBE.Id,true);
    	}
    	ext.saveForAll();
    	String json = ext.outJSONtfListString;    	
    }
}
*/