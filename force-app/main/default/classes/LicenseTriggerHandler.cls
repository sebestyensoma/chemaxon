public with sharing class LicenseTriggerHandler 
{
	public Set<string> getExistingAccountIdSet(Set<string> accountIdSet)
	{
		Set<string> returnSet = new Set<string>();
		
		List<Account> accountList;
		
		accountList =
			[
				SELECT
					Id
				FROM
					Account
				WHERE
					Id IN :accountIdSet
			];
			
		for(Account currentA : accountList)
		{
			returnSet.add(currentA.Id);
		}
		
		return returnSet;
	}
	
	public Map<string, Lead> getExistingLeadEmailStringMap(Set<string> accountEmailSet)
	{
		Map<string, Lead> returnMap = new Map<string, Lead>();
		
		List<Lead> leadList;
		List<string> leadStatusList = new List<string>();
		
		leadStatusList.add('New');
		leadStatusList.add('Open');
		leadStatusList.add('Emailed');
		leadStatusList.add('Called');
		
		leadList =
			[
				SELECT
					Id,
					Email
				FROM
					Lead
				WHERE
					Status IN :leadStatusList
					AND
					Email IN :accountEmailSet
			];
			
		for(Lead currentL : leadList)
		{
			returnMap.put(currentL.Email, currentL);
		}
		
		return returnMap;
	}
	
	public Lead createNewLead(License__c license)
	{
		Lead returnLead = new Lead();
		
		if(license.Contact_Last_Name_Eval__c == NULL || license.Contact_Last_Name_Eval__c == '')
		{
			license.Contact_Last_Name_Eval__c = 'null';
		}
		
		if(license.Account_Name__c == NULL || license.Account_Name__c == '')
		{
			license.Account_Name__c = 'null';
		}
		
		returnLead.LastName = license.Contact_Last_Name_Eval__c;
		returnLead.FirstName = license.Contact_First_Name_Eval__c;
		returnLead.Status = 'New';
		returnLead.Company = license.Account_Name__c;
		returnLead.Email = license.Account_Email__c;
		returnLead.LeadSource = 'Website Evaluation License Download';
		returnLead.Licence_No__c = license.Name;
		returnLead.Country = license.Country_Eval__c;
		returnLead.State = license.State_Eval__c;
		returnLead.Account_Alias__c = license.Account_Alias__c;
		
		return returnLead;
	}
	
	public Map<string, Lead> getNewLeadsEmailMap(List<Lead> leadList)
	{
		Map<string, Lead> returnMap = new Map<string, Lead>();
		
		Database.DMLOptions dmlOpts = new Database.DMLOptions();
		dmlOpts.assignmentRuleHeader.useDefaultRule= true;
		//dmlOpts.assignmentRuleHeader.assignmentRuleId= '01Q200000006NYS';
		
		for(Lead currentL : leadList)
		{
			currentL.setOptions(dmlOpts);
			currentL.T_Dummy__c=true;
		}
		
		try
		{
			insert leadList;
		}
		catch(exception e)
		{
			sendEmail(leadList, e);
		}
		
		List<Lead> newLeadList;
		
		newLeadList =
			[
				SELECT
					Id,
					Email
				FROM
					Lead
				WHERE
					Id IN :leadList
			];
			
		for(Lead currentL : newLeadList)
		{
			returnMap.put(currentL.Email, currentL);
		}
		
		return returnMap;
	}
	
	public void deleteLicenses(Set<id> idsToDeleteSet)
	{
		List<License__c> licensesToDelete =
			[
				SELECT
					Id
				FROM
					License__c
				WHERE
					Id IN :idsToDeleteSet
			];
	
		delete licensesToDelete;
	}
	
	private void sendEmail(List<Lead> leadList, exception e)
	{
		blob csvBlob;
		string subject;
		string finalstring;
		string csvname;
		Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		
		List<string> toAddresses = new list<string>();
		
		csvname = 'hibalista.csv';
		finalstring = 'License_Number__c';
		subject = 'Chemaxon licensz töltés hiba';
		
		for(Lead currentL : leadList)
		{
			finalstring += currentL.Licence_No__c + '\n';
		}
		
		csvBlob = Blob.valueOf(finalstring);
		csvAttc.setFileName(csvname);
		csvAttc.setBody(csvBlob);
		toAddresses.add('adam.bobor@attentioncrm.hu');
		//toAddresses.add('chemaxon@attentioncrm.hu');
		
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('a ' + e.getMessage() + ' hiba innen jött: licenseTriggerHandler @' + e.getLineNumber());
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
		
		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	}
}