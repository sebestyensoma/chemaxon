public class YTDDashboard2  {

	public Integer oldal { get; set; }

	// Company total
	public AdatSor totalYTD { get; set; }
	public AdatSor totalNewSales { get; set; }
	public AdatSor totalConsulting { get; set; }
	public AdatSor totalRenewal { get; set; }

	// EU - YTD Revenue
	public AdatSor euYTD { get; set; }
	public AdatSor euNewSales { get; set; }
	public AdatSor euConsulting { get; set; }
	public AdatSor euRenewal { get; set; }

	// US
	public AdatSor usYTD { get; set; }
	public AdatSor usNewSales { get; set; }
	public AdatSor usConsulting { get; set; }
	public AdatSor usRenewal { get; set; }

	// Patcore (Japan)
	public AdatSor jpnYTD { get; set; }
	public AdatSor jpnNewSales { get; set; }
	public AdatSor jpnRenewal { get; set; }

	// CloudS. (China)
	public AdatSor chnYTD { get; set; }
	public AdatSor chnNewSales { get; set; }
	public AdatSor chnRenewal { get; set; }

	// T&J. (S Korea)
	public AdatSor koreaYTD { get; set; }
	public AdatSor koreaNewSales { get; set; }
	public AdatSor koreaRenewal { get; set; }

	// Andvent (India)
	public AdatSor indiaYTD { get; set; }
	public AdatSor indiaNewSales { get; set; }
	public AdatSor indiaRenewal { get; set; }

	// Australia
	public AdatSor auYTD { get; set; }
	public AdatSor auNewSales { get; set; }
	public AdatSor auRenewal { get; set; }

	// Other
	public AdatSor otherYTD { get; set; }

	
	private String currencyCode;
	private String thisYearClause;
	private String lastYearClause;
	

	private final String SQL_BASE =
 		'SELECT SUM(Total_in_#CURR__c) total '
		+ 'FROM QuoteLineItem '
		+ 'WHERE #DATE_FILTER #STATUS_FILTER #LICENSE_FILTER #OPPORTUNITY_FILTER #ACCOUNT_FILTER #BILLING_FILTER'
		;

	public void init(){
		
		oldal = 0;
		currencyCode = 'USD';
		
		Date dateFrom = Date.newInstance(Date.today().year(), 1, 1);
    	Date dateTo = Date.today();

		thisYearClause =
    		'Quote.Invoice_Date__c >= ' + String.valueOf(dateFrom)
    		+ ' AND Quote.Invoice_Date__c <= ' + String.valueOf(dateTo);
		
		lastYearClause = 
    		'Quote.Invoice_Date__c >= ' + String.valueOf(dateFrom.addYears(-1))
    		+ ' AND Quote.Invoice_Date__c <= ' + String.valueOf(dateTo.addYears(-1));
		
		
		doReport();

	}



	public void doReport() {
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Company total
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Total YTD Revenue
		String statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		String licenseClause = '';
		String opportunityClause = '';
		String accountClause = '';
		String billingClause = '';

		totalYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\', \'Consulting\')) ';

		totalNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Consulting Revenue 
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c = \'Consulting\' ';

		totalConsulting = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';

		totalRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// EU - YTD Revenue

		// Total
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = ' AND (NOT Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = ' AND (NOT Quote.Opportunity.Account.Name LIKE \'%Patcore%\') AND Quote.Opportunity.Account.Revenue_related_team__c != \'US\' '; // TODO: Account Name does not contain Patcore melyik mező? Quote.Opportunity.Account.Name?

		euYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales (Annual, Perpetual)
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\', \'Consulting\')) ';
		opportunityClause = ' AND (NOT Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = ' AND (NOT Quote.Opportunity.Account.Name LIKE \'%Patcore%\') AND Quote.Opportunity.Account.Revenue_related_team__c != \'US\' '; // TODO: Account Name does not contain Patcore melyik mező? Quote.Opportunity.Account.Name?

		euNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Consulting 
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c = \'Consulting\' ';
		opportunityClause = ' AND (NOT Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = ' AND (NOT Quote.Opportunity.Account.Name LIKE \'%Patcore%\') AND Quote.Opportunity.Account.Revenue_related_team__c != \'US\' '; // TODO: Account Name does not contain Patcore melyik mező? Quote.Opportunity.Account.Name?

		euConsulting = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal 
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = ' AND (NOT Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = ' AND (NOT Quote.Opportunity.Account.Name LIKE \'%Patcore%\') AND Quote.Opportunity.Account.Revenue_related_team__c != \'US\' '; // TODO: Account Name does not contain Patcore melyik mező? Quote.Opportunity.Account.Name?

		euRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// US

		// Total YTD Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = ' AND (Quote.Opportunity.Account.Revenue_related_team__c = \'US\' OR Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = '';

		usYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\', \'Consulting\')) ';
		opportunityClause = ' AND (Quote.Opportunity.Account.Revenue_related_team__c = \'US\' OR Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = ''; 

		usNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Consulting Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c = \'Consulting\' ';
		opportunityClause = ' AND (Quote.Opportunity.Account.Revenue_related_team__c = \'US\' OR Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = '';

		usConsulting = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal 
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = ' AND (Quote.Opportunity.Account.Revenue_related_team__c = \'US\' OR Quote.Opportunity.Owner.Name IN (\'Bahram Manavi\', \'Kara West\',\'Aurora Costache\', \'David Deng\',\'Jonathan Patterson\', \'Richard Johnson\',\'Cassell Essell\')) ';
		accountClause = '';

		usRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Patcore (Japan)

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'Japan\', \'Fukuoka Japan\') ';

		jpnYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')) ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'Japan\', \'Fukuoka Japan\') ';

		jpnNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'Japan\', \'Fukuoka Japan\') ';

		jpnRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// CloudS. (China)

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'China\', \'Shanghai, China\') ';

		chnYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')) ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'China\', \'Shanghai, China\') ';

		chnNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN (\'China\', \'Shanghai, China\') ';

		chnRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// T&J. (S Korea)

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN(\'South Korea\', \'Korea\') ';

		koreaYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')) ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN(\'South Korea\', \'Korea\') ';

		koreaNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry IN(\'South Korea\', \'Korea\') ';

		koreaRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Andvent (India)

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'India\' ';

		indiaYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')) ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'India\' ';

		indiaNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'India\' ';

		indiaRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Australia

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'Australia\' ';

		auYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// New Sales Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND (NOT License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')) ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'Australia\' ';

		auNewSales = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);

		// Renewal
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = ' AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\') ';
		opportunityClause = '';
		accountClause = '';
		billingClause = ' AND Quote.Opportunity.Account.BillingCountry = \'Australia\' ';

		auRenewal = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Other

		// Revenue
		statusClause = ' AND Quote.Status IN (\'Invoiced\', \'Settled\', \'Ordered\') ';
		licenseClause = '';
		opportunityClause = '';
		accountClause = '';
		//billingClause = ' AND (NOT Quote.BillingCountry IN (\'Japan\',\'Fukuoka Japan\',\'China\',\'Shanghai, China\',\'South Korea\',\'Korea\',\'India\',\'Australia\',\'USA\',\'United States of America\',\'United States\',\'United State\',\'U.S.A\',\'Albania\',\'Andorra\',\'Armenia\',\'Austria\',\'Azerbaijan\',\'Belarus\',\'Belgium\',\'Bosnia and Herzegovina\',\'Bulgaria\',\'Croatia\',\'Croatia EU VAT no.: HR44205501677\',\'Cyprus\',\'Czech Republic\',\'Denmark\',\'Estonia\',\'Finland\',\'France\',\'Georgia\',\'Germany\',\'Greece\',\'Hungary\',\'Iceland\',\'Ireland\',\'Italy\',\'Kazahstan\',\'Kosovo\',\'Latvia\',\'Lichtenstein\',\'Lithuania\',\'Luxembourg\',\'Macedonia\',\'Malta\',\'Moldova\',\'Monaco\',\'Montenegro\',\'Netherlands\',\'The Netherlands\',\'Norway\',\'Poland\',\'Portugal\',\'Romania\',\'Russia\',\'San Marino\',\'Serbia\',\'Slovakia\',\'Slovenia\',\'Slovenija\',\'Spain\',\'Switzerland\',\'Sweden\',\'Turkey\',\'UK\',\'Ukraine\',\'United Kingdom\',\'England\')) ';
		billingClause = ' AND (NOT Quote.Opportunity.Account.BillingCountry IN (\'Japan\',\'Fukuoka Japan\',\'China\',\'Shanghai, China\',\'South Korea\',\'Korea\',\'India\',\'Australia\',\'USA\',\'United States of America\',\'United States\',\'United State\',\'U.S.A\',\'Albania\',\'Andorra\',\'Armenia\',\'Austria\',\'Azerbaijan\',\'Belarus\',\'Belgium\',\'Bosnia and Herzegovina\',\'Bulgaria\',\'Croatia\',\'Croatia EU VAT no.: HR44205501677\',\'Cyprus\',\'Czech Republic\',\'Denmark\',\'Estonia\',\'Finland\',\'France\',\'Georgia\',\'Germany\',\'Greece\',\'Hungary\',\'Iceland\',\'Ireland\',\'Italy\',\'Kazahstan\',\'Kosovo\',\'Latvia\',\'Lichtenstein\',\'Lithuania\',\'Luxembourg\',\'Macedonia\',\'Malta\',\'Moldova\',\'Monaco\',\'Montenegro\',\'Netherlands\',\'The Netherlands\',\'Norway\',\'Poland\',\'Portugal\',\'Romania\',\'Russia\',\'San Marino\',\'Serbia\',\'Slovakia\',\'Slovenia\',\'Slovenija\',\'Spain\',\'Switzerland\',\'Sweden\',\'Turkey\',\'UK\',\'Ukraine\',\'United Kingdom\',\'England\')) ';
		

		otherYTD = lekerdez(statusClause, licenseClause, opportunityClause, accountClause, billingClause);
	}


	private AdatSor lekerdez(String statusClause, String licenseClause, String opportunityClause, String accountClause, String billingClause){
		Adatsor ret = new AdatSor();

		String thisYearSql = SQL_BASE
    		.replace('#CURR', currencyCode)
    		.replace('#DATE_FILTER', thisYearClause)    		
			.replace('#STATUS_FILTER', statusClause)
			.replace('#LICENSE_FILTER', licenseClause)
			.replace('#OPPORTUNITY_FILTER', opportunityClause)
			.replace('#ACCOUNT_FILTER', accountClause)
			.replace('#BILLING_FILTER', billingClause);

		Decimal thisYTD = (Decimal)Database.query(thisYearSql)[0].get('total');

		String lastYearSql = SQL_BASE
    		.replace('#CURR', currencyCode)
    		.replace('#DATE_FILTER', lastYearClause)
    		.replace('#STATUS_FILTER', statusClause)
    		.replace('#LICENSE_FILTER', licenseClause)
			.replace('#OPPORTUNITY_FILTER', opportunityClause)
			.replace('#ACCOUNT_FILTER', accountClause)
			.replace('#BILLING_FILTER', billingClause);
    		
    	Decimal lastYTD = (Decimal)Database.query(lastYearSql)[0].get('total');

		ret.thisYear = AbstractReportController.formatInteger(thisYTD);
		ret.lastYear = AbstractReportController.formatInteger(lastYTD);
		if(thisYTD != null && lastYTD != null && lastYTD != 0) ret.growth = AbstractReportController.formatInteger(((thisYTD/lastYTD)-1)*100);
		else ret.growth = '';
		
		System.debug(thisYearSql);
		System.debug(thisYTD);
		System.debug(lastYearSql);
		System.debug(lastYTD);
		System.debug(JSON.serialize(ret));
		return ret;
	}


	public void page(){
		if(oldal == 0) oldal = 1;
		else if(oldal == 1) oldal = 0;
	}

	public class AdatSor{
		public String thisYear { get; set; }
		public String lastYear { get; set; }
		public String growth { get; set; }



	}

}