global class QuoteTargetFieldNuller {
	
	@future
    public static void TargetPriceNuller(String inId){
       Quote myQuote = [SELECT Target_Price__c FROM Quote Where Id=:inId];
       myQuote.Target_Price__c=null;
       update myQuote;
    }
    
    @future
    public static void TargetDiscountNuller(String inId){
       Quote myQuote = [SELECT Target_Discount__c FROM Quote Where Id=:inId];
       myQuote.Target_Discount__c=null;
       update myQuote;
    }
}