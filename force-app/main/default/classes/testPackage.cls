@isTest
private class testPackage {
	@isTest(SeeAllData=true) static void myUnitTest() {
		Account testAccount = new Account(Name='987Test Account');
        insert testAccount;
        Opportunity testOppty = new Opportunity(CurrencyIsoCode='EUR',AccountId=testAccount.Id,Name='TestOpportunity 3',LeadSource='Sales',Type='Annual',StageName='Needs Analysis',CloseDate=Date.today().addDays(10));
        insert testOppty;
        List<Pricebook2> myPricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        Quote testQuote = new Quote(Name='987Test Quote',OpportunityId=testOppty.Id,Pricebook2Id=myPricebooks[0].Id);
        insert testQuote;
        
        Package__c testPackage = new Package__c(Quote__c=testQuote.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        insert testPackage;        
        testPackage.Target_Price__c=20000;
        update testPackage;
        delete testPackage;
        
        Package__c testPackage2 = new Package__c(Opportunity__c=testOppty.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        insert testPackage2;        
        testPackage2.Target_Price__c=20000;
        update testPackage2;
        delete testPackage2;  
    }
}

/*
@isTest
private class testPackage {
	@isTest(SeeAllData=true) static void myUnitTest() {
		Account testAccount = new Account(Name='987Test Account');
        insert testAccount;
        Opportunity testOppty = new Opportunity(AccountId=testAccount.Id,Name='TestOpportunity 3',LeadSource='Sales',Type='Annual',StageName='Needs Analysis',CloseDate=Date.today().addDays(10));
        insert testOppty;
        List<Pricebook2> myPricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        Quote testQuote = new Quote(Name='987Test Quote',OpportunityId=testOppty.Id,Pricebook2Id=myPricebooks[0].Id);
        insert testQuote;
        
        Package__c testPackage = new Package__c(Quote__c=testQuote.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        insert testPackage;        
        testPackage.Target_Price__c=20000;
        update testPackage;
        delete testPackage;
        
        Package__c testPackage2 = new Package__c(Opportunity__c=testOppty.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        insert testPackage2;        
        testPackage2.Target_Price__c=20000;
        update testPackage2;
        delete testPackage2;  
    }
}
*/