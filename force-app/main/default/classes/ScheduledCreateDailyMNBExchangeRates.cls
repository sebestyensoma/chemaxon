/**
  @author	Gerse Mihály
  @created	2018-08-17
 
  Minden nap az aktuális árfolyam lekérdezése az MNB-ről, currency létrehozása
  Első futás 11.05-kor, exception esetén (pl még nem elérhetőek az aznapi árfolyamok) fél óra múlva újra próbálkozunk délután 15.10-ig  
*/
public class ScheduledCreateDailyMNBExchangeRates implements Schedulable {

	public Class MissingCurrencyException extends Exception { }

	public static String SCHEDULE_CRON_EXPRESSION = '0 5 11 ? * MON-FRI';

	public static void scheduleForEverydayRun(){
		System.schedule('ScheduledCreateDailyMNBExchangeRates: ' + SCHEDULE_CRON_EXPRESSION, SCHEDULE_CRON_EXPRESSION, new ScheduledCreateDailyMNBExchangeRates());
	}

	// Alapértelmezetten üres az exchangeRateDate, ilyenkor az aznapi árfolyamot kérdezzük le
	public String exchangeRateDateString { get; private set; }
	public static List<String> requiredCurrencies = new List<String> { 'EUR', 'GBP', 'CHF', 'JPY', 'SGD', 'USD' };

	public ScheduledCreateDailyMNBExchangeRates() {
		
	}
	public ScheduledCreateDailyMNBExchangeRates(String exchangeRateDateString) {
		this.exchangeRateDateString = exchangeRateDateString;
	}
	public ScheduledCreateDailyMNBExchangeRates(Date exchangeRateDate) {
		this.exchangeRateDateString = ((Datetime) exchangeRateDate).format('YYYY-MM-dd');
	}

	public void execute(SchedulableContext sc) {
		//System.enqueueJob(new QueueableWebServiceCallout(exchangeRateDate));
		queryMNBWebServiceFuture(exchangeRateDateString);
	}

	@future(callout=true)
	private static void queryMNBWebServiceFuture(String exchangeRateDateString){
		try {
			Map<String, Decimal> currencies;
			if( exchangeRateDateString == null || String.isBlank(exchangeRateDateString)){
				exchangeRateDateString = String.valueOf(Datetime.now().format('YYYY-MM-dd'));
			}
			//callout to web service
			currencies = MNBWebService.getCurrencies(exchangeRateDateString, String.join(requiredCurrencies, ','));
			
			if (!currencies.keySet().containsAll(requiredCurrencies)) {
				throw new MissingCurrencyException('Hiányzik valamelyik árfolyam: ' + currencies.keySet());
			}
			else {
				Currency__c curr = new Currency__c();
				curr.Name = exchangeRateDateString;
				for (String currencyName : currencies.keySet()) {
					switch on currencyName {
						when 'EUR' {
							curr.EUR__c = currencies.get(currencyName);
						}
						when 'GBP' {
							curr.GBP__c = currencies.get(currencyName);
						}
						when 'CHF' {
							curr.CHF__c = currencies.get(currencyName);
						}
						when 'JPY' {
							curr.JPY__c = currencies.get(currencyName);
						}
						when 'SGD' {
							curr.SGD__c = currencies.get(currencyName);
						}
						when 'USD' {
							curr.USD__c = currencies.get(currencyName);
						}
					}
				}
				insert curr;
			}
		}
		catch(Exception e) {
			if (!e.getMessage().contains('List index out of bounds')) {
				Util.sendTextBasedEmailToAddress(Util.UCEMAILADDRESS, 'ScheduledCreateDailyMNBExchangeRates exception',
						  Datetime.now() + '  ' + e.getMessage() + '  ' + e.getStackTraceString());
			}
			scheduleNextRun(exchangeRateDateString);
		}
	}

	@testVisible
	private static void scheduleNextRun(String exchangeRateDate) {
		Datetime nextScheduledRun = System.now().addMinutes(30);
		if( Test.isRunningTest() ) nextScheduledRun = datetime.newInstance(2020, 10, 11, 10, 15, 0);
		if (nextScheduledRun.hour() >= 15 && nextScheduledRun.minute() > 10) {
			sendErrorEmail();
		}
		else {
			if(!Test.isRunningTest()){
				System.schedule('ScheduledCreateDailyMNBExchangeRates '
			                + nextScheduledRun.format('YYYY-MM-dd HH:mm'),
			                '0 ' + nextScheduledRun.minute() + ' ' + nextScheduledRun.hour() + ' ' + nextScheduledRun.day() + ' ' + nextScheduledRun.month() + ' ? ' + nextScheduledRun.year()
							, new ScheduledCreateDailyMNBExchangeRates(exchangeRateDate));
			}
			// Korábban beidőzíttet és lefutott jobok feltakarítása
			for (CronTrigger job :[SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE '%ScheduledCreateDailyMNBExchangeRates%' AND State = 'DELETED']) {
				System.abortJob(job.Id);
			}
		}
	}

	@testVisible
	private static void sendErrorEmail(){
		String emailMessage =	'Dear Finance,\n\n'+
		'Today\'s exchange rate was not provided by the MNB, please check it manually.\n\n'+
		'Best regards,\n'+
		'CRM system';
		String toAddresses = Util.runningInASandbox
		? Util.UCEMAILADDRESS
		: Util.UCEMAILADDRESS +','+Util.FINANCEEMAILADDRESS;
		Util.sendTextBasedEmailToAddress( toAddresses , 'MNB exchange rate' , emailMessage );
	}
}