public with sharing class YTDDashboard {
    private final String SQL_BASE =
 		'SELECT SUM(Total_in_#CURR__c) total '
		+ 'FROM QuoteLineItem '
		+ 'WHERE #DATE_FILTER #COUNTRY_FILTER #LICENSE_FILTER'
		;
    
    public String currencyCode { get; set; }
    public String countryFilter { get; set; }
    public String licenseFilter { get; set; }
    
    public String thisYear { get; private set; }
    public String lastYear { get; private set; }
    
    public void init() {
    	currencyCode = 'EUR';
    	countryFilter = 'ALL';
    	licenseFilter = 'ALL';
    	
    	doReport();
    }
    
    public void doReport() {
    	String countryClause = buildCountryClause();
    	String licenseClause = buildLicenseClause();
    	
    	// This year
    	Date dateFrom = Date.newInstance(Date.today().year(), 1, 1);
    	Date dateTo = Date.today();
    	
    	String thisYearClause =
    		'Quote.Invoice_Date__c >= ' + String.valueOf(dateFrom)
    		+ ' AND Quote.Invoice_Date__c <= ' + String.valueOf(dateTo);
    	
    	String thisYearSql = SQL_BASE
    		.replace('#CURR', currencyCode)
    		.replace('#DATE_FILTER', thisYearClause)
    		.replace('#COUNTRY_FILTER', countryClause)
    		.replace('#LICENSE_FILTER', licenseClause);
    	
    	Decimal thisYTD = (Decimal)Database.query(thisYearSql)[0].get('total');
    	
    	// Last year
    	String lastYearClause = 
    		'Quote.Invoice_Date__c >= ' + String.valueOf(dateFrom.addYears(-1))
    		+ ' AND Quote.Invoice_Date__c <= ' + String.valueOf(dateTo.addYears(-1));
    		
    	String lastYearSql = SQL_BASE
    		.replace('#CURR', currencyCode)
    		.replace('#DATE_FILTER', lastYearClause)
    		.replace('#COUNTRY_FILTER', countryClause)
    		.replace('#LICENSE_FILTER', licenseClause);
    		
    	Decimal lastYTD = (Decimal)Database.query(lastYearSql)[0].get('total');
    	
    	// Output
    	thisYear = AbstractReportController.formatInteger(thisYTD);
    	lastYear = AbstractReportController.formatInteger(lastYTD);
    }
    
    // INTERNAL
    
    private String buildCountryClause() {
    	if (countryFilter == 'ALL') return '';
    	
    	if (countryFilter == 'EU') {
    		return 'AND Quote.Opportunity.Owner.UserRole.Name = \'EU CFT Team Member\'';
    	}
    	if (countryFilter == 'US') {
    		return 'AND Quote.Opportunity.Owner.UserRole.Name = \'US CFT Team Member\'';
    	}
    	if (countryFilter == 'JP') {
    		return 'AND Quote.Opportunity.Account.BillingCountry = \'Japan\'';
    	}
    	if (countryFilter == 'OTHER') {
    		return
    			'AND Quote.Opportunity.Owner.UserRole.Name != \'EU CFT Team Member\''
    			+ ' AND Quote.Opportunity.Owner.UserRole.Name != \'US CFT Team Member\''
    			+ ' AND Quote.Opportunity.Account.BillingCountry != \'Japan\''
    			;
    	}
    	
    	return '';
    }
    
    private String buildLicenseClause() {
    	if (licenseFilter == 'ALL') return '';
    	
    	if (licenseFilter == 'NEW') {
    		return 'AND License_Type__c IN (\'Annual\', \'Perpetual\')';
    	}
    	if (licenseFilter == 'RENEWAL') {
    		return 'AND License_Type__c IN (\'Annual Renewal\', \'Perpetual Renewal\')';
    	}
    	if (licenseFilter == 'CON') {
    		return 'AND License_Type__c = \'Consulting\'';
    	}
    	
    	return '';
    }
    
}