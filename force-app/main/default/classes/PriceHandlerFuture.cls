global class PriceHandlerFuture 
{
	@future
	public static void updateQli(Set<id> quoteIdSet)
	{
		List<QuoteLineItem> qliList = new List<QuoteLineItem>();
		
		qliList = 
			[
				SELECT
					Id,
					T_Calculate_Prices__c
				FROM
					QuoteLineItem
				WHERE
					IsDeleted = false
					AND
					QuoteId IN :quoteIdSet
			];
			
		for(QuoteLineItem currentQli : qliList)
		{
			currentQli.T_Calculate_Prices__c = !currentQli.T_Calculate_Prices__c;
		}
			
		update qliList;
	}
	
	public static void updateQliNonFuture(Set<id> quoteIdSet)
	{
		List<QuoteLineItem> qliList = new List<QuoteLineItem>();
		
		qliList = 
			[
				SELECT
					Id,
					T_Calculate_Prices__c
				FROM
					QuoteLineItem
				WHERE
					IsDeleted = false
					AND
					QuoteId IN :quoteIdSet
			];
			
		for(QuoteLineItem currentQli : qliList)
		{
			currentQli.T_Calculate_Prices__c = !currentQli.T_Calculate_Prices__c;
		}
			
		update qliList;
	}

	@future
	public static void updateOli(Set<id> opportunityIdSet)
	{
		List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
		
		oliList = 
			[
				SELECT
					Id,
					T_Calculate_Prices__c
				FROM
					OpportunityLineItem
				WHERE
					IsDeleted = false
					AND
					OpportunityId IN :opportunityIdSet
			];
			
		for(OpportunityLineItem currentOli : oliList)
		{
			currentOli.T_Calculate_Prices__c = !currentOli.T_Calculate_Prices__c;
		}
			
		update oliList;
	}


	public static void updateOliNonFuture(Set<id> opportunityIdSet)
	{
		List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
		
		oliList = 
			[
				SELECT
					Id,
					T_Calculate_Prices__c
				FROM
					OpportunityLineItem
				WHERE
					IsDeleted = false
					AND
					OpportunityId IN :opportunityIdSet
			];
			
		for(OpportunityLineItem currentOli : oliList)
		{
			currentOli.T_Calculate_Prices__c = !currentOli.T_Calculate_Prices__c;
		}
			
		update oliList;
	}
}