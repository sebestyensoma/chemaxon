@isTest
private class testAttachmentList {

	@isTest
    static void myUnitTest() {
    	
    	String name1 = 'Teszt1.pdf';
    	String name2 = 'Teszt2.pdf';
 
 		String body1 = 'Ez Har Mónika Teszt1.pdf csatolmányának a tartalma';
 		String body2 = 'Ez Kukor Ica Teszt2.pdf csatolmányának a tartalma';
        
        
        AttachmentList testObject = new AttachmentList();
        testObject.initPage();
        
        Account[] testAccounts = new Account[] {
        	new Account(
        		name = 'Kukor Ica'
        	),
        	new Account(
        		name = 'Har Mónika'
        	)
        };
        
        INSERT testAccounts;
        
        Attachment[] testAttachments = new Attachment[] {
			new Attachment(
        		ParentId = testAccounts[1].id,
        		name = name1,
        		body = blob.toPDF(body1),
        		contenttype = 'application/pdf'
        	),
        	new Attachment(
        		ParentId = testAccounts[0].id,
        		name = name2,
        		body = blob.toPDF(body2),
        		contenttype = 'application/pdf'
        	)
        };

        INSERT testAttachments;
        
        testObject.selectedExtension = testObject.extensions[0].getValue();
        testObject.getAccounts();
        System.assert(testObject.renderAccountTable);
        
        testObject.selectedExtension = testObject.extensions[1].getValue();
        testObject.getAccounts();
        System.assert(!testObject.renderAccountTable);

        testObject.selectedExtension= testObject.extensions[2].getValue();
        testObject.getAccounts();
        System.assert(testObject.renderAccountTable);
        
        System.assertEquals(testObject.accountsWithAttachment.size(), 2);
        
        testObject.actionParam = '2';
        testObject.showCorrectTable();
        
        System.assertEquals(testObject.accountsToTable[0].name, 'Har Mónika');
        System.assertEquals(testObject.currentPageNumber, 1);
        
    }
}