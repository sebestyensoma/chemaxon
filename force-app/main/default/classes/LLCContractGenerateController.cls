public class LLCContractGenerateController  {

	public PageReference onStart(){
		String qId = ApexPages.CurrentPage().GetParameters().get('qid');
		Quote q = [SELECT Id, Invoice_Type__c FROM Quote WHERE Id = :qId];
		if(q.Invoice_Type__c != 'LLC'){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You can not create LLC Contract for this Quote.'));
			return null;
		}else{
			return new PageReference('/apex/LLCContract?qid=' + qId);
		}
	}

}