public with sharing class SettledReportController extends AbstractReportController {

	// REPORTING
	
	protected override List<String> createColumnLabels() {
		return new String[] { 'Total' };
	}
	
	protected override void doReport() {
		// Fetch raw data
		List<QuoteLineItem> rawList = [
			SELECT License_Type__c, Quote.Invoice_Date__c, Quote.Actual_Payment_Date__c,
				Total_in_EUR__c, Total_in_USD__c, Total_in_GBP__c, Total_in_HUF__c, Total_in_JPY__c, Total_in_CHF__c, Total_in_SGD__c
			FROM QuoteLineItem
            WHERE Quote.Actual_Payment_Date__c != null AND Quote.Invoice_Date__c != null
			ORDER BY Quote.Invoice_Date__c ASC
		];
		
		// Process rows
		String fieldName = 'Total_in_' + currencyName + '__c';
		for (QuoteLineItem item : rawList) {
			Date todaysDate = Date.today();
			Date paymentDate = item.quote.actual_payment_date__c;
			if (paymentDate == null || paymentDate.daysBetween(todaysDate) < 0) continue;
			
			Decimal d = (Decimal)item.get(fieldName);
			if (d == null) continue;
			
			String key = extractKey(item);
			Map<String, Decimal> tableRow = getOrCreateRow(key);
			Decimal rowTotal = tableRow.get('Total');
			rowTotal += d;
			tableRow.put('Total', rowTotal);
		}
	}

    /** Render the HTML code for the table body */
    public override String getTableHtml() {
        XmlStreamWriter tableHtml = new XmlStreamWriter();
        tableHtml.writeStartElement(null, 'tbody', null);
        
        for (String key : tableKeys) {
            Map<String, Decimal> tableRow = tableData.get(key);
            
            tableHtml.writeStartElement(null, 'tr', null);
            tableHtml.writeStartElement(null, 'td', null);
            tableHtml.writeAttribute(null, null, 'class', 'key');
            tableHtml.writeCharacters(key);         
            tableHtml.writeEndElement();
                
            for (String label : columnLabels) {
                tableHtml.writeStartElement(null, 'td', null);
            tableHtml.writeAttribute(null, null, 'class', 'num');
                tableHtml.writeCharacters(formatInteger(tableRow.get(label)));          
                tableHtml.writeEndElement();
            }   
            tableHtml.writeEndElement();
        }
        
        tableHtml.writeEndElement();
        return tableHtml.getXmlString();
    }
}