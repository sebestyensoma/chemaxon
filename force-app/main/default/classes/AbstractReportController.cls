/** This class serves as the basis of the Deal Type, Settled and Product Code report controllers */
public abstract with sharing class AbstractReportController {

    // PROPERTIES

    // Form input values
    public String currencyName { get; set; }
    public String period { get; set; }
    
    // Pure data
    protected Boolean hasData = false;
        public Boolean getHasData() { return hasData; }
    
    protected Map<String, Map<String, Decimal>> tableData;
    protected List<String> tableKeys;
    protected List<String> columnLabels;
    
    public Map<String, Map<String, Decimal>> getTableData() { return tableData; }
    public List<String> getTableKeys() { return tableKeys; }
    public List<String> getColumnLabels() { return columnLabels; }
    
    // Form populators
    
    public List<SelectOption> getCurrencyNames() {
        List<SelectOption> currencyNames = new List<SelectOption>();
        currencyNames.add(new SelectOption('USD', 'USD'));
        currencyNames.add(new SelectOption('EUR', 'EUR'));
        currencyNames.add(new SelectOption('GBP', 'GBP'));
        currencyNames.add(new SelectOption('HUF', 'HUF'));
        currencyNames.add(new SelectOption('JPY', 'JPY'));
        currencyNames.add(new SelectOption('CHF', 'CHF'));
        currencyNames.add(new SelectOption('SGD', 'SGD'));
        return currencyNames;
    }

    public List<SelectOption> getPeriods() {
        List<SelectOption> periods = new List<SelectOption>();
        periods.add(new SelectOption('y', 'Yearly'));
        periods.add(new SelectOption('q', 'Quarterly'));
        periods.add(new SelectOption('m', 'Monthly'));
        return periods;
    }
    
    // ACTIONS
    
    /** Perform report after currency and period have been selected */
    public PageReference runReport() {
        hasData = true;
        tableData = new Map<String, Map<String, Decimal>>();
        tableKeys = new List<String>();
        
        // The actual processing
        columnLabels = createColumnLabels();
        doReport();
        
        // Sort and reverse row keys
        tableKeys.sort();
        List<String> reverseKeys = new String[tableKeys.size()];
        for(Integer i = tableKeys.size()-1; !tableKeys.isEmpty(); i--) {
            reverseKeys[i] = tableKeys.remove(0);
        }
        tableKeys = reverseKeys;
        
        return null;
    }
    
    public void download() {
    	currencyName = ApexPages.currentPage().getParameters().get('currency');
    	period = ApexPages.currentPage().getParameters().get('period');
    	runReport();
    }
    
    /** Implement this method to create the column labels. */
    protected abstract List<String> createColumnLabels();
    
    /** Implement this method to perform the actual reporting. */
    protected abstract void doReport();
    
    /** Implement this method to generate the table HTML. */
    public abstract String getTableHtml();
    
    // HELPER METHODS
    
    /** Determine the table row a QuoteLineItem belongs to, based on the set reporting period */
    protected String extractKey(QuoteLineItem item) {
        String result;
        
        // Extract year
        if (period == 'y') {
            result = String.valueOf(item.quote.Invoice_Date__c.year());
        }
        // Extract month
        else if (period == 'm') {
            String month = String.valueOf(item.quote.Invoice_Date__c.month());
            if (month.length() < 2) month = '0' + month;
            result = String.valueOf(item.quote.Invoice_Date__c.year()) + '/' + month;
        }
        else if (period == 'q') {
            String quarter;
            Integer month = item.quote.Invoice_Date__c.month();
            if (month >= 10) { quarter = 'Q4'; }
            else if (month >= 7) { quarter = 'Q3'; }
            else if (month >= 4) { quarter = 'Q2'; }
            else { quarter = 'Q1'; }
            
            result = String.valueOf(item.quote.Invoice_Date__c.year()) + '/' + quarter;
        }
        
        return result;
    }
    
    /** Get a row from the table data by its key, or create it if it does not exist yet */
    protected Map<String, Decimal> getOrCreateRow(String key) {
        Map<String, Decimal> row = tableData.get(key);
        
        if (row == null) {
            row = new Map<String, Decimal>();
            for (String label : columnLabels) {
                row.put(label, 0);
            }
            
            tableKeys.add(key);
            tableData.put(key, row);
        }
        
        return row;
    }
    
    /** Format an Decimal in "100 000 000" format */
    public static String formatInteger(Decimal n) {
    	if (n == null) return '';
    	
    	n = n.setScale(0, RoundingMode.HALF_UP);
        String base = String.valueOf(Math.abs(n));
        String result = '';
        Integer copyTo = base.length();
        Integer copyFrom = copyTo - 3;
        if (copyFrom < 0) copyFrom = 0;
        
        while (copyTo > 0) {
            result = base.substring(copyFrom, copyTo) + ' ' + result;
            copyFrom -= 3;
            copyTo -=3;
            if (copyFrom < 0) copyFrom = 0;
        }
        
        if (n < 0) result = '-' + result;
        return result;
    }
}