public with sharing class QLIPriceUpdater {
	@future
    public static void updateQLIs(String qIdSetJson){
    	system.debug('qIdSetJson:'+qIdSetJson);
		List<Id> quoteIds = (List<id>)JSON.deserialize(qIdSetJson, List<Id>.class);
		system.debug('quoteIds.size():'+quoteIds.size());
		List<QuoteLineItem> listUpdateQLIs = [	SELECT technical_Quote_Price_last_updated__c
							FROM QuoteLineItem 
							WHERE
								QuoteId IN :quoteIds
								AND
								Package__c=null];
		for(QuoteLineItem loopQLI:listUpdateQLIs){
			loopQLI.technical_Quote_Price_last_updated__c=Datetime.now();
		}
		if(listUpdateQLIs!=null && listUpdateQLIs.size()>0){
			update listUpdateQLIs;
			system.debug('QLIPriceUpdater: listUpdateQLIs.size():'+listUpdateQLIs.size());
		}
		
    }
}