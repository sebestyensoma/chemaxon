@isTest
private class testLicense
{
    /*
    	Name=Name
    	License_Type__c=License_Type__c
    	Support_Validity__c=Support_Validity__c
		Products_Names__c=Products_Names__c
		Validity__c=Validity__c
		Account_Email__c=Account_Email__c
		Account_ID_text__c=Account_ID_text__c
		Account_Name__c=Account_Name__c
		Contact_Name_Eval__c=Contact_Last_Name_Eval__c
    */
    
    // nincs lead és nincs account
    static testMethod void myUnitTest01()
    {
        List<License__c> licenseList = new List<License__c>();
        
        License__c license00 = new License__c();
        license00.Name = 'name 00';
		licenseList.add(license00);
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'contactNameEval01';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'contactNameEval02';
		licenseList.add(license02);
		
		License__c license03 = new License__c();
        license03.Name = 'name 03';
        license03.License_Type__c = 'licensetype';
        license03.Support_Validity__c = system.today();
        license03.Products_Names__c = 'products';
        license03.Validity__c = system.today();
        license03.Account_Email__c = 'email@email.email';
		license03.Account_ID_text__c = 'accountid03';
		license03.Account_Name__c = 'accountName03';
		license03.Contact_Last_Name_Eval__c = 'contactNameEval03';
		licenseList.add(license03);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id
				FROM
					License__c
				WHERE
					Id IN :licenseList
			];
			
		newLeadList =
			[
				SELECT
					Id
				FROM
					Lead
			];
			
		system.assertEquals(2, newLicenseList.size());
		//system.assertEquals(2, newLeadList.size()); 2019.02.12. gp
    }
    
    // van nyitott lead és nincs account
    static testMethod void myUnitTest02()
    {
        List<License__c> licenseList = new List<License__c>();
        List<Lead> leadList = new List<Lead>();
        
        Lead lead01 = new Lead();
        lead01.LastName = 'name01';
        lead01.Email = 'email@email.email1';
        lead01.Company = 'company01';
        lead01.Product_Area_Interest__c='test';
        leadList.add(lead01);
        
        Lead lead02 = new Lead();
        lead02.LastName = 'name02';
        lead02.Email = 'email@email.email2';
        lead02.Company = 'company02';
        lead02.Product_Area_Interest__c='test';
        leadList.add(lead02);
        
        insert leadList;        
        
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email1';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'name11';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email2';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'name12';
		licenseList.add(license02);
		
		License__c license03 = new License__c();
        license03.Name = 'name 03';
        license03.License_Type__c = 'evaluation';
        license03.Support_Validity__c = system.today();
        license03.Products_Names__c = 'products';
        license03.Validity__c = system.today();
        license03.Account_Email__c = 'email@email.email3';
		license03.Account_ID_text__c = 'accountid03';
		license03.Account_Name__c = 'accountName03';
		license03.Contact_Last_Name_Eval__c = 'name13';
		licenseList.add(license03);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id,
					Name,
					Lead__c
				FROM
					License__c
				WHERE
					Id IN :licenseList
				ORDER BY
					Name
				ASC
			];
			
		newLeadList =
			[
				SELECT
					Id,
					LastName
				FROM
					Lead
				ORDER BY
					LastName
				ASC
			];
			
		system.debug(newLicenseList);
		system.debug(newLeadList);
			
		system.assertEquals(3, newLicenseList.size());
		system.assertEquals(3, newLeadList.size());
		
		system.assertEquals(newLeadList[0].Id, newLicenseList[0].Lead__c);
		system.assertEquals(newLeadList[1].Id, newLicenseList[1].Lead__c);
		system.assertEquals(newLeadList[2].Id, newLicenseList[2].Lead__c);
    }
    
    // van nyitott lead és van account
    static testMethod void myUnitTest03()
    {
        List<License__c> licenseList = new List<License__c>();
        List<Lead> leadList = new List<Lead>();
        
        Account account01 = new Account();
        account01.Name = 'name01';
        insert account01;
        
        Lead lead01 = new Lead();
        lead01.LastName = 'name01';
        lead01.Email = 'email@email.email1';
        lead01.Company = 'company01';
        lead01.Product_Area_Interest__c='test';
        leadList.add(lead01);
        
        Lead lead02 = new Lead();
        lead02.LastName = 'name02';
        lead02.Email = 'email@email.email2';
        lead02.Company = 'company02';
        lead02.Product_Area_Interest__c='test';
        leadList.add(lead02);
        
        insert leadList;        
        
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email1';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'name11';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email2';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'name12';
		licenseList.add(license02);
		
		License__c license03 = new License__c();
        license03.Name = 'name 03';
        license03.License_Type__c = 'evaluation';
        license03.Support_Validity__c = system.today();
        license03.Products_Names__c = 'products';
        license03.Validity__c = system.today();
        license03.Account_Email__c = 'email@email.email3';
		license03.Account_ID_text__c = account01.Id;
		license03.Account_Name__c = 'accountName03';
		license03.Contact_Last_Name_Eval__c = 'name13';
		licenseList.add(license03);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id,
					Name,
					Lead__c,
					Account__c
				FROM
					License__c
				WHERE
					Id IN :licenseList
				ORDER BY
					Name
				ASC
			];
			
		newLeadList =
			[
				SELECT
					Id,
					LastName
				FROM
					Lead
				ORDER BY
					LastName
				ASC
			];
			
		system.debug(newLicenseList);
		system.debug(newLeadList);
			
		system.assertEquals(3, newLicenseList.size());
		system.assertEquals(3, newLeadList.size());
		
		system.assertEquals(newLeadList[0].Id, newLicenseList[0].Lead__c);
		system.assertEquals(newLeadList[1].Id, newLicenseList[1].Lead__c);
		system.assertEquals(newLeadList[2].Id, newLicenseList[2].Lead__c);
		system.assertEquals(account01.Id, newLicenseList[2].Account__c);
    }
        
    // van lezárt lead és nincs account
    static testMethod void myUnitTest04()
    {
        List<License__c> licenseList = new List<License__c>();
        List<Lead> leadList = new List<Lead>();
        
        Lead lead01 = new Lead();
        lead01.LastName = 'name01';
        lead01.Email = 'email@email.email1';
        lead01.Company = 'company01';
        lead01.Product_Area_Interest__c='test';
        leadList.add(lead01);
        
        Lead lead02 = new Lead();
        lead02.LastName = 'name02';
        lead02.Email = 'email@email.email2';
        lead02.Company = 'company02';
        lead02.Product_Area_Interest__c='test';
        leadList.add(lead02);
        
        Lead lead03 = new Lead();
        lead03.LastName = 'name03';
        lead03.Email = 'email@email.email3';
        lead03.Company = 'company03';
        lead03.Status = 'Unqualified';
        lead03.Product_Area_Interest__c='test';
        leadList.add(lead03);
        
        insert leadList;        
        
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email1';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'name11';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email2';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'name12';
		licenseList.add(license02);
		
		License__c license03 = new License__c();
        license03.Name = 'name 03';
        license03.License_Type__c = 'evaluation';
        license03.Support_Validity__c = system.today();
        license03.Products_Names__c = 'products';
        license03.Validity__c = system.today();
        license03.Account_Email__c = 'email@email.email3';
		license03.Account_ID_text__c = 'accountid03';
		license03.Account_Name__c = 'accountName03';
		license03.Contact_Last_Name_Eval__c = 'name13';
		licenseList.add(license03);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id,
					Name,
					Lead__c
				FROM
					License__c
				WHERE
					Id IN :licenseList
				ORDER BY
					Name
				ASC
			];
			
		newLeadList =
			[
				SELECT
					Id,
					LastName
				FROM
					Lead
				ORDER BY
					LastName
				ASC
			];
			
		system.debug(newLicenseList);
		system.debug(newLeadList);
			
		system.assertEquals(3, newLicenseList.size());
		system.assertEquals(4, newLeadList.size());
		
		system.assertEquals(newLeadList[0].Id, newLicenseList[0].Lead__c);
		system.assertEquals(newLeadList[1].Id, newLicenseList[1].Lead__c);
		system.assertEquals(newLeadList[3].Id, newLicenseList[2].Lead__c);
    }
    
    // van lezárt lead és van account
    static testMethod void myUnitTest05()
    {
        List<License__c> licenseList = new List<License__c>();
        List<Lead> leadList = new List<Lead>();
        
        Account account01 = new Account();
        account01.Name = 'name01';
        insert account01;
        
        Lead lead01 = new Lead();
        lead01.LastName = 'name01';
        lead01.Email = 'email@email.email1';
        lead01.Company = 'company01';
        lead01.Product_Area_Interest__c='test';
        leadList.add(lead01);
        
        Lead lead02 = new Lead();
        lead02.LastName = 'name02';
        lead02.Email = 'email@email.email2';
        lead02.Company = 'company02';
        lead02.Product_Area_Interest__c='test';
        leadList.add(lead02);
        
        Lead lead03 = new Lead();
        lead03.LastName = 'name03';
        lead03.Email = 'email@email.email3';
        lead03.Company = 'company03';
        lead03.Status = 'Unqualified';
        lead03.Product_Area_Interest__c='test';
        leadList.add(lead03);
        
        insert leadList;        
        
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email1';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'name11';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email2';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'name12';
		licenseList.add(license02);
		
		License__c license03 = new License__c();
        license03.Name = 'name 03';
        license03.License_Type__c = 'evaluation';
        license03.Support_Validity__c = system.today();
        license03.Products_Names__c = 'products';
        license03.Validity__c = system.today();
        license03.Account_Email__c = 'email@email.email3';
		license03.Account_ID_text__c = account01.Id;
		license03.Account_Name__c = 'accountName03';
		license03.Contact_Last_Name_Eval__c = 'name13';
		licenseList.add(license03);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id,
					Name,
					Lead__c,
					Account__c
				FROM
					License__c
				WHERE
					Id IN :licenseList
				ORDER BY
					Name
				ASC
			];
			
		newLeadList =
			[
				SELECT
					Id,
					LastName
				FROM
					Lead
				ORDER BY
					LastName
				ASC
			];
			
		system.debug(newLicenseList);
		system.debug(newLeadList);
			
		system.assertEquals(3, newLicenseList.size());
		system.assertEquals(4, newLeadList.size());
		
		system.assertEquals(newLeadList[0].Id, newLicenseList[0].Lead__c);
		system.assertEquals(newLeadList[1].Id, newLicenseList[1].Lead__c);
		system.assertEquals(newLeadList[3].Id, newLicenseList[2].Lead__c);
		system.assertEquals(account01.Id, newLicenseList[2].Account__c);
    }
    
    // lead konvertálás
    static testMethod void myUnitTest06()
    {
    	List<License__c> licenseList = new List<License__c>();
        
        License__c license01 = new License__c();
        license01.Name = 'name 01';
        license01.License_Type__c = 'evaluation';
        license01.Support_Validity__c = system.today();
        license01.Products_Names__c = 'products';
        license01.Validity__c = system.today();
        license01.Account_Email__c = 'email@email.email1';
		license01.Account_ID_text__c = 'accountid01';
		license01.Account_Name__c = 'accountName01';
		license01.Contact_Last_Name_Eval__c = 'contactNameEval01';
		licenseList.add(license01);
		
		License__c license02 = new License__c();
        license02.Name = 'name 02';
        license02.License_Type__c = 'EVALUATION';
        license02.Support_Validity__c = system.today();
        license02.Products_Names__c = 'products';
        license02.Validity__c = system.today();
        license02.Account_Email__c = 'email@email.email2';
		license02.Account_ID_text__c = 'accountid02';
		license02.Account_Name__c = 'accountName02';
		license02.Contact_Last_Name_Eval__c = 'contactNameEval02';
		licenseList.add(license02);
		
		insert licenseList;
		
		
		List<License__c> newLicenseList;
		List<Lead> newLeadList;		
		
		newLicenseList =
			[
				SELECT
					Id, Lead__c
				FROM
					License__c
				WHERE
					Id IN :licenseList
			];
			
		newLeadList =
			[
				SELECT
					Id, Name
				FROM
					Lead
			];
			
		system.assertEquals(2, newLicenseList.size());
		//system.assertEquals(2, newLeadList.size()); 2019.02.12. gp
		
		system.debug('##### ' + newLicenseList);
		system.debug('##### ' + newLeadList);
		
		
		LeadStatus convertStatus =
			[
				SELECT MasterLabel
				FROM LeadStatus
				WHERE IsConverted = true
				LIMIT 1
			];
		
		List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();
		
		Database.LeadConvert lc01 = new Database.LeadConvert();
		string oppName01 = newLeadList[0].Name;
   
		lc01.setLeadId(newLeadList[0].Id);
		lc01.setOpportunityName(oppName01);
		lc01.setConvertedStatus(convertStatus.MasterLabel);
		leadConverts.add(lc01);
		
		Database.LeadConvert lc02 = new Database.LeadConvert();
		string oppName02 = newLeadList[1].Name;
   
		lc02.setLeadId(newLeadList[1].Id);
		lc02.setOpportunityName(oppName02);
		lc02.setConvertedStatus(convertStatus.MasterLabel);
   
		leadConverts.add(lc02);
		
		
		List<Database.LeadConvertResult> lcr = Database.convertLead(leadConverts);
		
		newLicenseList.clear();
		newLicenseList =
			[
				SELECT
					Account_Name__c, Account__c, Contact_Name__c, Lead__c
				FROM
					License__c
				ORDER BY
					Account_Name__c
				ASC
			];
		
		system.debug('##### ' + newLicenseList);
			
		Map<id, License__c> licenseLeadIdMap = new Map<id, License__c>();
		
		for(License__c currentL : newLicenseList)
		{
			licenseLeadIdMap.put(currentL.Lead__c, currentL);
		}
		
		system.debug('##### ' + licenseLeadIdMap);
		
		List<Lead> convertedLeadList;
		convertedLeadList =
			[
				SELECT 
					Id, Company, ConvertedAccountId, ConvertedContactId
				FROM
					Lead
				WHERE
					IsConverted = true
				ORDER BY
					Company
				ASC 
			];
			
		system.debug('##### ' + convertedLeadList);

		system.assertEquals(convertedLeadList[0].ConvertedAccountId, licenseLeadIdMap.get(convertedLeadList[0].Id).Account__c);
		system.assertEquals(convertedLeadList[0].ConvertedContactId, licenseLeadIdMap.get(convertedLeadList[0].Id).Contact_Name__c);
		system.assertEquals(convertedLeadList[1].ConvertedAccountId, licenseLeadIdMap.get(convertedLeadList[1].Id).Account__c);
		system.assertEquals(convertedLeadList[1].ConvertedContactId, licenseLeadIdMap.get(convertedLeadList[1].Id).Contact_Name__c);
    }
}