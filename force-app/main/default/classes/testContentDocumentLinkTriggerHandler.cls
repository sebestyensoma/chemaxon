@isTest 
private class testContentDocumentLinkTriggerHandler {

	@isTest(seealldata=true)
	private static void testName() {
		Account acct = new Account(Name='TEST_ACCT');
		insert acct;

		Agreement__c agr = new Agreement__c(Name = 'teszt', Contract_with__c = acct.Id);
		insert agr;

		Agreement_document__c ad = new Agreement_document__c(Name = 'name', Agreement__c = agr.Id);
		insert ad;

		ContentVersion contentVersion = new ContentVersion(
		  Title = 'Penguins',
		  PathOnClient = 'Penguins.jpg',
		  VersionData = Blob.valueOf('Test Content'),
		  IsMajorVersion = true
		);
		insert contentVersion;    
		List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId, FileExtension FROM ContentDocument];

		//create ContentDocumentLink  record 
		ContentDocumentLink cdl = New ContentDocumentLink();
		cdl.LinkedEntityId = ad.id;
		cdl.ContentDocumentId = documents[0].Id;
		cdl.shareType = 'V';
		insert cdl;

		ad = [SELECT Name FROM Agreement_document__c WHERE Id = :ad.Id];

		System.assertEquals(documents[0].Title+'.'+documents[0].FileExtension, ad.Name);
	}
}