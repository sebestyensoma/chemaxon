global class SchedulableQuoteHasPDF implements Schedulable, Database.Batchable<SObject> {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) {
		Database.executebatch(new SchedulableQuoteHasPDF(), 200);
	}

	global Database.QueryLocator start(Database.BatchableContext context) {
		// ütemezőből kerül meghívásra

		String query =	'SELECT Id, Quote_has_PDF__c ' +
						'FROM Quote';
		if(Test.isRunningTest()) query = 'SELECT Id, Quote_has_PDF__c FROM Quote WHERE CreatedDate = TODAY LIMIT 200';

		return Database.getQueryLocator(query);
	}


   	global void execute(Database.BatchableContext context, List<Quote> scope) {
		// ha pdf jön létre egy quote-hoz akkor egy checkbox kipipálódjon
		
		Map<Id, Quote> quoteMap = new Map<Id, Quote> (scope);

		Set<Id/*quote id*/> qPDFSet = new Set<Id>();
		List<AggregateResult> groupedResults = [SELECT Count(id), QuoteId FROM QuoteDocument WHERE QuoteId IN :quoteMap.keySet() GROUP BY QuoteId];
		for(AggregateResult ar : groupedResults){
			System.debug(ar.get('expr0') + ', ' + ar.get('QuoteId'));
			qPDFSet.add(String.valueOf(ar.get('QuoteId')));
		}

		List<Quote> quoteToUpdate = new List<Quote>();
		for(Quote q : scope){
			if(!qPDFSet.contains(q.Id) && q.Quote_has_PDF__c) {
				q.Quote_has_PDF__c = false;
				quoteToUpdate.add(q);
			}

			if(qPDFSet.contains(q.Id) && !q.Quote_has_PDF__c) {
				q.Quote_has_PDF__c = true;
				quoteToUpdate.add(q);
			}
		}

		update quoteToUpdate;
	}
	

	global void finish(Database.BatchableContext context) {
		
	}
}