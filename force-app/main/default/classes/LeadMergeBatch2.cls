/**
 * @author      Attention CRM Consulting <neckermann@attentioncrm.hu>
 * @version     2
 * @since       2016-09-29 (?K)
 */

global class LeadMergeBatch2 {
	
	public void action(String param) {
		List<Lead> leads = ((List<Lead>)Database.query(
			'SELECT Id, Forum_Id__c, Email, Download_packs__c, CreatedDate ' +
			'FROM Lead ' +
			'WHERE ' +
				param + ' != null ' +
				'AND ' +
				'isConverted = false ' +
			'order by ' + param + ' ASC, CreatedDate ASC' + 
			(test.isrunningtest() ? ' limit 50' : '')
		));

		List<Lead> duplLeads = new List<Lead>();
		String prev = null;
		for(Lead l : leads){
			if( (string)((sObject)l).get(param) == prev ){
				duplLeads.add(l);
				l.T_Duplicated__c = true;
			}
			prev = (string)((sObject)l).get(param);
		}
		update duplLeads;
	}

}