/** Clone a Quote and its Line Items
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2014-08-07 (SM)
 */
public with sharing class CloneQuote {

	public PageReference onStart() {
		Id quoteId = Id.valueOf(ApexPages.currentPage().getParameters().get('quoteId'));
		
		// Fetch originals
		Quote original = [
			SELECT technical_Targetprice_Other_Triggered__c, technical_Quote_Item_Perpetual_Sum__c, technical_Quote_Item_License_Type_Sum__c,
				technical_Quote_Item_Annual_Sum__c, technical_Discount_Other_Triggered__c, of_Quotes__c, TotalPrice, Tax_number__c, Tax,
				Target_Price__c, Target_Discount__c, T_Invoice_Date__c, T_Actual_Payment_Date__c, SystemModstamp, Subtotal, Status,
				Show_discount_on_quote__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName, ShippingLongitude, ShippingLatitude,
				ShippingHandling, ShippingCountry, ShippingCity, Quote_display_user_address__c, Quote_display_terms__c, Quote_display_support_expiry__c,
				Quote_display_subtotal__c, Quote_display_subtotalB__c, Quote_display_number__c, Quote_display_license_exp__c,
				Quote_display_expiration_date__c, Quote_display_discount__c, Quote_display_additional_details__c, QuoteNumber, Pricebook2Id,
				Prepared_for_Contact_Name__c, Plugins_Contained__c, Phone, Payment_Deadline__c, PO_No__c, OpportunityId, Name, Multiyear__c,
				LineItemCount, License_Type__c, License_Support_Expiration_Date__c, License_Key_Generation__c, License_Expiration_Date__c,
				LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, IsSyncing, IsDeleted, Invoice_Type__c, Invoice_No__c,
				Invoice_Display_Visible_Productslist__c, Invoice_Display_Visible_CoU__c, Invoice_Display_Visible_CoP__c, Invoice_Display_Reference_to__c,
				Invoice_Display_Products__c, Invoice_Display_Payment_Due_Days__c, Invoice_Display_Blank_Lines_Before_PI__c,
				Invoice_Display_Additional_LLC_Info__c, Invoice_Date__c, Instructions_for_Finance__c, Id, Grand_Total_wTax__c, Grand_Total_rounded__c,
				GrandTotal, Fax, External_id__c, ExpirationDate, Email, Discount, Description, Custom_Expiry__c, CurrencyIsoCode, CreatedDate,
				CreatedById, Contact_Quote__c, Contact_License__c, Contact_Invoice__c, ContactId, Company_Name__c, BillingStreet, BillingState,
				BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, Additional_LLC_Information__c,
				Actual_Payment_Date__c, Account_Name__c
			FROM Quote
			WHERE Id = :quoteId
		];
		
		QuoteLineItem[] originalItems = [
			SELECT technical_Quote_Price_last_updated__c, technical_Quote_Line_Item_Perpetual_ID__c, technical_Quote_Line_Item_Annual_ID__c,
				Valid__c, User_Limit__c, UnitPrice, Total_Price_rounded__c, TotalPrice, Target_Price__c, T_Product_Code_Type__c, TEST_Quote_Price__c,
				SystemModstamp, Subtotal, SortOrder, Quote_display_quantity__c, Quote_display_line_discount__c, Quote_Price__c, QuoteId, Quantity,
				PricebookEntryId, Price_in_USD__c, Package__c, Owner_in_Sales_Report__c, Original_Price__c, ListPrice, LineNumber, License_type__c,
				LastModifiedDate, LastModifiedById, Isplugin__c, IsPluginNF__c, IsDeleted, Id, External_id__c, End_User__c, Discount, Description,
				Custom_Quantity__c, Currency__c, CurrencyIsoCode, CreatedDate, CreatedById, Condition__c, Comment__c
			FROM QuoteLineItem
			WHERE QuoteId = :original.id AND PricebookEntry.Product2.IsActive = TRUE
		];
		
		// Create clones
		Quote clone = original.clone();
		clone.Status = 'Draft';
		clone.PO_No__c = null;
		clone.Invoice_Date__c = null;
		clone.Invoice_No__c = null;
		clone.Target_Price__c = null;
		clone.Target_Discount__c = null;
		insert clone;
		
		List<QuoteLineItem> cloneItems = new List<QuoteLineItem>();
		for (QuoteLineItem originalItem : originalItems) {
			QuoteLineItem cloneItem = originalItem.clone();
			cloneItem.QuoteId = clone.id;
			cloneItem.Target_Price__c = null;
			
			cloneItems.add(cloneItem);
		}
		insert cloneItems;
		
		return new PageReference('/' + clone.Id);
	}

}