@isTest
private class testPriceList
{
    // user log nem okay
    @isTest(seeAllData=true)
    static void myUnitTest01()
    {
        Product2 product;
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        product =
        	[
        		SELECT	Id, Name, User_Log__c
        		FROM	Product2
        		WHERE	IsActive = true
        		LIMIT 1
        	];
        
        product.User_Log__c = 0;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = product.Id;
        
        pl.getPriceList();
        
        msgList = ApexPages.getMessages();
        
        for(ApexPages.Message currentMsg : ApexPages.getMessages())
        {
            System.assert(currentMsg.getSummary().contains('Problem with the selected product: user log (0) is invalid.'));
        }
    }
    
    // bundle log nem okay
    @isTest(seeAllData=true)
    static void myUnitTest02()
    {
        Product2 product;
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        product =
        	[
        		SELECT	Id, Name, User_Log__c, Bundle_Log__c
        		FROM 	Product2
        		WHERE	IsActive = true
        				AND
        				PlugIn__c = true
        		LIMIT 1
        	];
        
        system.debug('##### ' + product);
        
        product.User_Log__c = 1.786;
        product.Bundle_Log__c = 0;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = product.Id;
        
        pl.getPriceList();
        
        msgList = ApexPages.getMessages();
        
        for(ApexPages.Message currentMsg : ApexPages.getMessages())
        {
            system.debug('###### ' + currentMsg);
            System.assert(currentMsg.getSummary().contains('Problem with the selected product: bundle log (0) is invalid.'));
        }
    }
    
    // productcode nem okay
    @isTest(seeAllData=true)
    static void myUnitTest03()
    {
        Product2 product;
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        product = [ SELECT Id, Name, User_Log__c, Bundle_Log__c FROM Product2 WHERE PlugIn__c = true LIMIT 1 ];
        
        product.User_Log__c = 1.786;
        product.Bundle_Log__c = 1.800;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = 'blabla';
        
        pl.getPriceList();
        
        msgList = ApexPages.getMessages();
        
        for(ApexPages.Message currentMsg : ApexPages.getMessages())
        {
            system.debug('###### ' + currentMsg.getSummary());
            
            system.assert(currentMsg.getSummary().contains('There is no pricebookEntry related to this product.'));
        }
    }
    
    // jek
    @isTest(seeAllData=true)
    static void myUnitTest04()
    {
        Product2 product;
        List<string> currencyList = new List<string>();
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        currencyList.add('CHF');
        currencyList.add('EUR');
        currencyList.add('GBP');
        currencyList.add('HUF');
        currencyList.add('JPY');
        currencyList.add('SGD');
        currencyList.add('USD');
        
        product = [ SELECT Id, Name, User_Log__c, Bundle_Log__c FROM Product2 WHERE ProductCode = 'JEK' LIMIT 1 ];
        
        product.User_Log__c = 1.786;
        product.Bundle_Log__c = 1.800;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = product.Id;
        
        for(string currentC : currencyList)
        {
        	pl.selectedCurrencyIsoCode = currentC;
        	pl.getPriceList();
			
			msgList.clear();        
        	msgList = ApexPages.getMessages();
        
        	system.assertEquals(0, msgList.size());
        	system.assert(pl.result.contains('Max. nodes or users'));
        }
    }
    
    // nem jek, nem plugin
    @isTest(seeAllData=true)
    static void myUnitTest05()
    {
        Product2 product;
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        product = [ 
			SELECT Id, Name, User_Log__c, Bundle_Log__c 
			FROM Product2 
			WHERE ProductCode != 'JEK' AND Plugin__c = false AND IsActive = true AND ProductCode = 'JCB'
			LIMIT 1 
		];
        //AND Name != 'JChem Neo4j' AND Name != 'JChem Microservices Markush Enumeration' AND Name != 'JChem Microservices Calculations'

        product.User_Log__c = 1.786;
        product.Bundle_Log__c = 1.800;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = product.Id;
        
        pl.getPriceList();
        
        msgList = ApexPages.getMessages();
        
        system.debug('##### ' + msgList);
        
        system.assertEquals(0, msgList.size());
        system.assert(pl.result.contains('Max. nodes or users'));
    }
    
    // nem jek, plugin
    @isTest(seeAllData=true)
    static void myUnitTest06()
    {
        Product2 product;
        List<ApexPages.Message> msgList = new List<ApexPages.Message>();
        
        product = [ SELECT Id, Name, User_Log__c, Bundle_Log__c FROM Product2 WHERE ProductCode != 'JEK' AND Plugin__c = true AND IsActive = true LIMIT 1 ];
        
        product.User_Log__c = 1.786;
        product.Bundle_Log__c = 1.800;
        update product;
        
        
        pagereference pageref = Page.PriceList;
        
        test.setCurrentPage(pageref);
        
        PriceList pl = new PriceList();
        pl.selectedProductId = product.Id;
        
        pl.getPriceList();
        
        msgList = ApexPages.getMessages();
        
        system.debug('##### ' + msgList);
        
        system.assertEquals(0, msgList.size());
        system.assert(pl.result.contains('Max. nodes or users'));
    }
}