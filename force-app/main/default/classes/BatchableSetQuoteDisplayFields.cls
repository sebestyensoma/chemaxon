global class BatchableSetQuoteDisplayFields implements Database.Batchable<SObject>, Database.Stateful {
	private String quoteId;
	global List<String> errorList = new List<String>();
	//global Set<Id> allIds = new Set<Id>();
	//global Set<Id> successIds = new Set<Id>();

	global BatchableSetQuoteDisplayFields() {
		quoteId = '';
	}
	
	global BatchableSetQuoteDisplayFields(String qId) {
		quoteId = ' AND Id = \'' + qId + '\'';
	}

	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		//return Database.getQueryLocator('SELECT Id, Quote_display_terms__c, Quote_display_user_address__c, Quote_display_terms_Consultancy__c, Quote_display_terms_Subscription__c, Opportunity.Owner.Lastname, Opportunity.Owner.Firstname, Opportunity.Owner.Email, Opportunity.Owner.Name FROM Quote WHERE Opportunity.T_Opportunity_renewal_test__c = true AND CreatedBy.Name = \'Attention Chemaxon Team\'' + quoteId);
		return Database.getQueryLocator('SELECT Id, Quote_display_terms__c, Quote_display_user_address__c, Quote_display_terms_Consultancy__c, Quote_display_terms_Subscription__c, Opportunity.Owner.Lastname, Opportunity.Owner.Firstname, Opportunity.Owner.Email, Opportunity.Owner.Name FROM Quote WHERE Status = \'Draft\'' + quoteId);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Quote> scope) {
		for (Quote q :scope) {
			//allIds.add(q.Id);

			q.Quote_display_terms__c = 'Mode of Delivery: License file sent via email, software downloaded from ChemAxon\'s website (http://www.chemaxon.com)\n\n' +
				'Please read our EULA at www.chemaxon.com/eula.\n\n' +
				'Inflation: Fees may be increased by relevant inflation data.\n\n' +
				'SEND PURCHASE ORDER TO:\n' +
				q.Opportunity.Owner.FirstName + ' ' + q.Opportunity.Owner.LastName + ' (see address above)\n\n' +
				'PAYMENT TERMS: Payment due 30 days from invoice date.';

			if (
				q.Opportunity.Owner.Name == 'Annamária Kovács' || 
				q.Opportunity.Owner.Name == 'Judit Kerékgyártó' || 
				q.Opportunity.Owner.Name == 'Gábor Apor' || 
				q.Opportunity.Owner.Name == 'Nóra Lapusnyik' || 
				q.Opportunity.Owner.Name == 'Luca Buzási' || 
				q.Opportunity.Owner.Name == 'Ágnes Bárány'
			) {
				q.Quote_display_user_address__c = q.Opportunity.Owner.FirstName + ' ' + q.Opportunity.Owner.LastName + '\n' +
					'ChemAxon Kft.\n'+ 
					'Záhony utca 7.\n' +
					'Budapest\n' +
					'1031\n' +
					'Hungary\n' +
					'http://www.chemaxon.com\n' +
					q.Opportunity.Owner.Email;
			} else if (
				q.Opportunity.Owner.Name == 'Richard Johnson' || 
				q.Opportunity.Owner.Name == 'Bahram Manavi' || 
				q.Opportunity.Owner.Name == 'John Yucel'
			) {
				q.Quote_display_user_address__c = q.Opportunity.Owner.FirstName + ' ' + q.Opportunity.Owner.LastName + '\n' +
					'3511 Silverside Road, Suite 105\n' +
					'Wilmington\n' +
					'19810\n' +
					'Delaware\n' +
					'United States of America\n' +
					'http://www.chemaxon.com\n' +
					q.Opportunity.Owner.Email;
			}


			q.Quote_display_terms_Consultancy__c = 'SEND PURCHASE ORDER TO:\n' +
				q.Opportunity.Owner.FirstName + ' ' + q.Opportunity.Owner.LastName + ' (see address above)\n\n' +
				'PAYMENT TERMS: Payment due 30 days from invoice date.';

			q.Quote_display_terms_Subscription__c = 'EUSA\n' +
				'ChemAxon Services listed in this document shall be provided under terms and conditions of ChemAxon\'s standard End User Subscription Agreement (EUSA; available at www.chemaxon.com/eusa).\n\n' +
				'Inflation: Fees may be increased by relevant inflation data.\n\n' +
				'SEND PURCHASE ORDER TO:\n' +
				q.Opportunity.Owner.FirstName + ' ' + q.Opportunity.Owner.LastName + ' (see address above)\n\n' +
				'PAYMENT TERMS: Payment due 30 days from invoice date.';
		}
		/*
		try {
			update scope;
		} catch(Exception e) {
			System.debug(e.getMessage());
			errorList.add(e.getMessage());
		}
		//*/
		//*
		Database.SaveResult[] srList = Database.update(scope, false);

		System.debug(scope.size());
		System.debug(srList.size());
		System.debug(JSON.serialize(scope));
		System.debug(JSON.serialize(srList));

		//for (Database.SaveResult sr : srList) {
		for (Integer i = 0; i < srList.size(); i++) {
			Database.SaveResult sr = srList.get(i);
			
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted account. Account ID: ' + sr.getId());
				//successIds.add(sr.getId());
			}
			else {
				//String errorLine = 'ID: ' + sr.getId()+ '. ----- '; // sr.getId() csak success-kor van
				String errorLine = 'ID: ' + scope.get(i).Id + '. ----- ';
				// Operation failed, so get all errors                
				for(Database.Error err : sr.getErrors()) {
					//System.debug('The following error has occurred.');                    
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					//System.debug('Account fields that affected this error: ' + err.getFields());
					errorLine += err.getStatusCode() + ': ' + err.getMessage() + '. -- ';
				}
				errorList.add(errorLine);
				System.debug('ERROR: '+errorLine);
			}

		} // for
		//*/
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		//List<String> errorList = new List<String>();
		//for (Id qId : allIds) {
			//if (!successIds.contains(qId)) errorList.add(String.valueOf(qId));
		//}
		
		
		List<Messaging.SingleEmailMessage> mailToSend = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
		emailMessage.setSubject('Chemaxon - quote update');
		emailMessage.setToAddresses(new List<String> { 'peter.gerencser@attentioncrm.com' });
		emailMessage.setSenderDisplayName('Chemaxon update info');
        
        
		emailMessage.setHtmlBody(
			'<html>' +
				'<head>' +
					'<style type="text/css">' +
					'</style>' +
				'</head>' +

				'<body>' +
				(errorList.isEmpty() ? 'A quote-ok update-je hiba nélkül megtörtént.' : 'Hibás quote id-k:<br/><br/>' + String.join(errorList,'<br/>')) +			
				'</body>' +     
			'</html>'
		);
        
		mailToSend.add(emailMessage);
		if(mailToSend.size() > 0) {
			Messaging.sendEmail(mailToSend);
		}
	}
}