public with sharing class CloneOpportunity {

	public CloneOpportunity(ApexPages.StandardController ctrl) {}
	
	public PageReference start() {
		Id oppId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
		
		Opportunity opp = [
			SELECT AccountId,Amount,Amount_EUR__c,Billing_Company__c,Calculated_Support__c,CampaignId,Comment__c,ContractEndDate__c,
				CurrencyIsoCode,Currency__c,Database__c,DealLostReason__c,Description,End_User__c,Expected_Next_Visit__c,External_id__c,
				Feature_request__c,Fiscal,FiscalQuarter,FiscalYear,ForecastCategory,ForecastCategoryName,
				Id,InitialBPRDate__c,IntegrationNeeds__c,IsClosed,IsDeleted,IsWon,LastActivityDate,LastModifiedById,LastModifiedDate,
				LastReferencedDate,LastViewedDate,Last_License_NO__c,LeadSource,License_Key_Generation_VAR_Opp__c,License_type_del__c,
				List_Price_Rollup__c,MainCompetitor__c,My_Amount_del__c,Name,Name_requirement__c,NextStep,NumberofLicenses__c,Number_of_Users__c,
				of_Opps__c,Other_Description__c,OwnerId,Partner__c,PERCENT_del__c,Plugins_Contained__c,Pricebook2Id,Products_Involved__c,
				Product_Area_Interest_G__c,Product_Area_Interest__c,Product__c,Purchase_Amount__c,Quote_Total_Price__c,Renewal_Base__c,
				Renewal_Percentage__c,Renewed_From_Opportunity_Owner__c,Renewed_From__c,RequirementsDescription__c,Sales_Price_Rollup__c,
				Short_Description_of_Problem__c,StageName,SyncedQuoteId,SystemModstamp,Type,Type_Description__c,Type_of_Company__c,Type_of_Users__c,
				T_Account_Owner_Id__c,T_Closed_Won__c,T_Default_Invoice_Type__c,T_End_User_Date__c,T_of_Opp__c,T_Product_Area_Interest_text__c,
				T_Today_30_Days__c,T_Total_Price_Rollup__c,Winning_Competitor__c,Win_Loss_Comment__c,Win_Loss_Contact__c,Win_Loss_Reason__c
			FROM Opportunity
			WHERE Id = :oppId
		];
		
		Opportunity cloneOpp = opp.clone();
		cloneOpp.CloseDate = Date.today();
		insert cloneOpp;
		
		// Quote
		Quote[] quotes = [
			SELECT technical_Targetprice_Other_Triggered__c, technical_Quote_Item_Perpetual_Sum__c, technical_Quote_Item_License_Type_Sum__c,
				technical_Quote_Item_Annual_Sum__c, technical_Discount_Other_Triggered__c, of_Quotes__c, TotalPrice, Tax_number__c, Tax,
				Target_Price__c, Target_Discount__c, T_Invoice_Date__c, T_Actual_Payment_Date__c, SystemModstamp, Subtotal, Status,
				Show_discount_on_quote__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName, ShippingLongitude, ShippingLatitude,
				ShippingHandling, ShippingCountry, ShippingCity, Quote_display_user_address__c, Quote_display_terms__c, Quote_display_support_expiry__c,
				Quote_display_subtotal__c, Quote_display_subtotalB__c, Quote_display_number__c, Quote_display_license_exp__c,
				Quote_display_expiration_date__c, Quote_display_discount__c, Quote_display_additional_details__c, QuoteNumber, Pricebook2Id,
				Prepared_for_Contact_Name__c, Plugins_Contained__c, Phone, Payment_Deadline__c, PO_No__c, OpportunityId, Name, Multiyear__c,
				LineItemCount, License_Type__c, License_Support_Expiration_Date__c, License_Key_Generation__c, License_Expiration_Date__c,
				LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, IsSyncing, IsDeleted, Invoice_Type__c, Invoice_No__c,
				Invoice_Display_Visible_Productslist__c, Invoice_Display_Visible_CoU__c, Invoice_Display_Visible_CoP__c, Invoice_Display_Reference_to__c,
				Invoice_Display_Products__c, Invoice_Display_Payment_Due_Days__c, Invoice_Display_Blank_Lines_Before_PI__c,
				Invoice_Display_Additional_LLC_Info__c, Invoice_Date__c, Instructions_for_Finance__c, Id, Grand_Total_wTax__c, Grand_Total_rounded__c,
				GrandTotal, Fax, External_id__c, ExpirationDate, Email, Discount, Description, Custom_Expiry__c, CurrencyIsoCode, CreatedDate,
				CreatedById, Contact_Quote__c, Contact_License__c, Contact_Invoice__c, ContactId, Company_Name__c, BillingStreet, BillingState,
				BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, Additional_LLC_Information__c,
				Actual_Payment_Date__c, Account_Name__c
			FROM Quote
			WHERE OpportunityId = :opp.Id
		];
		
		// Create clones
		Map<Id, Quote> quoteClones = new Map<Id, Quote>();
		for (Quote q : quotes) {
			Quote clone = q.clone();
			clone.OpportunityId = cloneOpp.Id;
			clone.Status = 'Draft';
			clone.PO_No__c = null;
			clone.Invoice_Date__c = null;
			clone.Invoice_No__c = null;
			clone.Target_Price__c = null;
			clone.Target_Discount__c = null;
			quoteClones.put(q.Id, clone);
		}
		insert quoteClones.values();
		
		// Items
		QuoteLineItem[] items = [
			SELECT technical_Quote_Price_last_updated__c, technical_Quote_Line_Item_Perpetual_ID__c, technical_Quote_Line_Item_Annual_ID__c,
				Valid__c, User_Limit__c, UnitPrice, Total_Price_rounded__c, TotalPrice, Target_Price__c, T_Product_Code_Type__c, TEST_Quote_Price__c,
				SystemModstamp, Subtotal, SortOrder, Quote_display_quantity__c, Quote_display_line_discount__c, Quote_Price__c, QuoteId, Quantity,
				PricebookEntryId, Price_in_USD__c, Package__c, Owner_in_Sales_Report__c, Original_Price__c, ListPrice, LineNumber, License_type__c,
				LastModifiedDate, LastModifiedById, Isplugin__c, IsPluginNF__c, IsDeleted, Id, External_id__c, End_User__c, Discount, Description,
				Custom_Quantity__c, Currency__c, CurrencyIsoCode, CreatedDate, CreatedById, Condition__c, Comment__c
			FROM QuoteLineItem
			WHERE QuoteId IN :quoteClones.keySet() AND PricebookEntry.Product2.IsActive = TRUE
		];
		
		List<QuoteLineItem> cloneItems = new List<QuoteLineItem>();
		for (QuoteLineItem item : items) {
			QuoteLineItem cloneItem = item.clone();
			cloneItem.QuoteId = quoteClones.get(item.QuoteId).id;
			//cloneItem.Target_Price__c = null;
			
			cloneItems.add(cloneItem);
		}
		insert cloneItems;
		
		return new PageReference('/' + cloneOpp.Id);
	}

}