@isTest
private class testEventBIU {
    static testMethod void myUnitTest() {
        Account testAccount = new Account(Name='987Test Account');
        insert testAccount;
        Event testEvent = new Event(StartDateTime = Datetime.now(), EndDateTime = Datetime.now().addMinutes(30),Subject='Meeting',Type='Meeting',WhatId=testAccount.Id);
        insert testEvent;
    }
}