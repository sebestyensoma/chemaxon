@isTest
private class testLLCContract {

   @istest(SeeAllData=true) static void myUnitTest() {
   		List<QuoteLineItem> qliList = new List<QuoteLineItem>();
   		
        Account testAccount = new Account();
		testAccount.Name='987Test Account';
        testAccount.BillingCity = 'city';
		testAccount.BillingCountry = 'United States of America';
		testAccount.BillingStreet = 'asd';
		testAccount.BillingPostalCode = '1111';
		testAccount.Copy_Billing_Address_to_Shipping_Address__c  = true;
		testAccount.BillingState = 'Alabama';
		testAccount.Size_of_the_company__c = 'Micro (1-50)';
        insert testAccount;
        Opportunity testOppty = new Opportunity(AccountId=testAccount.Id,Name='TestOpportunity 3',LeadSource='Sales',Type='Annual',StageName='Needs Analysis',CloseDate=Date.today().addDays(10),CurrencyIsoCode='EUR');
        insert testOppty;
        List<Pricebook2> myPricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        Quote testQuote = new Quote(Name='987Test Quote',OpportunityId=testOppty.Id,Pricebook2Id=myPricebooks[0].Id, Invoice_Type__c = 'Kft');
        insert testQuote;
        Package__c testPackage = new Package__c(Quote__c=testQuote.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        //insert testPackage;
       	
       	PricebookEntry pluginPBE = [SELECT Id FROM PricebookEntry WHERE Product2Id IN (SELECT Id FROM Product2 WHERE Name LIKE '%Plugin%' OR Name LIKE '%plugin%') AND IsActive=true AND Pricebook2Id= :myPricebooks[0].Id AND CurrencyIsoCode='EUR' LIMIT 1];
       	
       	
       	QuoteLineItem testQLI01 = new QuoteLineItem(QuoteId=testQuote.Id, License_Type__c = 'Annual', UnitPrice=750, Quantity=1, Discount=0, PricebookEntryId=pluginPBE.Id,User_Limit__c=10);
		qliList.add(testQLI01);
		
		QuoteLineItem testQLI02 = new QuoteLineItem(QuoteId=testQuote.Id, License_Type__c = 'Annual', UnitPrice=750, Quantity=1, Discount=0, PricebookEntryId=pluginPBE.Id,User_Limit__c=10);
		qliList.add(testQLI02);
		
		QuoteLineItem testQLI03 = new QuoteLineItem(QuoteId=testQuote.Id, License_Type__c = 'Perpetual Renewal', UnitPrice=750, Quantity=1, Discount=0, PricebookEntryId=pluginPBE.Id,User_Limit__c=10);
		qliList.add(testQLI03);
		
		insert qliList;
		
		PageReference testPref =  Page.LLCContract;
		testPref.getParameters().put('qid',testQuote.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(testQuote);
    	LLCContractController myController = new LLCContractController();
    	
		Test.startTest();
		myController.onStart();
    	
    	myController.getMyQuotes();
    }
}

/*
@isTest
private class testLLCContract {

   @istest(SeeAllData=true) static void myUnitTest() {
        Account testAccount = new Account(Name='987Test Account');
        insert testAccount;
        Opportunity testOppty = new Opportunity(AccountId=testAccount.Id,Name='TestOpportunity 3',LeadSource='Sales',Type='Annual',StageName='Needs Analysis',CloseDate=Date.today().addDays(10));
        insert testOppty;
        List<Pricebook2> myPricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        Quote testQuote = new Quote(Name='987Test Quote',OpportunityId=testOppty.Id,Pricebook2Id=myPricebooks[0].Id);
        insert testQuote;
        Package__c testPackage = new Package__c(Quote__c=testQuote.Id,Package_Type__c='Startup',User_Limit__c=100,Target_Price__c=10000);       
        insert testPackage;
       	
       	PricebookEntry pluginPBE = [SELECT Id FROM PricebookEntry WHERE Product2Id IN (SELECT Id FROM Product2 WHERE Name LIKE '%Plugin%' OR Name LIKE '%plugin%') AND IsActive=true AND Pricebook2Id= :myPricebooks[0].Id LIMIT 1];
       	
       	QuoteLineItem testQLI = new QuoteLineItem(QuoteId=testQuote.Id, UnitPrice=750, Quantity=1, Discount=0, PricebookEntryId=pluginPBE.Id,User_Limit__c=10);
		insert testQLI;
		
		PageReference testPref =  Page.LLCContract;
		testPref.getParameters().put('qid',testQuote.Id);
    	Test.setCurrentPage(testPref);
    	ApexPages.StandardController myStandardContr = new ApexPages.StandardController(testQuote);
    	LLCContractController myController = new LLCContractController();
    	myController.onStart();
    }
}
*/