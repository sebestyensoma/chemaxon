public class Util  {

	public static List<Pricebook2> standardPricebookList = new List<Pricebook2>();
	public static List<User> activeUserList = new List<User>();
	public static Id KJ;

	static{
		standardPricebookList = [
			SELECT
				Id
			FROM
				Pricebook2
			WHERE
				IsActive = true
				AND
				IsStandard = true
			LIMIT 1
		];

		for(User usr : [
            SELECT
                Id,
                LastName,
                FirstName,
                CompanyName,
				Name,
				IsActive
            FROM
                User
            WHERE
                IsActive = TRUE
        ]){
			if(usr.IsActive) activeUserList.add(usr);
			if(usr.Name == 'Judit Kerékgyártó') KJ = usr.Id;
		}


	}

	public static Boolean runningInASandbox {
		get {
			if (runningInASandbox == null) {
				runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
			}
			return runningInASandbox;
		}
		set;
	}

	public static final String UCEMAILADDRESS = 'chemaxon_admin@united-consult.hu';
	public static final String FINANCEEMAILADDRESS = 'finance@chemaxon.com';

	public static void sendTextBasedEmailToAddress(String toAddress, String subject, String message) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(toAddress.split(','));
		mail.setSubject(subject);
		mail.setPlainTextBody(message);
		if(! Test.isRunningTest() ){
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
	}
}