/**
 * @author      Attention CRM Consulting <chemaxon@attentioncrm.hu>
 * @version     1.0
 * @status		Functional/Tentative
 * @updated		2014-04-04 (SM)
 */ 

public with sharing class ReportController {
	private final String DEFAULT_CURRENCY = 'USD';
	
	public String tableHTML { get; set; }
	public String currencyName { get; set; }
	
	public String getTableHTML() { return tableHTML; }
	public String getCurrencyName() { return currencyName; }
	
	// CONSTRUCTOR AND SETUP
	
	public ReportController() {}
	
	// ACTIONS
	
	// Prepare report: monthly sums with percentile difference compared to same month of previous year
	public void reportAll() {
		// Prepare and run query
		String currencyParam = ApexPages.currentPage().getParameters().get('currency');
		currencyName = (currencyParam != null) ? currencyParam : DEFAULT_CURRENCY;
		String currencyField = 'Total_in_' + currencyName + '__c';
		
		List<QuoteLineItem> rawList = [
			SELECT Quote.Invoice_Date__c,
				Total_in_EUR__c, Total_in_USD__c, Total_in_CHF__c, Total_in_SGD__c, Total_in_JPY__c, Total_in_HUF__c, Total_in_GBP__c
			FROM QuoteLineItem
			ORDER BY Quote.Invoice_Date__c
		];
		
		Map<Integer, Decimal[]> years = new Map<Integer, Decimal[]>();
		Map<Integer, Decimal[]> midYearSums = new Map<Integer, Decimal[]>();
		Map<Integer, Decimal[]> differences = new Map<Integer, Decimal[]>();
		
		// Create and populate table
		for (QuoteLineItem item : rawList) if (item.quote.Invoice_Date__c != null) {
			Integer yearNum = item.quote.Invoice_Date__c.year();
			Integer monthNum = item.quote.Invoice_Date__c.month();
			
			// Add to the monthly sum
			Decimal num = (Decimal)item.get(currencyField);
			if (num != null) {
				Decimal[] year = getOrCreateYear(years, yearNum);
				year[monthNum-1] += num;
			}
		}

		// Calculate mid-year sums and differences
		for (Integer yearNum : years.keySet()) {
			Decimal sum = 0;
			Decimal[] year = years.get(yearNum);
			Decimal[] sumYear = new Decimal[12];
			for (Integer m = 0; m < 12; m++) {
				sum += year[m];
				sumYear[m] = sum;
			}
			midYearSums.put(yearNum, sumYear);
		}
		for (Integer yearNum : years.keySet()) {
			differences.put(yearNum, createDifferences(midYearSums, yearNum));
		}
		
		// Sort the maps by year, reverse, and generate table HTML
		List<Integer> yearNumbers = new List<Integer>(years.keySet());
		yearNumbers.sort();
		
		List<Integer> reverseList = new Integer[yearNumbers.size()];
		for(Integer i = yearNumbers.size()-1; !yearNumbers.isEmpty(); i--) {
			reverseList[i] = yearNumbers.remove(0);
		}
		yearNumbers = reverseList;
		
		tableHtml = '';
		for (Integer yearNum : yearNumbers) {
			Decimal[] year = years.get(yearNum);
			Decimal[] diff = differences.get(yearNum);
			Decimal[] sums = midYearSums.get(yearNum);
			
			tableHTML += '<tr><td class="year">' + yearNum + '</td>';
			for (Integer m = 0; m < 12; m++) {
				tableHTML += '<td>' + formatDecimal(year[m]) + '</td>';
				tableHTML += '<td>' + formatDecimal(sums[m]) + '</td>';
				tableHTML += '<td class="bline">';
				if (diff != null) {
					tableHTML += diff[m] + '%';
				}
				tableHTML += '</td>';
			}
			tableHTML += '</tr>';
		}
	}
	
	public void reportAPRenewal() {
		// Prepare and run query
		String currencyParam = ApexPages.currentPage().getParameters().get('currency');
		currencyName = (currencyParam != null) ? currencyParam : DEFAULT_CURRENCY;
		String currencyField = 'Total_in_' + currencyName + '__c';
	
		List<QuoteLineItem> rawList = [
			SELECT Quote.Invoice_Date__c, License_Type__c,
				Total_in_EUR__c, Total_in_USD__c, Total_in_CHF__c, Total_in_SGD__c, Total_in_JPY__c, Total_in_HUF__c, Total_in_GBP__c
			FROM QuoteLineItem
			WHERE License_type__c IN ('Annual Renewal', 'Perpetual Renewal')
			ORDER BY Quote.Invoice_Date__c
		];
		
		performAPReport(rawList, currencyField);
	}
	
	// Prepare report: monthly sums for annual/perpetual OR annual renewed/perpetual renewed, and the sum of the two
	public void reportAP() {
		// Prepare and run query
		String currencyParam = ApexPages.currentPage().getParameters().get('currency');
		currencyName = (currencyParam != null) ? currencyParam : DEFAULT_CURRENCY;
		String currencyField = 'Total_in_' + currencyName + '__c';
		
		List<QuoteLineItem> rawList = [
			SELECT Quote.Invoice_Date__c, License_Type__c,
				Total_in_EUR__c, Total_in_USD__c, Total_in_CHF__c, Total_in_SGD__c, Total_in_JPY__c, Total_in_HUF__c, Total_in_GBP__c
			FROM QuoteLineItem
			WHERE License_type__c IN ('Annual', 'Perpetual')
			ORDER BY Quote.Invoice_Date__c
		];

		performAPReport(rawList, currencyField);
	}
	
	// PRIVATE METHODS
	
	/** Helper method: checks if a year is already in a map. If not, creates and inserts it */
	private Decimal[] getOrCreateYear(Map<Integer, Decimal[]> yearMap, Integer yearNum) {
		Decimal[] year = yearMap.get(yearNum);
		if (year == null) {
			year = new Decimal[12];
			for (Integer m = 0; m < 12; m++) {
				year[m] = 0;
			}
			yearMap.put(yearNum, year);
		}
		return year;
	}
	
	/** Helper method: calcualtes percentile differences between two years in a year map */
	private Decimal[] createDifferences(Map<Integer, Decimal[]> yearMap, Integer yearNum) {
		if (!yearMap.containsKey(yearNum) || !yearMap.containsKey(yearNum - 1)) return null;
		
		Decimal[] thisYear = yearMap.get(yearNum);
		Decimal[] lastYear = yearMap.get(yearNum - 1);
		Decimal[] diff = new Decimal[12];
		
		for (Integer m = 0; m < 12; m++) {
			Decimal difference = (lastYear[m] != 0) ? (100.0 * (thisYear[m] - lastYear[m]) / lastYear[m]) : 0;
			diff[m] = difference.round(RoundingMode.HALF_UP);
		}
		
		return diff;
	}
	
	private void performAPReport(List<QuoteLineItem> rawList, String currencyField) {
		// Create and populate tables
		Set<Integer> allYears = new Set<Integer>();
		Map<Integer, Decimal[]> annualYears = new Map<Integer, Decimal[]>();
		Map<Integer, Decimal[]> perpetualYears = new Map<Integer, Decimal[]>();
		Map<Integer, Decimal[]> totalYears = new Map<Integer, Decimal[]>();
		Map<Integer, Decimal[]> midYearSums = new Map<Integer, Decimal[]>();
		
		for (QuoteLineItem item : rawList) if (item.quote.Invoice_Date__c != null) {
			Integer yearNum = item.quote.Invoice_Date__c.year();
			Integer monthNum = item.quote.Invoice_Date__c.month();
			
			Decimal num = (Decimal)item.get(currencyField);
			if (num != null) {
				Decimal[] year;
				
				// Filter for license type
				if (item.License_type__c == 'Annual' || item.License_type__c == 'Annual Renewal') year = getOrCreateYear(annualYears, yearNum);
				else if (item.License_type__c == 'Perpetual' || item.License_type__c == 'Perpetual Renewal') year = getOrCreateYear(perpetualYears, yearNum);
				else continue;
				
				allYears.add(yearNum);
				year[monthNum-1] += num;
				
				year = getOrCreateYear(totalYears, yearNum);
				year[monthNum-1] += num;
			}
		}
		
		// Sort the maps by year and reverse
		List<Integer> yearNums = new List<Integer>(allYears);
		yearNums.sort();
		
		List<Integer> reverseList = new Integer[yearNums.size()];
		for(Integer i = yearNums.size()-1; !yearNums.isEmpty(); i--) {
			reverseList[i] = yearNums.remove(0);
		}
		yearNums = reverseList;
		
		// Create mid-year sums
		for (Integer yearNum : yearNums) {
			Decimal[] totalYear = totalYears.get(yearNum); 
			Decimal[] midYearSum = getOrCreateYear(midYearSums, yearNum);
			
			Decimal sum = 0;
			for (Integer m = 0; m < 12; m++) {
				sum += totalYear[m];
				midYearSum[m] = sum;
			}
		}
		
		// Generate table HTML
		tableHTML = '';
		for (Integer yearNum : yearNums) {
			Decimal[] annualYear = annualYears.get(yearNum);
			Decimal[] perpetualYear = perpetualYears.get(yearNum);
			Decimal[] totalYear = totalYears.get(yearNum);
			Decimal[] midYearSum = midYearSums.get(yearNum);
			Decimal[] diffYear = createDifferences(midYearSums, yearNum);
			
			tableHTML += '<tr><td class="year">' + yearNum + '</td>';
			
			for (Integer m = 0; m < 12; m++) {
				// Annual
				tableHTML += '<td>';
				if (annualYear != null) {
					tableHTML += formatDecimal(annualYear[m]);		
				}
				tableHTML += '</td>';
				
				// Perpetual
				tableHTML += '<td>';
				if (perpetualYear != null) {
					tableHTML += formatDecimal(perpetualYear[m]);		
				}
				tableHTML += '</td>';
				
				// Total
				tableHTML += '<td>';
				if (totalYear != null) {
					tableHTML += formatDecimal(totalYear[m]);		
				}
				tableHTML += '</td><td>';
				if (midYearSum != null) {
					tableHTML += formatDecimal(midYearSum[m]);		
				}
				tableHTML += '</td><td class="bline">';
				if (diffYear != null) {
					tableHTML += diffYear[m] + '%';
				}
				tableHTML += '</td>';
			}
			tableHTML += '</tr>';
		}
	}
	
	/** Format a Decimal in "100 000 000" format */
	private static String formatDecimal(Decimal n) {
		if (n == null) return '';
		
		String base = String.valueOf(Math.abs(n.round(RoundingMode.HALF_UP)));
		String result = '';
		Integer copyTo = base.length();
		Integer copyFrom = copyTo - 3;
		if (copyFrom < 0) copyFrom = 0;
		
		while (copyTo > 0) {
			result = base.substring(copyFrom, copyTo) + ' ' + result;
			copyFrom -= 3;
			copyTo -=3;
			if (copyFrom < 0) copyFrom = 0;
		}
		
		if (n < 0) result = '-' + result;
		return result;
	}
}