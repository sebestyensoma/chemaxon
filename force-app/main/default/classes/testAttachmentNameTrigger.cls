/** 
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2015-11-16 (TD)
 * 
 */
@isTest(seeAllData=false)
private class testAttachmentNameTrigger {

    static testMethod void myUnitTest() {
        
        Account acc = new Account (Name='test Account');
        insert acc;
        
        Agreement__c agr = new Agreement__c (Contract_with__c=acc.id,Contract_category__c='CDA' ,X1_year_auto_renewal__c='1');
        insert agr;
        
        Agreement_document__c ad = new Agreement_document__c (Agreement__c=agr.id); 
        insert ad;
        
        Attachment attch = new Attachment(body = blob.toPDF('PDF doksi'),Name='Name', contenttype = 'application/pdf',ParentId=ad.id);
        insert attch;
        
        Attachment attch2 = new Attachment(body = blob.toPDF('PDF doksi'),Name='Name', contenttype = 'application/pdf',ParentId=ad.id);
        try{
        	insert attch2;
        }catch(exception ex){
        	
        }
    }
    
    static testMethod void myUnitTestNameFromLink() {
        
        Account acc = new Account (Name='test Account');
        insert acc;
        
        Agreement__c agr = new Agreement__c (Contract_with__c=acc.id,Contract_category__c='CDA' ,X1_year_auto_renewal__c='1');
        insert agr;
        
        Attachment attch = new Attachment(body = blob.toPDF('PDF doksi'),Name='Name', contenttype = 'application/pdf',ParentId=acc.id);
        insert attch;
        
        Agreement_document__c ad = new Agreement_document__c (Agreement__c=agr.id, Document_link__c='https://cs14.salesforce.com/'+attch.id); 
        insert ad;
        
    }
}