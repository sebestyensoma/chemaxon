public with sharing class InvoiceGenerator {
	//public Invoice__c myInvoice{get;set;}
	private Map<Id,Quote> mapIdToQuotes;
	public Quote myQuote{get;set;}
	public Id quoteId{get;set;}
	public void onStart(){
		quoteId = Apexpages.currentPage().getParameters().get('qid');
		mapIdToQuotes = new Map<Id,Quote>([SELECT Id, Invoice_Date__c,Invoice_No__c, PO_No__c,Invoice_Type__c  FROM Quote WHERE Id = :quoteId]);
		myQuote = mapIdToQuotes.values()[0];
		//myInvoice = new Invoice__c(Quote__c=mapIdToQuotes.values()[0].Id);		
	}
	
	public Pagereference action()
	{	
		if(myQuote.Invoice_Type__c == 'LLC')
		{
			if(myQuote.Invoice_Date__c!=null && myQuote.Invoice_No__c!=null && myQuote.PO_No__c!=null)
			{
				update myQuote;
				return new Pagereference('/'+quoteId);
			}
			else
			{
				 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the Invoice Date, Invoice No and PO No!'));
				 
				 return null;
			}
		}
		else
		{
			myQuote.Invoice_Date__c = NULL;
			myQuote.Invoice_No__c = NULL;
			myQuote.PO_No__c = NULL;
			
			update myQuote;
		}
		
		return new Pagereference('/'+quoteId);
		/*
		if(mapIdToQuotes!=NULL && mapIdToQuotes.size()==1){		
			//insert myInvoice;
			
			//Instert Invoice Line Items
			
			
			List<Invoice_Line_Item__c> myILIs = new List<Invoice_Line_Item__c>();
			for(QuoteLineItem loopQLI :[SELECT Id FROM QuoteLineItem WHERE QuoteId = :quoteId]){				
				myILIs.add(new Invoice_Line_Item__c(Quote_Line_Item__c=loopQLI.Id, Invoice__c=myInvoice.Id));
			}			
			if(myILIs.size()>0){
				insert myILIs;
			}
			
			
			//Insert Package connectors
			/*
			List<Package__c> myPackages = [SELECT Id FROM Package__c WHERE Quote__c = :quoteId];
			List<Package_Invoice_Connector__c> myPICs = new List<Package_Invoice_Connector__c>();
			for(Package__c loopPackage:myPackages){
				myPICs.add(new Package_Invoice_Connector__c(Invoice__c=myInvoice.Id,Package__c=loopPackage.Id));
			}			
			if(myPICs.size()>0){
				insert myPICs;
			}
			
			
		}
		*/
	}
	public Pagereference updateAction(){
		/*
		update myQuote;
		return new Pagereference('/apex/InvoiceGenerator?qid='+myQuote.Id);
		*/
		
		if(myQuote.Invoice_Type__c == 'LLC')
		{
			if(myQuote.Invoice_Date__c!=null && myQuote.Invoice_No__c!=null && myQuote.PO_No__c!=null)
			{
				update myQuote;
				return new Pagereference('/'+quoteId);
			}
			else
			{
				 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Please specify the Invoice Date, Invoice No and PO No!'));
				 
				 return null;
			}
		}
		else
		{
			myQuote.Invoice_Date__c = NULL;
			myQuote.Invoice_No__c = NULL;
			myQuote.PO_No__c = NULL;
			
			update myQuote;
		}
		
		return new Pagereference('/apex/InvoiceGenerator?qid='+myQuote.Id);
	}
	
}