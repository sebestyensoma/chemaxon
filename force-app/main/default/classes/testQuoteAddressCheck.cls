@isTest(seeAllData=true)
private class testQuoteAddressCheck {

    static testMethod void throwError() {
    	// Insert
		Account acc = new Account(
			Name = 'Test Account',
			Phone = '123456789',
			BillingCountry = 'Sweden'
		);
		insert acc;
		 
		Opportunity opp = new Opportunity(
			Name = 'Test Opp',
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			AccountId = acc.id
		);
		insert opp;
		
		Contact ct = new Contact(
			FirstName = 'Test',
			LastName = 'Contact',
			Email = 'test@example.com'
		);
		insert ct;
		
		Quote q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.id,
			License_Expiration_Date__c = Date.today(),
			PO_No__c = '12346',
			Contact_Invoice__c = ct.id,
			Contact_License__c = ct.id
		);
		insert q;
		
		// Execute
		try {
			q.Status = 'Ordered';
			update q;
			
			System.debug('{SM} FAIL! Ordered without VAT No.');
			System.assert(false);
		}
		catch (Exception e) {
			System.debug(e);
			System.debug('{SM} PASS. Error thrown.');
		}
    }
    
    static testMethod void noErrorNonEU() {
    	// Insert
		Account acc = new Account(
			Name = 'Test Account',
			Phone = '123456789',
			BillingCountry = 'Norway'
		);
		insert acc;
		 
		Opportunity opp = new Opportunity(
			Name = 'Test Opp',
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			AccountId = acc.id
		);
		insert opp;
		
		Contact ct = new Contact(
			FirstName = 'Test',
			LastName = 'Contact',
			Email = 'test@example.com'
		);
		insert ct;
		
		Quote q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.id,
			License_Expiration_Date__c = Date.today(),
			PO_No__c = '12346',
			Contact_Invoice__c = ct.id,
			Contact_License__c = ct.id
		);
		insert q;
		
		// Execute
		q.Status = 'Ordered';
		update q;
			
		System.debug('{SM} PASS. No error.');
    }
    
    static testMethod void noErrorEU() {
    	// Insert
		Account acc = new Account(
			Name = 'Test Account',
			Phone = '123456789',
			BillingCountry = 'Sweden',
			Tax_Number__c = '123456'
		);
		insert acc;
		 
		Opportunity opp = new Opportunity(
			Name = 'Test Opp',
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			AccountId = acc.id
		);
		insert opp;
		
		Contact ct = new Contact(
			FirstName = 'Test',
			LastName = 'Contact',
			Email = 'test@example.com'
		);
		insert ct;
		
		Quote q = new Quote(
			Name = 'Test Quote',
			OpportunityId = opp.id,
			License_Expiration_Date__c = Date.today(),
			PO_No__c = '12346',
			Contact_Invoice__c = ct.id,
			Contact_License__c = ct.id
		);
		insert q;
		
		// Execute
		q.Status = 'Ordered';
		update q;
			
		System.debug('{SM} PASS. No error.');
    }
}