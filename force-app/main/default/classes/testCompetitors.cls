@istest 
private class testCompetitors {
	
	@istest(seeAlldata=true) 
	private static void test(){
		Account acc = new Account(Name = 'Test');
		insert acc;
		ApexPages.StandardController stdc = new ApexPages.StandardController(acc);
 		Competitors ctrl = new Competitors(stdc);
 		
 		PageReference pageRef = Page.Competitors;
        Test.setCurrentPage(pageRef);
 		
 		ctrl.newLine.selectedForWhat = 'Web client query (Pconnect)';
 		ctrl.newLine.selectedCompetitor = 'Biovia';
 		ctrl.newLine.selectedProduct = 'Insight';
 		system.assertEquals(true, ctrl.newLine.getSelectedProductIsNotNull());
 		ctrl.save();
 		system.assertEquals(true, ctrl.getExistingLinesSizeIsNotZero());
 		
 		ctrl.deleteParam = ctrl.existingLines[0].Id;
 		ctrl.del();
 		
 		ctrl.back();
	}
}