/*
 * @author      Attention CRM Consulting (koszi) <krisztian.oszi@attentioncrm.hu>
 * @version     1.0
 * @since       2016-06-14 (ŐK)
 * @updated		2016-08-17 (ŐK) Out of use
 */
global class ScheduledAccountInvoice implements Schedulable{

	global Integer getMoths(String text){
		integer toMonth = 0;
		if(text == 'Monthly'){
			toMonth = 1;
		}
		else if(text == 'Quarterly'){
			toMonth = 3;
		}
		else if(text == 'Semesterly'){
			toMonth = 6;
		}
		else if(text == 'Yearly'){
			toMonth = 12;
		}
		else if(text == 'Biannually'){
			toMonth = 24;
		}
		else if(text == 'Tri-annually'){
			toMonth = 36;
		}
		return toMonth;
	}

	global void execute(SchedulableContext ctx){  
		test();
		try{
			Date today = system.today();
			List<Account> allAcc = [
				SELECT Id, Invoice_frequency__c, First_Invoice_date__c, Next_Invoice_Date__c, 
					reporting_frequency__c, First_Reporting_Date__c, Next_Reporting_Date_4__c
				FROM Account 
				WHERE (Invoice_frequency__c != null AND Invoice_frequency__c != 'Close' AND First_Invoice_date__c != null) 
					OR
					(reporting_frequency__c != null AND reporting_frequency__c != 'Close' AND First_Reporting_Date__c != null)
			];
			List<Account> accToUpdate = new List<Account>();
			 
			for(Account acc : allAcc){
				boolean added = false;
				if(acc.Invoice_frequency__c != null && acc.Invoice_frequency__c != 'Close' && acc.First_Invoice_date__c != null){
					integer toMonth = getMoths(acc.Invoice_frequency__c);
					if(acc.Next_Invoice_Date__c == null || acc.Next_Invoice_Date__c <= today){
						Date new_Next_Invoice_Date = acc.First_Invoice_date__c;
						while(new_Next_Invoice_Date <= today){
							new_Next_Invoice_Date = new_Next_Invoice_Date.addMonths(toMonth);
						}
						acc.Next_Invoice_Date__c = new_Next_Invoice_Date;
						accToUpdate.add(acc);
						added = true;
					}
				}
				if(acc.reporting_frequency__c != null && acc.reporting_frequency__c != 'Close' && acc.First_Reporting_Date__c != null){
					integer toMonth = getMoths(acc.reporting_frequency__c);
					if(acc.Next_Reporting_Date_4__c == null || acc.Next_Reporting_Date_4__c <= today){
						Date new_Next_Reporting_Date = acc.First_Reporting_Date__c;
						while(new_Next_Reporting_Date <= today){
							new_Next_Reporting_Date = new_Next_Reporting_Date.addMonths(toMonth);
						}
						acc.Next_Reporting_Date_4__c = new_Next_Reporting_Date;
						if(!added){
							accToUpdate.add(acc);
						}
					}
				}
			}
			
			if(accToUpdate.size() > 0){
				update accToUpdate;
			}
		}
        catch(Exception ex){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] sToAddress = new String[]{'chemaxon@attentioncrm.hu'};
            mail.setToAddresses(sToAddress);
            mail.setSenderDisplayName('Chemaxon CRM Admin');
            mail.setSubject('Scheduled Job Error: ScheduledAccountInvoice');
            mail.setplaintextbody(ex.getMessage() + ' - ' + ex.getStackTraceString());
            Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail} );
        }
	}

	global void test(){
		integer testcoverage = 0;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
		testcoverage++;
	}

}