({
    callAction : function(component, event, helper) {
        component.set("v.loading", true);
        let params = {
            "opptyId" : component.get("v.recordId")
        };
        helper.doAction("runOpportunityRenewalWithOpptyId", params, 'Nem sikerült megújítani az opportunityt.', component, event, function(res){
            if(res === "OK"){
                component.set("v.status", "success");
            }else{
                component.set("v.errorMessage", res);
                component.set("v.status", "error");
            }
            component.set("v.loading", false);
        });
    },
    handleClose : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    }
})
