trigger License on License__c (before insert, after insert)
{
	/*
	Ha beérkező license req. SF account id-vel rendelkezik akkor
	accounthoz kapcsolni ÉS leadet létrehozni (ahhoz is kapcsolni)
	-- ha nincs SF acc. id akkor csak lead-hez kapcsolni
	
	de ha van már nyitott lead az adott emailcímmel, akkor ahhoz csatoljuk
	és email figyelmeztetést küldünk
	*/
	
	if(Trigger.isInsert && Trigger.isBefore)
	{
		for(License__c license : Trigger.New)
		{
			if
			(
				license.Contact_Last_Name_Eval__c == '' ||
				license.Contact_Last_Name_Eval__c == 'NULL' ||
				license.Contact_Last_Name_Eval__c == 'null' ||
				license.Contact_Last_Name_Eval__c == NULL
			)
			{
				license.Contact_Last_Name_Eval__c = license.Contact_First_Name_Eval__c;
			}
			
			if
			(
				license.Account_ID_text__c == '' ||
				license.Account_ID_text__c == 'NULL' ||
				license.Account_ID_text__c == 'null' ||
				license.Account_ID_text__c == NULL
			)
			{
				license.Account_ID_text__c = NULL;
			}
			
			if
			(
				license.Opportunity_ID_text__c == '' ||
				license.Opportunity_ID_text__c == 'NULL' ||
				license.Opportunity_ID_text__c == 'null' ||
				license.Opportunity_ID_text__c == NULL
			)
			{
				license.Opportunity_ID_text__c = NULL;
			}
			
			if
			(
				license.Country_Eval__c == '' ||
				license.Country_Eval__c == 'NULL' ||
				license.Country_Eval__c == 'null' ||
				license.Country_Eval__c == NULL
			)
			{
				license.Country_Eval__c = NULL;
			}
			
			if
			(
				license.State_Eval__c == '' ||
				license.State_Eval__c == 'NULL' ||
				license.State_Eval__c == 'null' ||
				license.State_Eval__c == NULL
			)
			{
				license.State_Eval__c = NULL;
			}
			
			if
			(
				license.Account_Alias__c == '' ||
				license.Account_Alias__c == 'NULL' ||
				license.Account_Alias__c == 'null' ||
				license.Account_Alias__c == NULL
			)
			{
				license.Account_Alias__c = NULL;
			}
			
			if
			(
				license.Account_Email__c != '' &&
				license.Account_Email__c != 'NULL' &&
				license.Account_Email__c != 'null' &&
				license.Account_Email__c != NULL
			)
			{
				license.Account_Email__c = license.Account_Email__c.toLowerCase();
			}
		}
	}
	
	if(Trigger.isInsert && Trigger.isBefore)
	{
		Set<string> accountIdStringSet = new Set<string>();
		Set<string> accountEmailSet = new Set<string>();
		List<Account> accountList = new List<Account>();
		Set<string> accountIdSet = new Set<string>();
		List<Lead> existingLeadList = new List<Lead>();
		Map<string, Lead> existingLeadMap = new Map<string, Lead>();
		List<Lead> leadList = new List<Lead>();
		Map<string, Lead> leadMap = new Map<string, Lead>(); 
		
		for(License__c license : Trigger.New)
		{
			if
			(
				license.Account_ID_text__c != '' &&
				license.Account_ID_text__c != 'NULL' &&
				license.Account_ID_text__c != 'null' &&
				license.Account_ID_text__c != NULL
			)
			{
				accountIdStringSet.add(license.Account_ID_text__c);
			}
			
			if(license.Account_Email__c != '' && license.Account_Email__c != NULL)
			{
				accountEmailSet.add(license.Account_Email__c);
			}
		}
		
		if(accountIdStringSet.size() != 0)
		{
			accountList =
				[
					SELECT
						Id,
						Name
					FROM
						Account
					WHERE
						Id IN :accountIdStringSet
				];
				
			if(accountList.size() != 0)
			{
				for(integer i=0; i<accountList.size(); i++)
				{
					accountIdSet.add(accountList[i].Id);
					accountIdSet.add(string.valueof(accountList[i].Id).subString(0,15));
				}
			}
		}
		
		if(accountEmailSet.size() != 0)
		{
			system.debug(accountEmailSet);
			
			List<string> statusList = new List<string>();
			
			statusList.add('New');
			statusList.add('Open');
			statusList.add('Emailed');
			statusList.add('Called');
			
			existingLeadList = 
				[
					SELECT
						Id,
						Email
					FROM
						Lead
					WHERE
						Status IN :statusList
						AND
						Email IN :accountEmailSet
				];
				
			system.debug(existingLeadList);
				
			if(existingLeadList.size() != 0)
			{
				for(integer i=0; i<existingLeadList.size(); i++)
				{
					existingLeadMap.put(existingLeadList[i].Email, existingLeadList[i]);
				}
				
				system.debug('existing lead map: ' + existingLeadMap);
			}
		}
		
		for(License__c license : Trigger.New)
		{
			if(accountIdSet.contains(license.Account_ID_text__c))
			{
				license.Account__c = license.Account_ID_text__c;
			}
			
			if(existingLeadMap.containsKey(license.Account_Email__c))
			{
				license.Lead__c = existingLeadMap.get(license.Account_Email__c).Id;
			}
			else if(!existingLeadMap.containsKey(license.Account_Email__c) && license.License_Type__c == 'Evaluation')
			{
				system.debug('license.Products_Names__c ' + license.Products_Names__c);
				
				leadList.add
				(
					new Lead
						(
							//LastName = license.Account_Name__c + ' ' + license.Name,
							LastName = license.Contact_Last_Name_Eval__c,
							FirstName = license.Contact_First_Name_Eval__c,
							Status = 'New',
							Company = license.Account_Name__c,
							Email = license.Account_Email__c,
							Licence_No__c = license.Name,
							Country = license.Country_Eval__c,
							State = license.State_Eval__c,
							Account_Alias__c = license.Account_Alias__c,
							Product_Area_Interest__c=license.Products_Names__c
						)
				);
			}
		}
		
		if(leadList.size() != 0)
		{
			Database.DMLOptions dmlOpts = new Database.DMLOptions();
			dmlOpts.assignmentRuleHeader.assignmentRuleId= '01Q200000006NYS';
			
			for(Lead lead : leadList)
			{
				lead.setOptions(dmlOpts);
			}
			
			insert leadList;
			
			for(integer i=0; i<leadList.size(); i++)
			{
				leadMap.put(leadList[i].Licence_No__c, leadList[i]);
				
				system.debug('leadList[i].Licence_No__c = ' + leadList[i].Licence_No__c);
				system.debug('leadList[i].Id = ' + leadList[i].Id);
			}
		}
		
		for(License__c license : Trigger.New)
		{
			if(leadMap.containsKey(license.Name))
			{
				license.Lead__c = leadMap.get(license.Name).Id;
			
				system.debug('license = ' + license);
			}
		}
	}
	
	if(Trigger.isInsert && Trigger.isAfter)
	{
		string sfdcBaseUrl = url.getSalesforceBaseUrl().toExternalForm();
		Set<string> accountIdStringSet = new Set<string>();
		Set<string> accountEmailSet = new Set<string>();
		List<Account> accountList = new List<Account>();
		Set<string> accountIdSet = new Set<string>();
		List<Lead> existingLeadList = new List<Lead>();
		Map<string, Lead> existingLeadMap = new Map<string, Lead>();
		List<Lead> leadList = new List<Lead>();
		Map<string, Lead> leadMap = new Map<string, Lead>();
		List<Messaging.SingleEmailMessage> sendEmails = new List<Messaging.SingleEmailMessage>();
		Set<string> leadOwnerIdSet = new Set<string>();
		List<User> userList = new List<User>();
		Map<id, User> userMap = new Map<id, User>();
		
		system.debug('isafter');
		
		for(License__c license : Trigger.New)
		{
			system.debug('for');
			
			if(license.Account_Email__c != '' && license.Account_Email__c != NULL)
			{
				system.debug('forif');
				
				accountEmailSet.add(license.Account_Email__c);
				system.debug(license.Account_Email__c);
			}
		}
		
		system.debug('accountEmailSet.size(): ' + accountEmailSet.size());
		
		if(accountEmailSet.size() != 0)
		{
			existingLeadList = 
				[
					SELECT
						Id,
						Email,
						OwnerId
					FROM
						Lead
					WHERE
						(Status = 'New' OR Status = 'Open')
						AND
						Email IN :accountEmailSet
						AND
						CreatedDate != TODAY
				];
				
			if(existingLeadList.size() != 0)
			{
				for(integer i=0; i<existingLeadList.size(); i++)
				{
					existingLeadMap.put(existingLeadList[i].Email, existingLeadList[i]);
					leadOwnerIdSet.add(existingLeadList[i].OwnerId);
				}
			}
		}
		
		if(leadOwnerIdSet.size() != 0)
		{
			userList =
				[
					SELECT
						Id,
						FirstName
					FROM
						User
					WHERE
						Id IN :leadOwnerIdSet
				];
				
			if(userList.size() != 0)
			{
				for(integer i=0; i<userList.size(); i++)
				{
					userMap.put(userList[i].Id, userList[i]);
				}
			}
		}
		
		system.debug('existingLeadMap tartalma: ' + existingLeadMap);
		
		for(License__c license : Trigger.New)
		{
			if(existingLeadMap.containsKey(license.Account_Email__c))
			{
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    		List<string> toEmailStringList = new List<string>();
	    		List<string> ccEmailStringList = new List<string>();
	    		
	    		/*
	    		toEmailStringList.add(userIdMap.get(opportunityList[i].OwnerId).Email);
	    		ccEmailStringList.add('chemaxon@attentioncrm.hu');
	    		*/
	    		
	    		
	    		toEmailStringList.add(license.Lead_Owner_Email__c);
	    		ccEmailStringList.add('chemaxon@attentioncrm.hu');
	    		
	    		
	    		//toEmailStringList.add('adam.bobor@attentioncrm.hu');	    		
	    		
	    		mail.setToAddresses(toEmailStringList);
	    		mail.setCCAddresses(ccEmailStringList);
	    		
	    		mail.setSubject('"New license" email alert');
				mail.setUseSignature(false);
				mail.setCharset('UTF-8');
				
				mail.setHtmlBody
					(						
						'Dear ' + userMap.get(existingLeadMap.get(license.Account_Email__c).OwnerId).FirstName + ','
						+
						'<br/><br/>'
						+
						'New license has been attached to existing open lead. Click to see related records:'
						+
						'<br/><br/>'
						+
						'<a href="' + sfdcBaseUrl + '/' + existingLeadMap.get(license.Account_Email__c).Id + '">Lead</a>'
						+
						'<br/>'
						+
						'<a href="' + sfdcBaseUrl + '/' + license.Id + '">License</a>'
						+
						'<br/><br/>'
						+
						'Regards,<br/> CRM'
					);
				sendEmails.add(mail);
				system.debug(mail);
				Messaging.sendEmail(sendEmails);
			}
		}
	}
	
	/*
	Opportunity ID-val kapjuk a licenszeket, és ez alapján kitöltjük a lookupot + az Opp Accountot
	*/
	
	if((Trigger.isInsert/* || Trigger.isUpdate*/) && Trigger.isBefore)
	{
		Set<Id> opportunityIdSet = new Set<Id>();
		List<Opportunity> opportunityList = new List<Opportunity>();
		List<Opportunity> opportunityForUpdateList = new List<Opportunity>();
		Map<id, Opportunity> opportunityMap = new Map<id, Opportunity>();
		
		for(License__c license : Trigger.New)
		{
			if
			(
				license.Opportunity_ID_text__c != '' &&
				license.Opportunity_ID_text__c != 'NULL' &&
				license.Opportunity_ID_text__c != 'null' &&
				license.Opportunity_ID_text__c != NULL
			)
			{
				opportunityIdSet.add(license.Opportunity_ID_text__c);
			}
		}
		
		if(opportunityIdSet.size() != 0)
		{
			opportunityList =
				[
					SELECT
						Id,
						AccountId,
						Last_License_NO__c
					FROM
						Opportunity
					WHERE
						Id IN :opportunityIdSet
				];
				
			if(opportunityList.size() != 0)
			{
				for(integer i=0; i<opportunityList.size(); i++)
				{
					opportunityMap.put(opportunityList[i].Id, opportunityList[i]);
				}
			}
		}
		
		for(License__c license : Trigger.New)
		{			
			if(opportunityMap.containskey(license.Opportunity_ID_text__c))
			{
				Opportunity tempOpp = new Opportunity();
				tempOpp = opportunityMap.get(license.Opportunity_ID_text__c).clone(true, false, true, true);
				
				license.Opportunity__c = opportunityMap.get(license.Opportunity_ID_text__c).Id;
				license.Opportunity_Account__c = opportunityMap.get(license.Opportunity_ID_text__c).AccountId;
				
				tempOpp.Last_License_NO__c = license.Name;
				
				opportunityForUpdateList.add(tempOpp);
			}
		}
		
		if(opportunityForUpdateList.size() != 0)
		{
			try
			{
				update opportunityForUpdateList;
			}
			catch (exception e)
			{
				
			}
		}
	}
}