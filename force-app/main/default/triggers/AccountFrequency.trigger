/**
 * @author      Attention CRM Consulting (koszi) <krisztian.oszi@attentioncrm.hu>
 * @version     1.0
 * @since       2016-08-17 (?K)
 */
trigger AccountFrequency on Account (before insert, before update)  { 

	Integer getMoths(String text){
		integer toMonth = 0;
		if(text == 'Monthly'){
			toMonth = 1;
		}
		else if(text == 'Quarterly'){
			toMonth = 3;
		}
		else if(text == 'Semesterly'){
			toMonth = 6;
		}
		else if(text == 'Yearly'){
			toMonth = 12;
		}
		else if(text == 'Biannually'){
			toMonth = 24;
		}
		else if(text == 'Tri-annually'){
			toMonth = 36;
		}
		return toMonth;
	}
	
	Date today = system.today();
	for(Account acc : trigger.new){
		if(acc.Invoice_frequency__c != null && acc.Invoice_frequency__c != 'Close' && acc.First_Invoice_date__c != null){
			integer toMonth = getMoths(acc.Invoice_frequency__c);
			//if(acc.Next_Invoice_Date__c == null || acc.Next_Invoice_Date__c <= today){
				Date new_Next_Invoice_Date = acc.First_Invoice_date__c;
				while(new_Next_Invoice_Date <= today){
					new_Next_Invoice_Date = new_Next_Invoice_Date.addMonths(toMonth);
				}
				acc.Next_Invoice_Date__c = new_Next_Invoice_Date;
			//}
		}
		if(acc.reporting_frequency__c != null && acc.reporting_frequency__c != 'Close' && acc.First_Reporting_Date__c != null){
			integer toMonth = getMoths(acc.reporting_frequency__c);
			//if(acc.Next_Reporting_Date_4__c == null || acc.Next_Reporting_Date_4__c <= today){
				Date new_Next_Reporting_Date = acc.First_Reporting_Date__c;
				while(new_Next_Reporting_Date <= today){
					new_Next_Reporting_Date = new_Next_Reporting_Date.addMonths(toMonth);
				}
				acc.Next_Reporting_Date_4__c = new_Next_Reporting_Date;
			//}
		}
	}

}