trigger OpportunityLineItem on OpportunityLineItem (before insert, before update, after insert, after delete, after undelete)
{
    Set<id> changedUserNumberOpportunityIdSet = new Set<id>();

    if(!System.isFuture())
    {
        /*
        if(trigger.isInsert || trigger.isUpdate || trigger.isUndelete)
        {
            for(OpportunityLineItem currentOli : trigger.New)
            {
                changedUserNumberOpportunityIdSet.add(currentOli.OpportunityId);
            }
        }
        
        if(trigger.isDelete)
        {
            for(OpportunityLineItem currentOli : trigger.Old)
            {
                changedUserNumberOpportunityIdSet.add(currentOli.OpportunityId);
            }
        }
        */
        
        if(trigger.isInsert || trigger.isUndelete)
        {
            for(OpportunityLineItem currentOli : trigger.new)
            {
                changedUserNumberOpportunityIdSet.add(currentOli.OpportunityId);
            }
        }
        
        if(trigger.isDelete)
        {
            for(OpportunityLineItem currentOli : trigger.old)
            {
                changedUserNumberOpportunityIdSet.add(currentOli.OpportunityId);
            }
        }
    }
    
    
    ////////////////////
    /// STOP TRIGGER ///
    ////////////////////
    if(!trigger.isDelete)
    {
        for(OpportunityLineItem currOli : trigger.new)
        {
            /*
            if(TriggerStopper.quoteLineItemIdSet.contains(currQli.Id))
            {
                return;
            }
            */
            
            if(TriggerStopper.opportunityLineItemIdMap.containsKey(currOli.Id))
            {
                return;
            }
        }
    }
        
    system.debug('debuuug: ' + system.now() + TriggerStopper.opportunityLineItemIdMap);
    
    
    if(trigger.isBefore)
    {
        List<opportunityLineItem> existingPluginoliList = new List<opportunityLineItem>();
        List<opportunityLineItem> existingPluginoliForUpdateList = new List<opportunityLineItem>();
        Map<string, integer> numberOfExistingPluginsExternalIdMap = new Map<string, integer>();
        Map<string, integer> numberOfExistingPluginsForUpdateExternalIdMap = new Map<string, integer>();
        Map<string, integer> numberOfNewPluginsExternalIdMap = new Map<string, integer>();
        Set<id> opportunityIdSet = new Set<id>();
        Set<id> triggeredOliIdSet = new Set<id>();
        
        PriceHandler ph = new PriceHandler();
        
        if(trigger.isInsert)
        {
            if(opportunityIdSet.size() != 0)
            {
                numberOfExistingPluginsExternalIdMap = 
                    ph.calculateNumberOfExistingoliPlugins(opportunityIdSet, triggeredOliIdSet);
                    
                system.debug('kutya 01:' + numberOfExistingPluginsExternalIdMap);
                    
                existingPluginoliList = ph.getExistingPluginoliList(opportunityIdSet, triggeredOliIdSet);
                
                system.debug('kutya 02: ' + existingPluginoliList.size());
            }
            
            for(opportunityLineItem currentoli : trigger.New)
            {
                opportunityIdSet.add(currentoli.opportunityId);
                
                system.debug('zsiráf: ' + triggeredOliIdSet);           
                
                if(currentoli.Plugin__c == true)
                {
                    //key: opportunityid_LICENSETYPE_userlimit
                    
                    string pluginMapKey;
                    
                    pluginMapKey = ph.getPluginMapKey(currentOli);
                    
                    if(!numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, 1);
                    }
                    else
                    {
                        integer tempNumber;
                        
                        tempNumber = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                        
                        tempNumber++;
                        
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, tempNumber);
                    }
                }
            }
            
            for(opportunityLineItem currentoli : trigger.New)
            {
				System.debug(currentoli);
                if(currentOli.Created_By_Form__c == true)
                {
                    decimal salesPrice;
                    integer existingPlugins;
                    integer newPlugins;
                    string pluginMapKey;
                    
                    currentOli.Quantity = 1;
                    
                    pluginMapKey = ph.getPluginMapKey(currentOli);
                    
                    if(numberOfExistingPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        existingPlugins = numberOfExistingPluginsExternalIdMap.get(pluginMapKey);
                    }
                    else
                    {
                        existingPlugins = 0;
                    }
                    
                    if(numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        newPlugins = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                    }
                    else
                    {
                        newPlugins = 0;
                    }
                    
                    salesPrice = ph.calculateopportunityLineItemSalesPrice(currentoli, existingPlugins, newPlugins);
                    
                    if(currentoli.Target_Price__c != NULL)
                    {
                        if
                        (
                            currentoli.License_Type__c != NULL &&
                            currentoli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
                            currentoli.Opportunity_Renewal_Percentage__c != NULL
                        )
                        {
                            decimal tempSalesPrice;
                            tempSalesPrice = 
                                salesPrice * 
                                currentoli.Opportunity_Renewal_Percentage__c / 
                                100;
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentoli.UnitPrice = tempSalesPrice;
                            currentoli.Discount = NULL;
                        }
                        else
                        {
                        	currentoli.UnitPrice = currentoli.Target_Price__c;
                        	currentoli.Original_Price__c = salesPrice;
                        	currentoli.Discount = NULL;
                        }
                    }
                    else
                    {
                        currentoli.UnitPrice = salesPrice;
                        currentoli.Original_Price__c = salesPrice;
                        
                        if
                        (
                            currentoli.License_Type__c != NULL &&
                            currentoli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
                            currentoli.Opportunity_Renewal_Percentage__c != NULL
                        )
                        {
                            decimal tempSalesPrice;
                            tempSalesPrice = 
                                salesPrice * 
                                currentoli.Opportunity_Renewal_Percentage__c / 
                                100;
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentoli.UnitPrice = tempSalesPrice;
                        }
                        else
                        {
                            decimal tempSalesPrice;
                            tempSalesPrice = salesPrice;
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentoli.UnitPrice = tempSalesPrice;
                        }
                    }
                    
                    if(currentoli.Discount != NULL)
                    {
                        decimal tempTotalPrice;
                        
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        tempTotalPrice = salesPrice * ((100 - currentoli.Discount) / 100);
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        
                        tempTotalPrice = ph.customRounder(tempTotalPrice, NULL);
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        
                        system.debug('discount: ' + currentoli.Discount);
                        currentoli.Discount = (1 - (tempTotalPrice / salesPrice)) * 100;
                        system.debug('discount: ' + currentoli.Discount);
                    }
                }
                else
                {
                    // 20141125
                    TriggerStopper.addIdToMap('opportunityLineItemIdMap', currentOli.Id);
                }
            }
            
            if(existingPluginoliList.size() != 0)
            {
                system.debug(existingPluginOliList);
                
                //update existingPluginoliList;
            }
        }
        
        if(trigger.isUpdate)
        {
            triggeredOliIdSet.clear();
            opportunityIdSet.clear();
            
            for(opportunityLineItem currentoli : trigger.New)
            {
                currentOli.Quantity = 1;
                
                triggeredOliIdSet.add(currentOli.Id);
                
                opportunityIdSet.add(currentOli.OpportunityId);
                
                if(currentoli.Plugin__c == true)
                {
                    //key: opportunityid_LICENSETYPE_userlimit
                    
                    string pluginMapKey;
                    
                    pluginMapKey = ph.getPluginMapKey(currentOli);
                    
                    if(!numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, 1);
                    }
                    else
                    {
                        integer tempNumber;
                        
                        tempNumber = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                        
                        tempNumber++;
                        
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, tempNumber);
                    }
                }
            }
            
            system.debug('izéé 01: ' + numberOfNewPluginsExternalIdMap);
            system.debug('izéé 02: ' + numberOfExistingPluginsExternalIdMap);
            system.debug('izéé 03: ' + numberOfExistingPluginsForUpdateExternalIdMap);
            
            for(OpportunityLineItem currentOli : trigger.New)
            {
                if(currentOli.T_Calculate_Prices__c != trigger.oldMap.get(currentOli.Id).T_Calculate_Prices__c)
                {
	                decimal salesPrice;
	                integer existingPlugins;
	                integer newPlugins;
	                string pluginMapKey;
	                
	                pluginMapKey = ph.getPluginMapKey(currentOli);
	                
	                existingPlugins = 0;
	                
	                if(numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
	                {
	                    newPlugins = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
	                }
	                else
	                {
	                    newPlugins = 0;
	                }
	                
	                system.debug('currentOli: ' + currentoli);
	                system.debug('existingPlugins: ' + existingPlugins);
	                system.debug('newPlugins: ' + newPlugins);
	                
	                salesPrice = ph.calculateopportunityLineItemSalesPrice(currentoli, existingPlugins, newPlugins);
	                
	                currentoli.Original_Price__c = salesPrice;
	                
	                if(currentoli.Target_Price__c != NULL)
	                {
	                    if
	                    (
							currentoli.License_Type__c != null &&
	                        currentoli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
	                        currentoli.Opportunity_Renewal_Percentage__c != NULL
	                    )
	                    {
	                        decimal tempSalesPrice;
	                        tempSalesPrice = 
	                            salesPrice * 
	                            currentoli.Opportunity_Renewal_Percentage__c / 
	                            100;
	                        
	                        tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                        
	                        currentoli.UnitPrice = tempSalesPrice;
	                        currentoli.Discount = NULL;
	                        
	                        system.debug('unitPrice: ' + tempSalesPrice);
	                    }
	                    else
	                    {
	                    	currentoli.UnitPrice = currentoli.Target_Price__c;
	                    	currentoli.Discount = NULL;
	                    }
	                }
	                else
	                {
	                    currentoli.UnitPrice = salesPrice;
	                    
	                    if
	                    (
							currentoli.License_Type__c != null && 
	                        currentoli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
	                        currentoli.Opportunity_Renewal_Percentage__c != NULL
	                    )
	                    {
	                        decimal tempSalesPrice;
	                        tempSalesPrice = 
	                            salesPrice * 
	                            currentoli.Opportunity_Renewal_Percentage__c / 
	                            100;
	                        
	                        tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                        
	                        currentoli.UnitPrice = tempSalesPrice;
	                        
	                        system.debug('unitPrice: ' + tempSalesPrice);
	                    }
	                    else
	                    {
	                        decimal tempSalesPrice;
	                        tempSalesPrice = salesPrice;
	                        
	                        tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                        
	                        currentoli.UnitPrice = tempSalesPrice;
	                        
	                        system.debug('unitPrice: ' + tempSalesPrice);
	                    }
	                }
	                
	                if(currentoli.Discount != NULL)
	                {
	                    decimal tempTotalPrice;
	                    
	                    system.debug('tempTotalPrice: ' + tempTotalPrice);
	                    tempTotalPrice = salesPrice * ((100 - currentoli.Discount) / 100);
	                    system.debug('tempTotalPrice: ' + tempTotalPrice);
	                    
	                    tempTotalPrice = ph.customRounder(tempTotalPrice, NULL);
	                    system.debug('tempTotalPrice: ' + tempTotalPrice);
	                    
	                    system.debug('discount: ' + currentoli.Discount);
	                    currentoli.Discount = (1 - (tempTotalPrice / salesPrice)) * 100;
	                    system.debug('discount: ' + currentoli.Discount);
	                }
	                
	                // 20141124
	                if(currentOli.Target_price__c != NULL)
	                {
	                    currentOli.UnitPrice = currentOli.Target_Price__c;
	                }
	                else
	                {
	                    currentOli.UnitPrice = currentOli.Original_Price__c;
	                }
                }
            }
        }
    }
    
    if(trigger.isInsert && trigger.isAfter)
    {
		if(!Test.isRunningTest())
    	for(OpportunityLineItem currentOli : trigger.new)
    	{
    		TriggerStopper.addIdToMap('opportunityLineItemIdMap', currentOli.Id);
    	}
    }
    
    if(changedUserNumberOpportunityIdSet.size() != 0)
    {
        if(System.isBatch() || System.isFuture()) PriceHandlerFuture.updateOliNonFuture(changedUserNumberOpportunityIdSet);
		else PriceHandlerFuture.updateOli(changedUserNumberOpportunityIdSet);
    }
}