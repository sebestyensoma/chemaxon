/** 
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2015-12-03 (TD)
 * 
 */
trigger AttachmentLinkName on Agreement_document__c (before insert, before update) {
	
	for(Agreement_document__c ad : trigger.new){
		if(ad.Document_link__c!=null){
			try{
				String id=ad.Document_link__c.split('m/')[1];
				ad.Name=[select id,Name from Attachment where id=:id].Name;	
			}catch(exception ex){
				system.debug(ex.getMessage());	
			}
		}
	}

}