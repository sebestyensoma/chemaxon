// az opportunity-n levő Opportunity_product_names__c mezőbe az opportunityra felvett productok neveit kellene beírni abban a pillanatban, hogy a quote (QLI?) létrejön

trigger QLIProductToOpp on QuoteLineItem (after insert, after delete)  { 
    
    Set<Id> qId = new Set<Id>();
    //Set<Id> prodId = new Set<Id>();
    for(QuoteLineItem qli : Trigger.isInsert ? Trigger.new : Trigger.old){
        qId.add(qli.QuoteId);
        //prodId.add(qli.Product2Id);
    }

    //Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Name FROM Product2 WHERE Id IN :prodId]);

    Set<Id> prodId = new Set<Id>();
    Map<Id/*quote Id*/, List<QuoteLineItem>> qliByQuoteId = new Map<Id, List<QuoteLineItem>>();
    for(QuoteLineItem qli : [SELECT Id, Product2Id, QuoteId FROM QuoteLineItem WHERE QuoteId IN :qId]){
        List<QuoteLineItem> tmp = qliByQuoteId.get(qli.QuoteId);
        if(tmp == null){
            tmp = new List<QuoteLineItem>();
            qliByQuoteId.put(qli.QuoteId, tmp);
        }
        tmp.add(qli);
        prodId.add(qli.Product2Id);
    }
    Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Name FROM Product2 WHERE Id IN :prodId]);

    Set<Id> oppId = new Set<Id>();
    Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT Id, OpportunityId FROM Quote WHERE Id IN :qId]);
    for(Quote q : quoteMap.values()){
        oppId.add(q.OpportunityId);
    }


    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Opportunity_product_names__c FROM Opportunity WHERE Id IN :oppId]);


    Map<Id/*opp Id*/, List<Quote>> quoteByOppId = new Map<Id, List<Quote>>();
    for(Quote q : [SELECT Id, OpportunityId FROM Quote WHERE OpportunityId IN :oppId]){
        List<Quote> tmp = quoteByOppId.get(q.OpportunityId);
        if(tmp == null){
            tmp = new List<Quote>();
            quoteByOppId.put(q.OpportunityId, tmp);
        }
        tmp.add(q);
    }


    for(QuoteLineItem qli : Trigger.isInsert ? Trigger.new : Trigger.old){
        Opportunity opp = oppMap.get(quoteMap.get(qli.QuoteId).OpportunityId);      
        termeketHozzaad(opp);
    }


    update oppMap.values();


    private void termeketHozzaad(Opportunity opp){
        Set<String> tmp = new Set<String>();
        //if(opp.Opportunity_product_names__c != null) tmp = new Set<String>(opp.Opportunity_product_names__c.split(';'));


        for(Quote q : quoteByOppId.get(opp.Id)){
            for(QuoteLineItem qli : qliByQuoteId.get(q.Id)){
                tmp.add(prodMap.get(qli.Product2Id).Name);
            }
        }
        

        opp.Opportunity_product_names__c = String.join(new List<String>(tmp), ';');
    }
}


/*
trigger QLIProductToOpp on QuoteLineItem (after insert)  { 


    Set<Id> qId = new Set<Id>();
    Set<Id> prodId = new Set<Id>();
    for(QuoteLineItem qli : Trigger.new){
        qId.add(qli.QuoteId);
        prodId.add(qli.Product2Id);
    }


    Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Name FROM Product2 WHERE Id IN :prodId]);

    Set<Id> oppId = new Set<Id>();
    Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT Id, OpportunityId FROM Quote WHERE Id IN :qId]);
    for(Quote q : quoteMap.values()){
        oppId.add(q.OpportunityId);
    }


    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Opportunity_product_names__c FROM Opportunity WHERE Id IN :oppId]);
    for(QuoteLineItem qli : Trigger.new){
        Opportunity opp = oppMap.get(quoteMap.get(qli.QuoteId).OpportunityId);      
        termeketHozzaad(opp, qli);
    }


    update oppMap.values();



    private void termeketHozzaad(Opportunity opp, QuoteLineItem qli){
        Set<String> tmp;
        if(opp.Opportunity_product_names__c != null) tmp = new Set<String>(opp.Opportunity_product_names__c.split(';'));
        else tmp = new Set<String>();

        tmp.add(prodMap.get(qli.Product2Id).Name);
        //tmp.add(qli.Quote_display_line_discount__c);
        opp.Opportunity_product_names__c = String.join(new List<String>(tmp), ';');
    }

}
*/