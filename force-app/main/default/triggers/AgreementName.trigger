/** 
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2015-11-03 (TD)
 * 
 */
trigger AgreementName on Agreement__c (before insert, before update) {
	
	
		//Contract_with__c.Name első két szó + Contract_category_abbriviated__c (ha ki van töltve az agreement category) + Effective_date__c (optional) + X1_year_auto_renewal__c (ha true akkor AutoRenewal)
		//Ha Related_contract__c !=null akkor REF_TO után a related agreement adatai 
		if(trigger.isInsert){
			for(Agreement__c agr : trigger.new){
				if(agr.Contract_with__c!=null){
					
					Account acc = [select id,name from Account where id=:agr.Contract_with__c];
					String[] firstTwoArray = acc.Name.split(' ');
					
					agr.Name = (firstTwoArray.size()>1 ? firstTwoArray[0].capitalize()+firstTwoArray[1].capitalize() : firstTwoArray[0].capitalize()) + (agr.Contract_category_abbriviated__c!=null ? ('-'+agr.Contract_category_abbriviated__c) : '')+(agr.Effective_date_to_date__c!=null ? ('-'+String.valueOf(agr.Effective_date_to_date__c.day()).leftPad(2).replace(' ','0')+'/'+String.valueOf(agr.Effective_date_to_date__c.month()).leftPad(2).replace(' ','0')+'/'+String.valueOf(agr.Effective_date_to_date__c.year()).leftPad(2).replace(' ','0')) : '' );//+ (agr.X1_year_auto_renewal__c != null ? '-AutoRenewal+'+agr.X1_year_auto_renewal__c : '');
					
					if(agr.Related_contract__c!=null){
						
						Agreement__c relAgr = [select id,Contract_with__c,Contract_category_abbriviated__c,Effective_date_to_date__c,X1_year_auto_renewal__c from Agreement__c where id=:agr.Related_contract__c];
						//acc = [select id,Name from Account where id=:relAgr.Contract_with__c];
						//firstTwoArray = acc.Name.split(' ');
						
						agr.Name+='-'+'R2' /*+'-'+(firstTwoArray.size()>1 ? firstTwoArray[0]+' '+firstTwoArray[1] : firstTwoArray[0])*/ + (relAgr.Contract_category_abbriviated__c!=null ? ('-'+relAgr.Contract_category_abbriviated__c) : '') + (relAgr.Effective_date_to_date__c!=null ? ('-'+String.valueOf(relAgr.Effective_date_to_date__c.day()).leftPad(2).replace(' ','0')+'/'+String.valueOf(relAgr.Effective_date_to_date__c.month()).leftPad(2).replace(' ','0')+'/'+String.valueOf(relAgr.Effective_date_to_date__c.year()).leftPad(2).replace(' ','0')) : '' );// + (relAgr.X1_year_auto_renewal__c != null ? '-AutoRenewal+'+agr.X1_year_auto_renewal__c : '');
					}
				}
			}
		}
		//ha kitöltik az effective date mezőt, akkor újragenerálódik a name
		if(trigger.isUpdate){
			for(Agreement__c agr : trigger.new){
				if( trigger.OldMap.get(agr.id).Effective_date_to_date__c!=agr.Effective_date_to_date__c || 
					trigger.OldMap.get(agr.id).Contract_with__c!=agr.Contract_with__c || 
					trigger.OldMap.get(agr.id).Contract_category_abbriviated__c!=agr.Contract_category_abbriviated__c ||
					trigger.OldMap.get(agr.id).X1_year_auto_renewal__c!=agr.X1_year_auto_renewal__c ||
					trigger.OldMap.get(agr.id).Related_contract__c!=agr.Related_contract__c
					){
					if(agr.Contract_with__c!=null){
						
						Account acc = [select id,name from Account where id=:agr.Contract_with__c];
						String[] firstTwoArray = acc.Name.split(' ');
						
						agr.Name = (firstTwoArray.size()>1 ? firstTwoArray[0].capitalize()+firstTwoArray[1].capitalize() : firstTwoArray[0].capitalize()) + (agr.Contract_category_abbriviated__c!=null ? ('-'+agr.Contract_category_abbriviated__c) : '')+(agr.Effective_date_to_date__c!=null ? ('-'+String.valueOf(agr.Effective_date_to_date__c.day()).leftPad(2).replace(' ','0')+'/'+String.valueOf(agr.Effective_date_to_date__c.month()).leftPad(2).replace(' ','0')+'/'+String.valueOf(agr.Effective_date_to_date__c.year()).leftPad(2).replace(' ','0')) : '' );//+ (agr.X1_year_auto_renewal__c != null ? '-AutoRenewal+'+agr.X1_year_auto_renewal__c : '');
						
						if(agr.Related_contract__c!=null){
							
							Agreement__c relAgr = [select id,Contract_with__c,Contract_category_abbriviated__c,Effective_date_to_date__c,X1_year_auto_renewal__c from Agreement__c where id=:agr.Related_contract__c];
							//acc = [select id,Name from Account where id=:relAgr.Contract_with__c];
							//firstTwoArray = acc.Name.split(' ');
							
							agr.Name+='-'+'R2' /*+'-'+(firstTwoArray.size()>1 ? firstTwoArray[0]+' '+firstTwoArray[1] : firstTwoArray[0])*/ + (relAgr.Contract_category_abbriviated__c!=null ? ('-'+relAgr.Contract_category_abbriviated__c) : '') + (relAgr.Effective_date_to_date__c!=null ? ('-'+String.valueOf(relAgr.Effective_date_to_date__c.day()).leftPad(2).replace(' ','0')+'/'+String.valueOf(relAgr.Effective_date_to_date__c.month()).leftPad(2).replace(' ','0')+'/'+String.valueOf(relAgr.Effective_date_to_date__c.year()).leftPad(2).replace(' ','0')) : '' );// + (relAgr.X1_year_auto_renewal__c != null ? '-AutoRenewal+'+agr.X1_year_auto_renewal__c : '');
						}
					}
				}
			}
		}
	

}