/** 
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2015-11-16 (TD)
 * 
 */
trigger AttachmentNameTrigger on Attachment (after insert,before insert) {
	
	
	if(trigger.isBefore){
		//only one Attachment/Agreement document
		for(Attachment attch : trigger.new){
			if(attch.ParentId!=null && attch.ParentId.getSObjectType().getDescribe().getName()=='Agreement_document__c'){
				list<Attachment> attchList=[select id,Name FROM Attachment where ParentId=:attch.ParentId];
				if(attchList.size()>0) attch.ParentId.adderror('Only one Attachment/Agreement document');
			}				
		}
	}
	
	
	
	if(trigger.isAfter){
		//filling the related Agreement document's name with the attachment's name
		for(Attachment attch : trigger.new){
			if(attch.ParentId!=null && attch.ParentId.getSObjectType().getDescribe().getName()=='Agreement_document__c'){
				Agreement_document__c ad=[select id,Name FROM Agreement_document__c where id=:attch.ParentId];
				ad.Name=attch.Name;
				update ad;		
			}
		}
	}
	
	

}