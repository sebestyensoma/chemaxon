trigger QuoteAU on Quote (after update) 
{    
    Boolean targetDiscountNulledNow = false;
    Boolean isChanged = false;
    
    Map<Id,QuoteLineItem> myQLIs = new Map<Id,QuoteLineItem>([SELECT QuoteId,Discount,User_Limit__c FROM QuoteLineItem WHERE QuoteId IN :trigger.newMap.keySet()]);
    for(Quote loopQuote:trigger.new)
    {
        Quote oldQuote = trigger.oldMap.get(loopQuote.Id);
        Boolean targetPriceChanged = loopQuote.Target_Price__c != oldQuote.Target_Price__c;
        Boolean targetDiscountChanged = loopQuote.Target_Discount__c != oldQuote.Target_Discount__c;
        
        if(targetPriceChanged && targetDiscountChanged)
        {
            
        }
        else if(targetPriceChanged && loopQuote.Target_Price__c != null)
        {
            if(loopQuote.Target_Price__c>loopQuote.Subtotal)
            {
                
            }
            else
            {
                targetDiscountNulledNow = true;
                
                for(QuoteLineItem loopQLI : myQLIs.values())
                {
                    if(loopQLI.QuoteId == loopQuote.Id)
                    {
                        if(!isChanged)
                        {
                            isChanged=true;
                        }
                    }
                }
            }
        }
        else if(targetDiscountChanged && loopQuote.Target_Discount__c!=null && !targetDiscountNulledNow)
        {
            //Azért kell mert a Sync-es cuccok miatt, újra futna az egész folyamat
            QuoteTargetFieldNuller.TargetPriceNuller(loopQuote.Id);
            
            for(QuoteLineItem loopQLI : myQLIs.values())
            {
            	if(loopQLI.User_limit__c == NULL)
            	{
            		
            	}
            	
                if(loopQLI.QuoteId==loopQuote.Id)
                {
                    if(!isChanged && loopQLI.Discount != loopQuote.Target_Discount__c)
                    {
                            isChanged = true;
					}
                }
            }
        }
    }
    
    if(isChanged)
    {
        update myQLIs.values();
    }
}