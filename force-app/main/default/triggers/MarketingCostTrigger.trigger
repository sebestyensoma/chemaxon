trigger MarketingCostTrigger on Marketing_cost__c (after insert, after update, after delete)  { 


	if(Trigger.isAfter && Trigger.isInsert) MarketingCostTriggerHandler.AI(trigger.newMap);

	if(Trigger.isAfter && Trigger.isUpdate) MarketingCostTriggerHandler.AU(Trigger.oldMap, trigger.newMap);

	if(Trigger.isAfter && Trigger.isDelete) MarketingCostTriggerHandler.AD(trigger.oldMap);

}