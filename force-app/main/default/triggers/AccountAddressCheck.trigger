/** Trigger to verify Shipping and/or Billing Address may be set on Account
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2014-11-05 (SM)
 */
trigger AccountAddressCheck on Account (before insert, before update) {
	/*
	// Fetch Non-EU Countries List
	Set<String> nonEu = NonEUCountry__c.getAll().keySet();

	// 
	for (Account acc : trigger.new) if (acc.Tax_Number__c == null) {
		if (acc.BillingCountry != null && !nonEu.contains(acc.BillingCountry)) {
			acc.addError('If the billing country of the account is in the European Union, you should fill the VAT number field.');
		}
	}
	*/
}