/** Trigger to copy Shipping and/or Billing Address from Quote to Account at the user's request
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2014-08-07 (SM)
 * @updated		2015-10-19 (SM) added before insert/update branch
 */
trigger QuoteCopyAddresses on Quote (before insert, before update, after insert, after update) {
	if (trigger.isBefore) {
		// Filter out Quotes where copying is necessary
		Set<Id> oppIds = new Set<Id>();
		for (Quote newQuote : trigger.new) {
			oppIds.add(newQuote.OpportunityId);
		}
		
		Map<Id, Opportunity> opps = new Map<Id, Opportunity>([
			SELECT Id, Owner.FirstName, Owner.LastName, Owner.CompanyName,
				Owner.Street, Owner.City, Owner.State, Owner.PostalCode, Owner.Country,
				Owner.Phone, Owner.Fax, Owner.Email
			FROM Opportunity
			WHERE Id IN :oppIds
		]);
		
		for (Quote newQuote : trigger.new) {
			Opportunity opp = opps.get(newQuote.OpportunityId);
			if (opp == null) continue;
			
			if (opp.Owner.CompanyName == null) opp.Owner.CompanyName = '';
			if (opp.Owner.Street == null) opp.Owner.Street = '';
			if (opp.Owner.City == null) opp.Owner.City = '';
			if (opp.Owner.State == null) opp.Owner.State = '';
			if (opp.Owner.PostalCode == null) opp.Owner.PostalCode = '';
			if (opp.Owner.Country == null) opp.Owner.Country = '';
			if (opp.Owner.Phone == null) opp.Owner.Phone = '';
			if (opp.Owner.Fax == null) opp.Owner.Fax = '';
			if (opp.Owner.Email == null) opp.Owner.Email = '';
			
			//newQuote.Quote_display_user_address__c =
				//opp.Owner.FirstName + ' ' + opp.Owner.LastName + '\r\n' +
				//opp.Owner.CompanyName  + '\r\n' +
				//opp.Owner.Street + '\r\n' +
				//opp.Owner.City + '\r\n' +
				//opp.Owner.State + opp.Owner.PostalCode + '\r\n' +
				//opp.Owner.Country + '\r\n' +
				//opp.Owner.Phone + '\r\n' +
				//opp.Owner.Fax +
				//'\r\nhttp://www.chemaxon.com\r\n' +
				//opp.Owner.Email;
		}
	}

	// Changed on: 2020-04-29
	// Changed by: László Földi
	// Change description: disabled due to user complaint
	//if (trigger.isAfter) {
		//// Filter out Quotes where copying is necessary
		//Set<Id> quoteIds = new Set<Id>();
		//for (Quote newQuote : trigger.new) if (newQuote.Copy_Billing__c || newQuote.Copy_Shipping__c) {
			//quoteIds.add(newQuote.Id);
		//}
		
		//if (!quoteIds.isEmpty()) {
			//// Fetch Accounts via Opportunities
			//Quote[] quotes = [
				//SELECT Opportunity.AccountId,
					//Copy_Billing__c, BillingCountry, BillingCity, BillingStreet, BillingPostalCode, BillingState,
					//Copy_Shipping__c, ShippingCountry, ShippingCity, ShippingStreet, ShippingPostalCode, ShippingState
				//FROM Quote
				//WHERE Id IN :quoteIds
			//];
			//Set<Id> accountIds = new Set<Id>();
			//for (Quote newQuote : quotes) {
				//accountIds.add(newQuote.Opportunity.AccountId);
			//}
			//Map<Id, Account> accounts = new Map<Id, Account>([ SELECT Id FROM Account WHERE Id IN :accountIds ]);
			
			//// Perform copy where necessary
			//for (Quote newQuote : quotes) {
				//Account acc = accounts.get(newQuote.Opportunity.AccountId);
				
				//if (newQuote.Copy_Billing__c) {
					//acc.BillingCity = newQuote.BillingCity;
					//acc.BillingCountry = newQuote.BillingCountry;
					//acc.BillingStreet = newQuote.BillingStreet;
					//acc.BillingPostalCode = newQuote.BillingPostalCode;
					//acc.BillingState = newQuote.BillingState;
				//}
				//if (newQuote.Copy_Shipping__c) {
					//acc.ShippingCity = newQuote.ShippingCity;
					//acc.ShippingCountry = newQuote.ShippingCountry;
					//acc.ShippingStreet = newQuote.ShippingStreet;
					//acc.ShippingPostalCode = newQuote.ShippingPostalCode;
					//acc.ShippingState = newQuote.ShippingState;
				//}
			//}
		
			//// Update Accounts
			//update accounts.values();
		//}
	//}
}