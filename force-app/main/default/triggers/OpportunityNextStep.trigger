/**
 * @author      Attention CRM Consulting <neckermann@attentioncrm.hu>
 * @version     1.0
 * @status      wip
 * @since       2016-02-10 (TD)
 * @sask: Az időben legközelebbi nyitott task subject-jét tölti be az Opportunity nextStep mezejébe
 */
trigger OpportunityNextStep on Task (after update,after insert) {
	
	Opportunity opp;
	List<Task> task;
	
	try{
		opp=[select id from Opportunity where id=:trigger.new[0].whatId];
		
		task=[select id,ActivityDate,subject,status from Task where whatId=:opp.id and status!='Completed' AND ActivityDate>=:system.today() order by ActivityDate ASC];
		
		if(task!=null && !task.isEmpty()){	
			opp.NextStep=task[0].subject;
			
			update opp;
		}
	}catch(exception ex){
		system.debug(ex.getmessage());		
	}

}