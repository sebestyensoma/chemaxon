trigger Lead on Lead (before insert, after insert, before update, after update)
{
    /*
    Web-to-lead duplikáció mentesítés: többször kattintanak az aktiváló linkre
    */
    
    if(Trigger.isInsert && Trigger.isBefore)
    {
        Set<string> leadForumIdStringSet = new Set<string>();
        List<Lead> leadList = new List<Lead>();
        Map<string, Lead> leadForumIdMap = new Map<string, Lead>();
        
        for(Lead lead : trigger.new)
        {
        	
        	if(lead.Product_Area_Interest__c==null) lead.Product_Area_Interest__c='initial';
            system.debug('kutya: ' + lead);
            
            if(lead.LeadSource == 'Website Registration')
            {
                system.debug('cica: ' + lead.leadSource);
                
                leadForumIdStringSet.add(lead.Forum_Id__c);
            }
            
            if
            (
                lead.Forum_ID__c != '' &&
                lead.Forum_ID__c != NULL &&
                (lead.Account_Alias__c == '' || lead.Account_Alias__c == NULL)
            )
            {
                lead.Account_Alias__c = lead.Forum_ID__c;
            }
            
            if
            (
                lead.Account_Alias__c != '' &&
                lead.Account_Alias__c != NULL &&
                (lead.Forum_ID__c == '' || lead.Forum_ID__c == NULL)
            )
            {
                lead.Forum_ID__c = lead.Account_Alias__c;
            }
        }
        
        if(leadForumIdStringSet.size() != 0)
        {
            leadList =
                [
                    SELECT
                        Id,
                        Forum_ID__c,
                        LeadSource
                    FROM
                        Lead
                    WHERE
                        Forum_ID__c IN :leadForumIdStringSet
                        AND
                        LeadSource = 'Website Registration'
                        AND
                        IsConverted = FALSE
                ];
                
            system.debug('kutya' + leadList.size());
            
            for(Lead lead : leadList)
            {
                leadForumIdMap.put(lead.Forum_ID__c, lead); 
            }
        }
        
        for(Lead lead : trigger.new)
        {
            if(leadForumIdMap.containsKey(lead.Forum_ID__c))
            {
                lead.addError('Duplicate website registration');
            }   
        }
    }
    
    
    /*
    Lead trigger a konvertáláskor accountra kell hogy átmenjen a license req. 
    ÉS a contact-ra is.
    Ha már ki van töltve az account a license req-en akkor nem kell hozzányúlni
    (ha nem változik)
    */
    
    if(Trigger.isUpdate && Trigger.isAfter)
    {
        Map<id, Lead> leadMap = new Map<Id, Lead>();
        Map<id, Lead> leadMapDemo = new Map<Id, Lead>();
        Set<id> leadIdSet = new Set<id>();
        Set<id> leadIdSetDemo = new Set<id>();
        List<License__c> licenseList = new List<License__c>();
        List<Demo_Request__c> demoRequestList = new List<Demo_Request__c>();
        
        for(Lead lead : Trigger.New)
        {
        	if (lead.LeadSource == null) continue;
        	
            if (lead.LeadSource.equalsIgnoreCase('Website Evaluation License Download') && lead.isConverted == TRUE) // Website Registration
            {
                leadMap.put(lead.id, lead);
                leadIdSet.add(lead.Id);
            }
            
            if (lead.LeadSource.equalsIgnoreCase('Website Demo Request') && lead.isConverted == TRUE)
            {
                leadMapDemo.put(lead.id, lead);
                leadIdSetDemo.add(lead.Id);
            }
        }
        
        if(leadIdSet.size() != 0)
        {
            licenseList =
                [
                    SELECT
                        Account__c,
                        Contact_Name__c,
                        Lead__c,
                        License_ID__c
                    FROM
                        License__c
                    WHERE
                        Lead__c IN :leadIdSet
                ];
        }
        
        if(licenseList.size() != 0)
        {
            for(integer i=0; i<licenseList.size(); i++)
            {
                licenseList[i].Account__c = leadMap.get(licenseList[i].Lead__c).ConvertedAccountId;
                licenseList[i].Contact_Name__c = leadMap.get(licenseList[i].Lead__c).ConvertedContactId;
            }
            
            system.debug('##### ' + licenseList);
            
            update licenseList;
        }
        
        
        if(leadIdSetDemo.size() != 0)
        {
            demoRequestList =
                [
                    SELECT Lead__c, Contact__c
                    FROM Demo_Request__c
                    WHERE Lead__c IN :leadIdSetDemo
                ];
                
            for(Demo_Request__c currentDR : demoRequestList)
            {
                currentDR.Contact__c = leadMapDemo.get(currentDR.Lead__c).ConvertedContactId;
            }
            
            update demoRequestList;
        }
    }
    
    /*
    if(trigger.isBefore)
    {
        for(Lead lead : trigger.new)
        {
            lead.CampaignId = '701M0000000Ad87';
        }
    }
    */
    
    
    /*
    website demo request
    */
    if(trigger.isInsert && trigger.isAfter)
    {
        Set<id> leadIdSet = new Set<id>();
        
        for(Lead currentL : trigger.new)
        {
            if(currentL.LeadSource != NULL && currentL.LeadSource.equalsIgnoreCase('Website Demo Request'))
            {
                leadIdSet.add(currentL.Id);
            }
        }
        
        if(leadIdSet.size() != 0)
        {
            system.debug('##### leadIdSet: ' + leadIdSet);
            LeadHandlerFuture.createWebsiteDemoRequestRecords(leadIdSet);
        }
    }
}