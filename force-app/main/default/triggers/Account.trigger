trigger Account on Account (before delete) 
{
    if(trigger.isBefore && trigger.isDelete)
    {
        Set<string> megengedettUserNevek = new Set<string>();
        
        megengedettUserNevek.add('attentioncrm@chemaxon.com');
        megengedettUserNevek.add('attentioncrm@chemaxon.com.test');
        megengedettUserNevek.add('mmedzihradszky@chemaxon.com');
        megengedettUserNevek.add('mmedzihradszky@chemaxon.com.test');
        megengedettUserNevek.add('alukacs@chemaxon.com');
        megengedettUserNevek.add('alukacs@chemaxon.com.test');
        megengedettUserNevek.add('jkerekgyarto@chemaxon.com');
        megengedettUserNevek.add('jkerekgyarto@chemaxon.com.test');
        
        for(Account acc : Trigger.old)
        {
            if(!megengedettUserNevek.contains(UserInfo.getUserName()))
            {
                acc.addError('You are not allowed to delete account!');
            }
        }
    }
}