trigger EventBIU on Event (after insert, after update) {	
	if(trigger.new.size()==1 && (trigger.new[0].Type=='Meeting' || trigger.new[0].Subject=='Meeting') && trigger.new[0].WhatId!=null && trigger.new[0].WhatId.getSObjectType().getDescribe().getName()=='Account'){
		List<Account> myAccounts = [SELECT Last_Visit_Date__c FROM Account WHERE Id = :trigger.new[0].WhatId];
		if(myAccounts!=null && myAccounts.size()==1){
			Datetime endDateTime = trigger.new[0].EndDateTime;
			Date endDate = Date.newInstance(endDateTime.year(),endDateTime.month(),endDateTime.day());
			if(myAccounts[0].Last_Visit_Date__c==null || myAccounts[0].Last_Visit_Date__c < endDate){
				myAccounts[0].Last_Visit_Date__c = endDate;
				update myAccounts;
			}
		}		
	}
}