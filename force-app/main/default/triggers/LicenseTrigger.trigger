trigger LicenseTrigger on License__c (before insert, after insert) 
{
	/*
		Support_Validity__c=Support_Validity__c
		License_Type__c=License_Type__c
		Products_Names__c=Products_Names__c
		Validity__c=Validity__c
		Account_Email__c=Account_Email__c
		Name=Name
		Account_ID_text__c=Account_ID_text__c
		Account_Name__c=Account_Name__c
		Contact_Name_Eval__c=Contact_Last_Name_Eval__c
	*/
	
	/*
		csak eval licenszt töltünk
		
		-	ha van nyitott lead az account_email__c címhez, akkor oda kapcsoljuk
		-	ha nincs nyitott lead, akkor létrehozunk és hozzákapcsoljuk a licenszet
		-	ha kapunk account_id_text__c-et, akkor az ügyfélhez is kapcsoljuk a licenszet
	*/
	if(trigger.isInsert && trigger.isBefore)
	{
		List<Lead> leadsToInsertList = new List<Lead>();
		
		Set<string> accountIdSet = new Set<string>();
		Set<string> accountEmailSet = new Set<string>(); // ezek alapján keresünk nyitott leadet
		
		Set<string> existingAccountIdSet = new Set<string>();
		Map<string, Lead> existingLeadEmailStringMap = new Map<string, Lead>();
		Map<string, Lead> newLeadsEmailMap = new Map<string, Lead>();
		
		LicenseTriggerHandler lth = new LicenseTriggerHandler();
		
		for(License__c currentL : trigger.new)
		{
			if(currentL.License_Type__c != NULL && currentL.License_Type__c.equalsIgnoreCase('evaluation'))
			{
				if(currentL.Account_ID_text__c != NULL && !currentL.Account_ID_text__c.equalsIgnoreCase('NULL'))
				{
					accountIdSet.add(currentL.Account_ID_text__c);
				}
				
				if(currentL.Account_Email__c != NULL && !currentL.Account_Email__c.equalsIgnoreCase('NULL'))
				{
					currentL.Account_Email__c = currentL.Account_Email__c.toLowerCase();
					
					accountEmailSet.add(currentL.Account_Email__c);
				}
			}
		}
		
		if(accountIdSet.size() != 0)
		{
			existingAccountIdSet = lth.getExistingAccountIdSet(accountIdSet);
		}
		
		if(accountEmailSet.size() != 0)
		{
			existingLeadEmailStringMap = lth.getExistingLeadEmailStringMap(accountEmailSet);
		}
		
		for(License__c currentL : trigger.new)
		{
			if(currentL.License_Type__c != NULL && currentL.License_Type__c.equalsIgnoreCase('evaluation'))
			{
				if(currentL.Account_ID_text__c != NULL && existingAccountIdSet.contains(currentL.Account_ID_text__c))
				{
					currentL.Account__c = currentL.Account_ID_text__c;
				}
				
				if(currentL.Account_Email__c != NULL && existingLeadEmailStringMap.containsKey(currentL.Account_Email__c))
				{
					currentL.Lead__c = existingLeadEmailStringMap.get(currentL.Account_Email__c).Id;
				}
				
				if(currentL.Lead__c == NULL)
				{
					leadsToInsertList.add(lth.createNewLead(currentL));
				}
			}
		}
		
		if(leadsToInsertList.size() != 0)
		{
			newLeadsEmailMap = lth.getNewLeadsEmailMap(leadsToInsertList);
		}
		
		// második kör, a most beszúrt leadeket kapcsoljuk a licenszhez
		for(License__c currentL : trigger.new)
		{
			if(currentL.License_Type__c != NULL && currentL.License_Type__c.equalsIgnoreCase('evaluation'))
			{
				if(currentL.Lead__c == NULL && newLeadsEmailMap.containsKey(currentL.Account_Email__c))
				{
					currentL.Lead__c = newLeadsEmailMap.get(currentL.Account_Email__c).Id;
				}
			}
		}
	}
	
	if(trigger.isInsert && trigger.isAfter)
	{
		Set<id> idsToDeleteSet = new Set<id>();
		
		LicenseTriggerHandler lth = new LicenseTriggerHandler();
		
		for(License__c currentL : trigger.new)
		{
			if(currentL.License_Type__c == NULL || !currentL.License_Type__c.equalsIgnoreCase('evaluation'))
			{
				idsToDeleteSet.add(currentL.Id);
			}
		}
		
		if(idsToDeleteSet.size() != 0)
		{
			lth.deleteLicenses(idsToDeleteSet);
		}
	}
}