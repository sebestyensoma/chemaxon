trigger PackageAU on Package__c (after update) {
	if(trigger.new.size()==1 && !(trigger.old[0].Quote__c==null && trigger.new[0].Quote__c!=null) && trigger.new[0].Quote__c!=null && trigger.new[0].Opportunity__c==null && trigger.new[0].Package_Type__c!='All plugins'){
		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard=true)]);
		
				
		Map<Id,Product2> mapIdToProduct = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		Map<Id,QuoteLineItem> mapUpdateQLIs = new Map<Id,QuoteLineItem>([SELECT User_Limit__c,QuoteId,Quantity,PricebookEntryId,Package__c,Discount,UnitPrice FROM QuoteLineItem WHERE Package__c=:trigger.new[0].Id ]);				
		Map<Id,PricebookEntry> mapIdToPBE = new Map<Id,PricebookEntry>([SELECT Id,UnitPrice,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE Package__c=:trigger.new[0].Id)]);
		
		system.debug('mapIdToProduct:'+mapIdToProduct);
		system.debug('mapUpdateQLIs:'+mapUpdateQLIs);
		system.debug('mapIdToPBE:'+mapIdToPBE);
		
		Double ratioSumarized = 0.0;

		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:mapIdToProduct.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
			for(QuoteLineItem loopQLI:mapUpdateQLIs.values()){
				PricebookEntry loopPBE = mapIdToPBE.get(loopQLI.PricebookEntryId);
				system.debug('loopPBE:'+loopPBE);
				Product2 relatedProduct = mapIdToProduct.get(loopPBE.Product2Id);
				system.debug('relatedProduct:'+relatedProduct);
				loopQLI.User_Limit__c = trigger.new[0].User_Limit__c;
				loopQLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
			}
		}
		update mapUpdateQLIs.values();
	}
	
	
	if(trigger.new.size()==1 && !(trigger.old[0].Quote__c==null && trigger.new[0].Quote__c!=null) && trigger.new[0].Opportunity__c!=null && trigger.new[0].Package_Type__c!='All plugins'){
		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id IN (SELECT Id FROM Pricebook2 WHERE IsStandard=true)]);
		
				
		Map<Id,Product2> mapIdToProduct = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		Map<Id,OpportunityLineItem> mapUpdateOLIs = new Map<Id,OpportunityLineItem>([SELECT User_Limit__c,OpportunityId,Quantity,PricebookEntryId,Package__c,Discount,UnitPrice FROM OpportunityLineItem WHERE Package__c=:trigger.new[0].Id ]);				
		Map<Id,PricebookEntry> mapIdToPBE = new Map<Id,PricebookEntry>([SELECT Id,UnitPrice,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM OpportunityLineItem WHERE Package__c=:trigger.new[0].Id)]);
		
		system.debug('mapIdToProduct:'+mapIdToProduct);
		system.debug('mapUpdateOLIs:'+mapUpdateOLIs);
		system.debug('mapIdToPBE:'+mapIdToPBE);
		
		Double ratioSumarized = 0.0;

		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:mapIdToProduct.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
			for(OpportunityLineItem loopOLI:mapUpdateOLIs.values()){
				PricebookEntry loopPBE = mapIdToPBE.get(loopOLI.PricebookEntryId);
				system.debug('loopPBE:'+loopPBE);
				Product2 relatedProduct = mapIdToProduct.get(loopPBE.Product2Id);
				system.debug('relatedProduct:'+relatedProduct);
				loopOLI.User_Limit__c = trigger.new[0].User_Limit__c;
				loopOLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
			}
		}
		update mapUpdateOLIs.values();
	}
}