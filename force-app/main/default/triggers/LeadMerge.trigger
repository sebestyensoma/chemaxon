/**
 * @author      Attention CRM Consulting <neckermann@attentioncrm.hu>
 * @version     2
 * @since       2016-01-19 (TD)
 * @update		2016-06-06 (SM)
 * @update		2016-09-28 (?K)
 */
 
trigger LeadMerge on Lead (after insert) {
	
	Map<String,Lead> masterLeadMap = new Map<String,Lead>();
	Map<id,Lead> masterLeadIdMap = new Map<id,Lead>();
	Map<String,List<Lead>> masterDuplicateLeadListMap = new Map<String,List<Lead>>();
	Map<id,id> duplicateidMasterIdMap = new Map <id,id>();
	List<String> leadFields = new List<String>(Lead.SObjectType.getDescribe().fields.getMap().keySet());
	List<Demo_Request__c> demoRequestToUpdateList = new List<Demo_Request__c>();
	List<License__c> licensesToUpdateList = new List<License__c>();
	List<Competitor__c> competitorsToUpdateList = new List<Competitor__c>();
	List<Task> tasksToUpdateList = new List<Task>();
	List<Event> eventsToUpdateList = new List<event>();
	Set<id> duplicateIdSet = new Set<id>();
	Set<String> leadEmailset = new Set<String>();
	List<Lead> leadsToDel = new List<Lead>();
	List<Lead> mastersToUpdate = new List<Lead>(); 
	
	if (trigger.new.size() == 1 && trigger.new[0].T_Dummy__c!=true) {
		
		AggregateResult[] masterLeadsAggr = [select count(id),email EMAIL from Lead where isconverted=false and email!=null and email=:trigger.new[0].Email group by Email  HAVING count (id)>1];
		system.debug('TDBG'+trigger.new[0].Email);
		
		for(AggregateResult res : masterLeadsAggr){
		    leadEmailset.add((string)res.get('EMAIL'));
		}
		
		
		String leadQuery = ''
					        + ' SELECT ' + String.join(leadFields, ',')
					        + ' FROM Lead'
							+ ' WHERE email in : leadEmailset and email!=null and isconverted=false order by createddate asc';
					
		List<Lead> leadList=Database.query(leadQuery);
		
			
		
		/*
		List<Lead> leadLIst =[select id,lastmodifiedby.Name,email,lastmodifieddate,createddate,status,isconverted,owner.Name from Lead where email in : leadEmailset and email!=null and isconverted=false order by createddate asc];
		*/
		
		for(Lead lead : leadList){
			
		    if(!masterDuplicateLeadListMap.containsKey(lead.Email.toUppercase())){
		        masterLeadMap.put(lead.Email.toUppercase(),lead);
		        lead.T_Master__c=true;
		        mastersToUpdate.add(lead);
		        
		        masterDuplicateLeadListMap.put(lead.Email.toUppercase(),new list<lead>());
		    }else{
		        masterDuplicateLeadListMap.get(lead.Email.toUppercase()).add(lead);
		        lead.To_be_deleted__c=true;
		        lead.T_MasterId__c=masterLeadMap.get(lead.Email.toUppercase()).id;
		        leadsToDel.add(lead);
		        duplicateidMasterIdMap.put(lead.id,masterLeadMap.get(lead.Email.toUppercase()).id);        
		    }
		    
		}
		
		duplicateIdSet = duplicateidMasterIdMap.keySet();
		
		for(Demo_request__c dr : [select id,Lead__c from Demo_Request__c where Lead__c IN:duplicateidMasterIdMap.keySet()]){
		
		    if(duplicateidMasterIdMap.containsKey(dr.Lead__c)){
		        dr.Lead__c=duplicateidMasterIdMap.get(dr.Lead__c);
		        demoRequestToUpdateList.add(dr);
		    }   
		
		}
		
		
		
		for(License__c lc : [select id,Lead__c from License__c where Lead__c IN:duplicateidMasterIdMap.keySet()]){
		
		    if(duplicateidMasterIdMap.containsKey(lc.Lead__c)){
		        lc.Lead__c=duplicateidMasterIdMap.get(lc.Lead__c);
		        licensesToUpdateList.add(lc);
		    }   
		
		}
		
		for(Competitor__c comp : [select id,Lead__c from Competitor__c where Lead__c IN:duplicateidMasterIdMap.keySet()]){

		    if(duplicateidMasterIdMap.containsKey(comp.Lead__c)){
		        comp.Lead__c=duplicateidMasterIdMap.get(comp.Lead__c);
		        competitorsToUpdateList.add(comp);
		    }   
		
		}
		
		system.debug('DBG'+duplicateIdSet);
		
		try{
		    		
		    List<String> taskFields = new List<String>(Task.SObjectType.getDescribe().fields.getMap().keySet());
		    
		    String taskQuery = ''
		            + ' SELECT ' + String.join(taskFields, ',')
		            + ' FROM Task'
		            + ' WHERE WhoId IN:duplicateIdSet';    
		    
		    system.debug('DBG'+taskQuery);
		    
		    for(Task task : Database.query(taskQuery)){
		        if(duplicateidMasterIdMap.containsKey(task.WhoId)){
		            task.WhoId=duplicateidMasterIdMap.get(task.WhoId);
		            tasksToUpdateList.add(task);
		        }
		    }             
		    
		}catch(exception ex){
		    system.debug(ex.getMessage());    		
		}
		
		try{
		    		
		    List<String> eventFields = new List<String>(Event.SObjectType.getDescribe().fields.getMap().keySet());
		    
		    String eventQuery = ''
		            + ' SELECT ' + String.join(eventFields, ',')
		            + ' FROM Event'
		            + ' WHERE WhoId IN:duplicateIdSet';
		    
		    for(Event event : Database.query(eventQuery)){
		        if(masterDuplicateLeadListMap.containsKey(event.WhoId)){
		            event.WhoId=duplicateidMasterIdMap.get(event.WhoId);
		            eventsToUpdateList.add(event);
		        }
		    }             
		    
		}catch(exception ex){
		    system.debug(ex.getMessage());    		
		}
		    	
		    	
		
		
		
		for(String s : masterDuplicateLeadListMap.keySet()){
		     
		    for(Lead lead : masterDuplicateLeadListMap.get(s)){
			   	Lead masterLead = masterLeadMap.get(s);
		    
		        //if(masterLead.ownerid!=lead.ownerid && lead.ownerid!=null) masterLead.ownerid=lead.ownerid;
			    if(masterLead.status == 'New' && masterLead.status!=lead.status && lead.status!=null) masterLead.status=lead.status;
		        if(masterLead.leadSource!=lead.leadSource && lead.leadSource!=null) masterLead.leadSource=lead.leadSource;
		        if(masterLead.Industry!=lead.Industry && lead.Industry!=null) masterLead.Industry=lead.Industry;
		        if(masterLead.Internal_Referral__c!=lead.Internal_Referral__c && lead.Internal_Referral__c!=null) masterLead.Internal_Referral__c=lead.Internal_Referral__c;
		        if(masterLead.Partner_Referral__c!=lead.Partner_Referral__c && lead.Partner_Referral__c!=null) masterLead.Partner_Referral__c=lead.Partner_Referral__c;
		        if(masterLead.Client_Referral__c!=lead.Client_Referral__c && lead.Client_Referral__c!=null) masterLead.Client_Referral__c=lead.Client_Referral__c;
		        if(masterLead.Size_of_the_company__c!=lead.Size_of_the_company__c && lead.Size_of_the_company__c!=null) masterLead.Size_of_the_company__c=lead.Size_of_the_company__c;        
		        if(masterLead.First_contact_time__c!=lead.First_contact_time__c && lead.First_contact_time__c!=null) masterLead.First_contact_time__c=lead.First_contact_time__c;        
		        if(masterLead.Role__c!=lead.Role__c && lead.Role__c!=null) masterLead.Role__c=lead.Role__c;
		        if(masterLead.Title_Custom__c!=lead.Title_Custom__c && lead.Title_Custom__c!=null) masterLead.Title_Custom__c=lead.Title_Custom__c;
		        if(masterLead.Company!=lead.Company && lead.Company!=null) masterLead.Company=lead.Company;
		        if(masterLead.Phone!=lead.Phone && lead.Phone!=null) masterLead.Phone=lead.Phone;
		        if(masterLead.MobilePhone!=lead.MobilePhone && lead.MobilePhone!=null) masterLead.MobilePhone=lead.MobilePhone;
		        if(masterLead.Email!=lead.Email && lead.Email!=null) masterLead.Email=lead.Email;
		        if(masterLead.User_Group_Invitee__c!=lead.User_Group_Invitee__c && lead.User_Group_Invitee__c!=null) masterLead.User_Group_Invitee__c=lead.User_Group_Invitee__c;        
		        if(masterLead.Academic__c!=lead.Academic__c && lead.Academic__c!=null) masterLead.Academic__c=lead.Academic__c;
		        if(masterLead.Account__c!=lead.Account__c && lead.Account__c!=null) masterLead.Account__c=lead.Account__c;
		        if(masterLead.Description!=lead.Description && lead.Description!=null) masterLead.Description=lead.Description;
		        
		        if(masterLead.Street!=lead.Street && lead.Street!=null) masterLead.Street=lead.Street;
		        if(masterLead.State!=lead.State && lead.State!=null) masterLead.State=lead.State;
		        if(masterLead.PostalCode!=lead.PostalCode && lead.PostalCode!=null) masterLead.PostalCode=lead.PostalCode;
		        if(masterLead.Country!=lead.Country && lead.Country!=null) masterLead.Country=lead.Country;
		        if(masterLead.City!=lead.City && lead.City!=null) masterLead.City=lead.City;
		        
		        
		        
		        if (masterLead.Interest_area__c == null) masterLead.Interest_area__c = '';
		        if (masterLead.Download_packs__c == null) masterLead.Download_packs__c = '';
		        if (masterLead.Product_Area_Interest__c == null) masterLead.Product_Area_Interest__c = '';
		        if (masterLead.Competitor__c == null) masterLead.Competitor__c = '';
		        
		        if(masterLead.Interest_area__c!=lead.Interest_area__c && lead.Interest_area__c!=null){
		            masterLead.Interest_area__c = masterLead.Interest_area__c + ';' + lead.Interest_area__c;
		            Set<String> interestsArea = new Set<String>(masterLead.Interest_area__c.split(';'));
		            masterLead.Interest_area__c = String.join(new List<String>(interestsArea), ';');
		        }
		        
		        if(masterLead.Download_packs__c!=lead.Download_packs__c && lead.Download_packs__c!=null){
		            masterLead.Download_packs__c = masterLead.Download_packs__c + ';' + lead.Download_packs__c;
		            Set<String> packs = new Set<String>(masterLead.Download_packs__c.split(';'));
		            masterLead.Download_packs__c = String.join(new List<String>(packs), ';');
		        }
		        
		        if(masterLead.Product_Area_Interest__c!=lead.Product_Area_Interest__c && lead.Product_Area_Interest__c!=null){
		            masterLead.Product_Area_Interest__c = masterLead.Product_Area_Interest__c + ';' + lead.Product_Area_Interest__c;
		            Set<String> interests = new Set<String>(masterLead.Product_Area_Interest__c.split(';'));
		            masterLead.Product_Area_Interest__c = String.join(new List<String>(interests), ';');
		        }
		
		        if(masterLead.Competitor__c!=lead.Competitor__c && lead.Competitor__c!=null){
		            masterLead.Competitor__c = masterLead.Competitor__c + ';' + lead.Competitor__c;
		            Set<String> competitors = new Set<String>(masterLead.Competitor__c.split(';'));
		            masterLead.Competitor__c = String.join(new List<String>(competitors), ';');
		        } 
		    
		    }
		}
		
		
		update mastersToUpdate;
		delete leadsToDel;
		
		if(!demoRequestToUpdateList.isEmpty()) update demoRequestToUpdateList;
		if(!licensesToUpdateList.isEmpty()) update licensesToUpdateList;
		if(!competitorsToUpdateList.isEmpty()) update competitorsToUpdateList;
		if(!tasksToUpdateList.isEmpty()) update tasksToUpdateList;
		if(!eventsToUpdateList.isEmpty()) update eventsToUpdateList;
		
		
		
	}
	
	/*	
	Set<string> leadForumIdStringSet = new Set<string>();
    List<Lead> leadList = new List<Lead>();
    Map<string, Lead> leadForumIdMap = new Map<string, Lead>();
    Lead masterLead;
    
    if (trigger.new.size() == 1) {
	    for (Lead lead : trigger.new) {
	        if ((lead.LeadSource == 'Website Product Download' || lead.LeadSource == 'Website Download') && (lead.Forum_Id__c != null || lead.Email != null) && lead.Download_packs__c != null) {
	        	try {
		        	masterLead = [
		        		select id, Download_packs__c
		        		from Lead
		        		where 
							((Forum_Id__c = :lead.Forum_Id__c AND Forum_Id__c != null) OR (Email = :lead.Email AND Email != null))
		        			AND id != :lead.id
		        			AND isConverted = false
		        			AND LeadSource = 'Website Registration'
							AND Status != 'Qualified'
		        		limit 1
		        	];
		        	
		        	if (masterLead.Download_packs__c == null) masterLead.Download_packs__c = '';
		        	masterLead.Download_packs__c = masterLead.Download_packs__c + ';' + lead.Download_packs__c;
		        	Set<String> packs = new Set<String>(masterLead.Download_packs__c.split(';'));
		        	
		        	masterlead.Download_packs__c = String.join(new List<String>(packs), ';');
		        	masterLead.Last_Download_Date__c = Date.today();
	        	} catch(Exception ex) {
	        		system.debug('{SM} ' + ex.getMessage() + ' @' + ex.getLineNumber());
	        		system.debug('Master inserted');
	        	}
	        	
	        	if (masterLead != null) {
		        	update masterLead;
		        	
		        	Lead leadTodel = [ select id from Lead where id = :lead.Id ];
		        	delete leadTodel;
	        	}
	        }
	    }
    }
    */

}