trigger PackageAD on Package__c (before delete) {
	if(trigger.old!=null && trigger.old.size()==1){
		List<QuoteLineItem> listUpdateQLIs = [SELECT Id FROM QuoteLineItem WHERE Package__c=:trigger.old[0].Id ];
		delete listUpdateQLIs;
		List<OpportunityLineItem> listUpdateOLIs = [SELECT Id FROM OpportunityLineItem WHERE Package__c=:trigger.old[0].Id ];
		delete listUpdateOLIs;
	}
}