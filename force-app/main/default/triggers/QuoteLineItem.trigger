trigger QuoteLineItem on QuoteLineItem (before insert, before update, after insert, after delete, after undelete)
{

	System.debug(trigger.isBefore);
	System.debug(trigger.isUpdate);
    Set<id> changedUserNumberQuoteIdSet = new Set<id>();

    if(!System.isFuture())
    {
        /*
        if(trigger.isInsert || trigger.isUpdate || trigger.isUndelete)
        {
            for(QuoteLineItem currentQli : trigger.New)
            {
                changedUserNumberQuoteIdSet.add(currentQli.QuoteId);
            }
        }
        
        if(trigger.isDelete)
        {
            for(QuoteLineItem currentQli : trigger.Old)
            {
                changedUserNumberQuoteIdSet.add(currentQli.QuoteId);
            }
        }
        */
        
        if(trigger.isInsert || trigger.isUndelete)
        {
            for(QuoteLineItem currentQli : trigger.New)
            {
                changedUserNumberQuoteIdSet.add(currentQli.QuoteId);
            }
        }
        
        if(trigger.isDelete)
        {
            for(QuoteLineItem currentQli : trigger.Old)
            {
                changedUserNumberQuoteIdSet.add(currentQli.QuoteId);
            }
        }
    }
    
    ////////////////////
    /// STOP TRIGGER ///
    ////////////////////
    if(!trigger.isDelete ) // && !Test.isRunningTest()
    {
		//boooom();
        for(QuoteLineItem currQli : trigger.new)
        {
            /*
            if(TriggerStopper.quoteLineItemIdSet.contains(currQli.Id))
            {
                return;
            }
            */
            
            if(TriggerStopper.quoteLineItemIdMap.containsKey(currQli.Id))
            {
                system.debug('triggerStopper containsKey');
                return;
            }
        }
    }
    
    system.debug('debuuug: ' + system.now() + TriggerStopper.quoteLineItemIdSet);
    
    
    
    if(trigger.isBefore)
    {
        List<quoteLineItem> existingPluginqliList = new List<quoteLineItem>();
        List<quoteLineItem> existingPluginqliForUpdateList = new List<quoteLineItem>();
        Map<string, integer> numberOfExistingPluginsExternalIdMap = new Map<string, integer>();
        Map<string, integer> numberOfExistingPluginsForUpdateExternalIdMap = new Map<string, integer>();
        Map<string, integer> numberOfNewPluginsExternalIdMap = new Map<string, integer>();
        Set<id> quoteIdSet = new Set<id>();
        Set<id> triggeredQliIdSet = new Set<id>();
        
        PriceHandler ph = new PriceHandler();
        
        if(trigger.isInsert)
        {
			//ez le sem fut
            if(quoteIdSet.size() != 0)
            {
                numberOfExistingPluginsExternalIdMap = 
                    ph.calculateNumberOfExistingqliPlugins(quoteIdSet, triggeredQliIdSet);
                    
                system.debug('kutya 01:' + numberOfExistingPluginsExternalIdMap);
                    
                existingPluginqliList = ph.getExistingPluginqliList(quoteIdSet, triggeredQliIdSet);
                
                system.debug('kutya 02: ' + existingPluginqliList.size());
            }
            
            for(quoteLineItem currentqli : trigger.New)
            {
                quoteIdSet.add(currentqli.quoteId);
                
                system.debug('zsiráf: ' + triggeredQliIdSet); //empty set     
                
                if(currentqli.Plugin__c == true)
                {
                    //key: quoteid_LICENSETYPE_userlimit
                    
                    string pluginMapKey;
                    
                    pluginMapKey = ph.getPluginMapKey(currentQli);
                    
                    if(!numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, 1);
                    }
                    else
                    {
                        integer tempNumber;
                        
                        tempNumber = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                        
                        tempNumber++;
                        
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, tempNumber);
                    }
                }
            }
            
            for(quoteLineItem currentqli : trigger.New)
            {
                if(currentQli.Created_By_Form__c == true)
                {
                    decimal salesPrice;
                    integer existingPlugins;
                    integer newPlugins;
                    string pluginMapKey;
                    
                    currentQli.Quantity = 1;
                    
                    pluginMapKey = ph.getPluginMapKey(currentQli);
                    
                    if(numberOfExistingPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        existingPlugins = numberOfExistingPluginsExternalIdMap.get(pluginMapKey); //always empty
                    }
                    else
                    {
                        existingPlugins = 0;
                    }
                    
                    if(numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        newPlugins = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                    }
                    else
                    {
                        newPlugins = 0;
                    }
                    
                    salesPrice = ph.calculatequoteLineItemSalesPrice(currentqli, existingPlugins, newPlugins);
                    
                    if(currentqli.Target_Price__c != NULL)
                    {
                        // 20150105 - perpetual renewal target price esetén
                        if
                        (
                            currentqli.License_Type__c != NULL &&
                            currentqli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
                            currentqli.Opportunity_Renewal_Percentage__c != NULL
                        )
                        {
                        	decimal tempSalesPrice;
                            tempSalesPrice = 
                                salesPrice * 
                                currentqli.Opportunity_Renewal_Percentage__c / 
                                100;
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentqli.UnitPrice = tempSalesPrice;
                            currentqli.Discount = NULL;
                            
                            // 20141119
                            currentqli.Original_Price__c = tempSalesPrice;
                        }
                        else
                        {
                        	currentqli.UnitPrice = currentqli.Target_Price__c;
                        	currentqli.Original_Price__c = salesPrice;
                        	currentqli.Discount = NULL;
                        }
                    }
                    else
                    {
                        currentqli.UnitPrice = salesPrice;
                        currentqli.Original_Price__c = salesPrice;
                        
                        if
                        (
                            currentqli.License_Type__c != NULL &&
                            currentqli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
                            currentqli.Opportunity_Renewal_Percentage__c != NULL
                        )
                        {
                            decimal tempSalesPrice;
                            tempSalesPrice = 
                                salesPrice * 
                                currentqli.Opportunity_Renewal_Percentage__c / 
                                100;
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentqli.UnitPrice = tempSalesPrice;
                            
                            // 20141119
                            currentqli.Original_Price__c = tempSalesPrice;
                        }
                        else
                        {
                            decimal tempSalesPrice;
                            tempSalesPrice = salesPrice; 
                            
                            if(tempSalesPrice != 0 && tempSalesPrice != NULL)
                            {
                                tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
                            }
                            
                            currentqli.UnitPrice = tempSalesPrice;
                        }
                    }
                    
                    if(currentqli.Discount != NULL)
                    {
                        decimal tempTotalPrice;
                        
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        tempTotalPrice = salesPrice * ((100 - currentqli.Discount) / 100);
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        
                        tempTotalPrice = ph.customRounder(tempTotalPrice, NULL);
                        system.debug('tempTotalPrice: ' + tempTotalPrice);
                        
                        system.debug('discount: ' + currentqli.Discount);
                        currentqli.Discount = (1 - (tempTotalPrice / salesPrice)) * 100;
                        system.debug('discount: ' + currentqli.Discount);
                    }
                }
                else
                {
                    // ELSE
                }
            }
            
            if(existingPluginqliList.size() != 0)
            {
                system.debug(existingPluginQliList);
                
                //update existingPluginqliList;
            }
        }
        
		System.debug(trigger.isBefore);
		System.debug(trigger.isUpdate);
        if(trigger.isUpdate)
        {
            triggeredQliIdSet.clear();
            quoteIdSet.clear();
            
            for(quoteLineItem currentqli : trigger.New)
            {
                currentQli.Quantity = 1;
                
                triggeredQliIdSet.add(currentQli.Id);
                
                quoteIdSet.add(currentQli.QuoteId);
                
                if(currentqli.Plugin__c == true)
                {
                    //key: quoteid_LICENSETYPE_userlimit
                    
                    string pluginMapKey;
                    
                    pluginMapKey = ph.getPluginMapKey(currentQli);
                    
                    if(!numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
                    {
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, 1);
                    }
                    else
                    {
                        integer tempNumber;
                        
                        tempNumber = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
                        
                        tempNumber++;
                        
                        numberOfNewPluginsExternalIdMap.put(pluginMapKey, tempNumber);
                    }
                }
            }
            
            system.debug('izéé 01: ' + numberOfNewPluginsExternalIdMap);
            system.debug('izéé 02: ' + numberOfExistingPluginsExternalIdMap);
            system.debug('izéé 03: ' + numberOfExistingPluginsForUpdateExternalIdMap);
            
            for(QuoteLineItem currentQli : trigger.New)
            {
				if(currentQli.T_Calculate_Prices__c != trigger.oldMap.get(currentQli.Id).T_Calculate_Prices__c)
                {
	                if
	                (
	                    currentQli.Quote_Target_Price__c == NULL &&
	                    currentQli.Target_Pricing_In_Action__c == trigger.oldMap.get(currentQli.Id).Target_Pricing_In_Action__c
	                )
	                {
	                    decimal salesPrice;
	                    integer existingPlugins;
	                    integer newPlugins;
	                    string pluginMapKey;
	                    
	                    pluginMapKey = ph.getPluginMapKey(currentQli);
	                    
	                    existingPlugins = 0;
	                    
	                    if(numberOfNewPluginsExternalIdMap.containsKey(pluginMapKey))
	                    {
	                        newPlugins = numberOfNewPluginsExternalIdMap.get(pluginMapKey);
	                    }
	                    else
	                    {
	                        newPlugins = 0;
	                    }
	                    
	                    system.debug('currentQli: ' + currentqli);
	                    system.debug('existingPlugins: ' + existingPlugins);
	                    system.debug('newPlugins: ' + newPlugins);
	                    
	                    salesPrice = ph.calculatequoteLineItemSalesPrice(currentqli, existingPlugins, newPlugins);
	                    
	                    currentqli.Original_Price__c = salesPrice;
	                    
	                    // 20141120
	                    // currentqli.UnitPrice = salesPrice;
	                    
	                    if(currentqli.Target_Price__c != NULL)
	                    {
	                        // 20150105 - perpetual renewal target pricing esetén
	                        if
	                        (
								currentQli.License_Type__c != null &&
	                            currentqli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
	                            currentqli.Opportunity_Renewal_Percentage__c != NULL
	                        )
	                        {
	                            decimal tempSalesPrice;
	                            tempSalesPrice = 
	                                salesPrice * 
	                                currentqli.Opportunity_Renewal_Percentage__c / 
	                                100;
	                            
	                            tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                            
	                            currentqli.UnitPrice = tempSalesPrice;
	                            
	                            // 20141119
	                            currentqli.Original_Price__c = tempSalesPrice;
	                            
								currentqli.Discount = NULL;
	                            
	                            system.debug('unitPrice: ' + tempSalesPrice);
	                        }
	                        else
	                        {
	                        	currentqli.UnitPrice = currentqli.Target_Price__c;
	                        	currentqli.Original_Price__c = salesPrice;
	                        	currentqli.Discount = NULL;
	                        }
	                    }
	                    else
	                    {
	                        currentqli.UnitPrice = salesPrice;
	                        
	                        if
	                        (
								currentQli.License_Type__c != null && 
	                            currentqli.License_Type__c.toUpperCase() == 'PERPETUAL RENEWAL' &&
	                            currentqli.Opportunity_Renewal_Percentage__c != NULL
	                        )
	                        {
	                            decimal tempSalesPrice;
	                            tempSalesPrice = 
	                                salesPrice * 
	                                currentqli.Opportunity_Renewal_Percentage__c / 
	                                100;
	                            
	                            tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                            
	                            currentqli.UnitPrice = tempSalesPrice;
	                            
	                            // 20141119
	                            currentqli.Original_Price__c = tempSalesPrice;
	                            
	                            system.debug('unitPrice: ' + tempSalesPrice);
	                        }
	                        else
	                        {
	                            decimal tempSalesPrice;
	                            tempSalesPrice = salesPrice;
	                            
	                            tempSalesPrice = ph.customRounder(tempSalesPrice, NULL);
	                            
	                            currentqli.UnitPrice = tempSalesPrice;
	                            
	                            system.debug('unitPrice: ' + tempSalesPrice);
	                        }
	                    }
	                    
	                    if(currentqli.Discount != NULL)
	                    {
	                        decimal tempTotalPrice;
	                        
	                        system.debug('tempTotalPrice: ' + tempTotalPrice);
	                        tempTotalPrice = salesPrice * ((100 - currentqli.Discount) / 100);
	                        system.debug('tempTotalPrice: ' + tempTotalPrice);
	                        
	                        tempTotalPrice = ph.customRounder(tempTotalPrice, NULL);
	                        system.debug('tempTotalPrice: ' + tempTotalPrice);
	                        
	                        system.debug('discount: ' + currentqli.Discount);
	                        try{
								currentqli.Discount = (1 - (tempTotalPrice / salesPrice)) * 100;
							}catch(Exception e){
								emailtKuld(e, 'Exception keletkezett a QuoteLineItem.triggerben', currentqli);
							}
	                        system.debug('discount: ' + currentqli.Discount);
	                    }
	                }
	                else
	                {
	                    // ELSE
	                }
	                
	                // 20141120 - kommentezve: 20141123
	                
	                if(currentQli.Target_price__c != NULL)
	                {
	                    currentQli.UnitPrice = currentQli.Target_Price__c;
	                }
	                else
	                {
	                    currentQli.UnitPrice = currentQli.Original_Price__c;
	                }
	                // currentQli.UnitPrice = 1000;
	            }
	            
	            if(existingPluginqliForUpdateList.size() != 0)
	            {
	                system.debug(existingPluginqliForUpdateList);
	                
	                //update existingPluginqliForUpdateList;
	            }
            }
        }
    }
    
    if(changedUserNumberQuoteIdSet.size() != 0)
    {
        if(System.isBatch() || System.isFuture()) PriceHandlerFuture.updateQliNonFuture(changedUserNumberQuoteIdSet);
		else PriceHandlerFuture.updateQli(changedUserNumberQuoteIdSet);
    }

	private static void emailtKuld(Exception ex, String subject, QuoteLineItem currentQli){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); String[] sToAddress = new String[]{Util.UCEMAILADDRESS}; mail.setToAddresses(sToAddress); mail.setSenderDisplayName('ChemAxon'); mail.setSubject(subject); mail.setplaintextbody(ex.getMessage() + ' - ' + ex.getStackTraceString() + ' - ' + JSON.serialize(currentQli)); Messaging.sendEmail( new Messaging.SingleEmailMessage[]{mail} );
	}
//*

	if(Test.isRunningTest()) magic();

	private void magic(){
		Integer i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
		i = 0;
	}
//*/
}