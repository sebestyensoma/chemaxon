trigger CampaignMemberTrigger on CampaignMember (after insert) 
{
    List<Campaign> campaignList = new List<Campaign>();
    List<Lead> leadList = new List<Lead>();
    List<Lead> leadListForUpdate = new List<Lead>();
    
    Map<id, Campaign> campaignIdMap = new Map<id, Campaign>();
    Map<id, Lead> leadIdMap = new Map<id, Lead>();
    
    Set<id> campaignIdSet = new Set<id>();
    Set<id> leadIdSet = new Set<id>();
     
    
    for(CampaignMember cm : trigger.New)
    {
        campaignIdSet.add(cm.CampaignId);
        leadIdSet.add(cm.LeadId);
    }
    
    if(campaignIdSet.size() != 0)
    {
        campaignList =
            [
                SELECT
                    Id,
                    Internal_External__c
                FROM
                    Campaign
                WHERE
                    Id IN :campaignIdSet
            ];
            
        for(Campaign campaign : campaignList)
        {
            campaignIdMap.put(campaign.Id, campaign);
        }
    }
    
    if(leadIdSet.size() != 0)
    {
        leadList =
            [
                SELECT
                    Id,
                    LeadSource
                FROM
                    Lead
                WHERE
                    Id IN :leadIdSet
            ];
            
        for(Lead lead : leadList)
        {
            leadIdMap.put(lead.Id, lead);   
        }
    }
    
    for(CampaignMember cm : trigger.New)
    {
        Campaign tempCm;
        Lead tempLead;
        
        if(campaignIdMap.containsKey(cm.CampaignId))
        {
            tempCm = campaignIdMap.get(cm.CampaignId);
        }
        
        if(leadIdMap.containsKey(cm.LeadId))
        {
            tempLead = leadIdMap.get(cm.LeadId);
        }
        
        if(tempCm != NULL && tempLead != NULL)
        {
            if(tempCm.Internal_External__c == 'Internal')
            {
                tempLead.LeadSource = 'Event Internal';
                
                leadListForUpdate.add(tempLead);
            }
            
            if(tempCm.Internal_External__c == 'External')
            {
                tempLead.LeadSource = 'Event External';
                
                leadListForUpdate.add(tempLead);
            }
        }
    }
    
    if(leadListForUpdate.size() != 0)
    {
        update leadListForUpdate;
    }
}