/** Trigger to verify Shipping and/or Billing Address may be copied from Quote to Account at the user's request
 * @author		Attention CRM <attention@attentioncrm.hu>
 * @version		1.0
 * @created		2014-11-05 (SM)
 */
trigger QuoteCopyAddressCheck on Quote (before insert, before update) {

	// Filter out Quotes where copying is necessary
	Set<Id> oppIds = new Set<Id>();
	for (Quote newQuote : trigger.new) if (newQuote.Copy_Billing__c || newQuote.Copy_Shipping__c) {
		oppIds.add(newQuote.OpportunityId);
	}
	
	if (!oppIds.isEmpty()) {
		// Fetch Non-EU Countries List
		Set<String> nonEu = NonEUCountry__c.getAll().keySet();
		
		// Fetch Accounts via Opportunities
		Opportunity[] opps = [
			SELECT Id, AccountId
			FROM Opportunity
			WHERE Id IN :oppIds
		];
		Set<Id> accountIds = new Set<Id>();
		for (Opportunity opp : opps) {
			accountIds.add(opp.AccountId);
		}
		Map<Id, Account> accounts = new Map<Id, Account>([ SELECT Id, Tax_Number__c FROM Account WHERE Id IN :accountIds ]);
		Map</* Opportunity */ Id, Account> accByOpps = new Map<Id, Account>();
		for (Opportunity opp : opps) {
			accByOpps.put(opp.Id, accounts.get(opp.AccountId));
		}
		
		// Verify addresses
		for (Quote newQuote : trigger.new) {
			Account acc = accByOpps.get(newQuote.OpportunityId);
			
			if (newQuote.Copy_Billing__c && !nonEu.contains(newQuote.BillingCountry) && acc.Tax_Number__c == null) {
				newQuote.addError('EU országbeli cím csak olyan Ügyfélen adható meg, amelyen ki van töltve az Adószám!');
				continue;
			}
			if (newQuote.Copy_Shipping__c && !nonEu.contains(newQuote.BillingCountry) && acc.Tax_Number__c == null) {
				newQuote.addError('EU országbeli cím csak olyan Ügyfélen adható meg, amelyen ki van töltve az Adószám!');
				continue;
			}
		}
	}
}