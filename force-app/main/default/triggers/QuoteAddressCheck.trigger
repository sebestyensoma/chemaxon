/** Trigger to verify Billing Address before Quote is set to Ordered
 * @author      Attention CRM <attention@attentioncrm.hu>
 * @version     1.0
 * @created     2014-11-13 (SM)
 */
trigger QuoteAddressCheck on Quote (before update) {

    // Filter out Quotes where copying is necessary
    Set<Id> oppIds = new Set<Id>();
    for (Quote newQuote : trigger.new) {
        Quote oldQuote = trigger.oldMap.get(newQuote.Id);
        
        if (newQuote.Status != oldQuote.Status && newQuote.Status == 'Ordered') {
            oppIds.add(newQuote.OpportunityId);
        }
    }
    
    if (!oppIds.isEmpty()) {
        // Fetch Non-EU Countries List
        Set<String> nonEu = NonEUCountry__c.getAll().keySet();
        
        // Fetch Accounts via Opportunities
        Opportunity[] opps = [
            SELECT Id, AccountId
            FROM Opportunity
            WHERE Id IN :oppIds
        ];
        Set<Id> accountIds = new Set<Id>();
        for (Opportunity opp : opps) {
            accountIds.add(opp.AccountId);
        }
        Map<Id, Account> accounts = new Map<Id, Account>([ SELECT Id, BillingCountry, Tax_Number__c FROM Account WHERE Id IN :accountIds ]);
        Map</* Opportunity */ Id, Account> accByOpps = new Map<Id, Account>();
        for (Opportunity opp : opps) {
            accByOpps.put(opp.Id, accounts.get(opp.AccountId));
        }
        
        // Verify addresses
        for (Quote newQuote : trigger.new) if (oppIds.contains(newQuote.OpportunityId)) {
            Account acc = accByOpps.get(newQuote.OpportunityId);
            
            if (!nonEu.contains(acc.BillingCountry) && acc.Tax_Number__c == null) {
                newQuote.addError('If the billing country of the account is in the European Union, you should fill the VAT number field.');
                continue;
            }
        }
    }
}