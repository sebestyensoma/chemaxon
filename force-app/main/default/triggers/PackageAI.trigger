trigger PackageAI on Package__c (after insert) {
	/*
	if(trigger.new.size()==1 && trigger.new[0].Quote__c!=null && trigger.new[0].Opportunity__c==null){
		List<Quote> listExistingQuotes = [SELECT CurrencyIsoCode, Pricebook2Id, Id FROM Quote WHERE Id=:trigger.new[0].Quote__c];
		system.debug('listExistingQuotes.size():'+listExistingQuotes.size());
		system.debug('listExistingQuotes[0].Pricebook2Id:'+listExistingQuotes[0].Pricebook2Id);
		List<Package__c> listExistingPackage = [SELECT Package_Type__c FROM Package__c WHERE Quote__c=:trigger.new[0].Quote__c AND Id!=:trigger.new[0].Id];
		system.debug('size+listExistingPackage:'+listExistingPackage.size()+'+'+listExistingPackage);
		if(listExistingPackage!=null && listExistingPackage.size()>0 && trigger.new[0].Package_Type__c=='All products'){						
			trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
		}
		else if(listExistingPackage!=null && listExistingPackage.size()>0){
			for(Package__c loopExistingPackage: listExistingPackage){
				if(	loopExistingPackage.Package_Type__c==trigger.new[0].Package_Type__c
					||
					loopExistingPackage.Package_Type__c=='All products'
				){
					trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
				}
			}			
		}
		
		
		system.debug('trigger.new[0].Id:'+trigger.new[0].Id);
		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id = :listExistingQuotes[0].Pricebook2Id AND
				CurrencyIsoCode = :listExistingQuotes[0].CurrencyIsoCode AND
				IsActive = true
				]);
		system.debug('1.mapIdTOPricebookEntry.size()'+mapIdTOPricebookEntry.size());
		List<QuoteLineItem> listInsertQLIs = new List<QuoteLineItem>();		
		Map<Id,Product2> idToProductMap = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		
		Map<Id,PricebookEntry> mapIdToPBEExisting = new Map<Id,PricebookEntry>([SELECT Name, Id,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE QuoteId = :trigger.new[0].Quote__c)]);
		Map<Id,Product2> mapIdToProductExisting = new Map<Id,Product2>([SELECT Name,Ratio_for_Sales_Report__c FROM Product2 WHERE Id IN (SELECT Product2Id FROM PricebookEntry WHERE Id IN :mapIdToPBEExisting.keySet())]);
		
		for(Id loopId:idToProductMap.keySet()){
			if(mapIdToProductExisting.containsKey(loopId)){
				trigger.new[0].addError('This package contains a product ('+mapIdToProductExisting.get(loopId).name+') that is already added to the quote!');
			}
		}
		
		Double ratioSumarized = 0.0;
		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:idToProductMap.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
		}
			
		for(Id loopId:mapIdTOPricebookEntry.keySet()){			
			PricebookEntry loopPBE = mapIdTOPricebookEntry.get(loopId);			
			
			QuoteLineItem loopQLI = new QuoteLineItem(QuoteId=trigger.new[0].Quote__c ,Quantity=1,PricebookEntryId=loopId,Package__c=trigger.new[0].Id,Discount=0,UnitPrice=loopPBE.UnitPrice,User_Limit__c=trigger.new[0].User_Limit__c);			
			if(trigger.new[0].Package_Type__c!='All plugins'){			
				Product2 relatedProduct = idToProductMap.get(loopPBE.Product2Id);
				
				system.debug('relatedProduct.Ratio_for_Sales_Report__c: ' + relatedProduct.Ratio_for_Sales_Report__c);
				system.debug('ratioSumarized: ' + ratioSumarized);
				system.debug('trigger.new[0].Target_price__c: ' + trigger.new[0].Target_price__c);
								
				if(relatedProduct.Ratio_for_Sales_Report__c == NULL)
				{
					trigger.new[0].addError('Missing \'Ratio for Sales Report\'.');
					
					break;
				}
				
				if(ratioSumarized == NULL || ratioSumarized == 0)
				{
					trigger.new[0].addError('Missing \'Ratiosumarized\'.');
					
					break;
				}
				
				if(trigger.new[0].Target_price__c == NULL || trigger.new[0].Target_price__c == 0)
				{
					trigger.new[0].addError('Please specify the target price!');
					
					break;
				}
				
				system.debug('relatedProduct.Ratio_for_Sales_Report__c: ' + relatedProduct.Ratio_for_Sales_Report__c);
				system.debug('ratioSumarized: ' + ratioSumarized);
				system.debug('trigger.new[0].Target_price__c: ' + trigger.new[0].Target_price__c);
				
				loopQLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
				
				system.debug('Kedvezmény:'+loopQLI.Discount);
			}
			listInsertQLIs.add(loopQLI);
		}
		
		system.debug(listInsertQLIs);
		
		insert listInsertQLIs;
	}
	else if(trigger.new.size()==1 && trigger.new[0].Quote__c==null && trigger.new[0].Opportunity__c!=null){
		List<Opportunity> listExistingOpptys = [SELECT Pricebook2Id, Id, CurrencyIsoCode FROM Opportunity WHERE Id=:trigger.new[0].Opportunity__c];
		system.debug('listExistingOpptys.size()'+listExistingOpptys.size());
		system.debug('listExistingOpptys[0].Pricebook2Id:'+listExistingOpptys[0].Pricebook2Id);
		List<Package__c> listExistingPackage = [SELECT Package_Type__c FROM Package__c WHERE Opportunity__c=:trigger.new[0].Opportunity__c AND Id!=:trigger.new[0].Id];
		system.debug('size+listExistingPackage:'+listExistingPackage.size()+'+'+listExistingPackage);
		if(listExistingPackage!=null && listExistingPackage.size()>0 && trigger.new[0].Package_Type__c=='All products'){						
			trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
		}
		else if(listExistingPackage!=null && listExistingPackage.size()>0){
			for(Package__c loopExistingPackage: listExistingPackage){
				if(	loopExistingPackage.Package_Type__c==trigger.new[0].Package_Type__c
					||
					loopExistingPackage.Package_Type__c=='All products'
				){
					trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
				}
			}			
		}
		

		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id = :listExistingOpptys[0].Pricebook2Id AND
				CurrencyIsoCode = :listExistingOpptys[0].CurrencyIsoCode
				]);
		system.debug('2.mapIdTOPricebookEntry.size()'+mapIdTOPricebookEntry.size());
		List<OpportunityLineItem> listInsertOLIs = new List<OpportunityLineItem>();		
		Map<Id,Product2> idToProductMap = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		
		Map<Id,PricebookEntry> mapIdToPBEExisting = new Map<Id,PricebookEntry>([SELECT Name, Id,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE QuoteId = :trigger.new[0].Quote__c)]);
		Map<Id,Product2> mapIdToProductExisting = new Map<Id,Product2>([SELECT Name,Ratio_for_Sales_Report__c FROM Product2 WHERE Id IN (SELECT Product2Id FROM PricebookEntry WHERE Id IN :mapIdToPBEExisting.keySet())]);
		
		for(Id loopId:idToProductMap.keySet()){
			if(mapIdToProductExisting.containsKey(loopId)){
				trigger.new[0].addError('This package contains a product ('+mapIdToProductExisting.get(loopId).name+') that is already added to the quote!');
			}
		}
		
		Double ratioSumarized = 0.0;
		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:idToProductMap.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
		}
			
		for(Id loopId:mapIdTOPricebookEntry.keySet()){			
			PricebookEntry loopPBE = mapIdTOPricebookEntry.get(loopId);			
			OpportunityLineItem loopOLI = new OpportunityLineItem(OpportunityId=trigger.new[0].Opportunity__c ,Quantity=1,PricebookEntryId=loopId,Package__c=trigger.new[0].Id,Discount=0,UnitPrice=loopPBE.UnitPrice,User_Limit__c=trigger.new[0].User_Limit__c);			
			if(trigger.new[0].Package_Type__c!='All plugins'){				
				Product2 relatedProduct = idToProductMap.get(loopPBE.Product2Id);				
				loopOLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
				system.debug('Kedvezmény:'+loopOLI.Discount);
			}
			listInsertOLIs.add(loopOLI);
		}
		insert listInsertOLIs;
	}
	*/
}

/*
trigger PackageAI on Package__c (after insert) {
	if(trigger.new.size()==1 && trigger.new[0].Quote__c!=null && trigger.new[0].Opportunity__c==null){
		List<Quote> listExistingQuotes = [SELECT Pricebook2Id, Id FROM Quote WHERE Id=:trigger.new[0].Quote__c];
		system.debug('listExistingQuotes.size():'+listExistingQuotes.size());
		system.debug('listExistingQuotes[0].Pricebook2Id:'+listExistingQuotes[0].Pricebook2Id);
		List<Package__c> listExistingPackage = [SELECT Package_Type__c FROM Package__c WHERE Quote__c=:trigger.new[0].Quote__c AND Id!=:trigger.new[0].Id];
		system.debug('size+listExistingPackage:'+listExistingPackage.size()+'+'+listExistingPackage);
		if(listExistingPackage!=null && listExistingPackage.size()>0 && trigger.new[0].Package_Type__c=='All products'){						
			trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
		}
		else if(listExistingPackage!=null && listExistingPackage.size()>0){
			for(Package__c loopExistingPackage: listExistingPackage){
				if(	loopExistingPackage.Package_Type__c==trigger.new[0].Package_Type__c
					||
					loopExistingPackage.Package_Type__c=='All products'
				){
					trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
				}
			}			
		}
		
		
		system.debug('trigger.new[0].Id:'+trigger.new[0].Id);
		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id = :listExistingQuotes[0].Pricebook2Id
				]);
		system.debug('1.mapIdTOPricebookEntry.size()'+mapIdTOPricebookEntry.size());
		List<QuoteLineItem> listInsertQLIs = new List<QuoteLineItem>();		
		Map<Id,Product2> idToProductMap = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		
		Map<Id,PricebookEntry> mapIdToPBEExisting = new Map<Id,PricebookEntry>([SELECT Name, Id,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE QuoteId = :trigger.new[0].Quote__c)]);
		Map<Id,Product2> mapIdToProductExisting = new Map<Id,Product2>([SELECT Name,Ratio_for_Sales_Report__c FROM Product2 WHERE Id IN (SELECT Product2Id FROM PricebookEntry WHERE Id IN :mapIdToPBEExisting.keySet())]);
		
		for(Id loopId:idToProductMap.keySet()){
			if(mapIdToProductExisting.containsKey(loopId)){
				trigger.new[0].addError('This package contains a product ('+mapIdToProductExisting.get(loopId).name+') that is already added to the quote!');
			}
		}
		
		Double ratioSumarized = 0.0;
		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:idToProductMap.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
		}
			
		for(Id loopId:mapIdTOPricebookEntry.keySet()){			
			PricebookEntry loopPBE = mapIdTOPricebookEntry.get(loopId);			
			
			QuoteLineItem loopQLI = new QuoteLineItem(QuoteId=trigger.new[0].Quote__c ,Quantity=1,PricebookEntryId=loopId,Package__c=trigger.new[0].Id,Discount=0,UnitPrice=loopPBE.UnitPrice,User_Limit__c=trigger.new[0].User_Limit__c);			
			if(trigger.new[0].Package_Type__c!='All plugins'){			
				Product2 relatedProduct = idToProductMap.get(loopPBE.Product2Id);
				
				system.debug('relatedProduct.Ratio_for_Sales_Report__c: ' + relatedProduct.Ratio_for_Sales_Report__c);
				system.debug('ratioSumarized: ' + ratioSumarized);
				system.debug('trigger.new[0].Target_price__c: ' + trigger.new[0].Target_price__c);
								
				if(relatedProduct.Ratio_for_Sales_Report__c == NULL)
				{
					trigger.new[0].addError('Missing \'Ratio for Sales Report\'.');
					
					break;
				}
				
				if(ratioSumarized == NULL || ratioSumarized == 0)
				{
					trigger.new[0].addError('Missing \'Ratiosumarized\'.');
					
					break;
				}
				
				if(trigger.new[0].Target_price__c == NULL || trigger.new[0].Target_price__c == 0)
				{
					trigger.new[0].addError('Please specify the target price!');
					
					break;
				}
				
				system.debug('relatedProduct.Ratio_for_Sales_Report__c: ' + relatedProduct.Ratio_for_Sales_Report__c);
				system.debug('ratioSumarized: ' + ratioSumarized);
				system.debug('trigger.new[0].Target_price__c: ' + trigger.new[0].Target_price__c);
				
				loopQLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
				
				system.debug('Kedvezmény:'+loopQLI.Discount);
			}
			listInsertQLIs.add(loopQLI);
		}
		
		system.debug(listInsertQLIs);
		
		insert listInsertQLIs;
	}
	else if(trigger.new.size()==1 && trigger.new[0].Quote__c==null && trigger.new[0].Opportunity__c!=null){
		List<Opportunity> listExistingOpptys = [SELECT Pricebook2Id, Id FROM Opportunity WHERE Id=:trigger.new[0].Opportunity__c];
		system.debug('listExistingOpptys.size()'+listExistingOpptys.size());
		system.debug('listExistingOpptys[0].Pricebook2Id:'+listExistingOpptys[0].Pricebook2Id);
		List<Package__c> listExistingPackage = [SELECT Package_Type__c FROM Package__c WHERE Opportunity__c=:trigger.new[0].Opportunity__c AND Id!=:trigger.new[0].Id];
		system.debug('size+listExistingPackage:'+listExistingPackage.size()+'+'+listExistingPackage);
		if(listExistingPackage!=null && listExistingPackage.size()>0 && trigger.new[0].Package_Type__c=='All products'){						
			trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
		}
		else if(listExistingPackage!=null && listExistingPackage.size()>0){
			for(Package__c loopExistingPackage: listExistingPackage){
				if(	loopExistingPackage.Package_Type__c==trigger.new[0].Package_Type__c
					||
					loopExistingPackage.Package_Type__c=='All products'
				){
					trigger.new[0].addError('This package can not be sold with the existing package(s) on the quote!');
				}
			}			
		}
		
		//Ha nem menne a lekérdezés akkor ezekkel ki lehet vizsgálni
		/*system.debug('trigger.new[0].Id:'+trigger.new[0].Id);
		system.debug('Package type:'+trigger.new[0].Package_Type__c);
		system.debug('Product list:'+[SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)][0]);
		system.debug('listExistingOpptys[0].Pricebook2Id:'+listExistingOpptys[0].Pricebook2Id);
		system.debug('trigger.new[0].Opportunity__c:'+trigger.new[0].Opportunity__c);
		system.debug('trigger.new[0].Opportunity__r.CurrencyIsoCode:'+trigger.new[0].Opportunity__r.CurrencyIsoCode);
		*
		Map<Id,PricebookEntry> mapIdTOPricebookEntry = new Map<Id,PricebookEntry>( 
			[SELECT Id,UnitPrice,Product2Id
			FROM PricebookEntry
			WHERE 
				Product2Id IN (SELECT Id FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)) AND
				Pricebook2Id = :listExistingOpptys[0].Pricebook2Id
				]);
		system.debug('2.mapIdTOPricebookEntry.size()'+mapIdTOPricebookEntry.size());
		List<OpportunityLineItem> listInsertOLIs = new List<OpportunityLineItem>();		
		Map<Id,Product2> idToProductMap = new Map<Id,Product2>([SELECT Id,Ratio_for_Sales_Report__c FROM Product2 WHERE Package_Type__c includes (:trigger.new[0].Package_Type__c)]);
		
		Map<Id,PricebookEntry> mapIdToPBEExisting = new Map<Id,PricebookEntry>([SELECT Name, Id,Product2Id FROM PricebookEntry WHERE Id IN (SELECT PricebookEntryId FROM QuoteLineItem WHERE QuoteId = :trigger.new[0].Quote__c)]);
		Map<Id,Product2> mapIdToProductExisting = new Map<Id,Product2>([SELECT Name,Ratio_for_Sales_Report__c FROM Product2 WHERE Id IN (SELECT Product2Id FROM PricebookEntry WHERE Id IN :mapIdToPBEExisting.keySet())]);
		
		for(Id loopId:idToProductMap.keySet()){
			if(mapIdToProductExisting.containsKey(loopId)){
				trigger.new[0].addError('This package contains a product ('+mapIdToProductExisting.get(loopId).name+') that is already added to the quote!');
			}
		}
		
		Double ratioSumarized = 0.0;
		if(trigger.new[0].Package_Type__c!='All plugins'){
			for(Product2 loopProduct:idToProductMap.values()){
				if(loopProduct.Ratio_for_Sales_Report__c!=null){
					ratioSumarized+=loopProduct.Ratio_for_Sales_Report__c;
				}
			}
		}
			
		for(Id loopId:mapIdTOPricebookEntry.keySet()){			
			PricebookEntry loopPBE = mapIdTOPricebookEntry.get(loopId);			
			OpportunityLineItem loopOLI = new OpportunityLineItem(OpportunityId=trigger.new[0].Opportunity__c ,Quantity=1,PricebookEntryId=loopId,Package__c=trigger.new[0].Id,Discount=0,UnitPrice=loopPBE.UnitPrice,User_Limit__c=trigger.new[0].User_Limit__c);			
			if(trigger.new[0].Package_Type__c!='All plugins'){				
				Product2 relatedProduct = idToProductMap.get(loopPBE.Product2Id);				
				loopOLI.UnitPrice=(relatedProduct.Ratio_for_Sales_Report__c/ratioSumarized)*trigger.new[0].Target_price__c;
				system.debug('Kedvezmény:'+loopOLI.Discount);
			}
			listInsertOLIs.add(loopOLI);
		}
		insert listInsertOLIs;
	}
}
*/