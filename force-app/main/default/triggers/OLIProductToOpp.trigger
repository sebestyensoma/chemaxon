// az opportunity-n levő Opportunity_product_names__c mezőbe írjuk be az opportunityra felvett termékek (OLI-k) neveit (törléskor vegyük ki a termék nevét),
// amíg nem szinkronizáltunk quote-ot 
// 
// tesztosztály: TestQuoteLineItemCreate.cls

trigger OLIProductToOpp on OpportunityLineItem (after insert, after delete)  { 


	Set<Id> oppId = new Set<Id>();
	for(OpportunityLineItem oli : Trigger.isInsert ? Trigger.new : Trigger.old){
		oppId.add(oli.OpportunityId);
	}

	Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Opportunity_product_names__c FROM Opportunity WHERE Id IN :oppId /*AND SyncedQuoteId = null*/]);
	//System.debug(json.serialize(oppMap));

	Set<Id> prodId = new Set<Id>();
	Map<Id/*opp id*/, List<OpportunityLineItem>> oliByOppId = new Map<Id, List<OpportunityLineItem>>();
	for(OpportunityLineItem oli : [SELECT Id, OpportunityId, Product2Id FROM OpportunityLineItem WHERE OpportunityId IN :oppMap.keyset()]){
		List<OpportunityLineItem> tmp = oliByOppId.get(oli.OpportunityId);
		if(tmp == null){
			tmp = new List<OpportunityLineItem>();
			oliByOppId.put(oli.OpportunityId, tmp);
		}
		tmp.add(oli);
		prodId.add(oli.Product2Id);
	}
	Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Name FROM Product2 WHERE Id IN :prodId]);

	Map<Id, Opportunity> oppToUpdate = new Map<Id, Opportunity>();
	for(OpportunityLineItem oli : Trigger.isInsert ? Trigger.new : Trigger.old){
		if(!oppMap.containsKey(oli.OpportunityId)) continue;

		Opportunity opp = oppMap.get(oli.OpportunityId);
		//System.debug(opp);
		termeketHozzaad(opp);
		oppToUpdate.put(oli.OpportunityId, opp);
	}


	update oppToUpdate.values();


	private void termeketHozzaad(Opportunity opp){
		Set<String> tmp = new Set<String>();
		//if(opp.Opportunity_product_names__c != null) tmp = new Set<String>(opp.Opportunity_product_names__c.split(';'));


		if(oliByOppId.containsKey(opp.Id)){
			for(OpportunityLineItem oli : oliByOppId.get(opp.Id)){
				tmp.add(prodMap.get(oli.Product2Id).Name);
			}
		}

		opp.Opportunity_product_names__c = String.join(new List<String>(tmp), ';');
		//System.debug(opp.Opportunity_product_names__c);
	}


}