trigger QuoteLineItemAIU on QuoteLineItem (after insert, after update, before delete, after undelete,before update) {
	Set<Id> quoteIds = new Set<Id>();
	
	//Ha update-eljük és a user limit változik
	if(trigger.isBefore && Trigger.isUpdate){
		
		//Ha nem a future methodból volt hívva
		for(QuoteLineItem loopQLI:trigger.new){
			if(
				loopQLI.Package__c==null
				&&
				
				
				(
					loopQLI.User_Limit__c!=trigger.oldMap.get(loopQLI.Id).User_Limit__c
					||
					loopQLI.License_type__c!=trigger.oldMap.get(loopQLI.Id).License_type__c
				)
			){
				quoteIds.add(loopQLI.QuoteId);
			}
		}
		
		//Ha a future methodból van hívva
		for(QuoteLineItem loopQLI:trigger.new){
			if(
				loopQLI.Package__c==null
				&&
				loopQLI.technical_Quote_Price_last_updated__c!=
					trigger.oldMap.get(loopQLI.Id).technical_Quote_Price_last_updated__c
				&&
				loopQLI.Quote_Price__c!=null
			){
				loopQLI.UnitPrice=loopQLI.Quote_Price__c;
			}
		}
	}
	
	
	if(
		(Trigger.isInsert && Trigger.isAfter)
		||
		(Trigger.isUnDelete && Trigger.isAfter)
	)
	{
		for(QuoteLineItem loopQLI:Trigger.new){
			if(loopQLI.Package__c==null){
				quoteIds.add(loopQLI.QuoteId);
			}
		}
	}
	
	/*
	if(
		(Trigger.isUpdate && Trigger.isAfter)
	)
	{
		for(QuoteLineItem loopQLI:Trigger.new){
			if(	loopQLI.Isplugin__c
				&&
				loopQLI.technical_Quote_Price_last_updated__c==trigger.oldMap.get(loopQLI.Id).technical_Quote_Price_last_updated__c
			){
				quoteIds.add(loopQLI.QuoteId);
			}
		}
	}
	*/
	
	
	if(	Trigger.isDelete && Trigger.isBefore){
		for(QuoteLineItem loopQLI:Trigger.old){
			if(loopQLI.Package__c==null){
				quoteIds.add(loopQLI.QuoteId);
			}
		}
	}

	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		Set<Id> accIds = new Set<Id>();
		Set<Id> quoteIds = new Set<Id>();
		for (QuoteLineItem qli : Trigger.new) {
			if (qli.End_User__c != null && (Trigger.isInsert || qli.End_User__c != Trigger.oldMap.get(qli.Id).End_User__c)) {
				accIds.add(qli.End_User__c);
				quoteIds.add(qli.QuoteId);
			}
		}
		Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, First_Purchase_Date__c FROM Account WHERE Id IN :accIds]);
		Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT Id, Opportunity.StageName, Opportunity.CloseDate FROM Quote WHERE Id IN :quoteIds]);
		List<Account> accountsToUpdate = new List<Account>();
		for (QuoteLineItem qli : Trigger.new) {
			if (qli.End_User__c != null && (Trigger.isInsert || qli.End_User__c != Trigger.oldMap.get(qli.Id).End_User__c)) {
				if ((accountMap.get(qli.End_User__c).First_Purchase_Date__c == null || accountMap.get(qli.End_User__c).First_Purchase_Date__c > quoteMap.get(qli.QuoteId).Opportunity.CloseDate) && quoteMap.get(qli.QuoteId).Opportunity.StageName == 'Closed Won') {
					accountMap.get(qli.End_User__c).First_Purchase_Date__c = quoteMap.get(qli.QuoteId).Opportunity.CloseDate;
					accountsToUpdate.add(accountMap.get(qli.End_User__c));
				}
			}
		}
		if( ! accountsToUpdate.isEmpty()){
			update accountMap.values();
		}
		
	}

	
	if(quoteIds!=null && quoteIds.size()>0){
		System.debug('QLIPriceUpdater called');
		//QLIPriceUpdater.updateQLIs(JSON.serialize(quoteIds));
	}
}