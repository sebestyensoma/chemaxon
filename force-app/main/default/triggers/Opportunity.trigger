trigger Opportunity on Opportunity (before insert, before update, before delete, after update) // after update
{
	OpportunityHandler oh = new OpportunityHandler();
	
	if(trigger.isBefore && trigger.isInsert)
	{
		string prodArInt1;
		string prodArInt2;
		List<Account> accountList = new List<Account>();
		List<Lead> leadList = new List<Lead>();
		
		for(Opportunity opp : Trigger.New)
		{
			//accountList = [SELECT Name FROM Account WHERE Id IN (SELECT ConvertedAccountId FROM Lead WHERE ConvertedOpportunityId = :opp.Id) LIMIT 1];
			//leadList = [SELECT Product_Area_Interest__c FROM Lead WHERE ConvertedOpportunityId = :opp.Id LIMIT 1];
			
			if(leadList.size() != 0)
			{
				//prodArInt1 = leadList[0].Product_Area_Interest__c;
				//prodArInt2 = prodArInt1.replaceAll(';', ',');
			}
			
			if(opp.Product_Area_Interest__c != NULL && opp.Product_Area_Interest__c != '')
			{
				opp.T_Product_Area_Interest_text__c = 
					opp.Product_Area_Interest__c.replaceAll(';', ', ');
					
				if(accountList.size() != 0 && leadList.size() != 0)
				{
					//opp.Name = accountList[0].Name + ' - ' + prodArInt2;
				}
			} 
		}
	}
	
	Id standardPriceBookId;
	List<Pricebook2> standardPricebookList = new List<Pricebook2>();
	
	standardPricebookList = Util.standardPricebookList;
		//[
			//SELECT
				//Id
			//FROM
				//Pricebook2
			//WHERE
				//Name = 'Standard Price Book'
			//LIMIT 1
		//];
		
	if(standardPriceBookList.size() != 0)
	{
		standardPriceBookId = standardPricebookList[0].Id;
	}
	
	if(Trigger.isBefore && !trigger.isDelete)
	{		
		for(Opportunity opp : Trigger.New)
		{
			if(opp.Pricebook2Id == NULL && standardPricebookId != NULL)
			{
				opp.Pricebook2Id = standardPricebookId;
			}
			
			decimal tempListPriceRollup;
			decimal tempSalesPriceRollup;
			
			if(opp.List_Price_Rollup__c != NULL)
			{
				tempListPriceRollup = opp.List_Price_Rollup__c;
			}
			else
			{
				tempListPriceRollup = 0;
			}
			
			if(opp.Sales_Price_Rollup__c != NULL)
			{
				tempSalesPriceRollup = opp.Sales_Price_Rollup__c;
			}
			else
			{
				tempSalesPriceRollup = 0;
			}
			
			if(opp.Renewal_Percentage__c != NULL && opp.Renewal_Base__c == 'List Price')
			{
				opp.Calculated_Support__c = tempListPriceRollup * (opp.Renewal_Percentage__c/100);
			}
			else if(opp.Renewal_Percentage__c != NULL && opp.Renewal_Base__c == 'Sales Price')
			{
				opp.Calculated_Support__c = tempSalesPriceRollup * (opp.Renewal_Percentage__c/100);
			}
			else if(opp.Renewal_Percentage__c == NULL && opp.Renewal_Base__c == 'List Price')
			{
				opp.Calculated_Support__c = tempListPriceRollup * 0.25;
				opp.Renewal_Percentage__c = 25;
			}
			else if(opp.Renewal_Percentage__c == NULL && opp.Renewal_Base__c == 'Sales Price')
			{
				opp.Calculated_Support__c = tempSalesPriceRollup * 0.25;
				opp.Renewal_Percentage__c = 25;
			}
			else if(opp.Renewal_Percentage__c == NULL && opp.Renewal_Base__c != 'List Price' && opp.Renewal_Base__c != 'Sales Price')
			{
				opp.Calculated_Support__c = tempSalesPriceRollup * 0.25;
				opp.Renewal_Percentage__c = 25;
			}
			else if(opp.Renewal_Percentage__c != NULL && opp.Renewal_Base__c != 'List Price' && opp.Renewal_Base__c != 'Sales Price')
			{
				opp.Calculated_Support__c = tempSalesPriceRollup * (opp.Renewal_Percentage__c/100);
			}
		}
	}
	
	/*
	if(trigger.isAfter && trigger.isUpdate)
	{
		Map<id, Opportunity> opportunityIdMap = new Map<id, Opportunity>();
		
		OpportunityHandler oh = new OpportunityHandler();
		
		for(Opportunity currentOpportunity : trigger.New)
		{
			if(currentOpportunity.Renewal_Percentage__c != trigger.oldMap.get(currentOpportunity.Id).Renewal_Percentage__c)
			{
				opportunityIdMap.put(currentOpportunity.Id, currentOpportunity);
			}
		}
		
		if(opportunityIdMap.size() != 0)
		{
			oh.setRenewalUnitPrice(opportunityIdMap);
		}
	}
	*/
	
	if(trigger.isBefore && trigger.isDelete)
	{
		List<Quote> quoteList = new List<Quote>();
		//Set<string> megengedettUserNevek = new Set<string>();
		Set<id> opportunityIdSet = new Set<id>();
		Set<id> opportunityUndeletableIdSet = new Set<id>();
		
		/*megengedettUserNevek.add('attentioncrm@chemaxon.com');
		megengedettUserNevek.add('attentioncrm@chemaxon.com.test');
		megengedettUserNevek.add('mmedzihradszky@chemaxon.com');
		megengedettUserNevek.add('mmedzihradszky@chemaxon.com.test');
		megengedettUserNevek.add('alukacs@chemaxon.com');
		megengedettUserNevek.add('alukacs@chemaxon.com.test');*/
		Id adminProfileId = [SELECT Id FROM Profile WHERE Name like '%Admin%' OR Name = 'Rendszergazda' limit 1].id;
		
		for(Opportunity opp : trigger.old)
		{
			opportunityIdSet.add(opp.Id);
		}
		
		if(opportunityIdSet.size() != 0)
		{
			quoteList =
				[
					SELECT
						Id,
						OpportunityId
					FROM
						Quote
					WHERE
						OpportunityId IN :opportunityIdSet
						AND
						(
							Status = 'Invoiced'
							OR
							Status = 'Settled'
						)
				];
				
			for(Quote currentQuote : quoteList)
			{
				opportunityUndeletableIdSet.add(currentQuote.OpportunityId);
			}
		}
		
		for(Opportunity opp : trigger.old)
		{
			if
			(
				opportunityUndeletableIdSet.contains(opp.Id) &&
				UserInfo.getProfileId() != adminProfileId
				//!megengedettUserNevek.contains(UserInfo.getUserName())
			)
			{
				opp.addError('You are not allowed to delete this opportunity!');
			}
		}
	}
	
	
	
	// WF01:				End User
	// Evaluation Criteria:	Evaluate the rule when a record is created, and every time it’s edited
	// Rule Criteria:		NOT(ISBLANK(End_User__c )) && ISCHANGED(End_User__c)
	// Field to Update:		Opportunity: (T) End User Date
	// Formula Value:		TODAY()
	
	// WF02:				Opportunity close date
	// Evaluation Criteria:	Evaluate the rule when a record is created, and every time it’s edited
	// Rule Criteria:		DAY(CloseDate) != 1
	// Field to Update:		Opportunity: Close Date
	// Formula Value:		last day of the month. IF(MONTH(CloseDate) = 12, DATE(YEAR(CloseDate), 12, 31), DATE(YEAR(CloseDate), MONTH(CloseDate)+1, 1) - 1)
	
	if(trigger.isBefore && trigger.isInsert)
	{
		for(Opportunity currOpp : trigger.new)
		{
			// BEGIN WF01
			if(currOpp.End_User__c != NULL)
			{
				currOpp.T_End_User_Date__c = system.today();
			}
			// END WF01
			
			/* kommentezve 2018.12.07. gp
			// BEGIN WF02
			if(currOpp.CloseDate != NULL)
			{
				currOpp.CloseDate = oh.setLastDayOfMonth(currOpp.CloseDate);
			}
			// END WF02
			//*/
		}
	}
	
	if(trigger.isAfter && trigger.isUpdate)
	{
		Set<String> oppIds = new Set<String>();
		for(Opportunity currOpp : trigger.new)
		{
			// BEGIN WF01
			if
			(
				currOpp.End_User__c != trigger.oldMap.get(currOpp.Id).End_User__c &&
				currOpp.End_User__c != NULL
			)
			{
				currOpp.T_End_User_Date__c = system.today();
			}
			
			if
			(
				currOpp.End_User__c != trigger.oldMap.get(currOpp.Id).End_User__c &&
				currOpp.End_User__c == NULL
			)
			{
				currOpp.T_End_User_Date__c = NULL;
			}
			// END WF01
			
			/* kommentezve 2018.12.07. gp
			// BEGIN WF02
			if(currOpp.CloseDate != NULL)
			{
				currOpp.CloseDate = oh.setLastDayOfMonth(currOpp.CloseDate);
			}
			// END WF02
			//*/
			if(currOpp.StageName == 'Closed Won' && Trigger.oldMap.get(currOpp.Id).StageName != currOpp.StageName) {
				oppIds.add(currOpp.Id);
			}

		}
		List<Quote> quotes = [
				SELECT Id,
				(
						SELECT Id,
								End_User__c
						FROM QuoteLineItems
						WHERE
								End_User__r.First_Purchase_Date__c = null

				),
				Opportunity.CloseDate
				FROM Quote
				WHERE OpportunityId IN :oppIds];
		Set<Id> accIds = new Set<Id>();
		for(Quote currentQuote : quotes) {
			if(currentQuote.QuoteLineItems != null) {
				for (QuoteLineItem qli : currentQuote.QuoteLineItems) {
					if (qli.End_User__c != null) {
						accIds.add(qli.End_User__c);
					}
				}
			}
		}
		Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, First_Purchase_Date__c FROM Account WHERE Id IN :accIds]);
		for(Quote currentQuote : quotes) {
			if(currentQuote.QuoteLineItems != null) {
				for(QuoteLineItem qli: currentQuote.QuoteLineItems) {
					if (qli.End_User__c != null && (accountMap.get(qli.End_User__c).First_Purchase_Date__c == null || accountMap.get(qli.End_User__c).First_Purchase_Date__c > currentQuote.Opportunity.CloseDate)) {
						accountMap.get(qli.End_User__c).First_Purchase_Date__c = currentQuote.Opportunity.CloseDate;
					}
				}
			}
		}
		upsert accountMap.values();
	}


	if(trigger.isAfter && trigger.isUpdate){
		// opportunity-n ügyfél változásakor az új ügyfél címe kerüljön át az opp összes quote-jára
		
		Set<Id> oppId = new Set<Id>();
		Set<Id> accId = new Set<Id>();
		for(Opportunity opp : Trigger.new){
			if(opp.AccountId != Trigger.oldMap.get(opp.Id).AccountId) {
				oppId.add(opp.Id);
				accId.add(opp.AccountId);
			}
		}

		Map<Id/*opp id*/, List<Quote>> quoteByOppId = new Map<Id, List<Quote>>();
		for(Quote q : [
				SELECT 
					OpportunityId, BillingCity,BillingCountry,/*BillingGeocodeAccuracy,BillingLatitude,BillingLongitude,*/BillingPostalCode,BillingState,BillingStreet, 
					ShippingCity,ShippingCountry,/*ShippingGeocodeAccuracy,ShippingLatitude,ShippingLongitude,*/ShippingPostalCode,ShippingState,ShippingStreet 
				FROM Quote 
				WHERE OpportunityId IN :oppId
			]){
			List<Quote> tmp = quoteByOppId.get(q.OpportunityId);
			if(tmp == null){
				tmp = new List<Quote>();
				quoteByOppId.put(q.OpportunityId, tmp);
			}
			tmp.add(q);
		}

		Map<Id, Account> accMap = new Map<Id, Account>([
				SELECT 
					Id, BillingCity,BillingCountry,/*BillingGeocodeAccuracy,BillingLatitude,BillingLongitude,*/BillingPostalCode,BillingState,BillingStreet, 
					ShippingCity,ShippingCountry,/*ShippingGeocodeAccuracy,ShippingLatitude,ShippingLongitude,*/ShippingPostalCode,ShippingState,ShippingStreet 
				FROM Account 
				WHERE Id IN :accId
			]);


		for(Id oId : quoteByOppId.keySet()){
			for(Quote q : quoteByOppId.get(oId)){
				//q.BillingAddress = accMap.get(trigger.newMap.get(oId).AccountId).BillingAddress;
				q.BillingCity = accMap.get(trigger.newMap.get(oId).AccountId).BillingCity;
				q.BillingCountry = accMap.get(trigger.newMap.get(oId).AccountId).BillingCountry;
				//q.BillingGeocodeAccuracy = accMap.get(trigger.newMap.get(oId).AccountId).BillingGeocodeAccuracy;
				//q.BillingLatitude = accMap.get(trigger.newMap.get(oId).AccountId).BillingLatitude;
				//q.BillingLongitude = accMap.get(trigger.newMap.get(oId).AccountId).BillingLongitude;
				q.BillingPostalCode = accMap.get(trigger.newMap.get(oId).AccountId).BillingPostalCode;
				q.BillingState = accMap.get(trigger.newMap.get(oId).AccountId).BillingState;
				q.BillingStreet = accMap.get(trigger.newMap.get(oId).AccountId).BillingStreet;
				//q.ShippingAddress = accMap.get(trigger.newMap.get(oId).AccountId).ShippingAddress;
				q.ShippingCity = accMap.get(trigger.newMap.get(oId).AccountId).ShippingCity;
				q.ShippingCountry = accMap.get(trigger.newMap.get(oId).AccountId).ShippingCountry;
				//q.ShippingGeocodeAccuracy = accMap.get(trigger.newMap.get(oId).AccountId).ShippingGeocodeAccuracy;
				//q.ShippingLatitude = accMap.get(trigger.newMap.get(oId).AccountId).ShippingLatitude;
				//q.ShippingLongitude = accMap.get(trigger.newMap.get(oId).AccountId).ShippingLongitude;
				q.ShippingPostalCode = accMap.get(trigger.newMap.get(oId).AccountId).ShippingPostalCode;
				q.ShippingState = accMap.get(trigger.newMap.get(oId).AccountId).ShippingState;
				q.ShippingStreet = accMap.get(trigger.newMap.get(oId).AccountId).ShippingStreet;
			}
		}


		List<Quote> quoteToUpdate = new List<Quote>();
		for(Id oId : quoteByOppId.keySet()){
			quoteToUpdate.addAll(quoteByOppId.get(oId));
		}

		update quoteToUpdate;
	}

}