trigger Quote on Quote (before insert, after insert, after update, before update) // 
{
    System.debug('QUOTE TRIGGER STARTED');
    id standardPricebook2Id;
    Pricebook2 standardPricebook2;
    
    Set<id> opportunityIdSet = new Set<id>();
    Map<id, Quote> quoteMap = new Map<id, Quote>();
    List<Opportunity> opportunityList = new List<Opportunity>();
    List<Opportunity> opportunityToUpdateList = new List<Opportunity>();
    List<User> userList = new List<User>();
    Map<Id, User> userIdMap = new Map<Id, User>();
    List<Quote> quoteList = new List<Quote>();
    
    userList = Util.activeUserList;

	QuoteHandler handler = new QuoteHandler();
	switch on Trigger.OperationType {
		when BEFORE_INSERT {
			handler.beforeInsert(trigger.new);
		}

		when AFTER_INSERT {
		
		}

		when BEFORE_UPDATE {
			handler.beforeUpdate(trigger.oldMap, trigger.newMap);
		}

		when AFTER_UPDATE {
		
		}
	}

    if(trigger.isBefore)
    {
		try{	        
			standardPricebook2 = Util.standardPricebookList[0];
		}
		catch (Exception e){
			standardPricebook2 = null;
		}
            
        if(standardPricebook2 != NULL)
        {
            standardPricebook2Id = standardPricebook2.Id; 
        }
        
        for(Quote currentQuote : trigger.new)
        {
            if(currentQuote.Pricebook2Id == NULL)
            {
                if(standardPricebook2Id != NULL)
                {
                    currentQuote.Pricebook2Id = standardPricebook2Id;   
                }
            }
        }
    }
    
        
    for(User currentUser : userList)
    {
        userIdMap.put(currentUser.Id, currentUser);
    }
    
    for(Quote quote : trigger.New)
    {
        opportunityIdSet.add(quote.OpportunityId);
        quoteMap.put(quote.OpportunityId, quote);
    }
    
    if(opportunityIdSet.size() != 0)
    {
        opportunityList = 
            [
                SELECT
                    Id,
                    Purchase_Amount__c
                FROM
                    Opportunity
                WHERE
                    Id IN :opportunityIdSet
            ];
    }
    
    if(opportunityList.size() != 0)
    {
        for(Opportunity opp : opportunityList)
        {
            if(opp.Purchase_Amount__c != quoteMap.get(opp.Id).TotalPrice)
            {
                opp.Purchase_Amount__c = quoteMap.get(opp.Id).TotalPrice;
                
                opportunityToUpdateList.add(opp);   
            }
        }
    }
    
    if(opportunityToUpdateList.size() != 0)
    {
        update opportunityToUpdateList;
    }
    
    
    // 20141125 - status állítás
    
    if(trigger.isBefore)
    {
        for(Quote currentQuote : trigger.new)
        {
            if(currentQuote.Invoice_Date__c != NULL)
            {
                currentQuote.Status = 'Invoiced';
            }
            
            if(currentQuote.Actual_Payment_Date__c != NULL)
            {
                currentQuote.Status = 'Settled';
            }
        }
    }
    
    
    if(trigger.isAfter && trigger.isUpdate)
    {
        List<Quote> invoicedQuoteList = new List<Quote>();
        
        for(Quote currentQuote : trigger.New)
        {
            if(currentQuote.Status != trigger.oldMap.get(currentQuote.Id).Status)
            {
                if(currentQuote.Status == 'Invoiced' && currentQuote.Invoice_Date__c != NULL)
                {
                    invoicedQuoteList.add(currentQuote);
                    
                    continue;
                }
            }
            
            if
            (
                (
                    currentQuote.Invoice_Date__c != trigger.oldMap.get(currentQuote.Id).Invoice_Date__c &&
                    currentQuote.Invoice_Date__c != NULL
                )
                ||
                (
                    currentQuote.TotalPrice != trigger.oldMap.get(currentQuote.Id).TotalPrice &&
                    currentQuote.Invoice_Date__c != NULL
                )
            )
            {
                if(currentQuote.Status == 'Invoiced')
                {
                    invoicedQuoteList.add(currentQuote);
                }
                
                continue;
            }
        }
        
        QuoteHandler qh = new QuoteHandler();
        
        // Quick and dirty solution -- Ideiglenesen kikapcsoljuk a Settled statusra futast Chemaxon finance keresre. SM 2016-05-30
        qh.customCurrenciesSetter(invoicedQuoteList);
    }
    
    if(trigger.isBefore && trigger.isInsert)
    {
        List<Opportunity> opportunityList2 = new List<Opportunity>();
        
        Map<id, Opportunity> opportunityIdMap = new Map<id, Opportunity>();
        
        Set<id> opportunityIdSet2 = new Set<id>();
        
        for(Quote currentQuote : trigger.new)
        {
            opportunityIdSet2.add(currentQuote.OpportunityID);
        }
        
        if(opportunityIdSet2.size() != 0)
        {
            opportunityList2 =
                [
                    SELECT
                        Id,
                        CurrencyIsoCode
                    FROM
                        Opportunity
                    WHERE
                        Id IN :opportunityIdSet2
                ];
                
            for(Opportunity currentOpportunity : opportunityList2)
            {
                opportunityIDMap.put(currentOpportunity.Id, currentOpportunity);
            }
        }
        
        QuoteHandler qh = new QuoteHandler();
        
        for(Quote currentQuote : trigger.new)
        {
            Id currentUserId;
            string quoteTerms;
            string currencyIsoCode;
            User currentUser = new User();
            
            if(opportunityIDMap.containsKey(currentQuote.OpportunityId))
            {
                currencyIsoCode = opportunityIDMap.get(currentQuote.OpportunityId).CurrencyIsoCode;
            }
            
            system.debug(currencyIsoCode);
            
            currentUserId = UserInfo.getUserId();
            
            if(userIdMap.containsKey(currentUserId))
            {
                currentUser = userIdMap.get(currentUserId);
                
                system.debug('macska: ' + currencyIsoCode);
                
                quoteTerms = qh.getQuoteTerms(currentQuote.Invoice_Type__c, currentUser, currencyIsoCode);
                
                if(quoteTerms != NULL)
                {
                    //currentQuote.Quote_display_terms__c = quoteTerms;   
                }
            }
        }
    }

	if(trigger.isAfter && trigger.isInsert){
		Set<ID> oppIds = new Set<ID>();
		for(Quote q : trigger.new){
			oppIds.add(q.OpportunityId);
		}

		Set<ID> oppsWithExistingQuotes = new Set<ID>();
		for(Quote q : [SELECT ID, OpportunityId FROM Quote WHERE OpportunityId IN :oppIds AND ID NOT IN :trigger.newMap.keySet()]){
			oppsWithExistingQuotes.add(q.OpportunityId);
		}

		Map<Id, Quote> quotesToCreateLineItemsFor = new Map<Id, Quote>();
		for(Quote q : trigger.new){
			if(!oppsWithExistingQuotes.contains(q.OpportunityId)){
                quotesToCreateLineItemsFor.put(q.OpportunityId, q);
                //quotes not having related quotes with same oppty
			}
		}

		if(!quotesToCreateLineItemsFor.isEmpty()){
			QuoteHandler.createQlisFromOpportunity(quotesToCreateLineItemsFor);
		}
	}
    
    if(trigger.isBefore && trigger.isUpdate)
    {   
        QuoteHandler qh = new QuoteHandler();
        
        for(Quote currentQuote : trigger.new)
        {
            if(currentQuote.Invoice_Type__c != trigger.oldMap.get(currentQuote.Id).Invoice_Type__c)
            {
                Id currentUserId;
                string quoteTerms;
                User currentUser = new User();
                
                currentUserId = UserInfo.getUserId();
                
                if(userIdMap.containsKey(currentUserId))
                {
                    currentUser = userIdMap.get(currentUserId);
                    
                    quoteTerms = qh.getQuoteTerms(currentQuote.Invoice_Type__c, currentUser, currentQuote.CurrencyIsoCode);
                    
                    if(quoteTerms != NULL)
                    {
                        currentQuote.Quote_display_terms__c = quoteTerms;   
                    }
                }
            }
        }
    }
    
    if(trigger.isBefore)
    {
        if(trigger.isUpdate)
        {
            for(Quote currentQuote : trigger.New)
            {
                //if(currentQuote.Target_Price__c > currentQuote.Subtotal && currentQuote.Renewed_From__c == null)
                //{
                    //system.debug('250');
                    
                    //currentQuote.addError('The Target Price can not be greater than the Subtotal!');
                    
                    //break;
                //}
                
                //if
                //(
                    //currentQuote.Target_Price__c != NULL &&
                    //currentQuote.Target_Discount__c != NULL &&
                    //currentQuote.Target_Price__c != trigger.oldMap.get(currentQuote.Id).Target_Price__c &&
                    //currentQuote.Target_Discount__c != trigger.oldMap.get(currentQuote.Id).Target_Discount__c
                //)
                //{
                    //system.debug('261');
                    
                    //currentQuote.addError('You can not change the Target Price and the Target Discount at the same time!');
                    
                    //break;
                //}
                
                if
                (
                    currentQuote.Target_Price__c != NULL &&
                    currentQuote.Target_Price__c != trigger.oldMap.get(currentQuote.Id).Target_Price__c
                )
                {
                    currentQuote.Target_Discount__c = NULL;
                }
                
                if
                (
                    currentQuote.Target_Discount__c != NULL &&
                    currentQuote.Target_Discount__c != trigger.oldMap.get(currentQuote.Id).Target_Discount__c
                )
                {
                    currentQuote.Target_Price__c = NULL;
                }
            }
        }
    }
    
    if(trigger.isAfter)
    {
        List<Quote> quoteSetTargetPriceList = new List<Quote>();
        Set<id> quoteIdSetTargetPriceSet = new Set<id>();
        Set<id> quoteIdUnsetTargetPriceSet = new Set<id>();
        
        List<Quote> quoteSetTargetDiscountList = new List<Quote>();
        Set<id> quoteIdSetTargetDiscountSet = new Set<id>();
        Set<id> quoteIdUnsetTargetDiscountSet = new Set<id>();
        
        PriceHandler ph = new PriceHandler();
        
        if(trigger.isUpdate)
        {
            system.debug(quoteList);
            
            for(Quote currentQuote : trigger.new)
            {
                //if(currentQuote.Target_Price__c > currentQuote.Subtotal)
                //{
                    //system.debug('250');
                    
                    //currentQuote.addError('The Target Price can not be greater than the Subtotal!');
                    
                    //break;
                //}
                
                //if
                //(
                    //currentQuote.Target_Price__c != NULL &&
                    //currentQuote.Target_Discount__c != NULL &&
                    //currentQuote.Target_Price__c != trigger.oldMap.get(currentQuote.Id).Target_Price__c &&
                    //currentQuote.Target_Discount__c != trigger.oldMap.get(currentQuote.Id).Target_Discount__c
                //)
                //{
                    //system.debug('261');
                    
                    //currentQuote.addError('You can not change the Target Price and the Target Discount at the same time!');
                    
                    //break;
                //}
                
                system.debug(currentQuote);
                
                if
                (
                    currentQuote.Target_Price__c != NULL &&
                    currentQuote.Target_Price__c != trigger.oldMap.get(currentQuote.Id).Target_Price__c // 20141127
                )
                {
                    quoteIdSetTargetPriceSet.add(currentQuote.Id);
                    quoteSetTargetPriceList.add(currentQuote);
                }
                else if
                (
                    currentQuote.Target_Discount__c == NULL &&
                    currentQuote.Target_Price__c == NULL &&
                    trigger.oldMap.get(currentQuote.Id).Target_Price__c != NULL
                )
                {
                    quoteIdUnsetTargetPriceSet.add(currentQuote.Id);
                }
                
                
                // target discount
                // 20141125
                if
                (
                    currentQuote.Target_Discount__c != NULL &&
                    currentQuote.Target_Discount__c != trigger.oldMap.get(currentQuote.Id).Target_Discount__c
                )
                {
                    quoteIdSetTargetDiscountSet.add(currentQuote.Id);
                    quoteSetTargetDiscountList.add(currentQuote);
                }
                else if
                (
                    currentQuote.Target_Price__c == NULL &&
                    currentQuote.Target_Discount__c == NULL &&
                    trigger.oldMap.get(currentQuote.Id).Target_Discount__c != NULL
                )
                {
                    quoteIdUnsetTargetDiscountSet.add(currentQuote.Id);
                }
            }
        }
        
        if(quoteIdSetTargetPriceSet.size() != 0)
        {
            ph.setQuoteTargetPriceOnQuoteLineItem(quoteIdSetTargetPriceSet, quoteSetTargetPriceList);
        }
        
        if(quoteIdUnsetTargetPriceSet.size() != 0)
        {
            ph.unsetQuoteTargetPriceOnQuoteLineItem(quoteIdUnsetTargetPriceSet);
        }
        
        
        // target discount
        
        if(quoteIdSetTargetDiscountSet.size() != 0)
        {
            ph.setQuoteTargetDiscountOnQuoteLineItem(quoteIdSetTargetDiscountSet, quoteSetTargetDiscountList);
        }
        
        if(quoteIdUnsetTargetDiscountSet.size() != 0)
        {
            ph.unsetQuoteTargetDiscountOnQuoteLineItem(quoteIdUnsetTargetDiscountSet);
        }
    }
    
    // 20150319
    
    if(trigger.isAfter && trigger.isUpdate)
    {
        List<id> opportunityIdList = new List<id>();
        Set<string> quoteStatusSet = new Set<string>();
        
        quoteStatusSet.add('Ordered');
        quoteStatusSet.add('Invoiced');
        quoteStatusSet.add('Settled');
        
        for(Quote currentQ : trigger.new)
        {
            if
            (
                quoteStatusSet.contains(currentQ.Status)
                &&
                currentQ.Status != trigger.oldMap.get(currentQ.Id).Status
                //&&
                //currentQ.Opportunity.StageName != 'Closed Won'
            )
            {
                opportunityIdList.add(currentQ.OpportunityId);
            }
        }
        
        if(opportunityIdList.size() != 0)
        {
            QuoteHandlerFuture.setOpportunityStage(opportunityIdList, 'Closed Won');
        }
    }
    
    // 20150319



    ////  ha pdf jön létre egy quote-hoz akkor egy checkbox kipipálódjon 2018.10.09.
    //// QuoteDocument nem triggerelhető
	//// PDF generálásakor a quote-on a LastReferencedDate mező változik - trigger nem fut
	////	SchedulableQuoteHasPDF.cls ütemezett osztályban 2018.10.26.

	//if(Trigger.isBefore && Trigger.isUpdate){
		//System.debug('BU kezd');
		//Set<Id> qId = new Set<Id>();
		//for(Quote q : Trigger.new){
			//System.debug(trigger.newMap.get(q.Id).LastReferencedDate);
			//System.debug(trigger.oldMap.get(q.Id).LastReferencedDate);
			//System.debug(q.LastReferencedDate);
			//if(q.LastReferencedDate != Trigger.oldMap.get(q.Id).LastReferencedDate) qId.add(q.Id);
		//}
		//System.debug(qId);

		//Map<Id/*quote id*/, Integer> qPDFMap = new Map<Id, Integer>(); // quote-hoz hány PDF tartozik (amelyik quote-nak nincs PDF-e, az a quote nem kerül a map-be)
		//List<AggregateResult> groupedResults = [SELECT Count(id), QuoteId FROM QuoteDocument WHERE QuoteId IN :qId GROUP BY QuoteId];
		//for(AggregateResult ar : groupedResults){
			//System.debug(ar.get('expr0') + ', ' + ar.get('QuoteId'));
			//qPDFMap.put(String.valueOf(ar.get('QuoteId')), Integer.valueOf(ar.get('expr0')));
		//}

		//for(Quote q : Trigger.new){
			//if(q.LastReferencedDate != Trigger.oldMap.get(q.Id).LastReferencedDate && !qPDFMap.containsKey(q.Id)) {
				//q.Quote_has_PDF__c = false;
			//}

			//if(q.LastReferencedDate != Trigger.oldMap.get(q.Id).LastReferencedDate && qPDFMap.containsKey(q.Id)) {
				//q.Quote_has_PDF__c = true;
			//}
		//}
	//}

    System.debug('QUOTE TRIGGER FINISHED');
}