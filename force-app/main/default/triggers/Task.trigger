trigger Task on Task (after insert, after update) 
{
	List<Task> taskList = new List<Task>();
	
	if(trigger.isInsert)
	{
		for(Task task : trigger.New)
		{
			if(task.Application_Scientist__c != '')
			{
				taskList.add(task);
			}
		}	
	}
	
	if(trigger.isUpdate)
	{
		for(Task task : trigger.New)
		{
			if(task.Application_Scientist__c != '' && task.Application_Scientist__c != trigger.oldmap.get(task.Id).Application_Scientist__c)
			{
				taskList.add(task);
			}
		}
	}
	
	if(taskList.size() != 0)
	{
		AppsciEmail ae = new AppsciEmail();
		
		ae.AppsciEmailSending(taskList);
	}
}